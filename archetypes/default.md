{{- $titleWords := replace (replace .Name "-" " ") "_" " " -}}
{{- $keywords := split $titleWords " " -}}
{{- $title := strings.Title $titleWords -}}

+++
date = "{{ .Date }}"
title = "{{- $title -}}"
description = """
{{ $title }}
Post description is here. It will be shown in Google Search bar should the bot
believes it is appropriates.
"""
keywords = {{ replace (printf "%+q" $keywords) " " ", " }}
draft = true
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
]


[creators.JohnSmith]
"@type" = "Person"
"name" = "John Smith"

[creators.SuccessTeam]
"@type" = "Organization"
"name" = "Success Team"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = ""
# name = "{{- $title -}}"
pre = ""
weight = 1


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}
