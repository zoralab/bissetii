#!/bin/bash
PACKAGE_NAME="Dart-Sass"
PACKAGE_VERSION="1.37.5"


__generate_package_name() {
	__machine="$1"
	__arch="$2"

	__name="dart-sass-${PACKAGE_VERSION}-${__machine}-${__arch}"

	if [ "${__machine}" == "windows" ]; then
		__name="${__name}.zip"
	else
		__name="${__name}.tar.gz"
	fi

	echo "$__name"
	unset __name
}


__generate_url() {
	_base_url="https://github.com/sass/dart-sass/releases/download"
	echo "${_base_url}/${PACKAGE_VERSION}/${package_name}"
}


__supported_version() {
	case "$arch" in
	amd64)
		arch=x64
		case "$machine" in
		darwin|linux|windows)
			if [ "$machine" == "darwin" ]; then
				machine="macos"
			fi
			;;
		*)
			_print_status error "unsupported operating system."
			exit 1
			;;
		esac
		;;
	386)
		arch=ia32
		case "$machine" in
		linux|windows)
			if [ "$machine" == "darwin" ]; then
				machine="macos"
			fi
			;;
		*)
			_print_status error "unsupported operating system."
			exit 1
			;;
		esac
		;;
	*)
		_print_status error "unsupported cpu architecture."
		exit 1
		;;
	esac
}


__wrap_up() {
	__sass_path="${output_path}/sass"
	__dart_path="${output_path}/dart-sass/sass"

	echo "\
#!/bin/bash
$__dart_path \$@" > "$__sass_path"

	chmod +x "$__sass_path"
}
