# Description
Describe WHAT is the bug here. Leave your one-liner summary on the top.



## Expected Behavior
Explain the expected behavior.



## Current Behavior
Explain the current behavior.



## How to Reproduce
1. Describe the steps to reproduce the issues.
2. You can do it step by step demonstrated here.
3. Just remember to leave a space after your numbering.
2. You can use the "Add files" to add screenshots too.
3. Just list the steps out crisp and clear.



## Severity
Level of Security: ***Significant***

<!--
OPTIONS

***Critical***
danger, hang, freeze, or kill computer

***Severe***
blocking and can't use. Can't workaround it.

***Significant***
quite a problem but still usable. Can workaround it.

***Not significant***
just some minor touch-up. Everything is fine.
-->



## Urgency
Level of Urgency: ***Code BLUE***

<!--
OPTIONS

***Code BLACK***
Somebody's life is at stake (e.g. medical equipment, national security)

***Code RED***
Immediate attention! (e.g. my business is not running at all)

***Code BLUE***
Please plan out. (e.g. I can survive for now)

***Code GREEN***
Not urgent. (e.g. take your time)
-->


[comment]: # (Automation - Don't worry! Let's Cory takes over here.)
/label ~"Bug" ~"Discussion"
