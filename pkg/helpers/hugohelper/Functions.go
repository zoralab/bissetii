// Copyright 2020 ZORALab Enterprise (hello@zoralab.com)
// Copyright 2020 "Holloway" Chew, Kean Ho (hollowaykeanho@gmail.com)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package hugohelper

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

// Environment variables for configuring the Hugo helpers
const (
	// RootDirectory defines the tag for Hugo Theme module root directory.
	RootDirectory = "HUGO_GO_ROOT_DIRECTORY"
)

const (
	tmpExtension = ".tmp"
	warningTag   = "WARNING: Auto-generated file. DO NOT EDIT!"
	tagSpacing   = "\n\n"
)

// GeneratePartials is to generate the Hugo layouts/partials component.
//
// This function systematically generates all partials with automated warning
// message on the top for Hugo's consumption. It is designed to be used in
// Go test codes. This function reads the following environment variables for
// repository-level configurations:
//   1. `HUGO_GO_ROOT_DIRECTORY` - the root directory of the Hugo theme module.
//
// The `pathing` parameter must be Hugo Theme Module's compatible
// **relative pathing** to `HUGO_GO_ROOT_DIRECTORY`. Example:
//     `layouts/partials/metas/meta.html`.
func GeneratePartials(pathing string, content string) (err error) {
	if content != "" {
		content = "{{- /* " + warningTag + " */ -}}" +
			tagSpacing +
			strings.TrimSpace(content) +
			"\n"
	}

	return writeFile(pathing, content)
}

// GenerateTOMLData is to generate the Hugo data/*.toml file.
//
// This function systematically generates all TOML data file with automated
// warning message on the top for Hugo's consumption. It is designed to be used
// with Go test codes. This function reads the following environment variables
// for repository-level configurations:
//   1. `HUGO_GO_ROOT_DIRECTORY` - the root directory of the Hugo theme module.
//
// The `pathing` parameter must be Hugo Theme Module's compatible
// **relative pathing** to `HUGO_GO_ROOT_DIRECTORY`. Example:
//      `layouts/partials/metas/meta.html`.
func GenerateTOMLData(pathing string, content string) (err error) {
	if content != "" {
		content = "# " + warningTag +
			tagSpacing +
			strings.TrimSpace(content) +
			"\n"
	}

	return writeFile(pathing, content)
}

func writeFile(pathing string, content string) (err error) {
	var path, root string
	var file *os.File

	// check content generator function is not nil
	if content == "" {
		return fmt.Errorf(ErrorMissingContent)
	}

	// get OS environment variable for work directory.
	root = os.Getenv(RootDirectory)
	if root == "" {
		return fmt.Errorf(ErrorMissingRootDirectory)
	}

	// get relative pathing from given filepath
	if filepath.IsAbs(pathing) {
		pathing, err = filepath.Rel(root, pathing)
		if err != nil {
			return fmt.Errorf("%s : %v", ErrorBadFilepath, err)
		}
	}

	// check directory exists and create if it doesn't
	path = filepath.Join(root, filepath.Dir(pathing))

	err = os.MkdirAll(path, os.ModePerm)
	if err != nil {
		err = fmt.Errorf("%s : %v", ErrorBadDirectory, err)
		goto clearTempFile
	}

	// create output file with temp extensions
	path = filepath.Join(root, pathing)

	file, err = os.Create(path + tmpExtension)
	if err != nil {
		err = fmt.Errorf("%s : %v", ErrorCreateFileFailed, err)
		goto clearTempFile
	}

	_, err = file.WriteString(content)
	if err != nil {
		err = fmt.Errorf("%s : %v", ErrorCreateFileFailed, err)
		goto closeFile
	}

	err = file.Sync()

closeFile:
	file.Close()

	if err == nil {
		// overwrite output file using completed temp extensions
		err = os.Rename(path+tmpExtension, path)
	}

clearTempFile:
	_ = os.Remove(path + tmpExtension)

	return err
}
