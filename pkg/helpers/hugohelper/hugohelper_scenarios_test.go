// Copyright 2020 ZORALab Enterprise (hello@zoralab.com)
// Copyright 2020 "Holloway" Chew, Kean Ho (hollowaykeanho@gmail.com)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package hugohelper

func testScenarios() []*testScenario {
	return []*testScenario{
		{
			UID:      1,
			TestType: testGeneratePartials,
			Description: `
GeneratePartial is working properly when:
1. existing directory is not available.
2. root directory is set.
3. proper full filepath is given.
4. proper content is given.
5. expect no error.
`,
			Switches: map[string]bool{
				prepareDirectoryPath: false,
				setRootDirectory:     true,
				useProperFilepath:    true,
				useProperContent:     true,
				expectError:          false,
			},
		}, {
			UID:      2,
			TestType: testGeneratePartials,
			Description: `
GeneratePartial is working properly when:
1. existing directory is available.
2. root directory is set.
3. proper full filepath is given.
4. proper content is given.
5. expect no error.
`,
			Switches: map[string]bool{
				prepareDirectoryPath: true,
				setRootDirectory:     true,
				useProperFilepath:    true,
				useProperContent:     true,
				expectError:          false,
			},
		}, {
			UID:      3,
			TestType: testGeneratePartials,
			Description: `
GeneratePartial is working properly when:
1. existing directory is not available.
2. root directory is unset.
3. proper full filepath is given.
4. proper content is given.
5. expect error.
`,
			Switches: map[string]bool{
				prepareDirectoryPath: false,
				setRootDirectory:     false,
				useProperFilepath:    true,
				useProperContent:     true,
				expectError:          true,
			},
		}, {
			UID:      4,
			TestType: testGeneratePartials,
			Description: `
GeneratePartial is working properly when:
1. existing directory is not available.
2. root directory is set.
3. empty full filepath is given.
4. proper content is given.
5. expect no error.
`,
			Switches: map[string]bool{
				prepareDirectoryPath: false,
				setRootDirectory:     true,
				useEmptyFilepath:     true,
				useProperContent:     true,
				expectError:          true,
			},
		}, {
			UID:      5,
			TestType: testGeneratePartials,
			Description: `
GeneratePartial is working properly when:
1. existing directory is not available.
2. root directory is set.
3. bad full filepath is given.
4. proper content is given.
5. expect no error.
`,
			Switches: map[string]bool{
				prepareDirectoryPath: false,
				setRootDirectory:     true,
				useBadFilepath:       true,
				useProperContent:     true,
				expectError:          true,
			},
		}, {
			UID:      6,
			TestType: testGeneratePartials,
			Description: `
GeneratePartial is working properly when:
1. existing directory is not available.
2. root directory is set.
3. proper full filepath is given.
4. empty content is given.
5. expect no error.
`,
			Switches: map[string]bool{
				prepareDirectoryPath: false,
				setRootDirectory:     true,
				useProperFilepath:    true,
				useEmptyContent:      true,
				expectError:          true,
			},
		}, {
			UID:      7,
			TestType: testGeneratePartials,
			Description: `
GeneratePartial is working properly when:
1. invalid directory path is provided.
2. root directory is set.
3. proper full filepath is given.
4. proper content is given.
5. expect error.
`,
			Switches: map[string]bool{
				prepareInvalidDirectoryPath: true,
				setRootDirectory:            true,
				useProperFilepath:           true,
				useProperContent:            true,
				expectError:                 true,
			},
		}, {
			UID:      8,
			TestType: testGeneratePartials,
			Description: `
GeneratePartial is working properly when:
1. invalid file path is provided.
2. root directory is set.
3. proper full filepath is given.
4. proper content is given.
5. expect error.
`,
			Switches: map[string]bool{
				prepareInvalidFilePath: true,
				setRootDirectory:       true,
				useProperFilepath:      true,
				useProperContent:       true,
				expectError:            true,
			},
		}, {
			UID:      9,
			TestType: testGenerateTOMLData,
			Description: `
GenerateTOMLData is working properly when:
1. existing directory is not available.
2. root directory is set.
3. proper full filepath is given.
4. proper content is given.
5. expect no error.
`,
			Switches: map[string]bool{
				prepareDirectoryPath: false,
				setRootDirectory:     true,
				useProperFilepath:    true,
				useProperContent:     true,
				expectError:          false,
			},
		}, {
			UID:      10,
			TestType: testGenerateTOMLData,
			Description: `
GenerateTOMLData is working properly when:
1. existing directory is available.
2. root directory is set.
3. proper full filepath is given.
4. proper content is given.
5. expect no error.
`,
			Switches: map[string]bool{
				prepareDirectoryPath: true,
				setRootDirectory:     true,
				useProperFilepath:    true,
				useProperContent:     true,
				expectError:          false,
			},
		}, {
			UID:      11,
			TestType: testGenerateTOMLData,
			Description: `
GenerateTOMLData is working properly when:
1. existing directory is not available.
2. root directory is unset.
3. proper full filepath is given.
4. proper content is given.
5. expect error.
`,
			Switches: map[string]bool{
				prepareDirectoryPath: false,
				setRootDirectory:     false,
				useProperFilepath:    true,
				useProperContent:     true,
				expectError:          true,
			},
		}, {
			UID:      12,
			TestType: testGenerateTOMLData,
			Description: `
GenerateTOMLData is working properly when:
1. existing directory is not available.
2. root directory is set.
3. empty full filepath is given.
4. proper content is given.
5. expect no error.
`,
			Switches: map[string]bool{
				prepareDirectoryPath: false,
				setRootDirectory:     true,
				useEmptyFilepath:     true,
				useProperContent:     true,
				expectError:          true,
			},
		}, {
			UID:      13,
			TestType: testGenerateTOMLData,
			Description: `
GenerateTOMLData is working properly when:
1. existing directory is not available.
2. root directory is set.
3. bad full filepath is given.
4. proper content is given.
5. expect no error.
`,
			Switches: map[string]bool{
				prepareDirectoryPath: false,
				setRootDirectory:     true,
				useBadFilepath:       true,
				useProperContent:     true,
				expectError:          true,
			},
		}, {
			UID:      14,
			TestType: testGenerateTOMLData,
			Description: `
GenerateTOMLData is working properly when:
1. existing directory is not available.
2. root directory is set.
3. proper full filepath is given.
4. empty content is given.
5. expect no error.
`,
			Switches: map[string]bool{
				prepareDirectoryPath: false,
				setRootDirectory:     true,
				useProperFilepath:    true,
				useEmptyContent:      true,
				expectError:          true,
			},
		}, {
			UID:      15,
			TestType: testGenerateTOMLData,
			Description: `
GenerateTOMLData is working properly when:
1. invalid directory path is provided.
2. root directory is set.
3. proper full filepath is given.
4. proper content is given.
5. expect error.
`,
			Switches: map[string]bool{
				prepareInvalidDirectoryPath: true,
				setRootDirectory:            true,
				useProperFilepath:           true,
				useProperContent:            true,
				expectError:                 true,
			},
		}, {
			UID:      16,
			TestType: testGenerateTOMLData,
			Description: `
GenerateTOMLData is working properly when:
1. invalid file path is provided.
2. root directory is set.
3. proper full filepath is given.
4. proper content is given.
5. expect error.
`,
			Switches: map[string]bool{
				prepareInvalidFilePath: true,
				setRootDirectory:       true,
				useProperFilepath:      true,
				useProperContent:       true,
				expectError:            true,
			},
		},
	}
}
