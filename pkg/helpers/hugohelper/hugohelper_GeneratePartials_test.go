// Copyright 2020 ZORALab Enterprise (hello@zoralab.com)
// Copyright 2020 "Holloway" Chew, Kean Ho (hollowaykeanho@gmail.com)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package hugohelper

import (
	"testing"
)

func TestGeneratePartials(t *testing.T) {
	scenarios := testScenarios()

	for i, s := range scenarios {
		if s.TestType != testGeneratePartials {
			continue
		}

		// prepare
		th := s.prepareTHelper(t)
		path := s.generateOutputFilepath()
		content := s.generatePartialContent()
		s.prepareTestEnvironment(th, path)

		// test
		err := GeneratePartials(path, content)

		// assert
		s.assertFileOutput(th, path, content)
		th.ExpectError(err, s.Switches[expectError])
		th.ExpectUIDCorrectness(i, s.UID, false)
		s.log(th, map[string]interface{}{
			"filepath": path,
			"content":  content,
			"error":    err,
		})
		s.cleanTestEnvironment()
		th.Conclude()
	}
}
