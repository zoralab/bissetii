// Copyright 2020 ZORALab Enterprise (hello@zoralab.com)
// Copyright 2020 "Holloway" Chew, Kean Ho (hollowaykeanho@gmail.com)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package hugohelper

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"gitlab.com/zoralab/cerigo/os/file/filehelper"
	"gitlab.com/zoralab/cerigo/testing/thelper"
)

const (
	testGeneratePartials = "testGeneratePartials"
	testGenerateTOMLData = "testGenerateTOMLData"
)

const (
	expectError                 = "expectError"
	prepareDirectoryPath        = "prepareDirectoryPath"
	prepareInvalidDirectoryPath = "prepareInvalidDirectoryPath"
	prepareInvalidFilePath      = "prepareInvalidFilePath"
	setRootDirectory            = "setRootDirectory"
	useBadFilepath              = "useBadFilepath"
	useEmptyContent             = "useEmptyContent"
	useEmptyFilepath            = "useEmptyFilepath"
	useProperContent            = "useProperContent"
	useProperFilepath           = "useProperFilepath"
)

const (
	badFilepath            = "////dir///to/~~~/file,html"
	testDirectory          = "./tmp"
	relativeFilepath       = testDirectory + "/dir/to/file.html"
	targetContent          = "this is a content string"
	envBackupRootDirectory = RootDirectory + "_BACKUP"
)

type testScenario thelper.Scenario

func (s *testScenario) prepareTHelper(t *testing.T) *thelper.THelper {
	return thelper.NewTHelper(t)
}

func (s *testScenario) log(th *thelper.THelper, data map[string]interface{}) {
	th.LogScenario(thelper.Scenario(*s), data)
}

func (s *testScenario) assertFileOutput(th *thelper.THelper,
	path string,
	content string) {
	var f *filehelper.FileHelper
	var target string
	var b []byte
	var err error

	switch {
	case s.Switches[prepareInvalidFilePath]:
		return
	case s.Switches[prepareInvalidDirectoryPath]:
		return
	case !s.Switches[setRootDirectory]:
		goto invalidFilecheck
	case s.Switches[useProperFilepath] && s.Switches[useProperContent]:
	default:
		goto invalidFilecheck
	}

	b, err = ioutil.ReadFile(path)
	if err != nil {
		th.Errorf("error while reading file: %v\n", err)
		return
	}

	target = string(b)

	switch {
	case strings.Contains(target, content):
	default:
		th.Errorf("unknown content: %s\n", target)
	}

	return

invalidFilecheck:
	// always assume error file
	f = &filehelper.FileHelper{ChunkSize: 4096}

	if f.FileExists(path) {
		th.Errorf("malformed file created.")
	}
}

func (s *testScenario) generateOutputFilepath() (path string) {
	switch {
	case s.Switches[useBadFilepath]:
		path = badFilepath
	case s.Switches[useEmptyFilepath]:
		path = ""
	case s.Switches[useProperFilepath]:
		fallthrough
	default:
		path = relativeFilepath
	}

	return path
}

func (s *testScenario) generatePartialContent() (content string) {
	switch {
	case s.Switches[useEmptyContent]:
		content = ""
	case s.Switches[useProperContent]:
		fallthrough
	default:
		content = targetContent
	}

	return content
}

func (s *testScenario) prepareTestEnvironment(th *thelper.THelper,
	path string) {
	s.setEnvironmentVariables()

	switch {
	case s.Switches[prepareInvalidDirectoryPath]:
		s.prepareDirectoryPath(th, path)
	case s.Switches[prepareInvalidFilePath]:
		file, err := os.Create(testDirectory)
		if err != nil {
			th.Errorf("error preparing invalid filepath: %s", err)
			return
		}

		file.Close()
	case s.Switches[prepareDirectoryPath]:
		s.prepareDirectoryPath(th, filepath.Dir(path))
	default:
	}
}

func (s *testScenario) setEnvironmentVariables() {
	os.Setenv(envBackupRootDirectory, os.Getenv(RootDirectory))
	os.Unsetenv(RootDirectory)

	if s.Switches[setRootDirectory] {
		os.Setenv(RootDirectory, ".")
	}
}

func (s *testScenario) unsetEnvironmentVariables() {
	os.Setenv(RootDirectory, os.Getenv(envBackupRootDirectory))
	os.Unsetenv(envBackupRootDirectory)
}

func (s *testScenario) prepareDirectoryPath(th *thelper.THelper, dir string) {
	switch {
	case s.Switches[useProperFilepath]:
	default:
		return
	}

	err := os.MkdirAll(dir, os.ModePerm)
	if err != nil {
		th.Errorf("error prepare directory path: %s\n", err)
	}
}

func (s *testScenario) cleanTestEnvironment() {
	_ = os.RemoveAll(testDirectory)

	s.unsetEnvironmentVariables()
}
