// Copyright 2020 ZORALab Enterprise (hello@zoralab.com)
// Copyright 2020 "Holloway" Chew, Kean Ho (hollowaykeanho@gmail.com)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package hugohelper is for automating Hugo theme module production from Go.
//
// It was designed to assist any cross-compatible Go-Hugo and Go programming
// language template design interfaces. By facilitating such compatiblitiy,
// web HTML interfaces can be singularly designed at one place, saving the
// design time.
//
// Also, it allows Go developers and web designers to work effectively and
// communicate easily without needing to get confused with all the tools.
package hugohelper
