// Copyright 2020 ZORALab Enterprise (hello@zoralab.com)
// Copyright 2020 "Holloway" Chew, Kean Ho (hollowaykeanho@gmail.com)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package bissetii is the Go package for hosting Bissetii HTML templates.
//
// At the root of this package offers the unified data structure used in both
// Go and GoHugo alongside some helper functions.
//
// To render specific components from Bissetii template, you can explore the
// `components/` packages. Contents within that directory are strictly for
// `Go` to utilize.
//
// **WARNING: Go package is still under experimentations! Do not use.**
//
// ## Getting Started In General
//
// There is no self-made functions or differences when it comes to using
// Bissetii through Go packages. All you need to do is to `import` the
// appropriate component package and you're good to go.
//
// Most of the HTML template codes offered by Bissetii are mainly `string` in
// `const` group. Hence, it's merely using these constants in your template
// rendering.
package bissetii
