module gitlab.com/zoralab/bissetii/pkg

go 1.15

replace gitlab.com/zoralab/bissetii/pkg => ./

require gitlab.com/zoralab/cerigo v0.0.2
