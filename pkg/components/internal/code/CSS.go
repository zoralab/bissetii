// Copyright 2021 ZORALab Enterprise (hello@zoralab.com)
//
// Licensed under the Apache License, Version 2.0 (the License);
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an AS IS BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// WARNING: This file is auto-generated! DO NOT EDIT!
//          Original source codes from this directory:
//		1. amp.scss
//		2. critical.scss
//		3. tablet.scss
//		4. desktop.scss
//		5. widescreen.scss
//		6. print.scss
//		7. variables.scss
//		8. variablesAMP.scss

package code

// All Bissetii Compressed Compiled CSS
const (
	CSSAMP = `code{border-radius:var(--code-border-radius);padding:var(--code-padding);letter-spacing:var(--code-letter-spacing);color:var(--code-color);background:var(--code-background);overflow-wrap:var(--code-overflow-wrap)}pre{display:var(--pre-display);margin:var(--pre-margin);overflow:var(--pre-overflow);border-left:var(--pre-border-left);border-radius:var(--pre-border-radius);padding:var(--pre-padding);max-width:var(--pre-max-width);letter-spacing:var(--pre-letter-spacing);color:var(--pre-color);background:var(--pre-background)}pre code{--code-color:--pre-color;--code-padding:0;--code-background:transparent}`

	CSSCritical = `code{border-radius:var(--code-border-radius);padding:var(--code-padding);letter-spacing:var(--code-letter-spacing);color:var(--code-color);background:var(--code-background);overflow-wrap:var(--code-overflow-wrap)}pre{display:var(--pre-display);margin:var(--pre-margin);overflow:var(--pre-overflow);border-left:var(--pre-border-left);border-radius:var(--pre-border-radius);padding:var(--pre-padding);max-width:var(--pre-max-width);letter-spacing:var(--pre-letter-spacing);color:var(--pre-color);background:var(--pre-background)}pre code{--code-color:--pre-color;--code-padding:0;--code-background:transparent}`

	CSSTablet = ``

	CSSDesktop = ``

	CSSWidescreen = ``

	CSSPrint = `pre{break-inside:avoid}`

	CSSVariables    = ``
	CSSVariablesAMP = ``
)
