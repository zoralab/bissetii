package internal

// HTML is the output format for vanilla HTML
const (
	HTMLCode   = "HTML"
	HTMLName   = "html"
	HTMLSuffix = "html"
	HTMLPrefix = ""
)

// AMP is the output format for AMP HTML
const (
	AMPCode   = "AMP"
	AMPName   = "amp"
	AMPSuffix = "amp.html"
	AMPPrefix = "amp-"
)
