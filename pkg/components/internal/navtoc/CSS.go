// Copyright 2021 ZORALab Enterprise (hello@zoralab.com)
//
// Licensed under the Apache License, Version 2.0 (the License);
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an AS IS BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// WARNING: This file is auto-generated! DO NOT EDIT!
//          Original source codes from this directory:
//		1. amp.scss
//		2. critical.scss
//		3. tablet.scss
//		4. desktop.scss
//		5. widescreen.scss
//		6. print.scss
//		7. variables.scss
//		8. variablesAMP.scss

package navtoc

// All Bissetii Compressed Compiled CSS
const (
	CSSAMP = `nav.nav-toc{width:var(--nav-toc-width);padding:var(--nav-toc-padding);border:var(--nav-toc-border);border-radius:var(--nav-toc-border-radius);background:var(--nav-toc-background)}nav.nav-toc .title{margin:var(--nav-toc-title-margin);font-size:var(--nav-toc-title-font-size);text-align:var(--nav-toc-title-text-align);font-weight:var(--nav-toc-title-font-weight)}nav.nav-toc ol{margin:var(--nav-toc-list-margin);padding:var(--nav-toc-list-padding);counter-reset:nav-toc-item;list-style-type:none}nav.nav-toc ol>li{margin:var(--nav-toc-li-margin);counter-increment:nav-toc-item}nav.nav-toc ol>li p,nav.nav-toc ol>li a{display:inline}nav.nav-toc ol>li:before{content:counters(nav-toc-item, ".") ".";margin:0 1rem 0 0}`

	CSSCritical = `nav.nav-toc{width:var(--nav-toc-width);padding:var(--nav-toc-padding);border:var(--nav-toc-border);border-radius:var(--nav-toc-border-radius);background:var(--nav-toc-background)}nav.nav-toc .title{margin:var(--nav-toc-title-margin);font-size:var(--nav-toc-title-font-size);text-align:var(--nav-toc-title-text-align);font-weight:var(--nav-toc-title-font-weight)}nav.nav-toc ol{margin:var(--nav-toc-list-margin);padding:var(--nav-toc-list-padding);counter-reset:nav-toc-item;list-style-type:none}nav.nav-toc ol>li{margin:var(--nav-toc-li-margin);counter-increment:nav-toc-item}nav.nav-toc ol>li p,nav.nav-toc ol>li a{display:inline}nav.nav-toc ol>li:before{content:counters(nav-toc-item, ".") ".";margin:0 1rem 0 0}`

	CSSTablet = ``

	CSSDesktop = ``

	CSSWidescreen = ``

	CSSPrint = `nav.nav-toc{break-inside:avoid}nav.nav-toc li{break-inside:avoid}`

	CSSVariables    = ``
	CSSVariablesAMP = ``
)
