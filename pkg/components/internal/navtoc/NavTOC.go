// Copyright 2020 ZORALab Enterprise (hello@zoralab.com)
// Copyright 2020 "Holloway" Chew, Kean Ho (hollowaykeanho@gmail.com)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//nolint:lll
package navtoc

import (
	"gitlab.com/zoralab/bissetii/pkg/components/internal"
	"gitlab.com/zoralab/bissetii/pkg/components/internal/nav"
)

// Constants for managing the package as a whole.
const (
	// Name is the block name (`{{- define "this-name" -}}`).
	Name = "nav-toc"

	// AMPName is the AMP HTML block name.
	AMPName = internal.AMPPrefix + Name

	// ParametersComment are the list of comment headers describing inputs.
	ParametersComment = `
{{- /* .Class            = Nav-TOC class attribute.                   */ -}}
{{- /* .ID               = Nav-TOC id attribute.                      */ -}}
{{- /* .Title            = Nav-TOC title in HTML format.              */ -}}
{{- /* .List             = Nav-TOC primary list.                      */ -}}
{{- /*   .HTML           = HTML content for the item.                 */ -}}
{{- /*   .Items          = Sub list. Repeat List structure. Optional. */ -}}
`
)

// Dependencies are the dependent codes for building independent template.
//
// This is useful for rendering portable template usage like Hugo partials
// where they do not share a common definition source.
const (

	// DepHTML is the vanilla HTML output type.
	DepHTML = `
{{- define "` + Name + "-item" + `" -}}
<li {{- if .ID }} id="{{- .ID -}}"{{- end -}}
	{{- if .Class }} class="{{- .Class -}}"{{- end -}}
	{{- if .Style }} style="{{- .Style -}}"{{- end -}}>
	{{- if ne (len .Items) 0 }}
		<ol>
			{{- range .Items }}
				{{ template "` + Name + `-item" . -}}
			{{- end -}}
		</ol>
	{{- else }}
		{{ .HTML }}
	{{- end }}
</li>
{{ end -}}
`

	// DepAMPHTML is the Accelerated Mobile Pages HTML output type.
	DepAMPHTML = `
{{- define "` + AMPName + "-item" + `" -}}
<li {{- if .ID }} id="{{- .ID -}}"{{- end -}}
	{{- if .Class }} class="{{- .Class -}}"{{- end -}}
	{{- if .Style }} style="{{- .Style -}}"{{- end -}}>
	{{- if ne (len .Items) 0 }}
		<ol>
			{{- range .Items }}
				{{ template "` + AMPName + `-item" . -}}
			{{- end -}}
		</ol>
	{{- else }}
		{{ .HTML }}
	{{- end }}
</li>
{{ end -}}
`
)

// Full HTML codes for rendering templates without needing to parse file.
const (
	// HTML is the vanilla HTML output type.
	HTML = `
<nav class="nav-toc {{- if .Class }} {{ .Class -}}{{- end -}}"
{{- if .ID }}
	id="{{- .ID -}}"
{{- end -}}>
	{{- if .Title }}{{- .Title -}}{{- end }}
	<ol>
		{{- range .List.Items }}
			{{ template "` + Name + `-item" . -}}
		{{- end -}}
	</ol>
</nav>
`

	// AMPHTML is the Accelerated Mobile Pages HTML output type.
	AMPHTML = `
<nav class="nav-toc {{- if .Class }} {{ .Class -}}{{- end -}}"
{{- if .ID }}
	id="{{- .ID -}}"
{{- end -}}>
	<ol>
		{{- range .List.Items }}
			{{ template "` + AMPName + `-item" . -}}
		{{- end -}}
	</ol>
</nav>
`
)

// Data is the data structure for rendering the component.
type Data struct {
	// Class is the additional CSS class tag for NavTOC.
	Class string

	// ID is the ID tag for NavTOC.
	ID string

	// List is the main list of items.
	List *nav.Item

	// Title is the HTML format title for the NavTOC list.
	Title string
}
