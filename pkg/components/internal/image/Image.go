// Copyright 2020 ZORALab Enterprise (hello@zoralab.com)
// Copyright 2020 "Holloway" Chew, Kean Ho (hollowaykeanho@gmail.com)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//nolint:lll
package image

import (
	"gitlab.com/zoralab/bissetii/pkg/components/internal"
)

// Constants for managing the package as a whole.
const (
	// Name is the block name (`{{- define "this-name" -}}`).
	Name = "image"

	// AMPName is the AMP HTML block name.
	AMPName = internal.AMPPrefix + Name

	// ParametersComment are the list of comment headers describing inputs.
	ParametersComment = `
{{- /* .AltText           = the image alternate text                  */ -}}
{{- /* .SourceURL         = the image source URL                      */ -}}
{{- /* .Width             = explicit width setting                    */ -}}
{{- /* .Height            = explicit height setting                   */ -}}
{{- /* .SourceSet         = srcset in a single string                 */ -}}
{{- /* .Sizes             = media query for deploying different sizes */ -}}
{{- /* .IsMap             = image is url mapped. either "true" or ""  */ -}}
{{- /* .LoadingMode       = loading mode                              */ -}}
{{- /* .Layout            = AMP specific layout                       */ -}}
{{- /* .Class             = class tags                                */ -}}
{{- /* .Sources           = list of art-directed image sources        */ -}}
{{- /*   .SourceSet       = an image source with its source set       */ -}}
{{- /*   .Media           = the image source associated media query   */ -}}
`
)

// Full HTML codes for rendering templates without needing to parse file.
const (
	// HTML is the vanilla HTML output type.
	HTML = `
{{- if .Sources -}}
<picture>
	{{- range $s := .Sources }}
		<source srcset="{{- $s.SourceSet -}}"
			{{- if $s.Media }} media="{{- $s.Media -}}"{{- end -}}
			{{- if $s.Type }} type="{{- $s.Type -}}"{{- end -}}
		>
	{{- end }}
{{- end }}

<img alt="{{- .AltText -}}"
	src="{{- .SourceURL -}}"
{{- if .Class }}
	class="{{- .Class -}}"
{{- end -}}
{{- if .Width }}
	width="{{- .Width -}}"
{{- end -}}
{{- if .Height }}
	height="{{- .Height -}}"
{{- end -}}
{{- if .SourceSet }}
	srcset="{{- .SourceSet -}}"
{{- end -}}
{{- if .Sizes }}
	sizes="{{- .Sizes -}}"
{{- end -}}
{{- if eq .IsMap "true" }}
	ismap
{{- end -}}
{{- if .LoadingMode }}
	loading="{{- .LoadingMode -}}"
{{- end -}}
/>

{{- if .Sources }}
</picture>
{{- end }}
`

	// AMPHTML is the Accelerated Mobile Pages HTML output type.
	AMPHTML = `
<amp-img alt="{{- .AltText -}}"
	src="{{- .SourceURL -}}"
{{- if .Class }}
	class="{{- .Class -}}"
{{- end -}}
{{- if .Width }}
	width="{{- .Width -}}"
{{- end -}}
{{- if .Height }}
	height="{{- .Height -}}"
{{- end -}}
{{- if .SourceSet }}
	srcset="{{- .SourceSet -}}"
{{- end -}}
{{- if .Sizes }}
	sizes="{{- .Sizes -}}"
{{- end -}}
{{- if .Layout }}
	layout="{{- .Layout -}}"
{{- end -}}
></amp-img>
`
)

// Data is the data structure for rendering the component.
type Data struct {
	AltText     string
	SourceURL   string
	Width       string
	Height      string
	SourceSet   string
	Sizes       string
	IsMap       string
	LoadingMode string
	Layout      string
	Class       string
	Sources     []*Source
}

type Source struct {
	SourceSet string
	Media     string
	Type      string
}
