// Copyright 2021 ZORALab Enterprise (hello@zoralab.com)
//
// Licensed under the Apache License, Version 2.0 (the License);
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an AS IS BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// WARNING: This file is auto-generated! DO NOT EDIT!
//          Original source codes from this directory:
//		1. amp.scss
//		2. critical.scss
//		3. tablet.scss
//		4. desktop.scss
//		5. widescreen.scss
//		6. print.scss
//		7. variables.scss
//		8. variablesAMP.scss

package image

// All Bissetii Compressed Compiled CSS
const (
	CSSAMP = `amp-img{max-width:var(--image-max-width);height:var(--image-height);filter:var(--image-filter);border:var(--image-border);border-radius:var(--image-border-radius);animation:var(--image-animation)}`

	CSSCritical = `img{max-width:var(--image-max-width);height:var(--image-height);filter:var(--image-filter);border:var(--image-border);border-radius:var(--image-border-radius);animation:var(--image-animation)}`

	CSSTablet = ``

	CSSDesktop = ``

	CSSWidescreen = ``

	CSSPrint = `img{break-inside:avoid}`

	CSSVariables    = ``
	CSSVariablesAMP = ``
)
