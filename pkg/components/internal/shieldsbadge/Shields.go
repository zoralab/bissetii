// Copyright 2020 ZORALab Enterprise (hello@zoralab.com)
// Copyright 2020 "Holloway" Chew, Kean Ho (hollowaykeanho@gmail.com)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//nolint:lll
package shieldsbadge

import (
	"gitlab.com/zoralab/bissetii/pkg/components/internal"
)

// Constants for managing the package as a whole.
const (
	// Name is the block name (`{{- define "this-name" -}}`).
	Name = "badge-shields"

	// AMPName is the AMP HTML block name.
	AMPName = internal.AMPPrefix + Name

	// ParametersComment are the list of comment headers describing inputs.
	ParametersComment = `
{{- /* .Label                         = badge label                     */ -}}
{{- /* .Content                       = badge value content             */ -}}
{{- /* .Text.Color                    = hex value for text color        */ -}}
{{- /* .Color                         = hex value for background color  */ -}}
`
)

// Dependencies are the dependent codes for building independent template.
//
// This is useful for rendering portable template usage like Hugo partials
// where they do not share a common definition source.
const (
	// DepHTML is the vanilla HTML output type.
	DepHTML = ``

	// DepAMPHTML is the Accelerated Mobile Pages HTML output type.
	DepAMPHTML = ``
)

// Full HTML codes for rendering templates without needing to parse file.
const (
	// HTML is the vanilla HTML output type.
	HTML = `
<dl class="shields-badge" {{- if (or .Color .Text.Color) }} style="
	{{- if .Color }}--shieldsbadge-dd-background: {{ .Color -}};{{- end -}}
	{{- if .Text.Color }}--shieldsbadge-dd-color: {{ .Text.Color -}};{{- end -}}
"{{- end -}}>
	<dt>{{- .Label -}}</dt>
	<dd>{{ .Content -}}</dd>
</dl>
`

	// AMPHTML is the Accelerated Mobile Pages HTML output type.
	AMPHTML = HTML
)

// TextFormat is the value's text formatting data for Data structure.
type TextFormat struct {
	Color string
}

// Data is the data structure for rendering the component.
type Data struct {
	Label   string
	Content string
	Text    *TextFormat
	Color   string
}
