// Copyright 2021 ZORALab Enterprise (hello@zoralab.com)
//
// Licensed under the Apache License, Version 2.0 (the License);
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an AS IS BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// WARNING: This file is auto-generated! DO NOT EDIT!
//          Original source codes from this directory:
//		1. amp.scss
//		2. critical.scss
//		3. tablet.scss
//		4. desktop.scss
//		5. widescreen.scss
//		6. print.scss
//		7. variables.scss
//		8. variablesAMP.scss

package shieldsbadge

// All Bissetii Compressed Compiled CSS
const (
	CSSAMP = `dl.shields-badge{display:inline-flex;flex-wrap:nowrap;width:fit-content;--dt-margin:0;--dd-margin:0;border:var(--shieldsbadge-border);border-radius:var(--shieldsbadge-border-radius);margin:var(--shieldsbadge-margin);--dl-padding:var(--shieldsbadge-padding)}dl.shields-badge dt{flex-basis:auto;min-width:var(--shieldsbadge-dt-min-width);padding:var(--shieldsbadge-dt-padding);font-size:var(--shieldsbadge-dt-font-size);font-weight:var(--shieldsbadge-dt-font-weight);color:var(--shieldsbadge-dt-color);background:var(--shieldsbadge-dt-background)}dl.shields-badge dd{flex-basis:auto;white-space:var(--shieldsbadge-dd-white-space);padding:var(--shieldsbadge-dd-padding);font-size:var(--shieldsbadge-dd-font-size);font-weight:var(--shieldsbadge-dd-font-weight);color:var(--shieldsbadge-dd-color);background:var(--shieldsbadge-dd-background)}dl.shields-badge dd:before{content:none}`

	CSSCritical = `dl.shields-badge{display:inline-flex;flex-wrap:nowrap;width:fit-content;--dt-margin:0;--dd-margin:0;border:var(--shieldsbadge-border);border-radius:var(--shieldsbadge-border-radius);margin:var(--shieldsbadge-margin);--dl-padding:var(--shieldsbadge-padding)}dl.shields-badge dt{flex-basis:auto;min-width:var(--shieldsbadge-dt-min-width);padding:var(--shieldsbadge-dt-padding);font-size:var(--shieldsbadge-dt-font-size);font-weight:var(--shieldsbadge-dt-font-weight);color:var(--shieldsbadge-dt-color);background:var(--shieldsbadge-dt-background)}dl.shields-badge dd{flex-basis:auto;white-space:var(--shieldsbadge-dd-white-space);padding:var(--shieldsbadge-dd-padding);font-size:var(--shieldsbadge-dd-font-size);font-weight:var(--shieldsbadge-dd-font-weight);color:var(--shieldsbadge-dd-color);background:var(--shieldsbadge-dd-background)}dl.shields-badge dd:before{content:none}`

	CSSTablet = ``

	CSSDesktop = ``

	CSSWidescreen = ``

	CSSPrint = `dl.shields-badge,dl.shields-badge dd,dl.shields-badge dt{break-inside:avoid}`

	CSSVariables    = ``
	CSSVariablesAMP = ``
)
