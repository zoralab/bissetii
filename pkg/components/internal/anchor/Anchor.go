// Copyright 2020 ZORALab Enterprise (hello@zoralab.com)
// Copyright 2020 "Holloway" Chew, Kean Ho (hollowaykeanho@gmail.com)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//nolint:lll
package anchor

import (
	"gitlab.com/zoralab/bissetii/pkg/components/internal"
)

// Constants for managing the package as a whole.
const (
	// Name is the block name (`{{- define "this-name" -}}`).
	Name = "anchor"

	// AMPName is the AMP HTML block name.
	AMPName = internal.AMPPrefix + Name

	// ParametersComment are the list of comment headers describing inputs.
	ParametersComment = `
{{- /* <a> and <link> COMMON ATTRIBUTES                                   */ -}}
{{- /* .URL             = Link source URL value for href= attribute       */ -}}
{{- /* .Rel             = Link relationship value for rel= attribute      */ -}}
{{- /* .Lang            = Link language hinting for hreflang= attribute   */ -}}
{{- /* .OnLoad          = Link onload= value                              */ -}}


{{- /* <a> ELEMENT SPECIFIC ATTRIBUTES                                    */ -}}
{{- /* .Content         = URL content                                     */ -}}
{{- /* .ID              = ID attribute for the URL                        */ -}}
{{- /* .Class           = Class attribute for the URL                     */ -}}
{{- /* .Target          = Javascript action upon clicking the URL         */ -}}
{{- /* .Download        = Download flag to set URL as download            */ -}}


{{- /* <link> ELEMENT SPECIFIC ATTRIBUTES (VANILLA HTML5 ONLY)            */ -}}
{{- /* .LinkElement     = set 'true' to render <link> instead of <a>      */ -}}
{{- /* .Disabled        = Link disabled flag                              */ -}}
{{- /* .Type            = Link type= value                                */ -}}
{{- /* .As              = Link as= value                                  */ -}}
{{- /* .CrossOrigin     = Link CORS ID (MUST have .Integrity)             */ -}}
{{- /*                     1. 'anonymous' - no credential needed          */ -}}
{{- /*                     2. 'use-credentials' - use credentials         */ -}}
{{- /* .Integrity       = Link SRI (MUST have .CrossOrigin)               */ -}}
{{- /* .ImageSizes      = Link imagesizes= value                          */ -}}
{{- /* .ImageSrcSet     = Link imagesrcset= value                         */ -}}
{{- /* .Media           = Link media= value                               */ -}}
{{- /* .Sizes           = Link sizes= value                               */ -}}
{{- /* .Title           = Link title= value                               */ -}}
{{- /* .Referrer        = Link referrerpolicy= value                      */ -}}
`
)

// Dependencies are the dependent codes for building independent template.
//
// This is useful for rendering portable template usage like Hugo partials
// where they do not share a common definition source.
const (
	// DepHTML is the vanilla HTML output type.
	DepHTML = ``

	// DepAMPHTML is the Accelerated Mobile Pages HTML output type.
	DepAMPHTML = ``
)

// Full HTML codes for rendering templates without needing to parse file.
const (
	// HTML is the vanilla HTML output type.
	HTML = `
{{- if .LinkElement }}
	<link href="{{- .URL -}}"
		{{- if .Type }}{{ .Type -}}{{- end }}
		{{- if .Rel }} rel="{{ .Rel -}}"{{- end }}
		{{- if .Sizes }} sizes="{{ .Sizes -}}"{{- end }}
		{{- if .As }} as="{{ .As -}}"{{- end }}
		{{- if .Disabled }} disabled{{- end }}
		{{- if .Lang }} hreflang="{{- .Lang -}}" {{- end }}
		{{- if .ImageSizes }} imageSizes="{{- .ImageSizes -}}" {{- end }}
		{{- if .ImageSrcSet }} imageSrcSet="{{- .ImageSrcSet -}}" {{- end }}
		{{- if .Title }} title="{{- .Title -}}" {{- end }}
		{{- if .Referrer }} referrer="{{- .Referrer -}}" {{- end }}
		{{- if .Media }} media="{{- .Media -}}" {{- end }}
		{{- if .OnLoad }} onload="{{- .OnLoad -}}" {{- end -}}

		{{- if and .Integrity .CrossOrigin }}
			integrity="{{- .Integrity -}}"
			crossorigin="{{- .CrossOrigin -}}"
		{{- end }}
	/>
{{- else -}}
` + AMPHTML + `
{{- end }}
`

	// AMPHTML is the Accelerated Mobile Pages HTML output type.
	AMPHTML = `
<a href="{{- .URL -}}"
	{{- if .ID }} id="{{- .ID -}}"{{- end }}
	{{- if .Class }} class="{{- .Class -}}"{{- end }}
	{{- if .Rel }} rel="{{- .Rel -}}"{{- end }}
	{{- if .Lang }} hreflang="{{- .Lang -}}"{{- end }}
	{{- if .Target }} target="{{- .Target -}}"{{- end }}
	{{- if .Download }} download{{- end }}
	{{- if .OnLoad }} onload="{{- .OnLoad -}}"{{- end -}}
>
	{{ .Content -}}
</a>
`
)

// Anchor is the standard HTML anchor tag (`<a>`) data structure.
type Anchor struct {
	// Common
	URL    string
	Rel    string
	Lang   string
	OnLoad string

	// URL
	Content  string
	ID       string
	Class    string
	Target   string
	Download string
}

// Link is the HTML link tag (`<link>`) data structure.
type Link struct {
	// Common
	URL    string
	Rel    string
	Lang   string
	OnLoad string

	// Link
	Type        string
	As          string
	CrossOrigin string
	Integrity   string
	ImageSizes  string
	ImageSrcSet string
	Media       string
	Sizes       string
	Title       string
	Referrer    string
	LinkElement bool
	Disabled    bool
}
