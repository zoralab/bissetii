// Copyright 2021 ZORALab Enterprise (hello@zoralab.com)
// Copyright 2021 "Holloway" Chew, Kean Ho (hollowaykeanho@gmail.com)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//nolint:lll
package nav

// Item is the item data for the list.
type Item struct {
	// Class is the CSS class attributes for the list item <li>.
	Class string

	// HTML is the HTML body content for the particular list item.
	//
	// DO NOT use it to build sub-list. That belongs to Items.
	HTML string

	// ID is the ID tag for Nav item.
	ID string

	// Items is for housing list recursively (sub-list).
	//
	// If Items is not nil, HTML will be used as title of the sub-list.
	Items []*Item

	// Style is the inline CSS style attribute for the list item <li>.
	Style string

	// UID is the unique ID tag for identification when used as sub-list.
	//
	// Use shasum/random values if needed.
	UID string
}
