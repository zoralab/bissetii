// Copyright 2020 ZORALab Enterprise (hello@zoralab.com)
// Copyright 2020 "Holloway" Chew, Kean Ho (hollowaykeanho@gmail.com)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//nolint:lll
package list

import (
	"gitlab.com/zoralab/bissetii/pkg/components/internal"
)

// Constants for managing the package as a whole.
const (
	// Name is the block name (`{{- define "this-name" -}}`).
	Name = "list"

	// AMPName is the AMP HTML block name.
	AMPName = internal.AMPPrefix + Name

	// ParametersComment are the list of comment headers describing inputs.
	ParametersComment = `
{{- /* .IsOrdered         = List should be ordered (true/false)     */ -}}
{{- /* .ID                = List id attribute (optional)            */ -}}
{{- /* .Class             = List class attribute (optional)         */ -}}
{{- /* .Label             = List aria-label attribute (optional)    */ -}}
{{- /* .Contents          = List of HTML contents (slice)           */ -}}
`
)

// Dependencies are the dependent codes for building independent template.
//
// This is useful for rendering portable template usage like Hugo partials
// where they do not share a common definition source.
const (
	// DepHTML is the vanilla HTML output type.
	DepHTML = ``

	// DepAMPHTML is the Accelerated Mobile Pages HTML output type.
	DepAMPHTML = ``
)

// Full HTML codes for rendering templates without needing to parse file.
const (
	// HTML is the vanilla HTML output type.
	HTML = `
{{- if .IsOrdered }}
<ol {{- if .ID }}
	id="{{- .ID -}}"
{{- end }}
{{- if .Class }}
	class="{{- .Class -}}"
{{- end }}
{{- if .Label }}
	aria-label="{{- .Label -}}"
{{- end -}}
>

{{- else }}
<ul {{- if .ID }}
	id="{{- .ID -}}"
{{- end }}
{{- if .Class }}
	class="{{- .Class -}}"
{{- end }}
{{- if .Label }}
	aria-label="{{- .Label -}}"
{{- end -}}
>

{{- end }}



{{- range $content := .Contents }}
	<li>{{- $content -}}</li>
{{- end }}



{{- if .IsOrdered }}
</ol>
{{- else }}
</ul>
{{- end }}
`

	// AMPHTML is the Accelerated Mobile Pages HTML output type.
	AMPHTML = HTML
)

// Data is the data structure for rendering the component.
type Data struct {
	ID        string
	Class     string
	Label     string
	Contents  []string
	IsOrdered bool
}
