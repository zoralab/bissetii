// Copyright 2021 ZORALab Enterprise (hello@zoralab.com)
//
// Licensed under the Apache License, Version 2.0 (the License);
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an AS IS BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// WARNING: This file is auto-generated! DO NOT EDIT!
//          Original source codes from this directory:
//		1. amp.scss
//		2. critical.scss
//		3. tablet.scss
//		4. desktop.scss
//		5. widescreen.scss
//		6. print.scss
//		7. variables.scss
//		8. variablesAMP.scss

package list

// All Bissetii Compressed Compiled CSS
const (
	CSSAMP = `ol,ul{position:var(--list-position);padding-left:var(--list-padding-left);margin:var(--list-margin);list-style-type:var(--list-style-type);list-style-position:var(--list-style-position);list-style-image:var(--list-style-image)}ul{--list-style-type:disc}ol{--list-style-type:decimal}`

	CSSCritical = `ol,ul{position:var(--list-position);padding-left:var(--list-padding-left);margin:var(--list-margin);list-style-type:var(--list-style-type);list-style-position:var(--list-style-position);list-style-image:var(--list-style-image)}ul{--list-style-type:disc}ol{--list-style-type:decimal}`

	CSSTablet = ``

	CSSDesktop = ``

	CSSWidescreen = ``

	CSSPrint = `li{break-inside:var(--li-print-break-inside)}`

	CSSVariables    = ``
	CSSVariablesAMP = ``
)
