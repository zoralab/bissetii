// Copyright 2021 ZORALab Enterprise (hello@zoralab.com)
//
// Licensed under the Apache License, Version 2.0 (the License);
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an AS IS BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// WARNING: This file is auto-generated! DO NOT EDIT!
//          Original source codes from this directory:
//		1. amp.scss
//		2. critical.scss
//		3. tablet.scss
//		4. desktop.scss
//		5. widescreen.scss
//		6. print.scss
//		7. variables.scss
//		8. variablesAMP.scss

package note

// All Bissetii Compressed Compiled CSS
const (
	CSSAMP = `.note{display:grid;gap:var(--note-layout-gap);grid:var(--note-layout);padding:var(--note-padding);margin:var(--note-margin);background:var(--note-background);border-radius:var(--note-border-radius);border:var(--note-border);border-top:var(--note-border-top)}.note .icon{grid-area:note-icon;justify-self:center;align-self:center}.note .title{grid-area:note-title;margin:var(--note-title-margin);align-self:var(--note-title-align-self);justify-self:var(--note-title-justify-self);font-weight:var(--note-title-font-weight);color:var(--note-title-color)}.note .content{grid-area:note-content}.note.success{--note-background:var(--color-green-100);--note-border:0.1rem solid var(--color-green-700);--note-border-top:0.4rem solid var(--color-green-700);--note-title-color:var(--color-green-700)}.note.warning{--note-background:var(--color-yellow-100);--note-border:0.1rem solid var(--color-yellow-700);--note-border-top:0.4rem solid var(--color-yellow-700);--note-title-color:var(--color-yellow-700)}.note.error{--note-background:var(--color-red-100);--note-border:0.1rem solid var(--color-red-700);--note-border-top:0.4rem solid var(--color-red-700);--note-title-color:var(--color-red-700)}`

	CSSCritical = `.note{display:grid;gap:var(--note-layout-gap);grid:var(--note-layout);padding:var(--note-padding);margin:var(--note-margin);background:var(--note-background);border-radius:var(--note-border-radius);border:var(--note-border);border-top:var(--note-border-top)}.note .icon{grid-area:note-icon;justify-self:center;align-self:center}.note .title{grid-area:note-title;margin:var(--note-title-margin);align-self:var(--note-title-align-self);justify-self:var(--note-title-justify-self);font-weight:var(--note-title-font-weight);color:var(--note-title-color)}.note .content{grid-area:note-content}.note.success{--note-background:var(--color-green-100);--note-border:0.1rem solid var(--color-green-700);--note-border-top:0.4rem solid var(--color-green-700);--note-title-color:var(--color-green-700)}.note.warning{--note-background:var(--color-yellow-100);--note-border:0.1rem solid var(--color-yellow-700);--note-border-top:0.4rem solid var(--color-yellow-700);--note-title-color:var(--color-yellow-700)}.note.error{--note-background:var(--color-red-100);--note-border:0.1rem solid var(--color-red-700);--note-border-top:0.4rem solid var(--color-red-700);--note-title-color:var(--color-red-700)}`

	CSSTablet = ``

	CSSDesktop = ``

	CSSWidescreen = ``

	CSSPrint = `.note{break-inside:avoid}`

	CSSVariables    = ``
	CSSVariablesAMP = ``
)
