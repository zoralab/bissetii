// Copyright 2020 ZORALab Enterprise (hello@zoralab.com)
// Copyright 2020 "Holloway" Chew, Kean Ho (hollowaykeanho@gmail.com)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//nolint:lll
package note

const (
	ErrorIcon = `
<svg class="icon" width="25" height="25" version="1.1" viewBox="0 0 13.229 13.229" xmlns="http://www.w3.org/2000/svg">
<g transform="translate(1.1061 -.94431)" fill="#b20000">
<path transform="matrix(.26458 0 0 .26458 -1.1061 .94431)" d="m24.832 0a25 25 0 0 0-24.832 25 25 25 0 0 0 25 25 25 25 0 0 0 25-25 25 25 0 0 0-25-25 25 25 0 0 0-0.16797 0zm0.16797 3a22 22 0 0 1 22 22 22 22 0 0 1-22 22 22 22 0 0 1-22-22 22 22 0 0 1 22-22z" stroke-linecap="round" stroke-width="8"/>
<g transform="scale(1.0808 .9252)" stroke-width=".29421" style="font-variant-caps:normal;font-variant-east-asian:normal;font-variant-ligatures:normal;font-variant-numeric:normal" aria-label="Χ">
<path d="m1.8153 3.8804h1.2469l2.1319 3.1892 2.1434-3.1892h1.2469l-2.7582 4.1201 2.9421 4.4591h-1.2469l-2.4134-3.6489-2.4307 3.6489h-1.2527l3.0628-4.5798z" fill="#b20000" stroke-width=".29421" style="font-variant-caps:normal;font-variant-east-asian:normal;font-variant-ligatures:normal;font-variant-numeric:normal"/>
</g>
</g>
</svg>
`

	WarningIcon = `
<svg class="icon" width="25" height="25" version="1.1" viewBox="0 0 13.229 13.229" xmlns="http://www.w3.org/2000/svg">
<g transform="translate(1.1061 -.94431)">
<path transform="matrix(.55955 0 0 .63651 -.2204 4.4326)" d="m21.052 14.296-21.628-0.077477 10.881-18.692z" fill="#ffea00" stroke="#ffea00" stroke-linecap="round" stroke-linejoin="round" stroke-width="2.0148"/>
<g transform="scale(1.0636 .94018)" fill="#fff" stroke-width=".30947" style="font-variant-caps:normal;font-variant-east-asian:normal;font-variant-ligatures:normal;font-variant-numeric:normal" aria-label="!">
<path d="m4.5786 11.075h1.1884l0.25995-4.5678 0.061894-1.7207h-1.8321l0.061894 1.7207zm0.59418 3.2928c0.6437 0 1.1388-0.51991 1.1388-1.1884s-0.49515-1.1884-1.1388-1.1884c-0.6437 0-1.1265 0.51991-1.1265 1.1884s0.48277 1.1884 1.1265 1.1884z" fill="#fff" stroke-width=".30947" style="font-variant-caps:normal;font-variant-east-asian:normal;font-variant-ligatures:normal;font-variant-numeric:normal"/>
</g>
</g>
</svg>
`

	SuccessIcon = `
<svg class="icon" width="25" height="25" version="1.1" viewBox="0 0 13.229 13.229" xmlns="http://www.w3.org/2000/svg">
<g transform="translate(1.1061 -.94431)" fill="#0f0">
<path transform="matrix(.26458 0 0 .26458 -1.1061 .94431)" d="m24.832 0a25 25 0 0 0-24.832 25 25 25 0 0 0 25 25 25 25 0 0 0 25-25 25 25 0 0 0-25-25 25 25 0 0 0-0.16797 0zm0.16797 3a22 22 0 0 1 22 22 22 22 0 0 1-22 22 22 22 0 0 1-22-22 22 22 0 0 1 22-22z" stroke-linecap="round" stroke-width="8"/>
<g transform="scale(1.1085 .90215)" stroke-width=".35198" style="font-variant-caps:normal;font-variant-east-asian:normal;font-variant-ligatures:normal;font-variant-numeric:normal" aria-label="✓">
<path d="m2.3296 8.9975q0.26811 0 0.4056 0.43997 0.27498 0.82495 0.39185 0.82495 0.089369 0 0.18561-0.13749 1.9318-3.0936 3.5748-5.0047 0.42622-0.49497 1.3543-0.49497 0.21999 0 0.29561 0.041247 0.07562 0.041247 0.07562 0.10312 0 0.096244-0.22686 0.3781-2.6536 3.1898-4.9222 6.7371-0.15811 0.24748-0.64621 0.24748-0.49497 0-0.58434-0.04125-0.23373-0.10312-0.54996-1.0518-0.35748-1.0518-0.35748-1.3199 0-0.28873 0.48122-0.55684 0.29561-0.16499 0.52247-0.16499z" fill="#0f0" stroke-width=".35198" style="font-variant-caps:normal;font-variant-east-asian:normal;font-variant-ligatures:normal;font-variant-numeric:normal"/>
</g>
</g>
</svg>
`

	InfoIcon = `
<svg class="icon" width="25" height="25" version="1.1" viewBox="0 0 13.229 13.229" xmlns="http://www.w3.org/2000/svg">
<g transform="translate(1.1061 -.94431)">
<path d="m12.123 7.5589a6.6146 6.6146 0 0 1-6.6146 6.6146 6.6146 6.6146 0 0 1-6.6146-6.6146 6.6146 6.6146 0 0 1 6.6146-6.6146 6.6146 6.6146 0 0 1 6.6146 6.6146z" fill="#00f" stroke-linecap="round" stroke-width="2.1167"/>
<g transform="scale(1.2104 .82615)" fill="#fff" stroke-width=".51023" style="font-variant-caps:normal;font-variant-east-asian:normal;font-variant-ligatures:normal;font-variant-numeric:normal" aria-label="i">
<path d="m5.4897 5.2207c0.59187-0.53064 0.63269-0.57146 1.3266-1.1021-0.12246-0.24491-0.2245-0.40819-0.26532-0.46941-0.12246-0.24491-0.14287-0.26532-0.30614-0.6531-0.040819-0.081637-0.081637-0.16327-0.12246-0.26532-0.10205 0.081637-0.2245 0.16327-0.28573 0.2245-0.081637 0.081637-0.24491 0.20409-0.449 0.36737-0.10205 0.081637-0.24491 0.18368-0.4286 0.30614-0.081637 0.061228-0.16327 0.12246-0.24491 0.16327 0.14287 0.449 0.32655 0.77555 0.77555 1.4287zm1.4082 7.7147c-1.9389 1.0409-1.9797 1.0613-2.245 1.0613-0.2245 0-0.36737-0.2245-0.36737-0.57146 0-0.20409 0.061228-0.59187 0.18368-1.0409 1.0409-4.286 1.245-5.1023 1.245-5.2656 0-0.12246-0.10205-0.20409-0.24491-0.20409-0.34696 0-1.5511 0.67351-3.2247 1.796l-0.040819 0.57146c1.2041-0.6531 1.3062-0.69392 1.4899-0.69392 0.16327 0 0.24491 0.12246 0.24491 0.36737 0 0.26532-0.061228 0.55105-0.36737 1.7144-0.95924 3.5716-0.95924 3.5716-0.95924 4.0819 0 0.48982 0.20409 0.81637 0.48982 0.81637 0.34696 0 0.89801-0.26532 1.9797-0.95924 0.38778-0.24491 0.77555-0.48982 1.1633-0.75514 0.18368-0.10205 0.36737-0.2245 0.57146-0.34696z" fill="#fff" stroke-width=".51023" style="font-variant-caps:normal;font-variant-east-asian:normal;font-variant-ligatures:normal;font-variant-numeric:normal"/>
</g>
</g>
 </g>
</svg>
`
)
