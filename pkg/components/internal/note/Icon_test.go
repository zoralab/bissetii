// Copyright 2020 ZORALab Enterprise (hello@zoralab.com)
// Copyright 2020 "Holloway" Chew, Kean Ho (hollowaykeanho@gmail.com)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package note

import (
	"testing"

	"gitlab.com/zoralab/bissetii/pkg/helpers/hugohelper"
)

const (
	infoIconPath    = "layouts/partials/note-icon-info.svg"
	warningIconPath = "layouts/partials/note-icon-warning.svg"
	successIconPath = "layouts/partials/note-icon-success.svg"
	errorIconPath   = "layouts/partials/note-icon-error.svg"
)

func TestInfoIcon(t *testing.T) {
	err := hugohelper.GeneratePartials(infoIconPath, InfoIcon)
	if err != nil {
		t.Errorf("Failed to generate icon file: %v", err)
	}
}

func TestWarningIcon(t *testing.T) {
	err := hugohelper.GeneratePartials(warningIconPath, WarningIcon)
	if err != nil {
		t.Errorf("Failed to generate icon file: %v", err)
	}
}

func TestSuccessIcon(t *testing.T) {
	err := hugohelper.GeneratePartials(successIconPath, SuccessIcon)
	if err != nil {
		t.Errorf("Failed to generate icon file: %v", err)
	}
}

func TestErrorIcon(t *testing.T) {
	err := hugohelper.GeneratePartials(errorIconPath, ErrorIcon)
	if err != nil {
		t.Errorf("Failed to generate icon file: %v", err)
	}
}
