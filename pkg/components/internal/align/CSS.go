// Copyright 2021 ZORALab Enterprise (hello@zoralab.com)
//
// Licensed under the Apache License, Version 2.0 (the License);
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an AS IS BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// WARNING: This file is auto-generated! DO NOT EDIT!
//          Original source codes from this directory:
//		1. amp.scss
//		2. critical.scss
//		3. tablet.scss
//		4. desktop.scss
//		5. widescreen.scss
//		6. print.scss
//		7. variables.scss
//		8. variablesAMP.scss

package align

// All Bissetii Compressed Compiled CSS
const (
	CSSAMP = `.align{display:flex;width:100%;margin:0;padding:0}.align.left{justify-content:start}.align.center{justify-content:center}.align.right{justify-content:end}.align.justify{justify-content:space-between}.align.top{align-items:flex-start}.align.middle{align-items:center}.align.bottom{align-items:flex-end}.align.stretch{align-items:stretch}.align.baseline{align-items:baseline}`

	CSSCritical = `.align{display:flex;width:100%;margin:0;padding:0}.align.left{justify-content:start}.align.center{justify-content:center}.align.right{justify-content:end}.align.justify{justify-content:space-between}.align.top{align-items:flex-start}.align.middle{align-items:center}.align.bottom{align-items:flex-end}.align.stretch{align-items:stretch}.align.baseline{align-items:baseline}`

	CSSTablet = ``

	CSSDesktop = ``

	CSSWidescreen = ``

	CSSPrint = ``

	CSSVariables = `SASSPLACEHOLDER{--list-position:relative}`

	CSSVariablesAMP = `SASSPLACEHOLDER{--list-position:relative}`
)
