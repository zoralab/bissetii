// Copyright 2021 ZORALab Enterprise (hello@zoralab.com)
// Copyright 2021 "Holloway" Chew, Kean Ho (hollowaykeanho@gmail.com)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//nolint:lll
package align

import (
	"gitlab.com/zoralab/bissetii/pkg/components/internal"
)

// Constants for managing the package as a whole.
const (
	// Name is the block name (`{{- define "this-name" -}}`).
	Name = "align"

	// AMPName is the AMP HTML block name.
	AMPName = internal.AMPPrefix + Name

	// ParametersComment are the list of comment headers describing inputs.
	ParametersComment = `
{{- /* .Align           = content horizontal alignment             */ -}}
{{- /* .VAlign          = content vertical alignment               */ -}}
{{- /* .Content         = align content in HTML format             */ -}}
{{- /* .ID              = align id attribute (optional)            */ -}}
{{- /* .Class           = align class attribute (optional)         */ -}}
{{- /* .Style           = align style attribute (optional)         */ -}}
`
)

// Dependencies are the dependent codes for building independent template.
//
// This is useful for rendering portable template usage like Hugo partials
// where they do not share a common definition source.
const (
	// DepHTML is the vanilla HTML output type.
	DepHTML = ``

	// DepAMPHTML is the Accelerated Mobile Pages HTML output type.
	DepAMPHTML = ``
)

// Full HTML codes for rendering templates without needing to parse file.
const (
	// HTML is the vanilla HTML output type.
	HTML = `
<div {{ if .ID -}}id="{{- .ID -}}" {{ end -}}
	class="align {{- if eq .Align "left" }} left
		{{- else if eq .Align "center" }} center
		{{- else if eq .Align "right" }} right
		{{- else }} justify
		{{- end -}}
		{{- if eq .VAlign "top" }} top
		{{- else if eq .VAlign "middle" }} middle
		{{- else if eq .VAlign "bottom" }} bottom
		{{- else if eq .VAlign "stretch" }} stretch
		{{- else }} baseline
		{{- end -}}
		{{- if .Class }} {{ .Class -}}{{- end -}}"
	{{- if .Style }} style="{{- .Style -}}"{{- end -}}
>
	{{- .Content -}}
</div>
`

	// AMPHTML is the Accelerated Mobile Pages HTML output type.
	AMPHTML = HTML
)

// Data is the data structure for rendering the component.
type Data struct {
	ID      string
	Class   string
	Style   string
	Content string
}
