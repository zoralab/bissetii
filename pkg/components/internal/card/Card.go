// Copyright 2020 ZORALab Enterprise (hello@zoralab.com)
// Copyright 2020 "Holloway" Chew, Kean Ho (hollowaykeanho@gmail.com)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//nolint:lll
package card

import (
	"gitlab.com/zoralab/bissetii/pkg/components/internal"
)

// Constants for managing the package as a whole.
const (
	// Name is the block name (`{{- define "this-name" -}}`).
	Name = "card"

	// AMPName is the AMP HTML block name.
	AMPName = internal.AMPPrefix + Name

	// ParametersComment are the list of comment headers describing inputs.
	ParametersComment = `
{{- /* .Type              = Card type facilitated by Bissetii       */ -}}
{{- /* .Class             = Card class attribute                    */ -}}
{{- /* .Style             = Card style attribute                    */ -}}
{{- /* .ID                = Card id attribute                       */ -}}
{{- /* .Width             = Card width                              */ -}}
{{- /* .Height            = Card height                             */ -}}
{{- /* .Label             = Card name (used in aria-label)          */ -}}
{{- /* .Content           = Card content                            */ -}}
{{- /* .Thumbnail         = Card thumbnail image                    */ -}}
{{- /* .CTA               = Card List of call to action links       */ -}}
`
)

// Dependencies are the dependent codes for building independent template.
//
// This is useful for rendering portable template usage like Hugo partials
// where they do not share a common definition source.
const (
	// DepHTML is the vanilla HTML output type.
	DepHTML = ``

	// DepAMPHTML is the Accelerated Mobile Pages HTML output type.
	DepAMPHTML = ``
)

// Full HTML codes for rendering templates without needing to parse file.
const (
	// HTML is the vanilla HTML output type.
	HTML = `
<article class="Card {{ .Type -}}{{- if .Class }} {{ .Class -}}{{- end -}}"
{{- if .ID }}
	id="{{- .ID -}}"
{{- end }}
{{- if or .Width .Height }}
	style=" {{- if .Width -}}--card-width: {{ .Width -}};{{- end }}
		{{- if .Height }}--card-height: {{ .Height -}};{{- end }}
		{{- if .Style }} {{ .Style -}}{{- end -}}
	"
{{- end -}}
>
	{{- if .Thumbnail }}
	<div class="thumbnail">{{ .Thumbnail }}</div>
	{{- end }}
	{{- if .Content }}
	<div class="content">{{- .Content -}}</div>
	{{- end }}
	{{- if .CTA }}
	<div class="cta">{{- .CTA -}}</div>
	{{- end }}
</article>
`

	// AMPHTML is the Accelerated Mobile Pages HTML output type.
	AMPHTML = `
<article class="Card {{ .Type -}}{{- if .Class }} {{ .Class -}}{{- end -}}"
{{- if .ID }}
	id="{{- .ID -}}"
{{- end }}
{{- if or .Width .Height }}
	style="{{- if .Width -}}--card-width: {{ .Width -}};{{- end }}
		{{- if .Height }}--card-height: {{ .Height -}};{{- end }}
		{{- if .Style }} {{ .Style -}}{{- end -}}
	"
{{- end -}}
>
	{{- if .Thumbnail }}
	<div class="thumbnail">{{ .Thumbnail }}</div>
	{{- end }}
	{{- if .Content }}
	<div class="content">{{- .Content -}}</div>
	{{- end }}
	{{- if .CTA }}
	<div class="cta">{{- .CTA -}}</div>
	{{- end }}
</article>
`
)

// Data is the data structure for rendering the component.
type Data struct {
	Type      string
	Class     string
	ID        string
	Width     string
	Height    string
	Content   string
	Thumbnail string
	CTA       string
}
