// Copyright 2020 ZORALab Enterprise (hello@zoralab.com)
// Copyright 2020 "Holloway" Chew, Kean Ho (hollowaykeanho@gmail.com)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package script

import (
	"testing"

	"gitlab.com/zoralab/bissetii/pkg/helpers/hugohelper"
)

const (
	htmlPath = "layouts/partials/script.html"
)

func TestHTML(t *testing.T) {
	err := hugohelper.GeneratePartials(htmlPath,
		ParametersComment+DepHTML+HTML)
	if err != nil {
		t.Errorf("Failed to generate partial file: %v", err)
	}
}
