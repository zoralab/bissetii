// Copyright 2020 ZORALab Enterprise (hello@zoralab.com)
// Copyright 2020 "Holloway" Chew, Kean Ho (hollowaykeanho@gmail.com)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//nolint:lll
package script

import (
	core "gitlab.com/zoralab/bissetii/pkg/components/internal"
)

// Constants for managing the package as a whole.
const (
	// Name is the block name (`{{- define "this-name" -}}`).
	Name = "script"

	// AMPName is the AMP HTML block name.
	AMPName = core.AMPPrefix + Name

	// ParametersComment are the list of comment headers describing inputs.
	ParametersComment = `
{{- /* .ID                    = Script HTML ID attribute              */ -}}
{{- /* .URL                   = Script source URL                     */ -}}
{{- /* .Content               = Script actual content                 */ -}}
{{- /* .Type                  = Script type attribute                 */ -}}
{{- /* .CrossOrigin           = Script CORS ID (MUST have .Integrity) */ -}}
{{- /* .Integrity             = Script SRI (MUST have .CrossOrigin)   */ -}}
{{- /* .NoModule              = Script nomodule attribute flag        */ -}}
{{- /* .Referrer              = Script referrer policy                */ -}}
{{- /* .Nonce                 = Script cryptographic nonce            */ -}}
{{- /* .LoadMode              = Script download mode                  */ -}}
{{- /*                            1. nothing = load in sync           */ -}}
{{- /*                            2. 'async' = load in async          */ -}}
{{- /*                            3. 'defer' = load in defer          */ -}}
`
)

// Dependencies are the dependent codes for building independent template.
//
// This is useful for rendering portable template usage like Hugo partials
// where they do not share a common definition source.
const (
	// DepHTML is the vanilla HTML output type.
	DepHTML = ``

	// DepAMPHTML is the Accelerated Mobile Pages HTML output type.
	DepAMPHTML = ``
)

// Full HTML codes for rendering templates without needing to parse file.
const (
	// HTML is the vanilla HTML output type.
	HTML = `
<script{{- if .ID }} id="{{- .ID -}}" {{- end }}
{{- if .Type }}
	{{- .Type -}}
{{- end }}

{{- if .LoadMode }}
	{{ .LoadMode -}}
{{- end }}

{{- if and .Integrity .CrossOrigin }}
	integrity="{{- .Integrity -}}"
	crossorigin="{{- .CrossOrigin -}}"
{{- end }}

{{- if .NoModule }}
	nomodule
{{- end }}

{{- if .Referrer }}
	referrerpolicy="{{- .Referrer -}}"
{{- end }}

{{- if .Nonce }}
	nonce="{{- .Nonce -}}"
{{- end }}

{{- if .URL }}
	src="{{- .URL -}}"
{{- end -}}
>
	{{- if and .Content (not .URL) -}}{{- .Content -}}{{- end -}}
</script>
`

	// AMPHTML is the Accelerated Mobile Pages HTML output type.
	AMPHTML = `
{{- /* WARNING: AMP does not support Javascript! */ -}}
`
)

// Data is the data structure for rendering the component.
type Data struct {
	ID          string
	URL         string
	Content     string
	Type        string
	CrossOrigin string
	Integrity   string
	Referrer    string
	Nonce       string
	LoadMode    string
	NoModule    bool
}
