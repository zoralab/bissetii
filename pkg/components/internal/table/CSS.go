// Copyright 2021 ZORALab Enterprise (hello@zoralab.com)
//
// Licensed under the Apache License, Version 2.0 (the License);
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an AS IS BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// WARNING: This file is auto-generated! DO NOT EDIT!
//          Original source codes from this directory:
//		1. amp.scss
//		2. critical.scss
//		3. tablet.scss
//		4. desktop.scss
//		5. widescreen.scss
//		6. print.scss
//		7. variables.scss
//		8. variablesAMP.scss

package table

// All Bissetii Compressed Compiled CSS
const (
	CSSAMP = `table{display:var(--table-display);overflow:var(--table-overflow);white-space:var(--table-white-space);border-collapse:var(--table-border-collapse);max-width:var(--table-max-width);margin:var(--table-margin);padding:var(--table-padding)}table thead{background-color:var(--thead-background);color:var(--thead-color)}table tr{border-bottom:var(--tr-border-bottom)}table th,table td{vertical-align:var(--table-cell-vertical-align);padding:var(--table-cell-padding)}`

	CSSCritical = `table{display:var(--table-display);overflow:var(--table-overflow);white-space:var(--table-white-space);border-collapse:var(--table-border-collapse);max-width:var(--table-max-width);margin:var(--table-margin);padding:var(--table-padding)}table thead{background-color:var(--thead-background);color:var(--thead-color)}table tr{border-bottom:var(--tr-border-bottom)}table th,table td{vertical-align:var(--table-cell-vertical-align);padding:var(--table-cell-padding)}`

	CSSTablet = ``

	CSSDesktop = ``

	CSSWidescreen = ``

	CSSPrint = `table{break-inside:avoid;word-break:break-word}`

	CSSVariables    = ``
	CSSVariablesAMP = ``
)
