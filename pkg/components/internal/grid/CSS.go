// Copyright 2021 ZORALab Enterprise (hello@zoralab.com)
//
// Licensed under the Apache License, Version 2.0 (the License);
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an AS IS BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// WARNING: This file is auto-generated! DO NOT EDIT!
//          Original source codes from this directory:
//		1. amp.scss
//		2. critical.scss
//		3. tablet.scss
//		4. desktop.scss
//		5. widescreen.scss
//		6. print.scss
//		7. variables.scss
//		8. variablesAMP.scss

package grid

// All Bissetii Compressed Compiled CSS
const (
	CSSAMP = `.row{display:flex;width:var(--grid-width);justify-content:var(--grid-justify-content);align-items:var(--grid-align-items);flex-wrap:var(--grid-flex-wrap)}.row.align-top{--grid-align-items:flex-start}.row.align-middle{--grid-align-items:center}.row.align-bottom{--grid-align-items:bottom}.row.align-baseline{--grid-align-items:baseline}.row .column{display:var(--grid-column-display);max-width:var(--grid-column-max-width);margin:var(--grid-column-margin);padding:var(--grid-column-padding);visibility:var(--grid-column-visibility);flex-grow:calc(var(--grid-column-base) * 					var(--grid-column-multiplier));flex-basis:var(--grid-column-flex-basis)}.row .column.offset{--grid-column-visibility:hidden}.row .column.grid-2x{--grid-column-multiplier:2}.row .column.grid-3x{--grid-column-multiplier:3}.row .column.grid-4x{--grid-column-multiplier:4}.row .column.grid-5x{--grid-column-multiplier:5}.row .column.grid-6x{--grid-column-multiplier:6}.row .column.grid-7x{--grid-column-multiplier:7}.row .column.grid-8x{--grid-column-multiplier:8}.row .column.grid-9x{--grid-column-multiplier:9}.row .column.grid-10x{--grid-column-multiplier:10}`

	CSSCritical = `.row{display:flex;width:var(--grid-width);justify-content:var(--grid-justify-content);align-items:var(--grid-align-items);flex-wrap:var(--grid-flex-wrap)}.row.align-top{--grid-align-items:flex-start}.row.align-middle{--grid-align-items:center}.row.align-bottom{--grid-align-items:bottom}.row.align-baseline{--grid-align-items:baseline}.row .column{display:var(--grid-column-display);max-width:var(--grid-column-max-width);margin:var(--grid-column-margin);padding:var(--grid-column-padding);visibility:var(--grid-column-visibility);flex-grow:calc(var(--grid-column-base) * 					var(--grid-column-multiplier));flex-basis:var(--grid-column-flex-basis)}.row .column.offset{--grid-column-visibility:hidden}.row .column.grid-2x{--grid-column-multiplier:2}.row .column.grid-3x{--grid-column-multiplier:3}.row .column.grid-4x{--grid-column-multiplier:4}.row .column.grid-5x{--grid-column-multiplier:5}.row .column.grid-6x{--grid-column-multiplier:6}.row .column.grid-7x{--grid-column-multiplier:7}.row .column.grid-8x{--grid-column-multiplier:8}.row .column.grid-9x{--grid-column-multiplier:9}.row .column.grid-10x{--grid-column-multiplier:10}`

	CSSTablet = ``

	CSSDesktop = ``

	CSSWidescreen = ``

	CSSPrint = `.row{break-inside:avoid}`

	CSSVariables    = ``
	CSSVariablesAMP = ``
)
