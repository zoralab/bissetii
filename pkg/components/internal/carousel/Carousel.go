// Copyright 2020 ZORALab Enterprise (hello@zoralab.com)
// Copyright 2020 "Holloway" Chew, Kean Ho (hollowaykeanho@gmail.com)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//nolint:lll
package carousel

import (
	"gitlab.com/zoralab/bissetii/pkg/components/internal"
)

// Constants for managing the package as a whole.
const (
	// Name is the block name (`{{- define "this-name" -}}`).
	Name = "Carousel"

	// AMPName is the AMP HTML block name.
	AMPName = internal.AMPPrefix + Name

	// ParametersComment are the list of comment headers describing inputs.
	ParametersComment = `
{{- /* .ID                = Carousel ID                                 */ -}}
{{- /* .Class             = Carousel class attribute                    */ -}}
{{- /* .Timing            = Carousel timing (milliseconds)              */ -}}
{{- /* .Autoplay          = Carousel autoplay flag (true/false)         */ -}}
{{- /* .Height            = Carousel height                             */ -}}
{{- /* .Width             = Carousel width                              */ -}}
{{- /* .Layout            = Carousel style layout (used in AMP)         */ -}}
{{- /* .Total             = Carousel total slides (round number)        */ -}}
{{- /* .HideNav           = Hide Carousel nav control (true/false)      */ -}}
{{- /* .ShowPrint         = Print Carousel onto paper (true/false)      */ -}}
{{- /* .Next              = Navigation Next button HTML codes           */ -}}
{{- /* .Previous          = Navigation Previous button HTML codes       */ -}}
{{- /* .Slides            = Carousel slides list                        */ -}}
{{- /*      .ID           = Slide ID                                    */ -}}
{{- /*      .Position     = Slide Sorted Position Number (round)        */ -}}
{{- /*      .Thumbnail    = Thumbnail in HTML5 for filling nav panel    */ -}}
{{- /*      .Content      = Slide Content                               */ -}}
{{- /*      .Next         = Next slide                                  */ -}}
{{- /*      .Previous     = Previous slide                              */ -}}
`
)

// Dependencies are the dependent codes for building independent template.
//
// This is useful for rendering portable template usage like Hugo partials
// where they do not share a common definition source.
const (
	// DepHTML is the vanilla HTML output type.
	DepHTML = ``

	// DepAMPHTML is the Accelerated Mobile Pages HTML output type.
	DepAMPHTML = ``
)

// Full HTML codes for rendering templates without needing to parse file.
const (
	// HTML is the vanilla HTML output type.
	HTML = `
<section {{- if .ID }} id="{{- .ID -}}" {{- end }}
	class="Carousel
		{{- if .Class }} {{ .Class -}}{{- end -}}
		{{- if .ShowPrint }} print{{- end -}}
		{{- if .Autoplay }} autoplay{{- end -}}
	"
{{- if or .Width .Height }}
	style="{{- if .Height }}--carousel-height: {{ .Height -}};{{- end -}}
		{{- if .Width }}--carousel-width: {{ .Width -}};{{- end -}}
		{{- if .Total }}--carousel-total: {{ .Total -}};{{- end -}}
		{{- if .Timing }}--carousel-autoplay-timing: {{ .Timing -}};{{- end -}}
		{{- if .HideNav }}--carousel-nav-visibility: hidden;{{- end -}}
	"
{{- end -}}>
	<ol class="viewport">
{{- $main := . -}}
{{- range $slide := .Slides }}
		<li id="{{- $slide.ID -}}-{{- $slide.Position -}}">
			<div class="controller"></div>
			<article>{{- $slide.Content -}}</article>
			<nav>
				<a class="previous"
					href="#{{- $slide.Previous.ID -}} -
						{{- $slide.Previous.Position -}}">
					{{- $main.Previous -}}
				</a>
				<a class="next"
					href="#{{- $slide.Next.ID -}} -
						{{- $slide.Next.Position -}}">
					{{- $main.Next -}}
				</a>
			</nav>
		</li>
{{- end }}
	</ol>
	<nav class="panel"><ol>
{{- range $slide := .Slides }}
		<li><a href="#{{- $slide.ID -}}-{{- $slide.Position -}}">
			{{- $slide.Thumbnail -}}
		</a></li>
{{- end }}
	</ol></nav>
</section>
`

	// AMPHTML is the Accelerated Mobile Pages HTML output type.
	AMPHTML = `
<amp-carousel width="{{- .Width -}}"
	height="{{- .Height -}}"
	layout="{{- .Layout -}}"
	type="slides"
{{- if .Autoplay }}
	autoplay
{{- end }}
{{- if (not .HideNav) }}
	controls
{{- end }}
	delay="{{- .Timing -}}">
{{- range $slide := .Slides }}
	<article>{{- $slide.Content -}}</article>
{{- end }}
</amp-carousel>
`
)

// Slide is a single content data for each carousel slide.
type Slide struct {
	Next      *Slide
	Previous  *Slide
	ID        string
	Thumbnail string
	Content   string
	Position  uint
}

// Data is the data structure for rendering the component.
type Data struct {
	ID        string
	Class     string
	Timing    string
	Height    string
	Width     string
	Layout    string
	Next      string
	Previous  string
	Slides    []Slide
	Total     uint
	Autoplay  bool
	HideNav   bool
	ShowPrint bool
}
