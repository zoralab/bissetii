// Copyright 2020 ZORALab Enterprise (hello@zoralab.com)
// Copyright 2020 "Holloway" Chew, Kean Ho (hollowaykeanho@gmail.com)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//nolint:lll
package root

// Commons are the common `<meta>` tags used across all HTML output types.
const (
	// MetaGeneral specifies critical page `<meta>` information.
	MetaGeneral = `
<meta name="viewport"
	content="width=device-width,minimum-scale=1,initial-scale=1"/>
<meta name="description" content="{{- .Page.Description -}}" />
{{ if .Page.Keywords -}}
<meta name="keywords" content="{{- .Page.Keywords -}}" />
{{- end }}
{{ if .Page.Authors -}}
<meta name="author" content="{{- .Page.Creators -}}" />
{{- end }}
{{- if .Page.Format }}
<link rel="{{- .Page.Format.Rel -}}" href="{{- .Page.Format.URL -}}"
	{{- if .Page.Format.Type }}
	type="{{- .Page.Format.Type -}}"
	{{- end }} />
{{- end }}
`

	// MetaAlternativeFormats are the list of alternative viewing format.
	MetaAlternativeFormats = `
{{- range .Page.Alternative.Formats }}
<link rel="{{- .Rel -}}" type="{{- .Type -}}" href="{{- .URL -}}" />
{{- end }}
`

	// MetaAlternativeLanguages are the list of translated content.
	MetaAlternativeLanguages = `
{{- range .Page.Alternative.Languages }}
<link rel="alternate" hreflang="{{- .Code -}}" href="{{- .URL -}}" />
{{- end }}
`

	// MetaRobots are the list of specific SEO robots crawler rules.
	MetaRobots = `
{{- range $robot, $rules := .Page.Robots -}}
        <meta name="{{- $robot -}}" content="{{ $rules }}" />
{{ end -}}
`

	// MetaFavicons are the list of associated favicon links
	MetaFavicon = `
<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
`
)
