// Copyright 2020 ZORALab Enterprise (hello@zoralab.com)
// Copyright 2020 "Holloway" Chew, Kean Ho (hollowaykeanho@gmail.com)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//nolint:lll
package root

import (
	"gitlab.com/zoralab/bissetii/pkg/components/internal"
)

// Constants for managing the package as a whole.
const (
	// MetaName is the block name (`{{- define "this-name" -}}`).
	MetaName = "meta"

	// AMPName is the AMP HTML block name.
	MetaAMPName = internal.AMPPrefix + MetaName
)

// Dependencies are the dependent codes for building independent template.
//
// This is useful for rendering portable template usage like Hugo partials
// where they do not share a common definition source.
const (
	// MetaDepHTML is the vanilla HTML output type.
	MetaDepHTML = ``

	// MetaDepAMPHTML is the Accelerated Mobile Pages HTML output type.
	MetaDepAMPHTML = ``
)

// Full MetaHTML codes for rendering templates without needing to parse file.
const (
	// MetaHTML is the vanilla HTML output type.
	MetaHTML = MetaHTMLHead +
		MetaGeneral +
		MetaAlternativeFormats +
		MetaAlternativeLanguages +
		MetaRobots +
		MetaFavicon +
		MetaTwitter +
		MetaOpenGraph +
		MetaSchemaOrg +
		MetaHTMLGA

	// MetaAMPHTML is the Accelerated Mobile Pages HTML output type.
	MetaAMPHTML = MetaAMPHead +
		MetaAMPExtensions +
		MetaGeneral +
		MetaAlternativeFormats +
		MetaAlternativeLanguages +
		MetaRobots +
		MetaFavicon +
		MetaTwitter +
		MetaOpenGraph +
		MetaAMPBoilerplate +
		MetaSchemaOrg
)
