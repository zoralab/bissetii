// Copyright 2020 ZORALab Enterprise (hello@zoralab.com)
// Copyright 2020 "Holloway" Chew, Kean Ho (hollowaykeanho@gmail.com)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//nolint:lll
package root

const (
	// MetaTwitter renders meta tags specific to Twitter
	MetaTwitter = `
{{- if .Vendor.Twitter.Cards.Card }}
<meta name="twitter:card" content="{{- .Vendor.Twitter.Cards.Card -}}" />

	{{- if .Vendor.Twitter.Cards.Username }}
<meta name="twitter:site" content="{{- .Vendor.Twitter.Cards.Username -}}" />
	{{- end }}

	{{- if .Page.Creators }}
<meta name="twitter:creator" content="{{- .Page.Creators -}}" />
	{{- end }}
{{- end }}
`
)

// TwitterCards is the credential data structure for Twitter Publish.
//
// More info at: https://business.twitter.com/en/blog/twitter-cards-101.html
type TwitterCards struct {
	Card     string
	Username string
}

// Twitter is the 3rd-party Twitter Platform Credential Data
type Twitter struct {
	Cards *TwitterCards
}
