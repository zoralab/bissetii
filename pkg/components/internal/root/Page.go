// Copyright 2020 ZORALab Enterprise (hello@zoralab.com)
// Copyright 2020 "Holloway" Chew, Kean Ho (hollowaykeanho@gmail.com)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//nolint:lll
package root

import (
	"gitlab.com/zoralab/bissetii/pkg/components/internal/anchor"
	"gitlab.com/zoralab/bissetii/pkg/components/internal/image"
)

// Site is the site-level data structure for Page structure.
type Site struct {
	Title string // hugo: .Site.Title
}

// Date is the date dataset for the Page structure.
type Date struct {
	Created   string
	Published string
	Modified  string
}

// Language is the language dataset for the Page structure.
type Language struct {
	Code   string
	Prefix string
	URL    string
}

// Format is the alternative page format for the Page structure.
type Format struct {
	Name string
	Type string
	Rel  string
	URL  string
}

// Alternative is the list of alternatives for the Page structure.
type Alternative struct {
	Languages []*Language
	Formats   []*Format
}

// Vendor is the 3rd-party vendor data structure.
type Vendor struct {
	Twitter *Twitter
	Google  *Google
}

// Page is the page data structure.
type Page struct {
	Title       string
	Level       string // title heading level
	Site        *Site
	Creators    string
	Authors     string
	Publishers  string
	Type        string
	Date        *Date
	Link        *anchor.Anchor
	Description string
	Keywords    string
	Format      *Format
	Language    *Language
	Alternative *Alternative
	Thumbnails  []*image.Data
	Robots      map[string]string
	Schema      map[string]interface{}
	Content     string
	Copyright   string
	Extensions  []string

	// Nav data elements (possible recursive iterations)
	Pre      string
	UID      string
	Children []*Page
}

// Meta is the full meta data structure for filling this component.
type Meta struct {
	Page    *Page
	Section *Page
	Vendor  *Vendor
}
