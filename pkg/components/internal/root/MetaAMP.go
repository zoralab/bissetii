// Copyright 2020 ZORALab Enterprise (hello@zoralab.com)
// Copyright 2020 "Holloway" Chew, Kean Ho (hollowaykeanho@gmail.com)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//nolint:lll
package root

const (
	// MetaAMPHead is the leading `<meta>` tags for AMP HTML.
	MetaAMPHead = `
<meta charset="utf-8" />
<script async src="https://cdn.ampproject.org/v0.js"></script>
`

	// MetaAMPBoilerplate is the AMP required boilerplate codes in AMP HTML.
	MetaAMPBoilerplate = `<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
`

	// MetaAMPExtensions are to render the list of selected AMP JS modules.
	MetaAMPExtensions = `
{{- range $name := .Page.Extensions }}
	{{- if eq $name "sidebar" }}
<script async
	custom-element="amp-sidebar"
	src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js">
</script>
	{{- end }}
	{{- if eq $name "google-analytics" }}
<script async
	custom-element="amp-analytics"
	src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js">
</script>
	{{- end }}
	{{- if eq $name "form" }}
<script async
	custom-element="amp-form"
	src="https://cdn.ampproject.org/v0/amp-form-0.1.js">
</script>
	{{- end }}
	{{- if eq $name "carousel" }}
<script async
	custom-element="amp-carousel"
	src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js">
</script>
	{{- end }}
	{{- if eq $name "iframe" }}
<script async
	custom-element="amp-iframe"
	src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js">
</script>
	{{- end }}
{{- end }}
`
)
