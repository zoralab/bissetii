// Copyright 2020 ZORALab Enterprise (hello@zoralab.com)
// Copyright 2020 "Holloway" Chew, Kean Ho (hollowaykeanho@gmail.com)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//nolint:lll
package root

const (
	// MetaHTMLGA renders Google Analytics meta tags for vanilla HTML5.
	MetaHTMLGA = `
{{- if .Vendor.Google.Analytics.ID }}
<script>
window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
ga('create', '{{- .Vendor.Google.Analytics.ID -}}', 'auto');
ga('send', 'pageview');
</script>
<script async src='https://www.google-analytics.com/analytics.js'></script>
{{- end }}
`
)

// GA is the Google Analytics credentials data
//
// More info is available at: https://analytics.google.com/
type GA struct {
	ID string
}

// Google is the 3rd-party Google Platform Credential Data
type Google struct {
	Analytics *GA
}
