// Copyright 2021 ZORALab Enterprise (hello@zoralab.com)
//
// Licensed under the Apache License, Version 2.0 (the License);
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an AS IS BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// WARNING: This file is auto-generated! DO NOT EDIT!
//          Original source codes from this directory:
//		1. amp.scss
//		2. critical.scss
//		3. tablet.scss
//		4. desktop.scss
//		5. widescreen.scss
//		6. print.scss
//		7. variables.scss
//		8. variablesAMP.scss

package root

// All Bissetii Compressed Compiled CSS
const (
	CSSAMP = `@font-face{font-family:"Noto Sans";font-style:normal;font-weight:bold;src:local("Noto Sans"),url("/fonts/NotoSans-Bold.ttf")}@font-face{font-family:"Noto Sans";font-style:italic;font-weight:bold;src:local("Noto Sans"),url("/fonts/NotoSans-BoldItalic.ttf")}@font-face{font-family:"Noto Sans";font-style:italic;font-weight:400;src:local("Noto Sans"),url("/fonts/NotoSans-Italic.ttf")}@font-face{font-family:"Noto Sans";font-style:normal;font-weight:400;src:local("Noto Sans"),url("/fonts/NotoSans-Regular.ttf")}@font-face{font-family:"Noto Color Emoji";src:local("Noto Color Emoji"),url("/fonts/NotoColorEmoji.ttf")}html,body{margin:0;padding:0;width:100%}html{font-size:62.5%;height:100%;height:calc(100vh - calc(100vh - 100%));box-sizing:var(--box-sizing-html)}body{font-family:var(--font-family);font-size:var(--font-size);font-weight:var(--font-weight);letter-spacing:var(--font-char-spacing);line-height:var(--font-line-height);text-align:var(--font-align);min-height:100%;display:grid;gap:0;grid:var(--body-grid)}main{z-index:var(--z-index-main);grid-area:content;max-width:var(--main-max-width);margin:var(--main-margin);padding:var(--main-padding)}h1{font-size:var(--h1-font-size);line-height:var(--h1-line-height);letter-spacing:var(--h1-char-spacing);margin:var(--h1-margin);text-decoration:var(--h1-text-deco);text-decoration-color:var(--h1-text-deco-color);border-bottom:var(--h1-border-bottom)}h2{font-size:var(--h2-font-size);line-height:var(--h2-line-height);letter-spacing:var(--h2-char-spacing);margin:var(--h2-margin);text-decoration:var(--h2-text-deco);text-decoration-color:var(--h2-text-deco-color);border-bottom:var(--h2-border-bottom)}h3{font-size:var(--h3-font-size);line-height:var(--h3-line-height);letter-spacing:var(--h3-char-spacing);margin:var(--h3-margin);text-decoration:var(--h3-text-deco);text-decoration-color:var(--h3-text-deco-color);border-bottom:var(--h3-border-bottom)}h4{font-size:var(--h4-font-size);line-height:var(--h4-line-height);letter-spacing:var(--h4-char-spacing);margin:var(--h4-margin);text-decoration:var(--h4-text-deco);text-decoration-color:var(--h4-text-deco-color);border-bottom:var(--h4-border-bottom)}h5{font-size:var(--h5-font-size);line-height:var(--h5-line-height);letter-spacing:var(--h5-char-spacing);margin:var(--h5-margin);text-decoration:var(--h5-text-deco);text-decoration-color:var(--h5-text-deco-color);border-bottom:var(--h5-border-bottom)}h6{font-size:var(--h6-font-size);line-height:var(--h6-line-height);letter-spacing:var(--h6-char-spacing);margin:var(--h6-margin);text-decoration:var(--h6-text-deco);text-decoration-color:var(--h6-text-deco-color);border-bottom:var(--h6-border-bottom)}`

	CSSCritical = `@font-face{font-family:"Noto Sans";font-style:normal;font-weight:bold;src:local("Noto Sans"),url("/fonts/NotoSans-Bold.ttf")}@font-face{font-family:"Noto Sans";font-style:italic;font-weight:bold;src:local("Noto Sans"),url("/fonts/NotoSans-BoldItalic.ttf")}@font-face{font-family:"Noto Sans";font-style:italic;font-weight:400;src:local("Noto Sans"),url("/fonts/NotoSans-Italic.ttf")}@font-face{font-family:"Noto Sans";font-style:normal;font-weight:400;src:local("Noto Sans"),url("/fonts/NotoSans-Regular.ttf")}@font-face{font-family:"Noto Color Emoji";src:local("Noto Color Emoji"),url("/fonts/NotoColorEmoji.ttf")}html,body{margin:0;padding:0;width:100%}html{font-size:62.5%;height:100%;height:calc(100vh - calc(100vh - 100%));box-sizing:var(--box-sizing-html)}body{font-family:var(--font-family);font-size:var(--font-size);font-weight:var(--font-weight);letter-spacing:var(--font-char-spacing);line-height:var(--font-line-height);text-align:var(--font-align);min-height:100%;display:grid;gap:0;grid:var(--body-grid)}main{z-index:var(--z-index-main);grid-area:content;max-width:var(--main-max-width);margin:var(--main-margin);padding:var(--main-padding)}h1{font-size:var(--h1-font-size);line-height:var(--h1-line-height);letter-spacing:var(--h1-char-spacing);margin:var(--h1-margin);text-decoration:var(--h1-text-deco);text-decoration-color:var(--h1-text-deco-color);border-bottom:var(--h1-border-bottom)}h2{font-size:var(--h2-font-size);line-height:var(--h2-line-height);letter-spacing:var(--h2-char-spacing);margin:var(--h2-margin);text-decoration:var(--h2-text-deco);text-decoration-color:var(--h2-text-deco-color);border-bottom:var(--h2-border-bottom)}h3{font-size:var(--h3-font-size);line-height:var(--h3-line-height);letter-spacing:var(--h3-char-spacing);margin:var(--h3-margin);text-decoration:var(--h3-text-deco);text-decoration-color:var(--h3-text-deco-color);border-bottom:var(--h3-border-bottom)}h4{font-size:var(--h4-font-size);line-height:var(--h4-line-height);letter-spacing:var(--h4-char-spacing);margin:var(--h4-margin);text-decoration:var(--h4-text-deco);text-decoration-color:var(--h4-text-deco-color);border-bottom:var(--h4-border-bottom)}h5{font-size:var(--h5-font-size);line-height:var(--h5-line-height);letter-spacing:var(--h5-char-spacing);margin:var(--h5-margin);text-decoration:var(--h5-text-deco);text-decoration-color:var(--h5-text-deco-color);border-bottom:var(--h5-border-bottom)}h6{font-size:var(--h6-font-size);line-height:var(--h6-line-height);letter-spacing:var(--h6-char-spacing);margin:var(--h6-margin);text-decoration:var(--h6-text-deco);text-decoration-color:var(--h6-text-deco-color);border-bottom:var(--h6-border-bottom)}h1:before{content:var(--h1-before-content);margin:var(--h1-before-margin);color:var(--h1-before-color)}h2:before{content:var(--h2-before-content);margin:var(--h2-before-margin);color:var(--h2-before-color)}h3:before{content:var(--h3-before-content);margin:var(--h3-before-margin);color:var(--h3-before-color)}h4:before{content:var(--h4-before-content);margin:var(--h4-before-margin);color:var(--h4-before-color)}h5:before{content:var(--h5-before-content);margin:var(--h5-before-margin);color:var(--h5-before-color)}h6:before{content:var(--h6-before-content);margin:var(--h6-before-margin);color:var(--h6-before-color)}h1 .clean:before,h2 .clean:before,h3 .clean:before,h4 .clean:before,h5 .clean:before,h6 .clean:before{content:none;margin-right:initial}`

	CSSTablet = `body{--body-grid:var(--body-grid-tablet)}`

	CSSDesktop = `body{--body-grid:var(--body-grid-desktop)}`

	CSSWidescreen = `body{--body-grid:var(--body-grid-widescreen)}`

	CSSPrint = `:root{--font-size:12pt}h1,h2,h3,h4,h5,h6,p{display:block;break-inside:avoid}h1{break-before:page;break-after:avoid}h2,h3,h4,h5,h6{break-before:avoid;break-after:avoid}p{break-before:avoid;orphans:0;widows:0}`

	CSSVariables    = ``
	CSSVariablesAMP = ``
)
