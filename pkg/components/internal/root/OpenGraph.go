// Copyright 2020 ZORALab Enterprise (hello@zoralab.com)
// Copyright 2020 "Holloway" Chew, Kean Ho (hollowaykeanho@gmail.com)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//nolint:lll
package root

const (
	// MetaOpenGraph renders meta tags specific to OpenGraph
	MetaOpenGraph = `
<meta property="og:site_name" content="{{ .Page.Site.Title }}" />
<meta property="og:title" content="{{ .Page.Title }}" />
<meta property="og:locale" content="{{- .Page.Language.Code -}}" />
{{- range .Page.Alternative.Languages }}
<meta property="og:locale:alternate" content="{{- .Code -}}" />
{{- end }}
<meta property="og:description" content="{{ .Page.Description }}" />
<meta property="og:type" content="website" />
<meta property="og:url" content="{{ .Page.Link.URL }}" />
{{- range $x := .Page.Thumbnails -}}
	{{- if $x.URL }}
<meta property="og:image" content="{{- $x.URL -}}"/>
		{{- if $x.Type }}
<meta property="og:image:type" content="{{- $x.Type -}}" />
		{{- end }}
		{{- if $x.Width }}
<meta property="og:image:width" content="{{- $x.Width -}}" />
		{{- end }}
		{{- if $x.Height }}
<meta property="og:image:height" content="{{- $x.Height -}}" />
		{{- end }}
		{{- if $x.AltText }}
<meta property="og:image:alt" content="{{- $x.AltText -}}" />
		{{- end }}
	{{- end -}}
{{- end }}
`
)
