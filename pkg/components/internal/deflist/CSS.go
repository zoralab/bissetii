// Copyright 2021 ZORALab Enterprise (hello@zoralab.com)
//
// Licensed under the Apache License, Version 2.0 (the License);
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an AS IS BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// WARNING: This file is auto-generated! DO NOT EDIT!
//          Original source codes from this directory:
//		1. amp.scss
//		2. critical.scss
//		3. tablet.scss
//		4. desktop.scss
//		5. widescreen.scss
//		6. print.scss
//		7. variables.scss
//		8. variablesAMP.scss

package deflist

// All Bissetii Compressed Compiled CSS
const (
	CSSAMP = `dl{overflow:var(--dl-overflow);padding:var(--dl-padding);border-top:var(--dl-border);border-bottom:var(--dl-border);counter-reset:dd-items}dl dt{font-weight:var(--dt-font-weight);margin:var(--dt-margin);padding:var(--dt-padding);border-top:var(--dt-border-top);counter-reset:dd-items}dl dt:first-of-type{margin-top:0;border-top:none}dl dd{margin-inline-start:0;position:relative;counter-increment:dd-items;margin:var(--dd-margin);padding:var(--dd-padding)}dl dd:before{content:counter(dd-items) ". ";position:absolute;left:0}`

	CSSCritical = `dl{overflow:var(--dl-overflow);padding:var(--dl-padding);border-top:var(--dl-border);border-bottom:var(--dl-border);counter-reset:dd-items}dl dt{font-weight:var(--dt-font-weight);margin:var(--dt-margin);padding:var(--dt-padding);border-top:var(--dt-border-top);counter-reset:dd-items}dl dt:first-of-type{margin-top:0;border-top:none}dl dd{margin-inline-start:0;position:relative;counter-increment:dd-items;margin:var(--dd-margin);padding:var(--dd-padding)}dl dd:before{content:counter(dd-items) ". ";position:absolute;left:0}`

	CSSTablet = ``

	CSSDesktop = ``

	CSSWidescreen = ``

	CSSPrint = ``

	CSSVariables    = ``
	CSSVariablesAMP = ``
)
