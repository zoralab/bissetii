// Copyright 2021 ZORALab Enterprise (hello@zoralab.com)
//
// Licensed under the Apache License, Version 2.0 (the License);
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an AS IS BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// WARNING: This file is auto-generated! DO NOT EDIT!
//          Original source codes from this directory:
//		1. amp.scss
//		2. critical.scss
//		3. tablet.scss
//		4. desktop.scss
//		5. widescreen.scss
//		6. print.scss
//		7. variables.scss
//		8. variablesAMP.scss

package blockquote

// All Bissetii Compressed Compiled CSS
const (
	CSSAMP = `blockquote{color:var(--blockquote-color);background-color:var(--blockquote-background-color);border:var(--blockquote-border);border-left:var(--blockquote-border-left);border-radius:var(--blockquote-border-radius);margin:var(--blockquote-margin);padding:var(--blockquote-padding);font-style:var(--blockquote-font-style)}blockquote cite{display:var(--blockquote-cite-display);margin:var(--blockquote-cite-margin);font-size:var(--blockquote-cite-font-size);font-style:var(--blockquote-cite-font-style);text-align:var(--blockquote-cite-text-align)}`

	CSSCritical = `blockquote{color:var(--blockquote-color);background-color:var(--blockquote-background-color);border:var(--blockquote-border);border-left:var(--blockquote-border-left);border-radius:var(--blockquote-border-radius);margin:var(--blockquote-margin);padding:var(--blockquote-padding);font-style:var(--blockquote-font-style)}blockquote cite{display:var(--blockquote-cite-display);margin:var(--blockquote-cite-margin);font-size:var(--blockquote-cite-font-size);font-style:var(--blockquote-cite-font-style);text-align:var(--blockquote-cite-text-align)}`

	CSSTablet = ``

	CSSDesktop = ``

	CSSWidescreen = ``

	CSSPrint = `blockquote{break-inside:var(--blockquote-print-break-inside)}`

	CSSVariables    = ``
	CSSVariablesAMP = ``
)
