// Copyright 2021 ZORALab Enterprise (hello@zoralab.com)
// Copyright 2021 "Holloway" Chew, Kean Ho (hollowaykeanho@gmail.com)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//nolint:lll
package navsidebar

import (
	"gitlab.com/zoralab/bissetii/pkg/components/internal"
	"gitlab.com/zoralab/bissetii/pkg/components/internal/nav"
)

// Constants for managing the package as a whole.
const (
	// Name is the block name (`{{- define "this-name" -}}`).
	Name = "nav-sidebar"

	// AMPName is the AMP HTML block name.
	AMPName = internal.AMPPrefix + Name

	// ParametersComment are the list of comment headers describing inputs.
	ParametersComment = `
{{- /* .RightSide         = Set NavSidebar animation to push towards left */ -}}
{{- /* .Class             = NavSidebar class attribute                    */ -}}
{{- /* .ID                = NavSidebar id attribute                       */ -}}
{{- /* .Menu              = NavSidebar Menu icon HTML code                */ -}}
{{- /* .Close             = NavSidebar Close icon HTML code               */ -}}
{{- /* .List              = NavSidebar primary List                       */ -}}
{{- /*   .UID             = List UID for animation identification         */ -}}
{{- /*   .HTML            = HTML content for the item                     */ -}}
{{- /*   .Items           = Sub list. Repeat List structure. Optional.    */ -}}
`
)

// Dependencies are the dependent codes for building independent template.
//
// This is useful for rendering portable template usage like Hugo partials
// where they do not share a common definition source.
const (
	// DepHTML is the vanilla HTML output type.
	DepHTML = `
{{- define "` + Name + "-item" + `" -}}
<li {{- if .ID }} id="{{- .ID -}}"{{- end -}}
	{{- if .Class }} class="{{- .Class -}}"{{- end -}}
	{{- if .Style }} style="{{- .Style -}}"{{- end -}}>
	{{- if ne (len .Items) 0 }}
		<input id="nav-sidebar-submenu-{{- .UID -}}"
			type="checkbox">
		<label for="nav-sidebar-submenu-{{- .UID -}}">
			{{- .HTML -}}
		</label>
		<ul id="nav-sidebar-submenu-list-{{- .UID -}}">
			{{- range .Items }}
				{{ template "` + Name + `-item" . -}}
			{{- end -}}
		</ul>
	{{- else }}
		{{ .HTML }}
	{{- end }}
</li>
{{ end -}}
`

	// DepAMPHTML is the Accelerated Mobile Pages HTML output type.
	DepAMPHTML = `
{{- define "` + AMPName + "-item" + `" -}}
<li {{- if .ID }} id="{{- .ID -}}"{{- end -}}
	{{- if .Class }} class="{{- .Class -}}"{{- end -}}
	{{- if .Style }} style="{{- .Style -}}"{{- end -}}>
	{{- if ne (len .Items) 0 }}
		<input id="nav-sidebar-submenu-{{- .UID -}}"
			type="checkbox">
		<label for="nav-sidebar-submenu-{{- .UID -}}">
			{{- .HTML -}}
		</label>
		<ul id="nav-sidebar-submenu-list-{{- .UID -}}">
			{{- range .Items }}
				{{ template "` + AMPName + `-item" . -}}
			{{- end -}}
		</ul>
	{{- else }}
		{{ .HTML }}
	{{- end }}
</li>
{{ end -}}
`
)

// Full HTML codes for rendering templates without needing to parse file.
const (
	// HTML is the vanilla HTML output type.
	HTML = `
<nav class="nav-sidebar{{- if .RightSide }} right-side{{- end }}
	{{- if .DrawerMode }} drawer{{- end }}
	{{- if .Class }} {{ .Class -}}{{- end }}"
	{{- if .ID }}id="{{- .ID -}}"{{- end -}}>
	<input id="nav-sidebar-{{- .List.UID -}}"
		class="nav-sidebar-trigger"
		type="checkbox">
	<label class="nav-sidebar-trigger" for="nav-sidebar-{{- .List.UID -}}">
		{{- .Menu -}}
		{{- .Close -}}
	</label>
	<label class="nav-sidebar-shade" for="nav-sidebar-{{- .List.UID -}}">
	</label>
	<ul class="nav-sidebar-list">
		{{- if .List.HTML }}
			<li class="nav-sidebar-title">{{ .List.HTML }}</li>
		{{- end -}}
		{{- range .List.Items }}
			{{ template "` + Name + `-item" . -}}
		{{- end -}}
	</ul>
</nav>
`

	// AMPHTML is the Accelerated Mobile Pages HTML output type.
	AMPHTML = `
<nav class="nav-sidebar{{- if .RightSide }} right-side{{- end }}
	{{- if .DrawerMode }} drawer{{- end }}
	{{- if .Class }} {{ .Class -}}{{- end }}"
	{{- if .ID }}id="{{- .ID -}}"{{- end -}}>
	<button class="nav-sidebar-icon nav-sidebar-menu"
		on="tap:nav-sidebar-{{- .List.UID -}}.open">
		{{- .Menu -}}
	</button>
</nav>
<amp-sidebar id="nav-sidebar-{{- .List.UID -}}"
	class="nav-sidebar{{- if .RightSide }} right-side{{- end }}
	{{- if .Class }} {{ .Class -}}{{- end }}"
	layout="nodisplay"
	side="{{- if .RightSide -}} right {{- else -}} left {{- end -}}">
	<button id="nav-sidebar-{{- .List.UID -}}"
		class="nav-sidebar-icon nav-sidebar-close"
		on="tap:nav-sidebar-{{- .List.UID -}}.close">
		{{- .Close -}}
	</button>
	<ul class="nav-sidebar-list">
		{{- if .List.HTML }}
			<li class="nav-sidebar-title">{{ .List.HTML }}</li>
		{{- end -}}
		{{- range .List.Items }}
			{{ template "` + AMPName + `-item" . -}}
		{{- end -}}
	</ul>
</amp-sidebar>
`
)

// Data is the data structure for rendering the component.
type Data struct {
	// Class is the additiona CSS class tag for NavSidebar.
	Class string

	// Close is the HTML codes for illustrating the close trigger button.
	Close string

	// ID is the ID tag for NavSidebar.
	ID string

	// List is the main list of items.
	List *nav.Item

	// Menu is the HTML codes for illustrating the menu trigger button.
	Menu string

	// RightSide flag is to set NavSidebar appears from the right-side.
	//
	// When set to true, the Sidebar will animate as push to left direction
	// instead of conventional push to right direction.
	//
	// Default is false, which is left-side.
	RightSide bool

	// DrawerMode flag is to set NavSidebar always appear in drawer mode.
	//
	// When set to true, on bigger screen, the sidebar is to retain the
	// drawer open/close effect similar to mobile version. Otherwise,
	// it will be expanded automatically and have the drawer effect removed.
	DrawerMode bool
}
