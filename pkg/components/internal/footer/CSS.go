// Copyright 2021 ZORALab Enterprise (hello@zoralab.com)
//
// Licensed under the Apache License, Version 2.0 (the License);
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an AS IS BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// WARNING: This file is auto-generated! DO NOT EDIT!
//          Original source codes from this directory:
//		1. amp.scss
//		2. critical.scss
//		3. tablet.scss
//		4. desktop.scss
//		5. widescreen.scss
//		6. print.scss
//		7. variables.scss
//		8. variablesAMP.scss

package footer

// All Bissetii Compressed Compiled CSS
const (
	CSSAMP = `footer{display:var(--footer-display);margin:var(--footer-margin);padding:var(--footer-padding);color:var(--footer-color);background:var(--footer-background);border:var(--footer-border)}`

	CSSCritical = `footer{display:var(--footer-display);margin:var(--footer-margin);padding:var(--footer-padding);color:var(--footer-color);background:var(--footer-background);border:var(--footer-border)}`

	CSSTablet = ``

	CSSDesktop = ``

	CSSWidescreen = ``

	CSSPrint = `footer{display:--footer-display-print;-webkit-print-color-adjust:exact;color-adjust:exact;break-inside:avoid}`

	CSSVariables    = ``
	CSSVariablesAMP = ``
)
