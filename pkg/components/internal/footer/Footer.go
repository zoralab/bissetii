// Copyright 2020 ZORALab Enterprise (hello@zoralab.com)
// Copyright 2020 "Holloway" Chew, Kean Ho (hollowaykeanho@gmail.com)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//nolint:lll
package footer

import (
	"gitlab.com/zoralab/bissetii/pkg/components/internal"
)

// Constants for managing the package as a whole.
const (
	// Name is the block name (`{{- define "this-name" -}}`).
	Name = "footer"

	// AMPName is the AMP HTML block name.
	AMPName = internal.AMPPrefix + Name

	// ParametersComment are the list of comment headers describing inputs.
	ParametersComment = `
{{- /* .ID             - javascript unique ID tag                      */ -}}
{{- /* .Class          - CSS class tags                                */ -}}
{{- /* .Style          - inline CSS style statement in HTML attributes */ -}}
{{- /* .Content        - content in HTML ready format                  */ -}}
`
)

// Full HTML codes for rendering templates without needing to parse file.
const (
	// HTML is the vanilla HTML output type.
	HTML = `
<footer {{- if .ID }} id="{{- .ID }}"{{- end }}
	{{- if .Class }} class="{{- .Class -}}"{{- end }}
	{{- if .Style }} style="{{- .Style -}}"{{- end -}}
>
	{{ .Content }}
</footer>
`

	// AMPHTML is the Accelerated Mobile Pages HTML output type.
	AMPHTML = HTML
)

// Data is the data structure for rendering the component.
type Data struct {
	Copyright  string
	Publishers string
}
