// Copyright 2020 ZORALab Enterprise (hello@zoralab.com)
// Copyright 2020 "Holloway" Chew, Kean Ho (hollowaykeanho@gmail.com)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//nolint:lll
package iframe

import (
	"gitlab.com/zoralab/bissetii/pkg/components/internal"
)

// Constants for managing the package as a whole.
const (
	// Name is the block name (`{{- define "this-name" -}}`).
	Name = "iframe"

	// AMPName is the AMP HTML block name.
	AMPName = internal.AMPPrefix + Name

	// ParametersComment are the list of comment headers describing inputs.
	ParametersComment = `
{{- /* .URL         = the targeted iframe                               */ -}}
{{- /* .Sandbox     = the sandbox value for the iframe                  */ -}}
{{- /* .Width       = the width for the iframe                          */ -}}
{{- /* .Height      = the height for the iframe                         */ -}}
{{- /* .ID          = the id HTML attribute for the iframe              */ -}}
{{- /* .Class       = the class HTML attribute for the iframe           */ -}}
{{- /* .Content     = the content of the iframe                         */ -}}
{{- /* .Title       = the title for the iframe                          */ -}}
{{- /* .Layout      = the layout value (e.g. <amp-iframe>)              */ -}}

`
)

// Full HTML codes for rendering templates without needing to parse file.
const (
	// HTML is the vanilla HTML output type.
	HTML = `
<iframe src="{{- .URL -}}" sandbox="{{- .Sandbox -}}"
{{- if .Width }} width="{{- .Width -}}"{{- end }}
{{- if .Height }} height="{{- .Height -}}"{{- end }}
{{- if .ID }} id="{{- .ID -}}"{{- end }}
{{- if .Class }} class="{{- .Class -}}"{{- end }}
{{- if .Title }} title="{{- .Title -}}"{{- end -}}
>
        {{ .Content -}}
</iframe>
`

	// AMPHTML is the Accelerated Mobile Pages HTML output type.
	AMPHTML = `
<amp-iframe src="{{- .URL -}}"
        sandbox="{{- .Sandbox -}}"
        width="{{- .Width -}}"
        height="{{- .Height -}}"
{{- if .Class }} class="{{- .Class -}}" {{- end }}
        layout="{{- .Layout -}}">
        {{ .Content -}}
</amp-iframe>
`
)

// Data is the data structure for rendering the component.
type Data struct {
	URL     string
	SandBox string
	Width   string
	Height  string
	ID      string
	Class   string
	Content string
	Title   string
	Layout  string
}
