{{- $titleWords := replace (replace .Name "-" " ") "_" " " -}}
{{- $keywords := split $titleWords " " -}}
{{- $keywords = $keywords | append "hugo" "bissetii" -}}
{{- $title := strings.Title $titleWords -}}

+++
date = "{{ .Date }}"
title = "How to {{ $title -}}"
description = """
{{ $title }} post description is here.
"""
keywords = {{ replace (printf "%+q" $keywords) " " ", " }}
draft = true
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
]


[creators.ZORALab]
type = "Organization"
name = "ZORALab Team"

[creators.KeanHo]
type = "Person"
name = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "B) How-to (Hugo)"
name = "{{- $title -}}"
pre = ""
weight = 1


[schema]
selectType = "WebPage"
+++

# How to {{% param "title" %}} As Bissetii Module
{{% param "description" %}}
