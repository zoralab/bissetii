{{- $titleWords := replace (replace .Name "-" " ") "_" " " -}}
{{- $keywords := split $titleWords " " -}}
{{- $version := replace $titleWords " " "." -}}
{{- $title := lower $version -}}

+++
date = "{{ .Date }}"
title = "{{- $title }} Release Note"
description = """
Bissetii version `{{ $title }}` has a number of changes worth mentioning in its
(this) release note. There are notable changes, migrations notes, deprecation
notes and etc. It's good to read it through before use.
"""
keywords = [ "{{- $title -}}", "Bissetii", "release", "note" ]
draft = true
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
]


[creators.ZORALab]
type = "Organization"
name = "ZORALab Team"

[creators.KeanHo]
type = "Person"
name = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/releases/dev-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Release Note"

[thumbnails.1]
url = "/img/thumbnails/releases/dev-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Release Note"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "R) Released Versions"
name = "{{- $title -}}"
pre = "🎁"
weight = 1


[schema]
selectType = "WebPage"
+++

# Version `{{ $title }}` Release Note
{{% param "description" %}}


{{< note "warning" "Under Development" >}}
This version is stil under development.
{{< /note >}}



## What's New
Here are some of the key changes happening in this release.



### Backward Compatible Changes
Here are some of seamless upgardes:

1. feature 1


### Non-Backward Compatible Changes
Here are some of the **breaking** changes:

1. **Nothing is breaking so far**.




## Deprecation Notice
Here are the list of deprecation notices. Please migrate before its deadline!

1. **Nothing is deprecating**.




## Important Migrations Notice
Here are some important notices for moving up to this version from its
immediate past version:

1. **Nothing noticable**.




## Epilogue
That's all for Bissetii release note version `{{ $title -}}`. If you
have any questions, please place raise your query in our
[Issues Section](https://gitlab.com/zoralab/bissetii/-/issues).
