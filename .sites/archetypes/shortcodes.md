{{- $name := .Name -}}
{{- $keywords := split .Name " " -}}
{{- $keywords = $keywords | append "shortcode" "hugo" "bissetii" -}}
{{- $title := printf "%s %s" $name "Shortcode" -}}

+++
date = "{{ .Date }}"
title = "{{- $title -}}"
description = """
Page description here.
"""
keywords = {{ replace (printf "%+q" $keywords) " " ", " }}
draft = true
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
]


[creators.ZORALab]
type = "Organization"
name = "ZORALab Team"

[creators.KeanHo]
type = "Person"
name = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "C) Shortcodes (Hugo)"
name = "{{- $name }} (BETA)"
pre = ""
weight = 1


[schema]
selectType = "WebPage"
+++

# `{{ $name }}` Shortcode
{{% param "description" %}}

Example usage:

```markdown {linenos=table,hl_lines=[1,"2-3"],linenostart=1}
{{</* {{ $name }} ARG */>}}
	...
{{</* /{{ $name }} */>}}
```

This shortcode is available starting from `vX.Y.Z`.

{{< note "warning" "Beta Testing Notice" >}}
This is a fairly new component and it is still under user-level API beta
testing. Hence, there may be some major updates in the future that are
**not backward compatible**. Please **ONLY use at your own risk**.
{{< /note >}}




## Heads-Up
While the shortcode is useful, **it breaks the Markdown syntax conventions
due to Hugo specific shortcode feature**. Therefore, when using other Markdown
renderer, these shortcodes would be rendered as text instead of HTML code
fragments.

Also, be careful and stay safe while using this shortcode. It allows any form
of HTML to be embedded into your Markdown page.




## Using `{{ $name }}`
Describe how to write this page.

Since this is a HTML renderer in Markdown file, you want to call in the
shortcode using the `<` symbol instead of `%`. The brackets are as follow:

```markdown {linenos=table,hl_lines=[1,"2-3"],linenostart=1}
# This is Markdown Heading
Your normal text here.

{{</* {{ $name }} */>}}
	Any inner code here.
{{</* /{{ $name }} */>}}

## Back to Markdown
```

This will render the output as:

```html {linenos=table,hl_lines=[1,"2-3"],linenostart=1}
<h1>This is Markdown Heading</h1>
<p>Your normal text here.</p>

    ...RENDER YOUR OUTPUT HERE...
    ...RENDER YOUR OUTPUT HERE...

<h2>Back to Markdown></h2>
```




## Conclusion
That's all about `{{- $name -}}` shortcode in Bissetii. If you need more feature or
need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
