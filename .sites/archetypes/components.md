{{- $name := .Name -}}
{{- $keywords := split .Name " " -}}
{{- $keywords = $keywords | append "components" "html" "w3c" "bissetii" -}}
{{- $title := printf "%s %s" $name "Component" -}}

+++
date = "{{ .Date }}"
title = "{{- $name }} Component"
description = """
This page specify how to create `{{ $name }}` component using Bissetii styling.
It serves as the component specification and a manual on interacting with its
generator for content generation.
"""
keywords = {{ replace (printf "%+q" $keywords) " " ", " }}
draft = true
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
]


[creators.ZORALab]
type = "Organization"
name = "ZORALab Team"

[creators.KeanHo]
type = "Person"
name = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "K) Components"
name = "{{- $name }} (BETA)"
pre = ""
weight = 1


[schema]
selectType = "WebPage"
+++

# `{{- $name -}}` Component
{{% param "description" %}}

This component is available starting from `vX.Y.Z`.

{{< note "warning" "Beta Testing Notice" >}}
This is a fairly new component and it is still under user-level API beta
testing. Hence, there may be some major updates in the future that are
**not backward compatible**. Please **ONLY use at your own risk**.
{{< /note >}}




## Hugo
When using Bissetii as Hugo theme, this component has a few ways to implement
this component.



### `{{- $name -}}` Shortcode (available version `vX.Y.Z` and above)
This component facilitates a shortcode for this component complying to the
following pattern:

```markdown
{{</* {{ $name }} */>}}
	Any inner code here.
{{</* /{{ $name }} */>}}
```

Describe the shortcode.


#### Example
```markdown {linenos=table,hl_lines=[1,"2-3"],linenostart=1}
# This is Markdown Heading
Your normal text here.

{{</* {{ $name }} */>}}
	Any inner code here.
{{</* /{{ $name }} */>}}

## Back to Markdown
Your normal text here.
```

This will render the output as:

```html {linenos=table,hl_lines=[1,"2-3"],linenostart=1}
<h1>This is Markdown Heading</h1>
<p>Your normal text here.</p>

...RENDER YOUR OUTPUT HERE...

<h2>Back to Markdown></h2>
<p>Your normal text here.</p>
```



## Go
Coming Soon.




## HTML
This section covers how to deploy the component using HTML codes. Depending on
the type of HTML you're working on, please view the sub-sections accordingly.



### Vanilla HTML5
This component is usable in the conventional vanilla HTML5 code structure as
follows:

```html
{{< renderHTML html amp >}}
	...
{{< /renderHTML >}}
```

Explain the HTML5 codes accordingly.



### Accelerated Mobile Pages (AMP)
This component is usable in the conventional vanilla HTML5 code structure as
follows:

```html
{{< renderHTML html amp >}}
	...
{{< /renderHTML >}}
```

Explain the AMP-HTML codes accordingly.




## CSS
This section covers all the CSS parts for this component. Depending on your
customization interests, you can explore the sub-sections independently.



### CSS Styling Variables
This component comes with
[CSS variables](https://www.w3.org/TR/css-variables-1/) for customizing the
component's user interfaces based on your needs.


#### `--variable-name`
Describe the variable name here.




## Javascript
Describe which part of Javascript is responsible for the component.



### Trigger Points
Specify which part is the trigger point.




## Sass
Bissetii uses Dart Sass to compile the styling Sass codes into CSS file. This
component's Sass codes is available at the following location:

```
pkg/internal/NAME
```

Bissetii does not package Sass codes explictly so please view them via the
git repository.




## Epilogue
That is all about `{{- $name -}}` component in Bissetii. If you need more
feature or encountered a bug, please feel free to contact us by filing an issue
ticket at our [Issue Section](https://gitlab.com/zoralab/bissetii/-/issues). We
will be glad to help you.
