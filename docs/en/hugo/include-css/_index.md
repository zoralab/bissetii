+++
date = "2021-08-08T14:46:09+08:00"
title = "Include CSS Files with Hugo"
description = """
Although Bissetii itself is heading towards CSS framework, it allows its Hugo
interface to include external or custom CSS files seamelessly. That way, you
are not restricted to only use Bissetii CSS framework.
"""
keywords = ["hugo", "include", "css", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/how-to/getting-started/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/how-to/getting-started/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Interfaces | Hugo"
name = "Include CSS"
pre = "🌈"
weight = 5


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}




## Loading CSS and Assets
Bissetii always load its assets and external files **asynchonously** by default
to prevent page blocking. This loading behavior availability depends on the
Bissetii version you use.



### Version `v1.13.0` and above
Bissetii loads all its CSS files **asynchonously**.



### Version `v1.12.5` and below
{{< note "warning" "Important Notice" >}}
Support for CSS inclusion version `v1.12.5` and below is scheduled to be
stopped starting **June 30, 2021**. While the codes remain available, the team
will no longer answer any questions related to the matter.

**Please DO your CSS includes configurations migrations as soon as possible.**
{{< /note >}}
Bissetii loads each CSS components both synchonous and asynchnonous manner.
The sequences are as follows:

1. Main Sass Codes (**synchnonous loading**)
2. Plain CSS Inclusion (**asynchonous loading**)
3. Asset Concatenation (**asynchonous loading**)




## Sass/Scss Configuration Files
{{< note "error" "HEADS UP" >}}
Starting from version `v1.12.5` and above, Bissetii uses external software
`dart-sass` to compile its Sass/Scss files.

That means that Bissetii **no longer use Hugo extended feature to do the job**.

Hence:

1. Altering the Sass/Scss codes while Hugo is running will not reflect on
your page.
2. Hugo extended will report a lot of errors as its internal `libsass` Sass
features are overly lagging behind `dart-sass`.
{{< /note >}}

Bissetii compile `assets/main.sass` and `assets/amp.sass` using its library
`assets/css/bissetii`. Hence, **you should leave them as it is**.




## Including CSS
To include external CSS files, the configurations varies depending on the
Bissetii version you use.



### Version `v1.13.0` onwards
Starting from `v1.13.0`, Bissetii uses a single data file located in
`data/bissetii/assets/css.toml` to organize the CSS file inclusion. This is
mainly to facilitate additional security features like SRI integrity checking
and cross-origin ID.

Each CSS file metadata and how it should be loaded is stated in each entry
complying to the following pattern:

```toml {linenos=table,hl_lines=[],linenostart=1}
[0]
Algorithm = "include"
URL = "/css/myCSS.min.css"
CrossOrigin = "anonymous"
Integrity = "sha512-awraerrhe....reagaeg"
Disabled = false
Media = "all"
OnLoad = 'this.media="all"'           # be careful with the quote
```

* `[Basic Index ID]` - **COMPULSORY**
	* Keep it numerical for positioning purposes.
* `Algorithm` - **COMPULSORY**
	* The selected processing algorithm to handle this CSS. The values can
	  any of the following:
		* `compress` - concatenate all CSS files with the same
		  process algorithm into a single large CSS file.
		* `include` - add each CSS files with the same process algorithm
		  with dedicated `<link>` tag (multiple network downloads).
* `URL` - **COMPULSORY**
	* The CSS URL source. The URL can be Bissetii's compatible relative URL.
	  Depending on the **Algorithm**, your CSS file should be stored in
	  different directory:
		* For `compress`, place it inside `assets/` directory.
		* For `include`, place it inside `static/` directory.
* `CrossOrigin` - **COMPULSORY**
	* Cross Origin ID. The ID for cross-origin verification when downloading
	  the CSS from other origin. This requires a valid `Integrity` value in
	  order to operate properly. The possible values are:
		* `anonymous` - anyone can download via cross-origin as long as
			**SRI** is verified.
		* `use-credentials` - use request credentials for verifications.
* `Integrity` - **COMPULSORY**
	* The algorithmic shasum value for cross-origin request file
	  verification. It need a valid `CrossOrigin` value in order to operate
	  properly.
* `Disabled` - **OPTIONAL**
	* The `disabled` attribute flag.
	* Set `true` to disable this CSS to load.
* `Media` - **COMPULSORY**
	* The `media=` attribute for downloading and when to load the
	  stylesheet.
* `OnLoad` - **COMPULSORY**
	* The Javascript `onload=` HTML attribute. This allows one to
	  asynchonously load the CSS as long as the process algorithm permits.



### Version `v1.12.0` to Version `v1.12.5`
{{< note "warning" "Important Notice" >}}
Support for CSS inclusion version `v1.12.5` and below is scheduled to be
stopped starting **June 30, 2021**. While the codes remain available, the team
will no longer answer any questions related to the matter.

**Please DO your CSS includes configurations migrations as soon as possible.**
{{< /note >}}

Starting from `v1.12.0` to `v1.12.5`, Bissetii uses the TOML map table to
organize the configuration parameters inside `/data/bissetii` toml data file.

The responsible file for including the list of CSS / Sass / Scss files is
located in:

```
pattern  :                data/bissetii/includes.toml
directory:          docs/.data/bissetii/
repo-docs:          docs/.data/bissetii/includes.toml
```

#### Plain CSS Inclusion
To include plain CSS, you add the URL into the `includes.CSS` map table list.

Example:

```toml {linenos=table,hl_lines=["1-2"],linenostart=1}
[CSS]
files  = [                                   #--> just include the CSS-es
       #"https://cdn.example.com/myCSS.css"
]
```



#### Asset Concatenation
To include the Sass/Scss asset concatenations, you add the relative URL into
the `includes.CSS` map table list.

Example:

```toml {linenos=table,hl_lines=["1-2"],linenostart=1}
[CSS]
assets = [                                 #--> compile into 1 CSS
       #"css/myCSS.css"                    #--> $assetDir/css/myCSS.css
]
```





## Wrapping Up
That is all for including external CSS files with Bissetii into Hugo. If you
have any question to ask us directly, please feel free to raise it at our
[GitLab Issues Section](https://gitlab.com/zoralab/bissetii/-/issues). We will
be happy to assist you.
