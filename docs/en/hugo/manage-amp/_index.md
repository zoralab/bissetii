+++
date = "2021-08-08T17:10:42+08:00"
title = "Manage AMP with Bissetii in Hugo"
description = """
Bissetii supports AMP styling seamlessly from the get-go. However, due to the
AMP diet restrictions and many of its self-inventing AMPHTML syntax, careful
configurations are needed. This page guides you on how to manage AMP properly
with Bissetii in Hugo interface.
"""
keywords = ["hugo", "manage", "amp", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/how-to/getting-started/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/how-to/getting-started/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Interfaces | Hugo"
name = "Manage AMP"
pre = "⚡"
weight = 5


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}

Starting from version `v1.12.0`,
[Accelerated Mobile Pages (AMP)](https://amp.dev/) is supported and enabled by
default. This means that Bissetii shall generate both Vanilla HTML5 and AMPHTML
simultenously using Hugo's multiple output format feature.




## How Bissetii Process AMP
To keep the output files organized and sane, Bissetii opted to use `.amp.html`
file extension in a separate file for AMPHTML next to the Vanilla HTML5 file
with `.html` file extension. The end result will be something as such:

```
/my-path/
	index.html
	index.amp.html
```

Then as instructed by AMP specification, both pages are linked canonically.




## Enable/Disable AMP
Bissetii enabled the AMP by default. To disable it, you need to edit your
site directory's `outputs.toml` configuration file. The pathing is as shown:

```
pattern : <engine>/config/_default/outputs.toml
local   :          config/_default/outputs.toml
repo-doc:   .sites/config/_default/outputs.toml
```

**To enable**, simply add `"AMP"` into the list:

```
home = [ "HTML", "AMP" ]
page = [ "HTML", "AMP" ]
section = [ "HTML", "AMP" ]
taxonomy = [ "HTML", "AMP" ]
term = [ "HTML", "AMP" ]
```

**To disable**, simply remove the `"AMP"` from the list:

```
home = [ "HTML" ]
page = [ "HTML" ]
section = [ "HTML" ]
taxonomy = [ "HTML" ]
term = [ "HTML" ]
```




## Alters AMP File Extension
{{< note warning "Warning" >}}
Only edit the configuration if you know what you're doing. Otherwise, leave it
to Bissetii's default settings.
{{< /note >}}

Bissetii setup `amp.html` as AMP default file extension. To alters the file
extension, simply edit the `suffixes` value inside `mediaTypes.toml` config
file. The location of the file is as follows:

```
pattern : <engine>/config/_default/mediaTypes.toml
local   :          config/_default/mediaTypes.toml
repo-doc:   .sites/config/_default/mediaTypes.toml
```

Look for the `suffixes` field:

```toml {linenos=table,hl_lines=[2],linenostart=1}
["text/amphtml"]
suffixes = [ "amp.html" ]
```




## Adding AMP Javascript Extension Module
Funny enough, for certain components, you will need to add the AMP Javascript
extension module. Fortunately, for maintenance sake, Bissetii generates and
includes these modules seamlessly as you use Bissetii's components as per
their documentations.

Hence, you **DO NOT** need to manually supply these modules when using Bissetii
in your Hugo deployment.

The list of supported modules are listed inside `data/bissetii/extensions.toml`
data file.



### Include Module Globally
Bissetii allows you to include certain modules globally across every pages. To
do that, simply includes the modules into your `data/bissetii/amp.toml` file.

Example, to permanently include `amp-sidebar` module, the
`data/bissetii/amp.toml` file has the following:

```toml {linenos=table,hl_lines=[2],linenostart=1}
modules = [                               #--> include AMP Javascript Module
	"sidebar"
]
```



### Include Module Specifically
Bissetii facilitates you to include page-specific modules via the Hugo's front
matter list. The front matter also has a module table where you can include in.
Example, to include `amp-form`, (by reading the [Form Component]({{< link
"/components/form/" "this" "url-only" />}}) documentation,) the page's front
matter should have the following code snippets:

```toml {linenos=table,hl_lines=[5],linenostart=1}
+++
...
[modules]
extensions = [
	"form",
]
...
+++
```

Bissetii will then compile the list of page-specific modules with the global
modules list, sort out the duplicates, and then include them into the page
accordingly and seamlessly.




## Wrapping Up
That is all for managing AMP with Bissetii in Hugo. If you have any question to
ask us directly, please feel free to raise it at our
[GitLab Issues Section](https://gitlab.com/zoralab/bissetii/-/issues). We will
be happy to assist you.
