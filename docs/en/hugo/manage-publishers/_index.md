+++
date = "2021-08-11T17:35:41+08:00"
title = "Manage Publishers with Bissetii in Hugo"
description = """
After configuring and writing your pages, it's time to manage to publishers.
Hugo supports a wide varieties of publishers available on the Internet. Hence,
Bissetii must be able to provide support most of them.
"""
keywords = ["hugo", "manage", "publishers", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/how-to/getting-started/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/how-to/getting-started/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Interfaces | Hugo"
name = "Manage Publishers"
pre = "📚"
weight = 5


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}




## Run Hugo Server
Before performing publication, you need to know how to execute the Hugo server
for your development hosting. That way, it allows everyone including Bissetii
developers to synchonize your development and communications. The recommended
Hugo server command would be:

```bash {linenos=table,hl_lines=[],linenostart=1}
$ cd .sites                           # enters the engine
$ hugo server --buildDrafts \
	--disableFastRender \
	--bind "localhost" \
	--baseURL "localhost" \
	--port 8080
```

The hugo server instruction is to:

1. Render the draft regardless of the page status.
2. Disable fast rendering to avoid unnecessary caching.
3. Bind `localhost` domain for local access
4. A working port number.

To ensure Bissetii works properly with restrictive output format (e.g.
Accelerated Mobile Pages), **Bissetii disabled Hugo's auto-reload feature by
default**. Hence, you need to manually refresh the page to reflect your changes.

Also, be reminded that the `hugo server` will hold the terminal until it is
completed. Hence, open a new terminal to execute it.




## Build Your Website
Upon completing your development, you can proceed to build your site artifact
for publications. The build command Bissetii team uses would be:

```bash {linenos=table,hl_lines=[],linenostart=1}
$ cd .sites                           # enters the engine
$ hugo --minify
```

Bissetii configured the default output path is the `public/` directory located
at your root directory (or 1 level above).




## Commit to Publisher
With the compiled `public/` artifact ready to be published, the next thing is
to commit it. Depending on the server providers, each provider has its own set
of implementations.



### GitHub Pages
The most common static site generator hosting provider would be
[GitHub Pages](https://pages.github.com/). This section is for you if you're
using Github Pages as your primary hosting provider.


#### Manually Shift 404.html
Hugo compiled 404.html into the each language specific directory. Github Pages
specifically mentions that they can only accept 1 `404.html` file at the root
directory. Hence, you need to copy it out.

Example, say you have `en` language directory, your command would be something
as such:

```bash {linenos=table,hl_lines=[],linenostart=1}
$ cp public/en/404.html public/.
```

**DO NOT git commit your compiled `public/` directory into your current
branch**.


#### Create `gh-pages` Branch
Bissetii strongly recommends to publish the artifact using the `gh-pages` branch
method. This is because:

1. It can avoid duplicated artifact copies.
2. It avoids conflicting with your existing `docs/` directory.
3. It is easier, clearer, and simple to implement.

To start, simply create an empty `gh-pages` branch. **If you already done it and
have a `gh-pages` branch already, simply skip this step**. Otherwise, the
commands are as follows:

```bash {linenos=table,hl_lines=[],linenostart=1}
$ git checkout --orphan gh-pages
$ git reset --hard
$ git clean -fd
$ git commit --allow-empty -m "Init"
$ git push origin gh-pages:gh-pages
$ git checkout <back to your work branch>
```


#### Clean Up Old Artifact
For those who do not want to keep multiple artifact copy (no point keeping them
since you can rebuild at any point of given time and commit ID), proceed to
clean up the `gh-pages` branch to the first blank commit. The commands are:

```bash {linenos=table,hl_lines=[],linenostart=1}
$ git checkout gh-pages
$ git reset --hard "$(git log '--format=format:%H' | tail -1)"
$ git clean -fd
$ git checkout <back to your work branch>
```


#### Publish Your Artifact
Once you have the the `gh-pages` branch ready, proceed to publish your `public/`
directory. The command is:

```bash {linenos=table,hl_lines=[],linenostart=1}
$ git checkout gh-pages
$ git add -f public
$ git commit -m "Build output as of $(git log '--format=format:%H' -1)"
$ git push -f origin gh-pages
$ git checkout <back to your work branch>
```



### GitLab Pages
[GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) shares the same
instructions as GitHub Pages. The only 1 extra one-time setup is to add the
additional [GitLab CI](https://docs.gitlab.com/ee/ci/) instructions into your
`gitlab-ci.yml` file. They are:

```yaml {linenos=table,hl_lines=["3-18"],linenostart=1}
image: debian:latest

pages:
    stage: build
    tags:
        - linux
    environment:
        name: production
    only:
        refs:
            - gh-pages
    artifacts:
        paths:
            - public
    script:
        - mkdir -p public
        - shopt -s extglob
        - mv !(public|.*) public
```

Basically, this is to instruct GitLab CI to package and publish the `gh-pages`
branch artifacts into its GitLab Pages component. Otherwise, it would not work
on GitLab since it is entirely depends on its GitLab CI to perform its
repository processing.




## Wrapping Up
That is all for managing publishers with Bissetii in Hugo. If you have any
question to ask us directly, please feel free to raise it at our
[GitLab Issues Section](https://gitlab.com/zoralab/bissetii/-/issues). We will
be happy to assist you.
