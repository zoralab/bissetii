+++
date = "2021-08-08T16:26:59+08:00"
title = "Include Google Analytics into Hugo"
description = """
Bissetii recognizes metric capturing is one of the important component for the
web development and therefore facilitated a seamless way to include Google
Analytics. This page guides you on how to do it with Hugo.
"""
keywords = ["hugo", "include", "Google Analytics", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/how-to/getting-started/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/how-to/getting-started/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Interfaces | Hugo"
name = "Include Google Analytics"
pre = "📈"
weight = 5


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}


Based on the specification, Bissetii complies to its requirements by including
its Javascript in the `<head>` section.




## Adding Google Analytics ID
To enable Google Analytics, simply add your Google Analytics ID into
`docs/.data/bissetii/vendors/Google.toml` data file inside your `data/`
directory.

{{< note "note" "Note" >}}
Bisetii's default data directory is set to `docs/.data/`. Please check your
Hugo config for pathing.
{{< /note >}}

The field you're looking for is called `[Analytics].ID`. Here is an example:

```toml {linenos=table,hl_lines=[4],linenostart=1}
# All Google Related Configurations

[Analytics]
ID = "UA-1234565789"  # GA-ID
```

Setting this field as blank results with not including Google Analytics into
your website.




## Google Analytics for AMP
Due to the fact that Javascript is not available in AMP pages and only certain
AMP Javscript module is allowed, you need to add the triggers manually. Only
you know where to add the trigger.

There is no easy way to do that except using the
{{< link "/shortcodes/renderhtml/" "this" "" "" >}}renderHTML{{< /link >}}
shortcodes in each Markdown page. Bissetii recommends adding it in front of the
page title. Here is an example:


```markdown {linenos=table,hl_lines=["4-27"],linenostart=1}
...
+++

{{</* renderHTML "amp" */>}}
<amp-analytics type="gtag" data-credentials="include">
        <script type="application/json">
                {
                        "vars": {
                        "gtag_id": "<GA_MEASUREMENT_ID>",
                        "config": {
                                "<GA_MEASUREMENT_ID>": { "groups": "default" }
                                }
                        },
                        "triggers": {
                                "button": {
                                        "selector": "#the-button",
                                        "on": "click",
                                        "vars": {
                                                "event_name": "login",
                                                "method": "Google"
                                        }
                                }
                        }
                }
        </script>
</amp-analytics>
{{</* /renderHTML */>}}
```



## Wrapping Up
That is all for including Google Analytics with Bissetii into Hugo. If you
have any question to ask us directly, please feel free to raise it at our
[GitLab Issues Section](https://gitlab.com/zoralab/bissetii/-/issues). We will
be happy to assist you.
