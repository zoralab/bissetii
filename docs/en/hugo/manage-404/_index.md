+++
date = "2021-08-10T17:12:01+08:00"
title = "Manage 404 with Bissetii in Hugo"
description = """
Managing custom 404 page with Hugo across various hosting service providers is
never an easy job. Fortunately, Bissetii has its own support for facilitating
it. It's not easy but it is worth it.
"""
keywords = ["hugo", "manage", "404", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/how-to/getting-started/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/how-to/getting-started/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Interfaces | Hugo"
name = "Manage 404"
pre = "🚧"
weight = 5


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}

This feature is available since `v1.11.0`.




## The Hugo 404 Drama
Before we start, you need to understand that there is an existing drama with
Hugo custom 404 feature.



### Oxymoronic Responsibilities
From network and how Internet works point of view, **it is not Hugo's
responsibility to serve 404 page**. The real owner is the front-end server like
reverse proxy or content distribution network relay server (e.g. Github Pages,
GitLab Pages, Apache2, Nginx, and more).

However, since 404 is essentially a page communicating an encountered error
to a visitor and Hugo is responsible for custom 404 page rendering.
Non-technical indvidual tends to overlook this matter and blame Hugo for its
"incompetency" instead.

Hence, the fight between Hugo and the 404 service providers to come out with a
unilateral solution continued to this day.



### The Multi-Lingual Problem
Another problem with Hugo is that for multi-lingual implementation, 404 also
has and deserves its own language-specific pages. However, **only ONE 404 page
is allowed**. Hence, to determine the correct language to load is not an
easy job especially for a static site generated website.

Like it or otherwise, both popular free hosting providers (GitLab Pages and
Github Pages) **only accepts 1 page**.

To make matter worse, 404 page cannot be redirected to language-specific 404
page for compiled static site generated website as this will cause a chain
redirect and overwriting network response's code (from `404` into `301`/`302`)
which makes an already bad response worse.

Hence, good luck for customizing 404 page(s), designers!



### Unsupported Multiple Output Formats
Since 404 page is not part of the `docs/` directory, it is not covered by Hugo's
multiple output formats' content generator. Although this can be workaround by
creating 404 as part of `docs/`, obviously, the hosting service providers would
not care about it at all (as if multi-lingual problem isn't enough).




### The Never-Ending Arguments
Since there is no strong reason to convince Hugo developers to pursue single
`404` page rendering when multi-lingual implementation is done and yet the
current popular hosting providers are not cooperating as well, the drama
continues to this day.




## How Bissetii Workaround The Drama
To workaround the current issues at hand, Bissetii decided to guide its users
to **strictly design 1 single 404 page that is compatible with any languages**.
This can be done using many ideas such as language tabs, carousel sliding, and
etc. Bissetii base template for 404 page is percisely doing so.

Then, after each Hugo compilation, copy (not move) out the compiled `404.html`
and its other output formats like `404.amp.html` (if there are) and place it
into the directory as instructed by your hosting provider.




## Customize Bissetii 404 Page
Bissetii enabled custom 404 page by default. To customize the 404 page, simply
edit the `layouts/404.html` directly as instructed by Hugo.

Currently, Bissetii generates a fully independent 404 design via a single
partial template API that creates the whole page from scratch (as in without
using any Hugo or Bissetii partial tools). **Hence, you need to design your own
from scratch**.




## Wrapping Up
That is all for managing 404 with Bissetii in Hugo. If you have any question to
ask us directly, please feel free to raise it at our
[GitLab Issues Section](https://gitlab.com/zoralab/bissetii/-/issues). We will
be happy to assist you.
