+++
date = "2021-08-09T08:07:02+08:00"
title = "Manage Authorship with Bissetii in Hugo"
description = """
Bissetii facilitates an easier way to organize your authorship list after years
of experiences in Hugo. This way, you can properly credit all your authors and
editors without needing too much manual updates.
"""
keywords = ["hugo", "manage", "authorship", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/how-to/getting-started/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/how-to/getting-started/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Interfaces | Hugo"
name = "Manage Authorship"
pre = "👨‍👩‍👧‍👦"
weight = 5


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}




## How Bissetii Process Authorship
Bissetii organizes the authorship via front matter and a global data toml file.
Both of them carries the data structure for every entity (be it person or
company).

In a nutshell, Bissetii processes the list by:

1. Populate the global authorship list.
2. Append or Overwrite the authors from the page-specific authorship list.

Bissetii **is strictly using [Schema.Org](https://schema.org/)** data structure
with exact keywords and definitions to populate the author(s). Currently,
Bissetii recommends the following data structure (more compatible):

1. [Person](https://schema.org/Person) - for living individual
2. [Organization](https://schema.org/Organization) - for company and group

Hence, an author data structure can be the following:

```toml {linenos=table,hl_lines=[],linenostart=1}
[JohnSmith]
"@type" = "Person"
"name" = "John Smith"

[SuccessTeam]
"@type" = "Organization"
"name" = "Success Team"
```

{{< note "note" "Note" >}}
Certain fields like `@type` requires you to stringify the keyword since TOML
does not support key with symbol natively. Hence, the best practice is to
have all the keys as string, keeping things simple.
{{< /note >}}




## Setup Authorship
Bissetii faciliates 2 levels of authorship setup, allowing you to setup your
authors' profile neatly.



### Global-Level Authorship
Setting up global level authorship would have all the authors appear in every
pages regardless of their existence in the Page-Specific level authorship.
Bissetii processes them as `Publishers` and `Creators` in its page data
structure.

To setup the global-level authorship settings, you create a data file located
in `data/` directory, with pathing such that:

```
pattern   :           data/creators.toml
directory :     docs/.data/
repo-docs :     docs/.data/creators.toml
```

Inside the `data/creators.toml` file, you declare each of the profile using
his/her/its profile tag. An example is as follows:

```toml {linenos=table,hl_lines=[],linenostart=1}
# creators/authorship list
[JohnSmith]
"@type" = "Person"
"name" = "John Smith"

[SuccessTeam]
"@type" = "Organization"
"name" = "Success Team"
```



### Page-Specific Authorship
Apart from setting the authorship list globally, you can also setup authorship
list specifically for a page using the Hugo's front matter facility. Bissetii
uses this feature extensively and publishes this list as `Authors` and
(appending or updating) `Creators` in its page data structure.

Should there be a same author appearing in both global-level and page-specific
level authorship list, **the page-specific one will be used, overwriting the
global-level version**.

To setup page-specific authorship list, you need to explicitly declare the
`creators` table alongside with the profile tag. An example would be:

```toml {linenos=table,hl_lines=[],linenostart=1}
+++
...
[creators.JohnSmith]
"@type" = "Person"
"name" = "John Smith"

[creators.SuccessTeam]
"@type" = "Organization"
"name" = "Success Team"
...
+++
```




## Wrapping Up
That is all for managing authorship with Bissetii in Hugo. If you have any
question to ask us directly, please feel free to raise it at our
[GitLab Issues Section](https://gitlab.com/zoralab/bissetii/-/issues). We will
be happy to assist you.
