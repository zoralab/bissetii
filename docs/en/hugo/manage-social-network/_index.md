+++
date = "2021-08-09T13:40:48+08:00"
title = "Manage Social Network with Bissetii in Hugo"
description = """
Apart from SEO, Bissetii strives to be compatible with most major social
networks. This is where Bissetii shines from other Hugo themes where Bissetii
supplies a full interface to handle the sharing system easily and seamlessly.
"""
keywords = ["hugo", "manage", "social network", "sharing", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/how-to/getting-started/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/how-to/getting-started/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Interfaces | Hugo"
name = "Manage Social Network"
pre = "🤝"
weight = 5


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}




## Page Thumbnail Images
Bissetii uses the [OpenGraph](https://ogp.me/)'s `<meta property="oe:image">`
and
[Twitter Card](https://developer.twitter.com/en/docs/twitter-for-websites/cards/guides/getting-started)
sharing tags to define each of the thumbnail image.



### Image Requirements
Different network has different requirements for rendering the images. They can
be found as follows:

| Network  | URL                                                               |
|:--------:|:------------------------------------------------------------------|
| Facebook | https://developers.facebook.com/docs/sharing/webmasters/images/   |
| Twitter  | https://developer.twitter.com/en/docs/twitter-for-websites/cards/overview/summary-card-with-large-image |
| LinkedIn | https://www.linkedin.com/help/linkedin/answer/46687/making-your-website-shareable-on-linkedin?lang=en |
| Whatsapp | Not found. Only tested last image being used (1:1 aspect ratio) |
| Signal Messenger | Not found. |
| Telegram Messenger | Not found. |
| Line Messenger | https://developers.line.biz/en/faq/tags/media/#how-are-the-url-previews-generated |
| WeChat Messenger | Not found. |
| TicTok | Not found. |

To accomandate as many networks as possible, Bissetii compiled the following
image requirements to work in an optimized manner:


#### First Thumbnail
The first thumbnail should be:

1. `1200x630` or `1.91:1` aspect ratio.
2. Max `5MB` in download size (LinkedIn lowest).
3. Primarily for Facebook, LinkedIn, and Twitter.
4. `JPG` or `PNG` format.


#### Second Thumbnail
The follow up thumbnail should be:

1. `1200x1200` or `1:1` aspect ratio.
2. Max `8MB` in download size.
3. Primarily for high definition square thumbnails usage.
4. `JPG` or `PNG` format.


#### Last Thumbnail
The last thumbnail should be:

1. `400x400` or `1:1` aspect ratio.
2. Max `2MB` in download size.
3. Primarily for Whatsapp, and possibly other mobile messengers.
4. `JPG` or `PNG` format.



### Storage
The image storage location does not matter as long as its source URL is publicly
reachable and is able to handle large surge of requests.

For internal hosting storage placement:

1. Bissetii recommends storing them next to the content file (`_index.md`) in
   the same directory for organziational state.
2. Otherwise, storing and hosting inside `static/` directory is also working
   properly.



### Configure Thumbnail
Bissetii supplies thumbnail images via the page's Hugo's front matter with
`[thumbnails]` table. A page can has as many thumbnail images as long as it
complies with all the major supporting social network.

Once the image is available, you can proceed to configure the settings for the
page's thumbnail images. An example configurations (3 thumbnail images in total)
would be as follows:

```toml {linenos=table,hl_lines=[],linenostart=1}
[thumbnails.0]
url = "/img/thumbnails/how-to/getting-started/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/how-to/getting-started/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"
```

* `[thumbnails.N]` - **COMPULSORY**
	* Denotes the following fields belong to `thumbnails.N`. Recommended
	  `N` would be ascending number so that the list of `thumbnails` are
	   sorted out in your order.
* `url` - **COMPULSORY**
	* The image source URL.
* `width` - **COMPULSORY**
	* The width dimension of the image.
* `height` - **COMPULSORY**
	* The height dimension of the image.
* `alternateText` - **COMPULSORY**
	* The alternate text of the image describing the image content.


#### Configure Globally
To configure the thumbnail images globally, set those images' settings into
the following data file inside `data/` directory with the following pathing:

```
pattern  :                    data/bissetii/thumbnails.toml
repo-docs:              docs/.data/bissetii/thumbnails.toml
```


#### Configure Page-Specific
To configure page-specific thumbnail images, set those images' settings into
the page's Hugo front-matter.



### Validators
Some social network facilitates unit-testing validators. Feel free to test them
out while designing for the page thumbnails:

| Network  | URL |
|:---------|:----|
| Facebook | https://developers.facebook.com/tools/debug |
| Twitter  | https://cards-dev.twitter.com/validator |




## Twitter Card Specific Configurations
Twitter has its own specific configurations. Bissetii configured it to use
large image card by default. To change these Twitter specific settings, edit the
following file:

```
pattern  :       data/bissetii/vendors/Twitter.toml
repo-docs: docs/.data/bissetii/vendors/Twitter.toml
```

Specifically, edit any of the following configurations as per
[Twitter Documentation](https://developer.twitter.com/en/docs/tweets/optimize-with-cards/overview/abouts-cards):

```toml {linenos=table,hl_lines=[],linenostart=1}
# All Twitter Configurations

[Cards]
Card = "summary_large_image"
Username = "zoralab" # Twitter handle without '@'
```



## Wrapping Up
That is all for managing social network with Bissetii in Hugo. If you have any
question to ask us directly, please feel free to raise it at our
[GitLab Issues Section](https://gitlab.com/zoralab/bissetii/-/issues). We will
be happy to assist you.
