+++
date = "2021-08-10T14:20:56+08:00"
title = "Manage Redirect with Bissetii in Hugo"
description = """
Managing redirect in Hugo can be cumbersome so Bissetii came out with an easier
solution to handle the task. Through using Bissetii, any page can be set to
redirect with simple setup in the Page's front matter.
"""
keywords = ["hugo", "manage", "redirect", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/how-to/getting-started/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/how-to/getting-started/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Interfaces | Hugo"
name = "Manage Redirect"
pre = "🏕️"
weight = 5


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}



## Setup Page Redirect
Bissetii allows you to setup redirect URL easily using the `redirectURL` field
in the page's Hugo front matter. You can specify both absolute and Bissetii
compatible relative destination URL.

Example:

```toml {linenos=table,hl_lines=[5],linenostart=1}
+++
...
draft = false
type = ""
redirectURL="/en/components/link/"
layout = "single"
+++
...
```

{{< note info "Note" >}}
As a double precaution, Bissetii commented it out by default. If your page's
`redirectURL` is commented, uncomment it.
{{< /note >}}




## Activate Page Redirect
To activate or deactivate the redirect, you need to set the layout between
`redirect` and other layouts. What Bissetii did was to use meta refresh
redirect with `0 second` mechanism layout to perform the page redirection and
process the corresponding sitemap not to include the initiator page in its map.

To activate, simply set the layout to `redirect` as follows:

```toml {linenos=table,hl_lines=[6],linenostart=1}
+++
...
draft = false
type = ""
redirectURL="/en/components/link/"
layout = "redirect"
+++
...
```

To deactivate, simply change the layout to something else (e.g. `single` or
`list` depending on content) as follows:

```toml {linenos=table,hl_lines=[6],linenostart=1}
+++
...
draft = false
type = ""
redirectURL="/en/components/link/"
layout = "single"
+++
...
```



### Hiding The Menu Item
While the page can be redirected, the menu item does not. In fact, there is a
very high chance you will create the same colliding menu item. Hence, remember
to hide or disable the menu item from being usable. See
[Manage Menu]({{< link "/hugo/manage-menu/" "this" "url-only" />}}) for how
to do it.

Here is an example for disabling the menu item via the page's Hugo front matter:

```toml {linenos=table,hl_lines=["4-5"],linenostart=1}
+++
...
[menu.main]
parent = "disabled"
name = "My Title (Disabled)"
pre = "📓"
weight = 5
...
+++
```




## Be Careful
While using Bissetii's redirect implementation is a lot easier than the Hugo
counterpart, **there is no way to defend against infinity redirect looping**.

**Please make sure your redirected URL will not cause such consequences before
enabling**.

{{< note info "What is Infinity Redirect Looping" >}}
Basically a page redirect with no ends. Example:

```
Page 01 --> Page 01A --> Page 02 --> Page 02B --+
  ^                                             |
  |                                             |
  +----------------<------------<---------------+
```
So the user got redirect from `01` to `01A`, then to `02`, then to `02B` which
redirects back to `01` again. Hence, user won't be seeing any content.
{{< /note >}}




## Wrapping Up
That is all for managing redirect with Bissetii in Hugo. If you have any
question to ask us directly, please feel free to raise it at our
[GitLab Issues Section](https://gitlab.com/zoralab/bissetii/-/issues). We will
be happy to assist you.
