+++
date = "2021-08-08T14:46:09+08:00"
title = "Getting Started with Hugo"
description = """
Bissetii seamlessly and inherently supports interfacing with Go Hugo technology
for seamless, continuously improved, and content-focused static website
construction. This is where you get started.
"""
keywords = ["hugo", "getting started", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/how-to/getting-started/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/how-to/getting-started/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Interfaces | Hugo"
name = "Getting Started with Hugo"
pre = "🛫"
weight = 1


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}

This content is written in a procedural way from top to bottom like a steps by
steps manual.



## Understand Bissetii Differences
Although Bissetii is a Hugo Theme module, Bissetii does thing a lot different
from Hugo mainly for continuously improved purposes and scalability usage. Among
the key differences you need to understand are listed here.

Once you understand all the differences and accept the way Bissetii do things
differently, you can then proceed to the next step.


### 1. Using `.sites/` Directory
Bissetii uses `.sites/` directory as its Hugo site rendering directory as
differed from
[Hugo Quick Guide](https://gohugo.io/getting-started/quick-start/). Unlike
Hugo generated site directory, Bissetii had a bunch of critical default
configurations setup before use. Hence, it's best to use Bissetii's instruction
instead of Hugo's setup instructions.

The purpose is that Bissetii does not want to hog the entire git repository
just for website building. Instead, Bissetii wants to be flexible and facilitate
the documentation rendering for software documentation use cases. This approach
is called **Repository Documentation (`repo-docs`)**.

To do that, `.sites/` directory is a first move while retaining `docs/`
directory for housing all the documentations in place. The entire structure
would be as follows:

```bash {linenos=table,hl_lines=[],linenostart=1}
.			# your git root repository
├── docs/		# hugo docs/ directory
│   ├── .data/		# hugo data/ directory
│   │   ├── ...
│   │   └── bissetii	# bissetii specific data files and directories
│   │        └── ...
│   ├── en/		# i18n language specific docs/ directory
│   │   └── ...
│   └── zh-hans/	# i18n language specific docs/ directory
│       └── ...
└── .sites/		# hugo site directory (a.k.a. "engine")
    ├── archetypes
    ├── config
    ├── ...
    ├── static
    └── themes
```



### 2. Internationalization Support From The Start
Like it or otherwise, Bissetii enables internationalization (`i18n`)
multi-lingual support by default. This is mainly due to the URL construction
where the lesser the breaking of URL over time, the better support from
search engine optimization (SEO).

Internationalization in Hugo alters the URL by default so it can't be upgraded
somewhere in the future. Here is an example:

``` {linenos=table,hl_lines=[],linenostart=1}
With i18n (en):		https://www.example.com/en/home/
With i18n (zh-hans):	https://www.example.com/zh-hans/home/
Without:		https://www.example.com/home/
```

*Since the sudden switch to support i18n breaks the URL, `i18n` is enabled by
default from the get-go*.



### 3. Breaking Standard Markdown Convention
Like Hugo, Bissetii developed its own shortcodes and encourages the use of both
Hugo's and Bissetii's shortcodes as extensive as possible to achieve consistency
in your content creation rendering. This is mainly due to too many rendering
variable factors like multiple output format (e.g. HTML, and AMPHTML
simultenously), data processing where Hugo's native functions and shortcodes are
limited, and etc.

By doing so, *you will break the standard Markdown convention*.



### 4. For Advanced Website Developer From Start
Bissetii designed itself for advanced website developer from the start where
we cares about SEO, page optimizations, ranking, scalable customization,
no javascript environment, and multiple formats support from the start. Hence,
it is not for beginner use.

By using Bissetii theme module, *you're expected to already understood quite of
a number of "How Internet Works" especially dealing with search engines and
social network integrations*.




## Installation
Now that you understand and accept how Bissetii do things differently, you
can proceed to install Bissetii and Hugo in your local git repository.




### Install Hugo
Needless to say, you need to install Hugo software into your computer. See
[Hugo Installation Guide](https://gohugo.io/getting-started/installing) for
setting up Hugo in your repository.

{{< note "info" "Minimum Hugo Version" >}}
Check the latest version of the release note from the
[Downloads]({{< link "/releases/" "this" "url-only" />}}) to get the
Bissetii supported minimum Hugo version.
{{< /note >}}



### Install Git
Needless to say, you also need to have Git version control software available
in your computer. See
[Git Installation Guide](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
for setting up Git.




## Setup Hugo with Bissetii Git Repository
With all the dependencies ready, it's time to setup the Bissetii's Hugo site
directory into your repository. Here are the steps-by-steps way to set it up
from scratch.



### Create the Barebone Directories
You can start off by creating the Bissetii's `repo-docs` directories with only
`themes/` folder from the directory pattern as follows:

```bash {linenos=table,hl_lines=[],linenostart=1}
.			# your git root repository
└── .sites/
    └── themes
```

In Unix system, the command would be:

```bash {linenos=table,hl_lines=[],linenostart=1}
mkdir -p ./.sites/themes
```



### Clone Bissetii Theme Module
With the `themes/` directory available, proceed to clone Bissetii's git
repository into it. The git command pattern (and an example) would be:

```bash {linenos=table,hl_lines=[],linenostart=1}
# git clone https://gitlab.com/zoralab/bissetii.git <PATH/TO/THEMES/DIRECTORY>
git clone https://gitlab.com/zoralab/bissetii.git ./.sites/themes/bissetii
```

This output directory pattern will be as follows:

```bash {linenos=table,hl_lines=[],linenostart=1}
.			# your git root repository
└── .sites/
    └── themes
        └── bissetii	# the git cloned bissetii
            └── ...
```



### Unpack the Critical Contents
With Bissetii available as a theme module, it's time to unpack all the critical
directories.

Now, simply copy or create all the following directories and files from the
Bissetii theme module and place them accordingly. The full output directory
pattern alongside its setup instruction would be as follows:

```bash {linenos=table,hl_lines=[],linenostart=1}
.
├── docs/			# create this directory
│   ├─── .data/			# copy from .sites/themes/bissetii/data
│   │   ├── ...
│   │   └── bissetii		# check this must exist along with its contents
│   │        └── ...
│   ├── en/			# create this directory
│   │   └── .gitkeep		# create this file (empty file)
│   └── zh-hans/		# create this directory
│       └── .gitkeep		# create this file (empty file)
└── .sites/
    ├── archetypes		# copy from .sites/themes/bissetii/archetypes
    │   └── default.md		# check this must exist
    ├── assets			# create this directory
    │   └── css			# create this directory
    │   │   └── .gitkeep	# create this file (empty file)
    │   └── js			# create this directory
    │       └── .gitkeep	# create this file (empty file)
    ├── static			# create this directory
    │   └── .gitkeep		# create this file (empty file)
    ├── config			# copy from .sites/themes/bissetii/config
    │   └── _default		# check this must exist along with its contents
    │       └── ...
    ├── .gitignore		# copy from .sites/themes/bissetii/.gitignore
    └── themes
        └── bissetii		# this is .sites/themes/bissetii source folder
            └── ...
```

{{< note warning "Warning - Advanced Hugo User Only" >}}
The minimal viable setup pattern would be as follows:

```bash {linenos=table,hl_lines=[],linenostart=1}
.
├── docs/			# create this directory
│   ├─── .data/			# copy from .sites/themes/bissetii/data
│   │   ├── ...
│   │   └── bissetii		# check this must exist along with its contents
│   │        └── ...
│   ├── en/			# create this directory
│   │   └── .gitkeep		# create this file (empty file)
│   └── zh-hans/		# create this directory
│       └── .gitkeep		# create this file (empty file)
└── .sites/
    ├── config			# copy from .sites/themes/bissetii/config
    │   └── _default		# check this must exist along with its contents
    │       └── ...
    ├── .gitignore		# copy from .sites/themes/bissetii/.gitignore
    └── themes
        └── bissetii		# this is .sites/themes/bissetii source folder
            └── ...
```
{{< /note >}}



### Update the Hugo Configurations
Now that your Bissetii's Hugo site directory is setup correctly, it's time
to update some important configurations. Edit the
`.sites/config/_default/config.toml` and update the configurations accordingly.

Bissetii setup the configurations in a clean and operable manner. In case you
have no idea what to do, only update the following will do:

```toml {linenos=table,hl_lines=[],linenostart=1}
baseURL = "https://bissetii.zoralab.com"  # to your website base URL
```



### Create Your First Landing Page
With everything ready to deploy, you can now create your landing page. Head into
`.sites/` directory and create your landing page using the `hugo new` command.

Example, in Unix system, it would be:

```
cd .sites/
hugo new _index.md
```

This will create a file in `docs/en/` directory.

```bash {linenos=table,hl_lines=[],linenostart=1}
.
├── docs/
│   ├─── .data/
│   │   └── ...
│   ├── en/
│   │   ├── _index.md		# new page site created
│   │   └── .gitkeep
│   └─ ...
...
```

Update the contents and page metadata inside `_index.md` accordingly with test
data.

To ensure the translated page is working as well, simply copy the `_index.md`
other the other language's same location. The final output would be:

```bash {linenos=table,hl_lines=[],linenostart=1}
.
├── docs/
│   ├─── .data/
│   │   └── ...
│   ├── en/
│   │   ├── _index.md
│   │   └── .gitkeep
│   └─ zh-hans/
│       ├── _index.md		# copy one over here
│       └── .gitkeep
...
```

Launch the `hugo server` to view your test site. You should be able to see
the landing pages operating accordingly.




### You're Ready
Congrats! You're now fully ready to operate. Ensure you `git commit` your
setup before expanding the website content.




## Wrapping Up
That is all for getting started with Bissetii and Hugo. If you have any question
to ask us directly, please feel free to raise it at our
[GitLab Issues Section](https://gitlab.com/zoralab/bissetii/-/issues). We will
be happy to assist you.

Thanks for using Bissetii for your content creation! We hope you enjoy the
technology we developed over the years greatly helps you in your web
development.
