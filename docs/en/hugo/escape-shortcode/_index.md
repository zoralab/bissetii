+++
date = "2021-08-09T20:15:13+08:00"
title = "Escape Shortcode in Hugo"
description = """
Once in a while, you might want to print the shortcode instructions into your
page content instead of getting it rendered. This is called escaping and
fortunately, Hugo has a built-in method to perform such action.
"""
keywords = ["hugo", "escape", "shortcode", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/how-to/getting-started/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/how-to/getting-started/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Interfaces | Hugo"
name = "Escape Shortcode"
pre = "🏕️"
weight = 5


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}




## Escaping Shortcode
To escape a shortcode, you simply need to add comment syntax at the shortcode
braces as such:

```markdown
{{</*/* link "/img/sample.png" "this" "url-only" */*/>}}
```

This will be rendered as:

```markdown
{{</* link "/img/sample.png" "this" "url-only" */>}}
```

Note that you can perform deeper level commenting in order to print the escaped
shortcode like the above (as in, escaping the escaped shortcode).




## Wrapping Up
That is all for escaping shortcode with Bissetii in Hugo. If you have any
question to ask us directly, please feel free to raise it at our
[GitLab Issues Section](https://gitlab.com/zoralab/bissetii/-/issues). We will
be happy to assist you.
