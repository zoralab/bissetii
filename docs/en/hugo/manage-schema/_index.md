+++
date = "2021-08-11T07:18:46+08:00"
title = "Manage Schema.Org with Bissetii in Hugo"
description = """
Bissetii strives to provide an easy way to manage your page's Schema.Org data
without needing too much manual work. Although complicated, it is necessary to
boost your search engine optimization needs for easier parsing by crawlers.
"""
keywords = ["hugo", "manage", "schema.org", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/how-to/getting-started/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/how-to/getting-started/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Interfaces | Hugo"
name = "Manage Schema.Org"
pre = "🔬"
weight = 5


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}

This feature is available since `v1.12.0`.




## The `ld+json` Approach
Bissetii opted the use of `ld+json` approach as recommended by
[Google Search Central](https://developers.google.com/search/docs/advanced/structured-data/intro-structured-data#structured-data) mainly because Bissetii does not want its
content creators to write the content twice (once in Markdown, another in JSON).

This approach will generate a `<script type='application/ld+json'>` HTML tag in
the `<head>` section containing the Page data information. An example is shown
as follows:

```html {linenos=table,hl_lines=[],linenostart=1}
...
<head>
	...
	<script type="application/ld+json">
	{
		"@context": "https://schema.org/",
		"@type": "SoftwareApplication",
		"applicationcategory": ["Hugo Theme Module", "Go Template Module"],
		"author": [{
				"@type": "Organization",
				"name": "Bissetii Team",
				"slogan": "Design with Simplicity"
			},
			[{
				"name": "Holloway Chew Kean Ho",
				"type": "Person"
			}],
			[{
				"name": "ZORALab Team",
				"type": "Organization"
			}]
		],
		...
	}
	</script>
	...
</head>
...
```
{{< note info "Note" >}}
The above JSON was prettified for the sake of this document. The actual would
be compressed to save characters and sizes.
{{< /note >}}




## Facilitate Data Schema
Bissetii supplies a number of default data schema by default inside
`data/bissetii/schema/` directory. The most notable example would be
`WebPage.toml` where it will generate every page as web page data structure.

If you want to build your own data schema, simply add a new TOML data file into
`data/bissetii/schema/` directory.

You can refer to the following resources for referencing a usable data
structure:

1. [Schema.org Parameters](https://schema.org/docs/full.html)
2. [Google Search Gallery](https://developers.google.com/search/docs/guides/search-gallery)

{{< note warning "About Parameters" >}}
Currently, Bissetii team is still brainstorming on how to present its Hugo's
page parameters in a scalable way. For now, you might need to be an actual
Bissetii developer to create a supported version.
{{< /note >}}



### Validator
There are validators in the web allowing you to validate a page's Schema.org
data structure. Bissetii recommends the following:

1. [JSON Linter](https://jsonlint.com/)
2. [Google Rich Results Test](https://search.google.com/test/rich-results)
3. [Schema.org Validator](https://validator.schema.org/)




## Selecting A Schema
Now, for any page, you can select an available schema via the page's Hugo front
matter's `[schema]` table. The field is `selectType` and its value should be the
name of the TOML data file. The pattern is as such:

```toml {linenos=table,hl_lines=["3-4"],linenostart=1}
+++
...
[schema]
selectType = "SoftwareApplication"
...
```

Example:

| Value                    | Target                                          |
|:-------------------------|:------------------------------------------------|
| `"SoftwareApplication"`  | `data/bissetii/schema/SoftwareApplication.toml` |
| `"MyCustom"`             | `data/bissetii/schema/MyCustom.toml`            |
| `WebPage` or default     | `data/bissetii/schema/WebPage.toml`             |




## Still Continously Developing
Please keep in mind that this schema.org rendering is still under continuous
development to facilitate an optimized content creation experience (and data
structure maintenance).

Among notable developments would be page-specific schema fields via Hugo's
front matter, authorship correction, and etc.




## Wrapping Up
That is all for managing Schema.org with Bissetii in Hugo. If you have any
question to ask us directly, please feel free to raise it at our
[GitLab Issues Section](https://gitlab.com/zoralab/bissetii/-/issues). We will
be happy to assist you.
