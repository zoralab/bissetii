+++
date = "2021-08-09T11:46:35+08:00"
title = "Manage Menu with Bissetii in Hugo"
description = """
Bissetii has its own navigation menu management which can easily customize one
or many of its nav components with Hugo interface. The configurations are
data-driven so it will be a lot easier to manage.
"""
keywords = ["hugo", "manage", "menu", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/how-to/getting-started/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/how-to/getting-started/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Interfaces | Hugo"
name = "Manage Menu"
pre = "📓"
weight = 5


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}




## Customizing Icons and List Items
Starting from Bissetii version `v1.13.0`, the icons and list items' specific
customization are covered by the respective components' documentations. You
should go through them over there.



### Version `v1.12.5` and Below
{{< note "warning" "Deprecation Notice" >}}
Starting from version `v1.13.0`, the support for menu customization `v1.12.5`
and below is deprecated and the support is scheduled to be rescinded on
**July 31, 2022**.

Please migrate to the latest implementation instead.
{{< /note >}}

All icons embedded inside Sass were removed and revert to using HTML and images.
This includes the navigation bar menu (`☰`) and close icon (`✕`).

To change those icons, simply set the relative path in both `openIcon` and
`closeIcon` inside `data/bissetii/navbars.toml`'s `[sidebar]` map table:

Example:

```toml {linenos=table,hl_lines=["3-5"],linenostart=1}
# all navbars related configurations

[sidebar]
openIcon = "/img/css/menu-icon.svg"
closeIcon = "/img/css/close-icon.svg"
```

The default values (if the parameters are not available) would be:

```
# Close Icon (✕)
URL Path:               /img/css/close-icon.svg
Storage Location: static/img/css/close-icon.svg

# Menu Icon (☰)
URL Path:               /img/css/menu-icon.svg
Storage Location: static/img/css/menu-icon.svg
```




## Adding Icon to Menu Item
Starting from Bissetii version `v1.12.0` onwards, all menu item can be quickly
styled with creative icons using `menu.main.pre` field in the page's Hugo front
matter. Example:

```toml {linenos=table,hl_lines=[4],linenostart=1}
[menu.main]
parent = "Interfaces | Hugo"
name = "Manage Menu"
pre = "📓"
weight = 5
```

The value will be wrapped with `<pre>...</pre>` HTML syntax during the
rendering. The default is blank for keeping things clean.

The recommended value would be using the unicode symbol or emoji symbol. If you
want custom images, you need to consult the nav component's documentation since
not all components support such feature.




## Organizing Menu
Depending on the menu item nature, organizing menu items in Hugo can never be
a good experience. When using Hugo with Bissetii theme, Bissetii recommends the
following practices to keep your management sane.



### Menu is Always in Alphabetical Order
By default, Hugo always organize the menu items (be in parent menu item or
actual menu item) in an **alphabetical order** in accordance to the weight
value.

Hence, different weight value provides different set of menu items bundled
together, with the lowest weight bundle shown first (again, complying to
**alphabetical order**).



### Use `Weight` to Prioritize Actual Menu Item
For actual menu items (e.g. URL pointing to an actual page), you can make use
of `weight` field to organize its position. Example:

```toml {linenos=table,hl_lines=[5],linenostart=1}
[menu.main]
parent = "Interfaces | Hugo"
name = "Manage Menu"
pre = "📓"
weight = 5
```

Hugo will arrange the items with the lowest weight first to the highest.
Example, for the following items:

```md {linenos=table,hl_lines=[],linenostart=1}
Alpha     = 2
Bravo     = 1
Charlie   = 4
Delta     = 3
Epsilon   = 2
```

It will be arranged as:

```md {linenos=table,hl_lines=[],linenostart=1}
Bravo     # weight = 1
Alpha     # weight = 2, alphabetical "A" first
Epsilon   # weight = 2, alphabetical "E" later
Delta     # weight = 3
Charlie   # weight = 4
```



### Use Naming Prefix to Organize Parental Menu Item
For parental menu items (e.g. no URL but has a list of actual menu items as its
children), there is no direct way for organizing the menu. Currently, Hugo does
not offer any mechanism to organize them autonomously.

A quick workaround Bissetii recommends would be adding a sort-able naming
prefix to your parental menu items' `name`. Example:

```toml {linenos=table,hl_lines=[],linenostart=1}
parent = "B) Alpha"
parent = "A) Bravo"
parent = "D) Charlie"
parent = "C) Delta"
```

It will be arranged as:

```md {linenos=table,hl_lines=[],linenostart=1}
A) Bravo
	...
B) Alpha
	...
C) Delta
	...
D) Charlie
	...
```

Alternatively, you can creatively organize your navigation menu naming
convention (e.g. Bissetii changed from using naming prefix to catalog grouping).




## Blocking Menu Item
Bissetii facilitates a mechanism to easily block a menu item from being rendered
easily through the page's Hugo front matter. The mechanism is to set the
`parent` value matching to any of the keyword found in a block list data file.



### Customizing Blocking Keywords
To customize the list of blocking keywords, you need to supply your keyword into
the blocklist data file.

Example, Bissetii currently supplies `redirect` and `disabled` keywords to
automatically block all actual items having those `parent` values as follows:

```toml {linenos=table,hl_lines=[],linenostart=1}
# List all the parent's value to be blocked from being organized into structure
#
# Example:
# 1. "redirect" = true   ==> blocks all nav.Name = "redirect"
# 2. "disabled" = true   ==> blocks all nav.Name = "disabled"
"redirect" = true
"disabled" = true
```

For Bissetii version `v1.13.0`, the keywords is added into
`data/bissetii/blocklist.toml` data file.

For Bissetii version `v1.12.5` and below, the keyword is added into
`data/bissetii/navbars.toml` data file with `[blocklist]` table as such:

```toml {linenos=table,hl_lines=[],linenostart=1}
# all navbars related configurations

...

[blocklist]
"redirect" = true
"Nav List Item Name" = true
```

{{< note "warning" "Deprecation Notice" >}}
Starting from version `v1.13.0`, the support for menu blocklisting version
`v1.12.5` and below is deprecated and the support is scheduled to be rescinded
on **July 31, 2022**.

Please migrate to the latest implementation instead.
{{< /note >}}



### Blocking The Menu Item
Now, to block the menu item, simply use one of the blocking keyword from the
blocklist data file. Example, if Bissetii's `"disabled"` is available and set to
`true`, the blocking would be:

```toml {linenos=table,hl_lines=["2"],linenostart=1}
[menu.main]
parent = "disabled"
name = "Manage Menu"
pre = "📓"
weight = 5
```

To prevent naming collision from Hugo rendering, it's best to append a suffix
into the name as well. The end result would be:

```toml {linenos=table,hl_lines=["2-3"],linenostart=1}
[menu.main]
parent = "disabled"
name = "Manage Menu (Disabled)"
pre = "📓"
weight = 5
```




## Wrapping Up
That is all for managing menu with Bissetii in Hugo. If you have any question to
ask us directly, please feel free to raise it at our
[GitLab Issues Section](https://gitlab.com/zoralab/bissetii/-/issues). We will
be happy to assist you.
