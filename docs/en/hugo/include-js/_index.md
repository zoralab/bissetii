+++
date = "2021-08-08T15:33:08+08:00"
title = "Include JS Files with Hugo"
description = """
Although Bissetii itself is mainly not using any Javascript, it allows its Hugo
interface to include external or custom JS files seamelessly. That way, you
are not restricted to only CSS usage.
"""
keywords = ["hugo", "include", "js", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/how-to/getting-started/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/how-to/getting-started/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Interfaces | Hugo"
name = "Include JS"
pre = "💫"
weight = 5


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}

However, please keep in mind that **Javascript will not work for
Accelerated Mobile Pages (AMP) since the prohibition is part of its main
requirement**. Hence, please ensure your critical components do not depend on
Javascript if AMP is enabled.




## How Bissetii Loads Javascript
Bissetii uses both `defer` and `async` loading to download and load Javascript
files by default.

Javascript, especially dealing with its frameworks, usually comes in large
files. This can block downloading bandwidth when they are downloaded
synchonously with the main HTML and CSS body. That is why Bissetii persued
the asynchonous loading whenever possible.

Also, the position (as in when to download and load) is entirely depends on
the Bissetii version that you're using.



### Version `1.13.0` and above
Bissetii loads all Javascripts in the end of the `<body>` sections regardless
of its `async` and `defer` attributes.
**You still need them to manage their respective initializations**.


### Version `1.12.5` and Below
{{< note "warning" "Important Notice" >}}
Support for JS inclusion version `v1.12.5` and below is scheduled to be
stopped starting **July 31, 2022**. While the codes remain available, the team
will no longer answer any questions related to the matter.

**Please DO your JS includes configurations migrations as soon as possible.**
{{< /note >}}

Bissetii loads the `defer` Javascripts in the `<head>` section while
`async` in the end of the `<body>` section.




## Javascript Inclusion
Depending on Bissetii version you're using, the configuration varies
accordingly.



### Version `v1.13.0` Onwards
Bissetii uses a single data file located in `data/bissetii/assets/js.toml` to
organize the JS file inclusion. This is mainly to facilitate additional
security features like SRI integrity checking and cross-origin ID.

Each JS file metadata and how it should be loaded is stated in each entry
complying to the following pattern:

```toml {linenos=table,hl_lines=[],linenostart=1}
[0]
Algorithm = "compress"
ID = "script-myJS"            # file in 'assets/js/myJS.min.js'
URL = "/js/myJS.min.js"
Type = "application/x-javascript"
LoadMode = "defer"
CrossOrigin = "anonymous"
Integrity = "sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg=="
#Content = """ """
```

* `[Index ID]` - **COMPULSORY**
	* Keep it numerical for positioning purposes.
* `Algorithm` - **COMPULSORY**
	* The algorithm to process the script. It can be the following:
		* `compress` will concatenate all CSS files with the same
		  process algorithm into a single large CSS file.
		* `include` will add each CSS files with the same process
		  algorithm with dedicated `<script>` tag (multiple downloads).
* `ID` - **OPTIONAL**
	* The `id=` HTML attribute.
* `URL` - **COMPULSORY**
	* The Bissetii compatible relative URL. The files storage location
	  varies depending on the progress algorithm.
		* for `compress`, place it inside `assets/` directory.
		* for `include`, place it inside `static/` directory.
* `Type` - **OPTIONAL**
	* The `type=` HTML attribute.
* `LoadMode` - **COMULSORY**
	* The script loading mode. This allows one to asynchonously load
	  Javascript. It can be:
		* `""` (nothing) - load synchnonously and execute immediately
		* `"async"` - load asynchonously and execute immediately
		* `"defer"` - load asynchonously and execute after the page is loaded.
* `CrossOrigin` - **COMPULSORY**
	* The ID for cross-origin request download verification. It need a valid
	  `Integrity` value to operate properly. The value can be any of the
	  following:
		* `anonymous` - anyone can download via cross-origin as long as
		  `Integrity` is verified.
		* `use-credentials`  - use credentials to perform the request
		  alongside `Integrity`.
* `Integrity` - **COMPULSORY**
	* The algorithmic shasum value for cross-origin request file
	  verification. It need a valid `CrossOrigin` value to operate properly.
* `Content` - **OPTIONAL**
	* The inline content of the script. This only works if `URL` is
	  disabled or empty.



### Version `v1.12.0` to `v1.12.5`
{{< note "warning" "Important Notice" >}}
Support for JS inclusion version `v1.12.5` and below is scheduled to be
stopped starting **July 31, 2021**. While the codes remain available, the team
will no longer answer any questions related to the matter.

**Please DO your JS includes configurations migrations as soon as possible.**
{{< /note >}}

Bissetii relies on the 4 map table variables to configure Javascript.
The file that is responsible for configuring Javascript is located in the
following file:

```bash
pattern  :           data/bissetii/includes.toml
directory:     docs/.data/
repo-docs:     docs/.data/bissetii/includes.toml
```


#### Plain Javascript Inclusion
To include the JS, simply add the URL into `asyncFiles` or `deferFiles` under
`[includes.JS]` map table.

Example:

```toml {linenos=table,hl_lines=["1-2", "8-9"],linenostart=1}
[JS]                                      #--> vanilla HTML's Javascript assets
asyncFiles = [                            #--> just include and async-load
       # "js/myLib.js"                    #--> $assetDir/js/myLib.js
]

# ---OR---

[JS]                                      #--> vanilla HTML's Javascript assets
deferFiles = [                            #--> just include and defer-load
       # "js/myLib.js"                    #--> $assetDir/js/myLib.js
]
```


#### Asset Concatenation
To include the JS, simply add the URL into `asyncAssets` or `deferAssets` under
`[includes.JS]` map table.

Example:

```toml {linenos=table,hl_lines=["1-2", "8-9"],linenostart=1}
[JS]                                      #--> vanilla HTML's Javascript assets
asyncAssets = [                           #--> compile into 1 JS and async-load
       # "js/myLib.js"                    #--> $assetDir/js/myLib.js
]

# ---OR---

[JS]                                      #--> vanilla HTML's Javascript assets
deferAssets = [                           #--> compile into 1 JS and defer-load
       # "js/myLib.js"                    #--> $assetDir/js/myLib.js
]
```




## Wrapping Up
That is all for including external JS files with Bissetii into Hugo. If you
have any question to ask us directly, please feel free to raise it at our
[GitLab Issues Section](https://gitlab.com/zoralab/bissetii/-/issues). We will
be happy to assist you.
