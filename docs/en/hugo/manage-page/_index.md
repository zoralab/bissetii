+++
date = "2021-08-11T10:50:06+08:00"
title = "Manage Page with Bissetii in Hugo"
description = """
Managing a page with Bissetii is a little different from the Hugo's manual.
Due to many of Bissetii core features, there are a lot of key setup to be done
in order to allow a page operating smoothly.
"""
keywords = ["hugo", "manage", "page", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/how-to/getting-started/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/how-to/getting-started/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Interfaces | Hugo"
name = "Manage Page"
pre = "📝"
weight = 5


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}




## Languages
Bissetii enabled Hugo's multi-lingual feature for the sake of removing
possibility of creating broken URL (SEO hates it). Hence, the first you need to
do is to ensure the languages for your Hugo site configurations are correct.

The most important file would be the `.sites/config/_default/languages.toml` for
each supporting languages. At minimum, you need 2 langauges in order to instruct
Hugo to maintain its language code setup.

Bissetii by default setup `English` and `Simplified Chinese` as a default
configuration. The configuration file is as follows:

```toml {linenos=table,hl_lines=[],linenostart=1}
[en]
languageCode = "en"
languageName = "English"
contentDir = "../docs/en"
title = "My New Hugo Site"

[zh-hans]
languageCode = "zh-hans"
languageName = "Chinese (Simplified)"
contentDir = "../docs/zh-hans"
title = "我的Hugo网站"
```
* `[ID]` - **COMPULSORY**
	* The langauge data table identification. Can be any so long it is
	  alphanumeric, hyphen (`-`), or underscore (`_`). Space (` `) is not
	  allowed.
	* Although freedom is available, Bissetii suggests to keep this value
	  same as `languageCode` to avoid confusion.
* `languageCode` - **COMULSORY**
	* The [ISO 639-1](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes)
	  and optionally
	  [ISO 3166 Alpha 2](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2)
	  compliant internationalization and localization language codes.
	  Example:
		* `en` for English
		* `en-US` for English localized to United States
		* `en-UK` for English localized to United Kingdom
		* `en-MY` for English localized to Malaysia
		* `zh-hans` for Simplified Chinese
		* `zh-hans-CN` for Simplified Chinese localized to China
		* `zh-hans-TW` for Simplified Chinese localized to Taiwan
	* See [Google's Localization Guide](https://developers.google.com/search/docs/advanced/crawling/localized-versions) to learn more.
	* Will be used for data identification.
	* Will be used for URI path language prefixing.
* `languageName` - **COMPULSORY**
	* The name of the langauge (Label).
	* Recommend the label to be in the native language.
* `contentDir` - **COMPULSORY**
	* The directory holding the contents for the language.
* `title` - **COMPULSORY**
	* The title of the site appeared for the language.

Please arrange the order where the primary language is always at the top. For
obvious reason,

For obvious reason, Bissetii supports
**[directory-level transation](https://gohugo.io/content-management/multilingual/#translation-by-content-directory)**
from the get-go as it is very practical and easier to distribute among all
language translators while the content creators only works on the primary
language content.



### Selecting Primary Language
To select the primary language from your list of languages, simply update the
`defaultContentLanguage` field with the language `ID` (the one with `[]` braces)
in your `.sites/config/_default/config.toml` file. Example:

```toml {linenos=table,hl_lines=[2],linenostart=1}
...
defaultContentLanguage = "en"
defaultContentLanguageInSubdir = true
hasCJKLanguage = true
...
```



### Redirect Unused Second Language
Due to Hugo's requirement to maintain a minimum 2 languages in order retain the
language code in the URL construction, you can
[redirect]({{< link "/hugo/manage-redirect/" "this" "url-only" />}}) the unused
second language page to your primary language landing page. That will ensure
visitor will always arrive at the primary language's landing page.



### No Changes with `hugo new` Command
Bissetii setup the theme not to intefere with `hugo new` command in order to
keep things aligned with Hugo. You can still continue create the content
directly using the original pathing. This will always create the content inside
the primary language `content/` directory. Your list of actions would be
something as such:

```bash {linenos=table,hl_lines=[2],linenostart=1}
$ cd .sites                                 # enter the .sites directory
$ hugo new path/to/url-pathing-name.md      # create respecting the URI path
...
$ cd ..                                     # exit the .sites directory
```

{{< note info "Note" >}}
As content creators, remember to only work inside the primary language
`content/` directory. The idea is to distribute that `content/` directory to
your translator and have him/her work them out accordingly. Then, he/she will
return a fully translated `content/` directory to place it back.

It's simplier and maintainable compared to individual string or paragraph
translation.
{{< /note >}}




## Recommended Content Organization
If you notice, each content page has a lot of its associated files like a list
of thumbnail images, possible PDF file, and etc. Bissetii recommends the use
of `directory/_index.md` approach, in the same directory, the creators can store
all the associated files in it. The file structure would be:

```bash {linenos=table,hl_lines=["4-5"],linenostart=1}
docs/
   +- en/
   |   +- ...
   |   +- about-us/
   |          +- _index.md                   # main page using _index.md
   |          +- thumbnail-1200x628.png      # thumbnail.0
   |          +- thumbnail-1200x1200.png     # thumbnail.1
   |          +- thumbnail-480x480.png       # thumbnail.2
   |          +- my-portfolio.pdf            # any other associated files
   |          +- ...
   |
   |
   ...
```

This way, you can calmly, safely, and keeping distribute process simple
when working with your translators counterpart without worrying about
untangling the dependencies located elsewhere (hint: `data/` and `static/`
directories management nightmare).

Another benefit is when the page is scaling with its child pages, you do not
need to perform large scale overhaul just to keep content organization sane.




## Page's Hugo Front Matter
Once a page is created, the extra step you need to do is to fill up or edit
the Bissetii generated page's Hugo front matter template before writing your
content. Among notable changes would be:

1. `date` - Hugo compatible timestamp to indicate its creation date. Not to
   confused with modified date which is obtained from your git latest timestamp
   to the content file.
2. `title` - a sensible language specific title that will appear in page title
and your first heading (`h1`).
3. `keywords` - sort out your content's key takeaways' keywords.
4. `description` - a 150-300 characters long summary of the page. It is used
   for advertising the page via search engine display.
5. `creators` for page authorships. See [Manage Authorship]({{< link
   "/hugo/manage-authorship/" "this" "url-only" />}}) for instructions.
6. `thumbnails` for page thumbnail image management. See
   [Manage Social Network]({{< link "/hugo/manage-social-network/" "this"
   "url-only" />}}) for instructions.
7. `menu.main` for navigation management. See [Manage Menu]({{< link
   "/hugo/manage-menu/" "this" "url-only" />}}) for instructions.
8. `schema` for Schema.org data structure management. See
   [Manage Schema.org]({{< link "/hugo/manage-schema/" "this" "url-only" />}})



### Intensively Using Bissetii's Components' Shortcodes
Once all the front-matter settings are set, you're free to create the page
contents accordingly.

When using with Bissetii, please stick to using as many Bissetii's Hugo
shortcodes as Bissetii can offer via its components to standardize the content
input. The main reason is that Hugo also generates multiple output formats
(e.g. Vanilla HTML5 and AMPHTML) simultenously from your single content data
file.

Hence, the shortcode apporach is currently the most sane practice in
terms of maintenance and scalability so use them whenever available.




## Manage Development and Publications
Now that you're ready to create your web content, you can proceed to setup your
hugo server for development and later on, manage your publishers. Visit
[Manage Publishers]({{< link "/hugo/manage-publishers/" "this" "url-only" />}})
to learn more.




## Wrapping Up
That is all for managing page with Bissetii in Hugo. If you have any question to
ask us directly, please feel free to raise it at our
[GitLab Issues Section](https://gitlab.com/zoralab/bissetii/-/issues). We will
be happy to assist you.
