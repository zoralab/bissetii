+++
date = "2020-11-29T20:21:42+08:00"
title = "cards Shortcode"
description = """
`cards` shortcode is a multiple `card` renderer suitable for rendering  mulitple
cards data like a deck. Unlike `card` which is only specific to a single card
data, `cards` takes data directory path and automatically parse each card's
data.
"""
keywords = ["cards", "shortcode", "hugo", "bissetii"]
draft = false
type = ""
redirectURL="/en/components/card/"
layout = "render"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
]


[creators.ZORALab]
type = "Organization"
name = "ZORALab Team"

[creators.KeanHo]
type = "Person"
name = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "disabled"
name = "cards (BETA)"
pre = "📚"
weight = 1


[schema]
selectType = "WebPage"
+++

# `cards` Shortcode
{{% param "description" %}}

Example usage:

```markdown {linenos=table,hl_lines=[],linenostart=1}
{{</* cards "bissetii.data.cards" "grid" "embed" */>}}
{{</* cards "bissetii.data.cards" "card" */>}}
```

This shortcode is available starting from `v1.13.0`.

If you are looking to render a single particular card, see
[Card Shortcode]({{< link "/shortcodes/card" "this" "url-only" />}})
(notice the singular form).

{{< note "warning" "Beta Testing Notice" >}}
This is a fairly new shortcode and it is still under user-level API beta
testing. Hence, there may be some **major updates in the future that are not
backward compatible**. Please use at your own risk.
{{< /note >}}




## Heads-Up
While the shortcode is useful, **it breaks the Markdown syntax conventions
due to Hugo specific shortcode feature**. Therefore, when using other Markdown
renderer, these shortcodes would be rendered as text instead of HTML code
fragments.

Also, be careful and stay safe while using this shortcode. It allows any form
of HTML to be embedded into your Markdown page.




## Using `cards`
The parameters are equivalent to `card` with 1 exception:

1. It accepts directory holding card TOML data files like "a deck of cards".

The rest of the parameters will be directly fed to each
[Card Shortcode]({{< link "/shortcodes/card" "this" "url-only" />}}) under the
hood.

Since this is a HTML renderer in Markdown file, you want to call in the
shortcode using the `<` symbol instead of `%`. The brackets are as follow:

```markdown {linenos=table,hl_lines=[1,"2-3"],linenostart=1}
# This is Markdown Heading
Your normal text here.

{{</* cards "bissetii.data.cards" "grid" "embed" */>}}

## Back to Markdown
```

This will render the output as:

```html {linenos=table,hl_lines=[1,"2-3"],linenostart=1}
<h1>This is Markdown Heading</h1>
<p>Your normal text here.</p>

{{% cards "bissetii.data.cards" "grid" "embed" %}}

<h2>Back to Markdown></h2>
```




## Conclusion
That's all about `cards` shortcode in Bissetii. If you need more feature or
need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
