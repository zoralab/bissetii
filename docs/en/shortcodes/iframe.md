+++
date = "2020-08-04T12:08:55+08:00"
title = "iframe Shortcode"
description = """
`iframe` is various outputs has different HTML tags like `amp-iframe` for AMP
HTML and `iframe` for Vanilla HTML. Bissetii created this shortcode to generate
the necessary iframe HTML codes accordingly and simultenously.
"""
keywords = ["iframe", "shortcode", "hugo", "bissetii"]
draft = false
type = ""
redirectURL="/en/components/iframe"
layout = "redirect"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "sidebar",
]


[creators.ZORALab]
type = "Organization"
name = "ZORALab Team"

[creators.KeanHo]
type = "Person"
name = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "redirect"
name = "iframe (BETA)"
pre = "🔮"
weight = 1


[schema]
selectType = "WebPage"
+++

# `iframe` Shortcode
{{% param "description" %}}

Example usage:

```markdown {linenos=table,hl_lines=[1],linenostart=1}
{{</* iframe "default" "bissetii.data.iframes.example.myExample" */>}}
```

This shortcode is available starting from `v1.12.1`.

{{< note "warning" "Beta Testing Notice" >}}
This is a fairly new shortcode and it is still under user-level API beta
testing. Hence, there may be some **major updates in the future that are not
backward compatible**. Please use at your own risk.
{{< /note >}}



## Heads-Up
While the shortcode is useful, **it breaks the Markdown syntax conventions
due to Hugo specific shortcode feature**. Therefore, when using other Markdown
renderer, these shortcodes would be rendered as text instead of HTML code
fragments.

Also, be careful and stay safe while using this shortcode. It allows any form
of HTML to be embedded into your Markdown page.




## Using `iframe`
There are a number of requirements for using `iframe` shortcode. You should
fulfill them from the top to the bottom in sequence.



### Create Dataset in Data File
Due to the highly robust, unique, and use-case specific security requirements
for each `iframe`, it is at best to use dataset approach via the Hugo `data/`
directory. `iframe` shortcode recognizes the following `TOML` data file pattern:

```toml {linenos=table,hl_lines=["1-3","7-12", "16-17"],linenostart=1}
[myExample]
URL = "/"
Sandbox = [
	"allow-scripts",
	"allow-same-origin",
]
ID = "my-iframe-id-01"
Width = "300"
Height = "300"
Class = "custom-iframe"
VanillaHTML = "bissetii-only/iframe/myExample/content.html"
AMPHTML = """
### Markdown Header
This is a markdown message.
"""
Layout = "repsonsive"
Title = "Example iframe Data File"
```

* **Line 1** - defines the dataset title.
* **Line 2** - specifies the `iFrame` URL.
* **Line 3** - specifies the `iFrame` `sandbox=` permissions property. Default
               is empty value. This field is an array of strings holding all
               compatible sandbox security policy.
* **Line 7** - specifies the `iFrame` ID HTML attribute.
* **Line 8** - defines the `width` parameter. This goes by pixel or etc.
               defining on how wide you need it to be. Dynamic values like
               percentage won’t work here since the component is dynamically
               styled. This is **compulsory** for AMP HTML.
* **Line 9** - defines the `height` parameter. This goes by pixel or etc.
               defining on how high you need it to be. Dynamic values like
               percentage won’t work here since the component is dynamically
               styled. This is **compulsory** for AMP HTML.
* **Line 10** - the values for `class=` property.
* **Line 11** - defines the iframe content for Vanilla HTML. It first scans for
                available partial for scanning. If the file is unavailable,
                it will then process the content as Markdown format. Should any
                HTML error occurs, the content will finally be processed as HTML
                format.
* **Line 12** - defines the iframe content partials path for AMP HTML. Its
                choices of processing is the same as `VanillaHTML`.
* **Line 16** - defines iFrame `layout=` property. This is only used in AMP HTML
                where the value is always `responsive`.
* **Line 17** - defines iFrame `title=` property. This is for accessibility
                usage.


### Import AMP-iFrame Extension
For Accelerated Mobile Pages (AMP) HTML output, before proceeding to call the
shortcode, you need to import the `amp-iframe` in order to render `<amp-iframe>`
for AMP output. Example:

```toml {linenos=table,hl_lines=[6],linenostart=1}
...

[amp]
modules = [
	...,
	"amp-iframe",
]


...
```


### Calling Shortcode
```
{{</* iframe <type> <datasetPath> */>}}
```

`iframe` shortcode takes 2 parameters:

1. `<type>` - the type of iframe.
2. `<dataPath>` - the data path for the dataset in the data path.


#### `type` Parameter
The first `type` parameter is a reserved parameter mainly to facilitate various
kinds of iFrame styling in the future. To see all types of available iFrame
styling, please visit:
[iFrame Component]({{< link "/components/iframe/"  "this" "url-only" />}}).


#### `dataPath` Parameter
The `dataPath` paramter is a `Pathing.Name` combination to select a dataset
from a given data file inside `data/` directory. It goes by the format of
`relative.path.to.file.listName`. For example:

```
Dataset    :                            .mySite
Stored File:  data/subdirectory/myIFrame.toml
dataPath   :       subdirectory.myIFrame.mySite
```

The `dataPath` should be: `subdirectory.myIFrame.mySite`.



### Example Usage
Since this is a HTML renderer in Markdown file, you want to call in the
shortcode using the `<` symbol instead of `%`. The brackets are as follow:

```markdown {linenos=table,hl_lines=[4],linenostart=1}
# This is Markdown Heading
Your normal text here.

{{</* iframe "default" "bissetii.data.iframes.example.myExample" */>}}

## Back to Markdown
```

This will render the output as:

```html {linenos=table,hl_lines=["4-10"],linenostart=1}
<h1>This is Markdown Heading</h1>
<p>Your normal text here.</p>

{{% iframe "default" "bissetii.data.iframes.example.myExample" %}}

<h2>Back to Markdown></h2>
```




## Conclusion
That's all about `iframe` shortcode in Bissetii. If you need more feature or
need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
