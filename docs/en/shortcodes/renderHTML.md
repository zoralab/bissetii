+++
date = "2019-12-20T08:56:25+08:00"
title = "renderHTML Shortcode"
description = """
As we move forward with Hugo, the clear direction is to maintain separation
between Markdown and HTML. Hence, a lot of mixed HTML in Markdown rendering
no longer working properly. This is where "renderHTML" shortcode fills the gap.
"""
keywords = ["renderHTML", "shortcode", "hugo", "bissetii"]
draft = false
type = ""
redirectURL="/en/components/hugo/#renderhtml-shortcode"
layout = "redirect"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[amp]
modules = [
        # Example: "amp-sidebar",
]


[creators.ZORALab]
type = "Organization"
name = "ZORALab Team"

[creators.KeanHo]
type = "Person"
name = "Holloway Chew Kean Ho"


[thumbnail]
url = "/img/thumbnails/default.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "disabled"
name = "renderHTML"
pre = "📝"
weight = 1


[schema]
selectType = "WebPage"
+++

# `renderHTML` Shortcode
As we move forward with Hugo, the clear direction is to maintain separation
between Markdown and HTML. Hence, a lot of mixed HTML in Markdown rendering
no longer working properly. This is where `renderHTML` shortcode fills in the
gap.

This

Example usage:

```markdown
{{</* renderHTML html */>}}
...
{{</* /renderHTML */>}}
```

This shortcode is available starting from `v1.9.0`,



## Heads-Up
While the shortcode is useful, **it breaks the Markdown syntax conventions
due to Hugo specific shortcode feature**. Therefore, when using other
Markdown renderer, these shortcodes would be rendered as text instead of
HTML code fragments.

Also, be careful and stay safe while using this shortcode. It allows any form
of HTML to be embedded into your Markdown page.



## Using `renderHTML`
The shortcode takes in fixed formatting values depending on your targeted
outputs:

1. `"html"` - for Vanilla HTML output (available starting `v1.9.0`).
2. `"amp"`  - for AMP HTML output (available starting `v1.12.0`).

Since this is a HTML renderer in Markdown file, you want to call in the
shortcode using the `<` symbol instead of `%`.

An example to render both `html` and `amphtml` would be:

```markdown {hl_lines=[4, 8]}
# This is Markdown Heading
Your normal text here.

{{</* renderHTML "html" "amp" */>}}
<h2>Your HTML Code Here </h2>

<p>This allows you to apply HTML as a snippet renderer in Markdown.</p>
{{</* /renderHTML */>}}

## Back to Markdown
```

This will render the output as:

```html
<h1>This is Markdown Heading</h1>
<p>Your normal text here.</p>

{{% renderHTML "html" "amp" %}}
<h2>Your HTML Code Here </h2>

<p>This allows you to apply HTML as a snippet renderer in Markdown.</p>
{{% /renderHTML %}}

<h2>Back to Markdown></h2>
```




## Conclusion
That's all about `renderHTML` shortcode in Bissetii. If you need more feature or
need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
