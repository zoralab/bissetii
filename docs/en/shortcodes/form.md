+++
date = "2020-07-08T21:56:07+08:00"
title = "form Shortcode"
description = """
`form` shortcode is a tool to generate the `form` component correctly across
multiple outputs. This shortcode is meant to organize form cleaning based on
the selected output format.
"""
keywords = ["form", "shortcode", "hugo", "bissetii"]
draft = false
type = ""
redirectURL="/en/components/form/"
layout = "redirect"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
	"form",
]


[creators.ZORALab]
type = "Organization"
name = "ZORALab Team"

[creators.KeanHo]
type = "Person"
name = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "redirect"
name = "form"
pre = "⌨️"
weight = 1


[schema]
selectType = "WebPage"
+++

# `form` Shortcode
{{% param "description" %}}

Example usage:

```markdown
{{</* form "" "post" "shortcodes/form" */>}}
	...
<fieldset class="border center">
	<input type="reset" value="Reset" accesskey="s" />
	<input class="pinpoint" type="submit" value="Apply" />
</fieldset>
	...
{{</* /form */>}}
```

This shortcode is available starting from `v0.12.0`.




## Heads-Up
While the shortcode is useful, **it breaks the Markdown syntax conventions
due to Hugo specific shortcode feature**. Therefore, when using other Markdown
renderer, these shortcodes would be rendered as text instead of HTML code
fragments.

Also, be careful and stay safe while using this shortcode. It allows any form
of HTML to be embedded into your Markdown page.




## Pre-Requisite
Depending on your Bissetii version, you need to include extended modules.

Some output format (e.g. AMP) requires additional Javascript module to load
before using the HTML form.



### Version `v1.13.0` and Above
You need to include `form` into `modules.extensions` inside your Hugo page's
front-matter. Here is an example:

```toml {linenos=table,hl_lines=[6],linenostart=1}
...

[modules]
extensions = [
	# Example: "sidebar",
	"form",
]

...
```



### Version `v1.12.5` and Below
{{< note "warning" "Important Notice" >}}
Support for `form` shortcode version `v1.12.0` to `v1.12.5` is scheduled to be
stopped starting **June 30, 2021**. While the codes remain available, the team
will no longer answer any questions related to the matter.

**Please DO your migrations as soon as possible.**
{{< /note >}}
You need to include `amp-form` into `amp.modules` inside your Hugo page's
front-matter. Here is an example:

```toml {linenos=table,hl_lines=[6],linenostart=1}
...

[amp]
modules = [
	# Example: "amp-sidebar",
	"amp-form",
]

...
```




## Using `form`
Since Bissetii supports AMP form rendering, there is a need to use shortcode
to organize the form properly.

Here is the shortcode pattern:

```md {linenos=table,hl_lines=[],linenostart=1}
{{</* form "<outputType>" "<method>" "<action>" "<ID>" "<Class>" "<Label>" */>}}
	<content>
{{</* /form */>}}
```

We will walk through each of the parameters in detail.



### `<content>` Parameter
This field holds the content for the form like the input and label fields. The
write up should be in HTML format.



### `<outputType>` Parameter
{{< note "note" "Important Note" >}}
Starting from version `v1.13.0`, **this field is no longer used**. For backward
compatibility, you should **supply empty string**.
{{< /note >}}

This parameter is to selectively render the form based on a list of outputs
separated by bar (`|`). Example:

1. for vanilla html only, it is: `"html"`.
2. for amp html only, it is: `"amp"`.
3. for both vanilla and amp html, it is `"html|amp"`.

This is only use for Bissetii version `v1.12.0` to `v1.12.5`. For higher
version, form will render on all output formats for content consistency. Hence,
you should always **supply empty string**.



### `<method>` Parameter
This field defines the form method attribute. To comply with
[HTML5](https://html.spec.whatwg.org/multipage/form-control-infrastructure.html#attr-fs-method)
and [XHTML](https://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd) standards,
this field will be **converted to lowercase automatically**. Example, when given
`GET`, it will become `get`.



### `<action>` Parameter
This field defines the form action attribute (usually a link). Bissetii's
relative URL is compatible with this field. However, since Hugo is a static
site generator, it is **recommended to provide an absoluate URL to your backend
server API instead**.



### `<ID>` Parameter
This field is optional and will be filled into the form's `id=` attribute.

If you are using the following parameters but this parameter, you
**must supply this field with an empty string**.

{{< note "info" "FOR YOUR INFORMATION" >}}
This feature is only available starting from `v1.13.0`.
{{< /note >}}

Example:

```md {linenos=table,hl_lines=[],linenostart=1}
{{</* form "" "post" "shortcodes/form" "my-form-id" */>}}
{{</* form "" "post" "shortcodes/form" */>}}
```



### `<Class>` Parameter
This field is optional and will be filled into the form's `class=` attribute.

If you are using the following parameters but this parameter, you
**must supply this field with an empty string**.

{{< note "info" "FOR YOUR INFORMATION" >}}
This feature is only available starting from `v1.13.0`.
{{< /note >}}

Example:

```md {linenos=table,hl_lines=[],linenostart=1}
{{</* form "" "post" "shortcodes/form" "my-form-id" "class-form" */>}}
{{</* form "" "post" "shortcodes/form" "" "class-form" */>}}
{{</* form "" "post" "shortcodes/form" */>}}
```



### `<Label>` Parameter
This field is optional and will be filled into the form's `aria-label=`
attribute.

If you are using the following parameters but this parameter, you
**must supply this field with an empty string**.

{{< note "info" "FOR YOUR INFORMATION" >}}
This feature is only available starting from `v1.13.0`.
{{< /note >}}

Example:

```md {linenos=table,hl_lines=[],linenostart=1}
{{</* form "" "post" "shortcodes/form" "my-form-id" "class-form" "My Form" */>}}
{{</* form "" "post" "shortcodes/form" "my-form-id" "" "My Form" */>}}
{{</* form "" "post" "shortcodes/form" "" "class-form" "My Form" */>}}
{{</* form "" "post" "shortcodes/form" "" "" "My Form" */>}}
{{</* form "" "post" "shortcodes/form" */>}}
```




## Example Usage
Here is an example for how to use `form` shortcode. Given that:

```md {linenos=table,hl_lines=[],linenostart=1}
{{</* form "" "post" "shortcodes/form" "my-form" "class-form" "My Form" */>}}
	<fieldset class="border center">
		<input type="reset" value="Reset" accesskey="s" />
		<input class="pinpoint" type="submit" value="Apply" />
	</fieldset>
{{</* /form */>}}
```

The above example is rendered into the following output depending on whether
you're viewing in AMP or vanilla HTML):

```html {linenos=table,hl_lines=[],linenostart=1}
{{% form "" "post" "shortcodes/form" "my-form" "class-form" "My Form" %}}
	<fieldset class="border center">
		<input type="reset" value="Reset" accesskey="s" />
		<input class="pinpoint" type="submit" value="Apply" />
	</fieldset>
{{% /form %}}
```




## Conclusion
That's all about `form` shortcode in Bissetii. If you need more feature or
need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
