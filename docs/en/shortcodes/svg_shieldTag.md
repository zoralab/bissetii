+++
date = "2020-01-06T20:43:49+08:00"
title = "svg/shieldTag Shortcode"
description = """
Shields.io badges is one of the famously used component to display many of its
cool content. Rather than depending on external server to render the badge,
Bissetii's svg/sheildTag shortcode lets you create an in-house Shields.io badge
easily and hosted locally.
"""
keywords = ["svg/shieldTag", "shortcode", "hugo", "bissetii"]
draft = false
type = ""
redirectURL="/en/components/shields-badge"
layout = "redirect"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[amp]
modules = [
        # Example: "amp-sidebar",
]


[creators.ZORALab]
type = "Organization"
name = "ZORALab Team"

[creators.KeanHo]
type = "Person"
name = "Holloway Chew Kean Ho"


[thumbnail]
url = "/img/thumbnails/default.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "redirect"
name = "svg/shieldTag"
pre = "🔰"
weight = 1


[schema]
selectType = "WebPage"
+++

# `svg/shieldTag` Shortcode
[Shields.io](https://shields.io/) badges is one of the famously used component
to display many of its cool content. Rather than depending on external server to
render the badge, Bissetii's svg/sheildTag shortcode lets you create an in-house
Shields.io badge easily and hosted locally.

Example usage:

```md
{{</* svg/shieldTag "Core Language" "Hugo" "#004d40" "#ffffff" */>}}
```

This shortcode is available starting from `v1.11.0`.




## Heads-Up
While the shortcode is useful, **it breaks the Markdown syntax conventions
due to Hugo specific shortcode feature**. Therefore, when using other Markdown
renderer, these shortcodes would be rendered as text instead of HTML code
fragments.

Also, be careful and stay safe while using this shortcode. It allows any form
of HTML to be embedded into your Markdown page.




## Using `svg/shieldTag`
The shortcode takes a number of inputs with the same order from Shields.io:

1. `badge label` - the title
2. `badge value` - the value
3. `background color` - the HTML color hex-code for background. Use dark color.
4. `text color` - [OPTIONAL] the HTML color hex-code for text. Default is white.
5. `URL` - **[DEPRECATED]** the optional wrapping URL.

{{< note "warning" "Deprecation Notice" >}}
Starting from version `v1.13.0`, the 5th parameter is no longer used for URL
and the support is scheduled to be removed on **June 30, 2020**.

If you need URL wrapping, please use dedicated
[Link Shortcode]({{< link "/shortcodes/link/" "this" "url-only" />}}) instead.
{{< /note >}}

Since this is a HTML renderer in Markdown file, you want to call in the
shortcode using the `<` symbol instead of `%`. The brackets are as follow:

```markdown
{{</* svg/shieldTag "my Title Here"
	"value" "#004d40"
	"#ffffff"
	"https://shields.io" */>}}
```

This will render the tag as:

{{< svg/shieldTag "my Title Here"
	"value"
	"#004d40"
	"#ffffff"
	"https://shields.io" >}}
