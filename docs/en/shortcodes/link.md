+++
date = "2020-06-27T15:55:15+08:00"
title = "link Shortcode"
description = """
Bissetii now has a proper shortcode to manage multi-language, relative BaseURL
link instead of the conventional Markdown. This way, it's easier to remember
one shortcode than multiple ones over the place and easily manage the link
across multiple outputs.

"""
keywords = ["link", "shortcode", "hugo", "bissetii"]
draft = false
type = ""
redirectURL="/en/components/anchor"
layout = "redirect"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[amp]
modules = [
        # Example: "amp-sidebar",
]


[creators.ZORALab]
type = "Organization"
name = "ZORALab Team"

[creators.KeanHo]
type = "Person"
name = "Holloway Chew Kean Ho"


[thumbnail]
url = "/img/thumbnails/default.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "redirect"
name = "link"
pre = "🔗"
weight = 1


[schema]
selectType = "WebPage"
+++

# `link` Shortcode
{{% param "description" %}}

By default, Hugo is not able to process irregular `BaseURL` such as
`https://zoralab.gitlab.io/bissetii`. In fact, it only recognizes
`https://zoralab.gitlab.io` its base URL. This is not feasible for websites
hosted on Git version control platforms
such as [Github Pages](https://pages.github.com/) and
[GitLab Pages](https://about.gitlab.com/stages-devops-lifecycle/pages/) as
project-level pages. Therefore, the need for an independent `link` shortcode is
required.

Also, when using `link` shortcode in multiple outputs with non-base relative
URL (e.g. `some/path`), it will process and point the link towards its
designated output (e.g. AMP will have `index.amp.html` in the end if no file is
specified).

This shortcode is available starting from `v1.12.0`.

{{< note "warning" "NON-BACKWARD COMPATIBILITY NOTICE" >}}
Starting from version `v1.13.0`, this shortcode uses many Hugo's
[urls.Parse function](https://gohugo.io/functions/urls.parse/), which in turns
using [Go's `net/url` Package](https://godoc.org/net/url#URL) to process all the
URLs. The reason is to ensure this shortcode is
[RFC3986](https://tools.ietf.org/html/rfc3986#section-4.1) compliant.

While the parameters are not affected, **the data for them are**. Please ensure
your full documentations migrations before commiting!

Another notable security changes is that
**one will no longer be able to supply user info embedded URL** such as this
example: `https://username:password@www.example.com`. Doing so will raise as
fatal error instead.

The reason to ban embedded URL in this shortcode mainly because Hugo is a static
site generator, not a web application. Hence, there is no reason for anyone to
hard-code sensitive information like password into your web artifact.
{{< /note >}}




## Heads-Up
While the shortcode is useful, **it breaks the Markdown syntax conventions
due to Hugo specific shortcode feature**. Therefore, when using other Markdown
renderer, these shortcodes would be rendered as text instead of HTML code
fragments.

Be careful and stay safe while using this shortcode. It allows any form of HTML
to be embedded into your Markdown page.




## Using `link`
There are serveral ways to use `link` shortcode.


### URL-Only Construction
`link` is capable of constructing the canonical, multi-lingual URL-only output.
This is useful when you just want to use the URL for inserting into linking
area.

{{< note "warning" "Important Note" >}}
Starting from version `v1.13.0`, you need to apply self-closure for every
stand-alone shortcode calls. Here is an example:

```md {linenos=table,hl_lines=[],linenostart=1}
{{%/* link "mylink" "this" "url-only" */%}}
		⥥
{{%/* link "mylink" "this" "url-only" /*/%}}
```

Notice the tailing `/` slash at the end.
{{< /note >}}


To use URL-only construction, you can do the following pattern:

```md {linenos=table,hl_lines=[],linenostart=1}
{{</* link <URL> <LANG> "url-only" /*/>}}
```

1. The 1st parameter (`<URL>`) is the URL you want to construct. It accepts both
   absolute and relative URL.
2. The 2nd parameter is the language prefix selection (`<LANG>`). This is only
   used for relative URL conversion. To ensure dynamic processing, you can
   supply `"this"` as value to use current URL's language prefix.
3. The 3rd parameter **MUST** be hard-coded into `"url-only"` as value.

Let's look at the following examples:

```md {linenos=table,hl_lines=[],linenostart=1}
# Supplying Absolute Value
{{</* link "https://www.example.com" "" "url-only" /*/>}}
		⥥
{{< link "https://www.example.com" "" "url-only" />}}

# Supplying Absolute Value with language prefix
{{</* link "https://www.example.com" "this" "url-only" /*/>}}
		⥥
{{< link "https://www.example.com" "this" "url-only" />}}

# Relative to Base URL (e.g. to access static files)
{{</* link "/img/logo.png" "" "url-only" /*/>}}
		⥥
{{< link "/img/logo.png" "" "url-only" />}}

# Relative to Base URL with language Prefix as "zh-hans"
{{</* link "/img/logo.png" "zh-hans" "url-only" /*/>}}
		⥥
{{< link "/img/logo.png" "zh-hans" "url-only" />}}

# Path/Directory relative to Base URL
{{</* link "/img/" "" "url-only" /*/>}}
		⥥
{{< link "/img/" "" "url-only" />}}

# Path/Directory (no tail) relative to Base URL
{{</* link "/img" "" "url-only" /*/>}}
		⥥
{{< link "/img" "" "url-only" />}}

# Path/Directory relative to Base URL with "this" language prefix
{{</* link "/img/" "this" "url-only" /*/>}}
		⥥
{{< link "/img/" "this" "url-only" />}}

# Path/Directory (no tail) relative to Base URL with "this" language prefix
{{</* link "/img" "this" "url-only" /*/>}}
		⥥
{{< link "/img" "this" "url-only" />}}

# Path/Directory relative to Base URL with "zh-hans" language prefix
{{</* link "/img/" "zh-hans" "url-only" /*/>}}
		⥥
{{< link "/img/" "zh-hans" "url-only" />}}

# Path/Directory (no tail) relative to Base URL with  "zh-hans" language prefix
{{</* link "/img" "zh-hans" "url-only" /*/>}}
		⥥
{{< link "/img" "zh-hans" "url-only" />}}

# Relative to current URL (without leading "/" slash)
{{</* link "img/logo.png" "" "url-only" /*/>}}
		⥥
{{< link "img/logo.png" "" "url-only" />}}

# Relative to current URL with language prefix overwrite to "zh-hans"
{{</* link "img/logo.png" "zh-hans" "url-only" /*/>}}
		⥥
{{< link "img/logo.png" "zh-hans" "url-only" />}}

# Relative to current URL using the current language prefix `this`
{{</* link "img/logo.png" "this" "url-only" /*/>}}
		⥥
{{< link "img/logo.png" "this" "url-only" />}}

# Path/Directory relative to current URL
{{</* link "img/" "" "url-only" /*/>}}
		⥥
{{< link "img/" "" "url-only" />}}

# Path/Directory (no tail) relative to current URL
{{</* link "img" "" "url-only" /*/>}}
		⥥
{{< link "img" "" "url-only" />}}

# Path/Directory relative to current URL with "this" language prefix
{{</* link "img/" "this" "url-only" /*/>}}
		⥥
{{< link "img/" "this" "url-only" />}}

# Path/Directory (no tail) relative to current URL with "this" language prefix
{{</* link "img" "this" "url-only" /*/>}}
		⥥
{{< link "img" "this" "url-only" />}}

# Path/Directory relative to current URL with "zh-hans" language prefix
{{</* link "img/" "zh-hans" "url-only" /*/>}}
		⥥
{{< link "img/" "zh-hans" "url-only" />}}

# Path/Directory (no tail) relative to current URL with "zh-hans" language prefix
{{</* link "img" "zh-hans" "url-only" /*/>}}
		⥥
{{< link "img" "zh-hans" "url-only" />}}
```

{{< note "info" "TIP" >}}
You can also reuse the Markdown link construction format. Example:
```md {linenos=table,hl_lines=[],linenostart=1}
[My Link]({{</* link "/img/logo.png" "" url-only /*/>}})
```
Produces the following markdown under-the-hood:
```md {linenos=table,hl_lines=[],linenostart=1}
[My Link]({{% link "/img/logo.png" "" url-only /%}})
```
{{< /note >}}




### Closing Tag
{{< note "warning" "Important Note" >}}
Starting from version `v1.13.0`, this function is removed in favor of the
inner content ocnstruction (see *Inner Content* sub-section below) and to stay
in compliance with RFC3986 implementation.

Supplying `"close"` as first parameter will yield an error.
{{< /note >}}

For version `v1.12.5` and below, close tag is done by supplying the first
parameter as hard-coded `"close"` value as such:

```md
{{</* link "close" */>}}
	 ⥥
	</a>
```




### HTML Link Tag
`link` is also primarily used for generating a full-fletch HTML `<a>` tag. This
is the default behavior for generating the HTML `<a>` tag with any supported
attributes.

We will walk through each parameter for creating the HTML link tag.


#### Link URL
The 1st parameter is the URL itself. Likewise, `link` accepts both absolute and
relative URL with Bissetii's own `BaseURL` processing.

**This is a compulsory parameter**. Here are the usage examples:

```md {linenos=table,hl_lines=[],linenostart=1}
# Absolute link
{{%/* link "https://www.example.com" /*/%}}
		⥥
{{% link "https://www.example.com" /%}}

# File relative to base URL
{{%/* link "/img/logo.png" /*/%}}
		⥥
{{% link "/img/logo.png" /%}}

# Path/directory relative to base URL
{{%/* link "/img" /*/%}}
		⥥
{{% link "/img" /%}}

# File Relative link to current URL
{{%/* link "img/logo.png" /*/%}}
		⥥
{{% link "img/logo.png" /%}}

# Path/directory relative to current URL
{{%/* link "img" /*/%}}
		⥥
{{% link "img" /%}}
```




#### Language Prefix
The 2nd parameter is the specific language prefix. This is used for Bissetii
internal URL processing for relative URLs. When a language is recongized and
there is no given overwrite directive (see
*Overwrite Language Hinting Link Parameter* below), this shortcode will
automatically fills in `hreflang=` attribute.

For supporting dynamic processing, this parameter accepts `"this"` as a value
where it uses the current page's language prefix instead. If the relative URL
is relative to base URL while leaving this field as empty string `""` will cause
the URL relative to BaseURL.

**This is a compulsory parameter**. Here are the usage examples:

```md {linenos=table,hl_lines=[],linenostart=1}
# Supplying Absolute Value
{{%/* link "https://www.example.com" "" /*/%}}
		⥥
{{% link "https://www.example.com" "" /%}}

# Supplying Absolute Value with 'this' language prefix
{{%/* link "https://www.example.com" "this" /*/%}}
		⥥
{{% link "https://www.example.com" "this" /%}}

# Supplying Absolute Value with 'zh-hans' language prefix
{{%/* link "https://www.example.com" "zh-hans" /*/%}}
		⥥
{{% link "https://www.example.com" "zh-hans" /%}}

# Relative to Base URL (e.g. to access static files)
{{%/* link "/img/logo.png" "" /*/%}}
		⥥
{{% link "/img/logo.png" "" /%}}

# Relative to Base URL with language Prefix as "zh-hans"
{{%/* link "/img/logo.png" "zh-hans" /*/%}}
		⥥
{{% link "/img/logo.png" "zh-hans" /%}}

# Path/Directory relative to Base URL
{{%/* link "/img/" "" /*/%}}
		⥥
{{% link "/img/" "" /%}}

# Path/Directory (no tail) relative to Base URL
{{%/* link "/img" "" /*/%}}
		⥥
{{% link "/img" "" /%}}

# Path/Directory relative to Base URL with "this" language prefix
{{%/* link "/img/" "this" /*/%}}
		⥥
{{% link "/img/" "this" /%}}

# Path/Directory (no tail) relative to Base URL with "this" language prefix
{{%/* link "/img" "this" /*/%}}
		⥥
{{% link "/img" "this" /%}}

# Path/Directory relative to Base URL with "zh-hans" language prefix
{{%/* link "/img/" "zh-hans" /*/%}}
		⥥
{{% link "/img/" "zh-hans" /%}}

# Path/Directory (no tail) relative to Base URL with  "zh-hans" language prefix
{{%/* link "/img" "zh-hans" /*/%}}
		⥥
{{% link "/img" "zh-hans" /%}}

# Relative to current URL (without leading "/" slash)
{{%/* link "img/logo.png" "" /*/%}}
		⥥
{{% link "img/logo.png" "" /%}}

# Relative to current URL using the current language prefix `this`
{{%/* link "img/logo.png" "this" /*/%}}
		⥥
{{% link "img/logo.png" "this" /%}}

# Relative to current URL with language prefix overwrite to "zh-hans"
{{%/* link "img/logo.png" "zh-hans" /*/%}}
		⥥
{{% link "img/logo.png" "zh-hans" /%}}

# Path/Directory relative to current URL
{{%/* link "img/" "" /*/%}}
		⥥
{{% link "img/" "" /%}}

# Path/Directory (no tail) relative to current URL
{{%/* link "img" "" /*/%}}
		⥥
{{% link "img" "" /%}}

# Path/Directory relative to current URL with "this" language prefix
{{%/* link "img/" "this" /*/%}}
		⥥
{{% link "img/" "this" /%}}

# Path/Directory (no tail) relative to current URL with "this" language prefix
{{%/* link "img" "this" /*/%}}
		⥥
{{% link "img" "this" /%}}

# Path/Directory relative to current URL with "zh-hans" language prefix
{{%/* link "img/" "zh-hans" /*/%}}
		⥥
{{% link "img/" "zh-hans" /%}}

# Path/Directory (no tail) relative to current URL with "zh-hans" language prefix
{{%/* link "img" "zh-hans" /*/%}}
		⥥
{{% link "img" "zh-hans" /%}}
```


#### Relationship
The 3rd parameter is the link relationship. This is useful for specifying
link relationship between the current page and the linked page. One can use it
to describe links for web crawlers such as `nofollow`.

This parameter **is optional and should be set blank (`""`)** if the following
parameters are used.

Here are the usage examples:

```md {linenos=table,hl_lines=[],linenostart=1}
# Supplying Absolute Value
{{%/* link "https://www.example.com" "" "nofollow" /*/%}}
		⥥
{{% link "https://www.example.com" "" "nofollow" /%}}

# Supplying Absolute Value with 'this' language prefix
{{%/* link "https://www.example.com" "this" "nofollow" /*/%}}
		⥥
{{% link "https://www.example.com" "this" "nofollow" /%}}

# Supplying Absolute Value with 'zh-hans' language prefix
{{%/* link "https://www.example.com" "zh-hans" "nofollow" /*/%}}
		⥥
{{% link "https://www.example.com" "zh-hans" "nofollow" /%}}

# Relative to Base URL (e.g. to access static files)
{{%/* link "/img/logo.png" "" "nofollow" /*/%}}
		⥥
{{% link "/img/logo.png" "" "nofollow" /%}}

# Relative to Base URL with language Prefix as "zh-hans"
{{%/* link "/img/logo.png" "zh-hans" "nofollow" /*/%}}
		⥥
{{% link "/img/logo.png" "zh-hans" "nofollow" /%}}

# Path/Directory relative to Base URL
{{%/* link "/img/" "" "nofollow" /*/%}}
		⥥
{{% link "/img/" "" "nofollow" /%}}

# Path/Directory (no tail) relative to Base URL
{{%/* link "/img" "" "nofollow" /*/%}}
		⥥
{{% link "/img" "" "nofollow" /%}}

# Path/Directory relative to Base URL with "this" language prefix
{{%/* link "/img/" "this" "nofollow" /*/%}}
		⥥
{{% link "/img/" "this" "nofollow" /%}}

# Path/Directory (no tail) relative to Base URL with "this" language prefix
{{%/* link "/img" "this" "nofollow" /*/%}}
		⥥
{{% link "/img" "this" "nofollow" /%}}

# Path/Directory relative to Base URL with "zh-hans" language prefix
{{%/* link "/img/" "zh-hans" "nofollow" /*/%}}
		⥥
{{% link "/img/" "zh-hans" "nofollow" /%}}

# Path/Directory (no tail) relative to Base URL with  "zh-hans" language prefix
{{%/* link "/img" "zh-hans" "nofollow" /*/%}}
		⥥
{{% link "/img" "zh-hans" "nofollow" /%}}

# Relative to current URL (without leading "/" slash)
{{%/* link "img/logo.png" "" "nofollow" /*/%}}
		⥥
{{% link "img/logo.png" "" "nofollow" /%}}

# Relative to current URL using the current language prefix `this`
{{%/* link "img/logo.png" "this" "nofollow" /*/%}}
		⥥
{{% link "img/logo.png" "this" "nofollow" /%}}

# Relative to current URL with language prefix overwrite to "zh-hans"
{{%/* link "img/logo.png" "zh-hans" "nofollow" /*/%}}
		⥥
{{% link "img/logo.png" "zh-hans" "nofollow" /%}}

# Path/Directory relative to current URL
{{%/* link "img/" "" "nofollow" /*/%}}
		⥥
{{% link "img/" "" "nofollow" /%}}

# Path/Directory (no tail) relative to current URL
{{%/* link "img" "" "nofollow" /*/%}}
		⥥
{{% link "img" "" "nofollow" /%}}

# Path/Directory relative to current URL with "this" language prefix
{{%/* link "img/" "this" "nofollow" /*/%}}
		⥥
{{% link "img/" "this" "nofollow" /%}}

# Path/Directory (no tail) relative to current URL with "this" language prefix
{{%/* link "img" "this" "nofollow" /*/%}}
		⥥
{{% link "img" "this" "nofollow" /%}}

# Path/Directory relative to current URL with "zh-hans" language prefix
{{%/* link "img/" "zh-hans" "nofollow" /*/%}}
		⥥
{{% link "img/" "zh-hans" "nofollow" /%}}

# Path/Directory (no tail) relative to current URL with "zh-hans" language prefix
{{%/* link "img" "zh-hans" "nofollow" /*/%}}
		⥥
{{% link "img" "zh-hans" "nofollow" /%}}
```


#### Target Action
The 4th parameter is the link target action. This is useful for specifying
the link action upon clicked like opening the linked page in a new tab or
windows.

This parameter is optional and **should be set blank (`""`)** if the following
parameters are used.

Here are the usage examples:

```md {linenos=table,hl_lines=[],linenostart=1}
# Supplying Absolute Value
{{%/* link "https://www.example.com" "" "" "_blank" /*/%}}
		⥥
{{% link "https://www.example.com" "" "" "_blank" /%}}

# Supplying Absolute Value with 'this' language prefix
{{%/* link "https://www.example.com" "this" "" "_blank" /*/%}}
		⥥
{{% link "https://www.example.com" "this" "" "_blank" /%}}

# Supplying Absolute Value with 'zh-hans' language prefix
{{%/* link "https://www.example.com" "zh-hans" "" "_blank" /*/%}}
		⥥
{{% link "https://www.example.com" "zh-hans" "" "_blank" /%}}

# Relative to Base URL (e.g. to access static files)
{{%/* link "/img/logo.png" "" "" "_blank" /*/%}}
		⥥
{{% link "/img/logo.png" "" "" "_blank" /%}}

# Relative to Base URL with language Prefix as "zh-hans"
{{%/* link "/img/logo.png" "zh-hans" "" "_blank" /*/%}}
		⥥
{{% link "/img/logo.png" "zh-hans" "" "_blank" /%}}

# Path/Directory relative to Base URL
{{%/* link "/img/" "" "" "_blank" /*/%}}
		⥥
{{% link "/img/" "" "" "_blank" /%}}

# Path/Directory (no tail) relative to Base URL
{{%/* link "/img" "" "" "_blank" /*/%}}
		⥥
{{% link "/img" "" "" "_blank" /%}}

# Path/Directory relative to Base URL with "this" language prefix
{{%/* link "/img/" "this" "" "_blank" /*/%}}
		⥥
{{% link "/img/" "this" "" "_blank" /%}}

# Path/Directory (no tail) relative to Base URL with "this" language prefix
{{%/* link "/img" "this" "" "_blank" /*/%}}
		⥥
{{% link "/img" "this" "" "_blank" /%}}

# Path/Directory relative to Base URL with "zh-hans" language prefix
{{%/* link "/img/" "zh-hans" "" "_blank" /*/%}}
		⥥
{{% link "/img/" "zh-hans" "" "_blank" /%}}

# Path/Directory (no tail) relative to Base URL with  "zh-hans" language prefix
{{%/* link "/img" "zh-hans" "" "_blank" /*/%}}
		⥥
{{% link "/img" "zh-hans" "" "_blank" /%}}

# Relative to current URL (without leading "/" slash)
{{%/* link "img/logo.png" "" "" "_blank" /*/%}}
		⥥
{{% link "img/logo.png" "" "" "_blank" /%}}

# Relative to current URL using the current language prefix `this`
{{%/* link "img/logo.png" "this" "" "_blank" /*/%}}
		⥥
{{% link "img/logo.png" "this" "" "_blank" /%}}

# Relative to current URL with language prefix overwrite to "zh-hans"
{{%/* link "img/logo.png" "zh-hans" "" "_blank" /*/%}}
		⥥
{{% link "img/logo.png" "zh-hans" "" "_blank" /%}}

# Path/Directory relative to current URL
{{%/* link "img/" "" "" "_blank" /*/%}}
		⥥
{{% link "img/" "" "" "_blank" /%}}

# Path/Directory (no tail) relative to current URL
{{%/* link "img" "" "" "_blank" /*/%}}
		⥥
{{% link "img" "" "" "_blank" /%}}

# Path/Directory relative to current URL with "this" language prefix
{{%/* link "img/" "this" "" "_blank" /*/%}}
		⥥
{{% link "img/" "this" "" "_blank" /%}}

# Path/Directory (no tail) relative to current URL with "this" language prefix
{{%/* link "img" "this" "" "_blank" /*/%}}
		⥥
{{% link "img" "this" "" "_blank" /%}}

# Path/Directory relative to current URL with "zh-hans" language prefix
{{%/* link "img/" "zh-hans" "" "_blank" /*/%}}
		⥥
{{% link "img/" "zh-hans" "" "_blank" /%}}

# Path/Directory (no tail) relative to current URL with "zh-hans" language prefix
{{%/* link "img" "zh-hans" "" "_blank" /*/%}}
		⥥
{{% link "img" "zh-hans" "" "_blank" /%}}
```


#### Class
The 5th parameter is the class attribute for link. This is useful for CSS
styling using class tag.

This parameter is optional and **should be set blank (`""`)** if the following
parameters are used.

Here are the usage examples:

```md {linenos=table,hl_lines=[],linenostart=1}
# Supplying Absolute Value
{{%/* link "https://www.example.com" "" "" "" "button" /*/%}}
		⥥
{{% link "https://www.example.com" "" "" "" "button" /%}}

# Supplying Absolute Value with 'this' language prefix
{{%/* link "https://www.example.com" "this" "" "" "button" /*/%}}
		⥥
{{% link "https://www.example.com" "this" "" "" "button" /%}}

# Supplying Absolute Value with 'zh-hans' language prefix
{{%/* link "https://www.example.com" "zh-hans" "" "" "button" /*/%}}
		⥥
{{% link "https://www.example.com" "zh-hans" "" "" "button" /%}}

# Relative to Base URL (e.g. to access static files)
{{%/* link "/img/logo.png" "" "" "" "button" /*/%}}
		⥥
{{% link "/img/logo.png" "" "" "" "button" /%}}

# Relative to Base URL with language Prefix as "zh-hans"
{{%/* link "/img/logo.png" "zh-hans" "" "" "button" /*/%}}
		⥥
{{% link "/img/logo.png" "zh-hans" "" "" "button" /%}}

# Path/Directory relative to Base URL
{{%/* link "/img/" "" "" "" "button" /*/%}}
		⥥
{{% link "/img/" "" "" "" "button" /%}}

# Path/Directory (no tail) relative to Base URL
{{%/* link "/img" "" "" "" "button" /*/%}}
		⥥
{{% link "/img" "" "" "" "button" /%}}

# Path/Directory relative to Base URL with "this" language prefix
{{%/* link "/img/" "this" "" "" "button" /*/%}}
		⥥
{{% link "/img/" "this" "" "" "button" /%}}

# Path/Directory (no tail) relative to Base URL with "this" language prefix
{{%/* link "/img" "this" "" "" "button" /*/%}}
		⥥
{{% link "/img" "this" "" "" "button" /%}}

# Path/Directory relative to Base URL with "zh-hans" language prefix
{{%/* link "/img/" "zh-hans" "" "" "button" /*/%}}
		⥥
{{% link "/img/" "zh-hans" "" "" "button" /%}}

# Path/Directory (no tail) relative to Base URL with  "zh-hans" language prefix
{{%/* link "/img" "zh-hans" "" "" "button" /*/%}}
		⥥
{{% link "/img" "zh-hans" "" "" "button" /%}}

# Relative to current URL (without leading "/" slash)
{{%/* link "img/logo.png" "" "" "" "button" /*/%}}
		⥥
{{% link "img/logo.png" "" "" "" "button" /%}}

# Relative to current URL using the current language prefix `this`
{{%/* link "img/logo.png" "this" "" "" "button" /*/%}}
		⥥
{{% link "img/logo.png" "this" "" "" "button" /%}}

# Relative to current URL with language prefix overwrite to "zh-hans"
{{%/* link "img/logo.png" "zh-hans" "" "" "button" /*/%}}
		⥥
{{% link "img/logo.png" "zh-hans" "" "" "button" /%}}

# Path/Directory relative to current URL
{{%/* link "img/" "" "" "" "button" /*/%}}
		⥥
{{% link "img/" "" "" "" "button" /%}}

# Path/Directory (no tail) relative to current URL
{{%/* link "img" "" "" "" "button" /*/%}}
		⥥
{{% link "img" "" "" "" "button" /%}}

# Path/Directory relative to current URL with "this" language prefix
{{%/* link "img/" "this" "" "" "button" /*/%}}
		⥥
{{% link "img/" "this" "" "" "button" /%}}

# Path/Directory (no tail) relative to current URL with "this" language prefix
{{%/* link "img" "this" "" "" "button" /*/%}}
		⥥
{{% link "img" "this" "" "" "button" /%}}

# Path/Directory relative to current URL with "zh-hans" language prefix
{{%/* link "img/" "zh-hans" "" "" "button" /*/%}}
		⥥
{{% link "img/" "zh-hans" "" "" "button" /%}}

# Path/Directory (no tail) relative to current URL with "zh-hans" language prefix
{{%/* link "img" "zh-hans" "" "" "button" /*/%}}
		⥥
{{% link "img" "zh-hans" "" "" "button" /%}}
```


#### ID Tagging
Starting from version `v1.13.0`, you can set a link with ID tagging
(`id=` attribute in HTML link tag) with the 6th parameters.

This parameter is optional and **should be set blank (`""`)** if the following
parameters are used.

Here are the usage examples:

```md {linenos=table,hl_lines=[],linenostart=1}
# Supplying Absolute Value
{{%/* link "https://www.example.com" "" "" "" "" "my-id" /*/%}}
		⥥
{{% link "https://www.example.com" "" "" "" "" "my-id" /%}}

# Supplying Absolute Value with 'this' language prefix
{{%/* link "https://www.example.com" "this" "" "" "" "my-id" /*/%}}
		⥥
{{% link "https://www.example.com" "this" "" "" "" "my-id" /%}}

# Supplying Absolute Value with 'zh-hans' language prefix
{{%/* link "https://www.example.com" "zh-hans" "" "" "" "my-id" /*/%}}
		⥥
{{% link "https://www.example.com" "zh-hans" "" "" "" "my-id" /%}}

# Relative to Base URL (e.g. to access static files)
{{%/* link "/img/logo.png" "" "" "" "" "my-id" /*/%}}
		⥥
{{% link "/img/logo.png" "" "" "" "" "my-id" /%}}

# Relative to Base URL with language Prefix as "zh-hans"
{{%/* link "/img/logo.png" "zh-hans" "" "" "" "my-id" /*/%}}
		⥥
{{% link "/img/logo.png" "zh-hans" "" "" "" "my-id" /%}}

# Path/Directory relative to Base URL
{{%/* link "/img/" "" "" "" "" "my-id" /*/%}}
		⥥
{{% link "/img/" "" "" "" "" "my-id" /%}}

# Path/Directory (no tail) relative to Base URL
{{%/* link "/img" "" "" "" "" "my-id" /*/%}}
		⥥
{{% link "/img" "" "" "" "" "my-id" /%}}

# Path/Directory relative to Base URL with "this" language prefix
{{%/* link "/img/" "this" "" "" "" "my-id" /*/%}}
		⥥
{{% link "/img/" "this" "" "" "" "my-id" /%}}

# Path/Directory (no tail) relative to Base URL with "this" language prefix
{{%/* link "/img" "this" "" "" "" "my-id" /*/%}}
		⥥
{{% link "/img" "this" "" "" "" "my-id" /%}}

# Path/Directory relative to Base URL with "zh-hans" language prefix
{{%/* link "/img/" "zh-hans" "" "" "" "my-id" /*/%}}
		⥥
{{% link "/img/" "zh-hans" "" "" "" "my-id" /%}}

# Path/Directory (no tail) relative to Base URL with  "zh-hans" language prefix
{{%/* link "/img" "zh-hans" "" "" "" "my-id" /*/%}}
		⥥
{{% link "/img" "zh-hans" "" "" "" "my-id" /%}}

# Relative to current URL (without leading "/" slash)
{{%/* link "img/logo.png" "" "" "" "" "my-id" /*/%}}
		⥥
{{% link "img/logo.png" "" "" "" "" "my-id" /%}}

# Relative to current URL using the current language prefix `this`
{{%/* link "img/logo.png" "this" "" "" "" "my-id" /*/%}}
		⥥
{{% link "img/logo.png" "this" "" "" "" "my-id" /%}}

# Relative to current URL with language prefix overwrite to "zh-hans"
{{%/* link "img/logo.png" "zh-hans" "" "" "" "my-id" /*/%}}
		⥥
{{% link "img/logo.png" "zh-hans" "" "" "" "my-id" /%}}

# Path/Directory relative to current URL
{{%/* link "img/" "" "" "" "" "my-id" /*/%}}
		⥥
{{% link "img/" "" "" "" "" "my-id" /%}}

# Path/Directory (no tail) relative to current URL
{{%/* link "img" "" "" "" "" "my-id" /*/%}}
		⥥
{{% link "img" "" "" "" "" "my-id" /%}}

# Path/Directory relative to current URL with "this" language prefix
{{%/* link "img/" "this" "" "" "" "my-id" /*/%}}
		⥥
{{% link "img/" "this" "" "" "" "my-id" /%}}

# Path/Directory (no tail) relative to current URL with "this" language prefix
{{%/* link "img" "this" "" "" "" "my-id" /*/%}}
		⥥
{{% link "img" "this" "" "" "" "my-id" /%}}

# Path/Directory relative to current URL with "zh-hans" language prefix
{{%/* link "img/" "zh-hans" "" "" "" "my-id" /*/%}}
		⥥
{{% link "img/" "zh-hans" "" "" "" "my-id" /%}}

# Path/Directory (no tail) relative to current URL with "zh-hans" language prefix
{{%/* link "img" "zh-hans" "" "" "" "my-id" /*/%}}
		⥥
{{% link "img" "zh-hans" "" "" "" "my-id" /%}}
```


#### Downloadable Link
Starting from version `v1.13.0`, you can set a link to be downloadable
(`download` attribute in HTML link tag) with the 7th parameters.

This parameter is optional and **should be set blank (`""`)** if the following
parameters are used.

Here are the usage examples:

```md {linenos=table,hl_lines=[],linenostart=1}
# Supplying Absolute Value
{{%/* link "https://www.example.com" "" "" "" "" "" "download" /*/%}}
		⥥
{{% link "https://www.example.com" "" "" "" "" "" "download" /%}}

# Supplying Absolute Value with 'this' language prefix
{{%/* link "https://www.example.com" "this" "" "" "" "" "download" /*/%}}
		⥥
{{% link "https://www.example.com" "this" "" "" "" "" "download" /%}}

# Supplying Absolute Value with 'zh-hans' language prefix
{{%/* link "https://www.example.com" "zh-hans" "" "" "" "" "download" /*/%}}
		⥥
{{% link "https://www.example.com" "zh-hans" "" "" "" "" "download" /%}}

# Relative to Base URL (e.g. to access static files)
{{%/* link "/img/logo.png" "" "" "" "" "" "download" /*/%}}
		⥥
{{% link "/img/logo.png" "" "" "" "" "" "download" /%}}

# Relative to Base URL with language Prefix as "zh-hans"
{{%/* link "/img/logo.png" "zh-hans" "" "" "" "" "download" /*/%}}
		⥥
{{% link "/img/logo.png" "zh-hans" "" "" "" "" "download" /%}}

# Path/Directory relative to Base URL
{{%/* link "/img/" "" "" "" "" "" "download" /*/%}}
		⥥
{{% link "/img/" "" "" "" "" "" "download" /%}}

# Path/Directory (no tail) relative to Base URL
{{%/* link "/img" "" "" "" "" "" "download" /*/%}}
		⥥
{{% link "/img" "" "" "" "" "" "download" /%}}

# Path/Directory relative to Base URL with "this" language prefix
{{%/* link "/img/" "this" "" "" "" "" "download" /*/%}}
		⥥
{{% link "/img/" "this" "" "" "" "" "download" /%}}

# Path/Directory (no tail) relative to Base URL with "this" language prefix
{{%/* link "/img" "this" "" "" "" "" "download" /*/%}}
		⥥
{{% link "/img" "this" "" "" "" "" "download" /%}}

# Path/Directory relative to Base URL with "zh-hans" language prefix
{{%/* link "/img/" "zh-hans" "" "" "" "" "download" /*/%}}
		⥥
{{% link "/img/" "zh-hans" "" "" "" "" "download" /%}}

# Path/Directory (no tail) relative to Base URL with  "zh-hans" language prefix
{{%/* link "/img" "zh-hans" "" "" "" "" "download" /*/%}}
		⥥
{{% link "/img" "zh-hans" "" "" "" "" "download" /%}}

# Relative to current URL (without leading "/" slash)
{{%/* link "img/logo.png" "" "" "" "" "" "download" /*/%}}
		⥥
{{% link "img/logo.png" "" "" "" "" "" "download" /%}}

# Relative to current URL using the current language prefix `this`
{{%/* link "img/logo.png" "this" "" "" "" "" "download" /*/%}}
		⥥
{{% link "img/logo.png" "this" "" "" "" "" "download" /%}}

# Relative to current URL with language prefix overwrite to "zh-hans"
{{%/* link "img/logo.png" "zh-hans" "" "" "" "" "download" /*/%}}
		⥥
{{% link "img/logo.png" "zh-hans" "" "" "" "" "download" /%}}

# Path/Directory relative to current URL
{{%/* link "img/" "" "" "" "" "" "download" /*/%}}
		⥥
{{% link "img/" "" "" "" "" "" "download" /%}}

# Path/Directory (no tail) relative to current URL
{{%/* link "img" "" "" "" "" "" "download" /*/%}}
		⥥
{{% link "img" "" "" "" "" "" "download" /%}}

# Path/Directory relative to current URL with "this" language prefix
{{%/* link "img/" "this" "" "" "" "" "download" /*/%}}
		⥥
{{% link "img/" "this" "" "" "" "" "download" /%}}

# Path/Directory (no tail) relative to current URL with "this" language prefix
{{%/* link "img" "this" "" "" "" "" "download" /*/%}}
		⥥
{{% link "img" "this" "" "" "" "" "download" /%}}

# Path/Directory relative to current URL with "zh-hans" language prefix
{{%/* link "img/" "zh-hans" "" "" "" "" "download" /*/%}}
		⥥
{{% link "img/" "zh-hans" "" "" "" "" "download" /%}}

# Path/Directory (no tail) relative to current URL with "zh-hans" language prefix
{{%/* link "img" "zh-hans" "" "" "" "" "download" /*/%}}
		⥥
{{% link "img" "zh-hans" "" "" "" "" "download" /%}}
```


#### Overwrite Language Hinting Link
Starting from version `v1.13.0`, you can overwrite the default language hinting
(`hreflang=` attribute in HTML link tag) with the 8th parameters.

This parameter is optional and **should be set blank (`""`)** if the following
parameters are used.

Here are the usage examples:

```md {linenos=table,hl_lines=[],linenostart=1}
# Supplying Absolute Value
{{%/* link "https://www.example.com" "" "" "" "" "" "" "overwrite" /*/%}}
		⥥
{{% link "https://www.example.com" "" "" "" "" "" "" "overwrite" /%}}

# Supplying Absolute Value with 'this' language prefix
{{%/* link "https://www.example.com" "this" "" "" "" "" "" "overwrite" /*/%}}
		⥥
{{% link "https://www.example.com" "this" "" "" "" "" "" "overwrite" /%}}

# Supplying Absolute Value with 'zh-hans' language prefix
{{%/* link "https://www.example.com" "zh-hans" "" "" "" "" "" "overwrite" /*/%}}
		⥥
{{% link "https://www.example.com" "zh-hans" "" "" "" "" "" "overwrite" /%}}

# Relative to Base URL (e.g. to access static files)
{{%/* link "/img/logo.png" "" "" "" "" "" "" "overwrite" /*/%}}
		⥥
{{% link "/img/logo.png" "" "" "" "" "" "" "overwrite" /%}}

# Relative to Base URL with language Prefix as "zh-hans"
{{%/* link "/img/logo.png" "zh-hans" "" "" "" "" "" "overwrite" /*/%}}
		⥥
{{% link "/img/logo.png" "zh-hans" "" "" "" "" "" "overwrite" /%}}

# Path/Directory relative to Base URL
{{%/* link "/img/" "" "" "" "" "" "" "overwrite" /*/%}}
		⥥
{{% link "/img/" "" "" "" "" "" "" "overwrite" /%}}

# Path/Directory (no tail) relative to Base URL
{{%/* link "/img" "" "" "" "" "" "" "overwrite" /*/%}}
		⥥
{{% link "/img" "" "" "" "" "" "" "overwrite" /%}}

# Path/Directory relative to Base URL with "this" language prefix
{{%/* link "/img/" "this" "" "" "" "" "" "overwrite" /*/%}}
		⥥
{{% link "/img/" "this" "" "" "" "" "" "overwrite" /%}}

# Path/Directory (no tail) relative to Base URL with "this" language prefix
{{%/* link "/img" "this" "" "" "" "" "" "overwrite" /*/%}}
		⥥
{{% link "/img" "this" "" "" "" "" "" "overwrite" /%}}

# Path/Directory relative to Base URL with "zh-hans" language prefix
{{%/* link "/img/" "zh-hans" "" "" "" "" "" "overwrite" /*/%}}
		⥥
{{% link "/img/" "zh-hans" "" "" "" "" "" "overwrite" /%}}

# Path/Directory (no tail) relative to Base URL with  "zh-hans" language prefix
{{%/* link "/img" "zh-hans" "" "" "" "" "" "overwrite" /*/%}}
		⥥
{{% link "/img" "zh-hans" "" "" "" "" "" "overwrite" /%}}

# Relative to current URL (without leading "/" slash)
{{%/* link "img/logo.png" "" "" "" "" "" "" "overwrite" /*/%}}
		⥥
{{% link "img/logo.png" "" "" "" "" "" "" "overwrite" /%}}

# Relative to current URL using the current language prefix `this`
{{%/* link "img/logo.png" "this" "" "" "" "" "" "overwrite" /*/%}}
		⥥
{{% link "img/logo.png" "this" "" "" "" "" "" "overwrite" /%}}

# Relative to current URL with language prefix overwrite to "zh-hans"
{{%/* link "img/logo.png" "zh-hans" "" "" "" "" "" "overwrite" /*/%}}
		⥥
{{% link "img/logo.png" "zh-hans" "" "" "" "" "" "overwrite" /%}}

# Path/Directory relative to current URL
{{%/* link "img/" "" "" "" "" "" "" "overwrite" /*/%}}
		⥥
{{% link "img/" "" "" "" "" "" "" "overwrite" /%}}

# Path/Directory (no tail) relative to current URL
{{%/* link "img" "" "" "" "" "" "" "overwrite" /*/%}}
		⥥
{{% link "img" "" "" "" "" "" "" "overwrite" /%}}

# Path/Directory relative to current URL with "this" language prefix
{{%/* link "img/" "this" "" "" "" "" "" "overwrite" /*/%}}
		⥥
{{% link "img/" "this" "" "" "" "" "" "overwrite" /%}}

# Path/Directory (no tail) relative to current URL with "this" language prefix
{{%/* link "img" "this" "" "" "" "" "" "overwrite" /*/%}}
		⥥
{{% link "img" "this" "" "" "" "" "" "overwrite" /%}}

# Path/Directory relative to current URL with "zh-hans" language prefix
{{%/* link "img/" "zh-hans" "" "" "" "" "" "overwrite" /*/%}}
		⥥
{{% link "img/" "zh-hans" "" "" "" "" "" "overwrite" /%}}

# Path/Directory (no tail) relative to current URL with "zh-hans" language prefix
{{%/* link "img" "zh-hans" "" "" "" "" "" "overwrite" /*/%}}
		⥥
{{% link "img" "zh-hans" "" "" "" "" "" "overwrite" /%}}
```



#### Inner Content
Lastly, starting from version `v1.13.0`, all links now have a inner content
wrapper (notice the above has self-closing tag `/` in the end. This wrapper
allows one to wrap the link content easily.

The content format is parsed according to the following sequence:
1. If partial name is given (e.g. `custom/myCode.html` for
   `layouts/partials/custom/myCode.html`) and the file exists, the partial
   file will be parsed.
2. If sequence No.1 fails, then the content will be parsed as Markdown format.
3. If HTML related errors are detected from sequence No.2, the content will
   resort to the final format: HTML.

It is ultimately your responsibility to ensure the content are working across
multiple output formats. Please keep in mind that a shortcode can be used in
another shortcodes.

Here are some of the examples with `image` shortcode used inside the `link`
shortcode inner content for multiple output format friendliness:

```md {linenos=table,hl_lines=[],linenostart=1}
# Wrap Markdown
{{%/* link "/license/bissetii-license.pdf" "" "" "" "" "download" */%}}
{{%/* image "Demo Image"
	"/img/thumbnails/default-1200x1200.png"
	"2500"
	"1250"
	"false"
	"lazy"
	""
	"responsive"
	"dark-color" */%}}
### Download
{{%/* /link */%}}
		⥥
{{% link "/license/bissetii-license.pdf" "" "" "" "" "download" %}}
{{% image "Demo Image"
	"/img/thumbnails/default-1200x1200.png"
	"2500"
	"1250"
	"false"
	"lazy"
	""
	"responsive"
	"dark-color" %}}
### Download
{{% /link %}}


# Wrap HTML
{{%/* link "/license/bissetii-license.pdf" "" "" "" "" "download" */%}}
{{%/* image "Demo Image"
	"/img/thumbnails/default-1200x1200.png"
	"2500"
	"1250"
	"false"
	"lazy"
	""
	"responsive"
	"dark-color" */%}}
<p>Download</p>
{{%/* /link */%}}
		⥥
{{% link "/license/bissetii-license.pdf" "" "" "" "" "download" %}}
{{% image "Demo Image"
	"/img/thumbnails/default-1200x1200.png"
	"2500"
	"1250"
	"false"
	"lazy"
	""
	"responsive"
	"dark-color" %}}
<p>Download</p>
{{% /link %}}
```




## Epilogue
That's all about `link` shortcode in Bissetii. If you need more feature or
need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
