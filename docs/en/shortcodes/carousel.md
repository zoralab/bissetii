+++
date = "2020-07-30T17:13:36+08:00"
title = "carousel Shortcode"
description = """
`carousel` shortcode allows you to create Bissetii compatible Carousel HTML
codes when Bissetii is used as Hugo Theme. Carousel codes are usually long and
thick so this shortcode is created to ease some of the coding burden while
deploying to various outputs like Vanilla HTML and AMP HTML simultenously.
"""
keywords = ["carousel", "shortcode", "hugo", "bissetii"]
draft = false
type = ""
redirectURL="/en/components/carousel/"
layout = "redirect"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
	"carousel",
]


[creators.ZORALab]
type = "Organization"
name = "ZORALab Team"

[creators.KeanHo]
type = "Person"
name = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "redirect"
name = "carousel (BETA)"
pre = "🗂️"
weight = 1


[schema]
selectType = "WebPage"
+++

# `carousel` Shortcode
{{% param "description" %}}

Example usage:

```markdown {linenos=table,hl_lines=[],linenostart=1}
{{</* carousel "horizontal-slide"
	"bissetii.data.carousels.picture-gallery" */>}}
```

This shortcode is available starting from `v1.12.1`.

{{< note "warning" "Beta Testing Notice" >}}
This is a fairly new shortcode and it is still under user-level API beta
testing. Hence, there may be some **major updates in the future that are not
backward compatible**. Please use at your own risk.
{{< /note >}}




## Heads-Up
While the shortcode is useful, **it breaks the Markdown syntax conventions
due to Hugo specific shortcode feature**. Therefore, when using other Markdown
renderer, these shortcodes would be rendered as text instead of HTML code
fragments.

Also, be careful and stay safe while using this shortcode. It allows any form
of HTML to be embedded into your Markdown page.





## Using `carousel`
There are a number of requirements for using `carousel` shortcodes. You should
create the component listed here starting from top to bottom.



### Create Slide Contents
Bissetii has 3 ways to produce the slide contents:

1. Creating HTML partials (first detection)
2. Process as Markdown content (second fallback)
3. Process as HTML content (final fallback)

You can create the slides' content directly inside the data file or as a
separate HTML partial files.



### Create Dataset in Data File
Due to the complexity of a `carousel` HTML codes and the fact that Hugo can
generate multiple outputs simultenously, it makes a lot of sense to create a
data file to hold the data structure. Carousel shortcode recogizes the
following `TOML` pattern:

```toml {linenos=table,hl_lines=[],linenostart=1}
Label       = "MyGallery"       # Aria-label
Timing      = "5000"            # Slide presentation duration in millisecond

[HTML]                          # Output Format (all uppercase)
Width       = "auto"            # Carousel width as a whole
Height      = "500px"           # Carousel height
Layout      = "responsive"      # Carousel styling layout
Autoplay    = true              # Carousel autoplay flag (true / false)

[AMP]
Width       = "1200px"
Height      = "500px"
Layout      = "responsive"
Autoplay    = true


[slides.1]                      # arrange slides in a numbering position array
HTML = "path/to/slide1.html"                    # HTML content
AMPHTML = "part/to/slide1.amp.html"             # AMP content


[slides.2]
HTML = """
### This can be a markdown.
Yes, it can be markdown.
"""
AMPHTML = """
### This can be a markdown.
Yes, it can be markdown.
"""


[slides.3]
HTML = """
<h3>This can also be HTML</h3>
<p>Yes, you can use HTML here</p>
"""
AMPHTML = """
<h3>This can also be HTML</h3>
<p>Yes, you can use HTML here</p>
"""
```

You may repeat the slide dataset as much as you want to increase the carousel
slides. As shown above, the example carousel has 3 slides.



### Add Required Dependencies
You need to add `carousel` into your `[modules] - extension` list. Depending
on output format (e.g. AMP), this is a required step. Example:

```toml {linenos=table,hl_lines=[6],linenostart=1}
...

[modules]
extensions = [
	...,
	"carousel",
]

...
```


### Shortcode Parameter
```
{{</* carousel "<type>" "<dataPath>" */>}}
```
`carousel` shortcode only takes in 2 inputs:

1. `type` - the type of carousel.
2. `dataPath` - the data path for the dataset in the data path.


#### `type` parameter
The first `type` parameter is a reserved parameter mainly to facilitate various
kinds of carousels in the future. To see all types of carousel, please visit:
{{< link "/components/carousel/" "this" "" "" "" >}}
Carousel Component
{{< /link >}}.


#### `dataPath` parameter
The second `dataPath` parameter is a `Pathing.Name` combination to select a
dataset from a given data file inside `data/` directory. It goes by the format
of `relative.path.to.file`, combining filepath and dataset name.

For example:
```
Stored File:  data/subdirectory/myCarousel.toml
dataPath   :       subdirectory.myCarousel
```
The `dataPath` should be `subdirectory.myCarousel`.



### Example Usage
Since this is a HTML renderer in Markdown file, you want to call in the
shortcode using the `<` symbol instead of `%`. The brackets are as follow:

```markdown {linenos=table,hl_lines=["4-5"],linenostart=1}
# This is Markdown Heading
Your normal text here.

{{</* carousel "horizontal-slide"
	"bissetii.data.carousels.picture-gallery" */>}}

## Back to Markdown
```

Depending on which output (e.g. `AMP` or `Vanilla`) HTML you're viewing, this
will render the output as:

```html {linenos=table,hl_lines=[],linenostart=1}
<h1>This is Markdown Heading</h1>
{{% carousel "horizontal-slide"
	"bissetii.data.carousels.picture-gallery" %}}
<h2>Back to Markdown></h2>
```




## Epilogue
That's all for carousel shortcode. If you have any question, please feel free
to raise your query in our
[Issues Section](https://gitlab.com/zoralab/bissetii/-/issues).
