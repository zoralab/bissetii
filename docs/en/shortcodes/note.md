+++
date = "2020-08-03T19:08:44+08:00"
title = "note Shortcode"
description = """
`note` shortcode is a Hugo-only feature to quickly create the Bissetii's note
component. This allows content creators to focus on writing the contents instead
of rendering HTML here and there. By default, the shortcode renders the content
as markdown. If HTML is detected, the shortcode will switch the rendering to
safe HTML instead.
"""
keywords = ["note", "shortcode", "hugo", "bissetii"]
draft = false
type = ""
redirectURL="/en/components/note/"
layout = "redirect"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "sidebar",
]


[creators.ZORALab]
type = "Organization"
name = "ZORALab Team"

[creators.KeanHo]
type = "Person"
name = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "disabled"
name = "note (BETA)"
pre = "🗒️"
weight = 1


[schema]
selectType = "WebPage"
+++

# `note` Shortcode
{{% param "description" %}}

Example usage:

```markdown {linenos=table,hl_lines=[1,"2-3"],linenostart=1}
{{</* note "warning" "WARNING" */>}}
	Please proceed with extreme caution.
{{</* /note */>}}
```

This shortcode is available starting from `v1.12.1`.

{{< note "warning" "IMPORTANT NOTICE" >}}
This is a fairly new shortcode and it is still under user-level API beta
testing. Hence, there may be some major updates that are not backward
compatible.
{{< /note >}}




## Heads-Up
While the shortcode is useful, **it breaks the Markdown syntax conventions
due to Hugo specific shortcode feature**. Therefore, when using other Markdown
renderer, these shortcodes would be rendered as text instead of HTML code
fragments.

Also, be careful and stay safe while using this shortcode. It allows any form
of HTML to be embedded into your Markdown page.




## Using `note`
```markdown
{{</* note <type> <title> */>}}
	<content>
{{</* /note */>}}
```

`note` uses both parameters and content as its input methods. For parameters,
`note` shortcode accepts the following in strict order:

1. `<type>` - the type of notes. See
   [Note Component]({{< link "/components/note/" "this" "url-only" />}}) for
   available types.
2. `<title>` - the title of the note. It is bolded by default.

As for `<content>` inside the shortcode wrapper, you **MUST** decide either to
write as **strictly** Markdown format or **strictly** HTML format but
**not both simultenously**.

The shortcode will first render the `<content>` as Markdown format.
**Should it detects any HTML codes, it will automatically switch to safeHTML
format rendering instead**.

The shortcode will render the output accordingly and seamlessly to its output
types automatically (example: vanilla HTML for normal HTML output; AMP HTML for
AMP output).



### Example
Since this is a HTML renderer in Markdown file, you want to call in the
shortcode using the `<` symbol instead of `%`. The brackets are as follow:

```markdown {linenos=table,hl_lines=["4-11"],linenostart=1}
# This is Markdown Heading
Your normal text here.

{{</* note "info" "Important Note" */>}}
This is the content of the note. By default, it is rendered as Markdown. Should
any HTML codes is detected, the shortcode with switch to render safeHTML
instead. Hence, please choose wisely.

Also, if you are using shortcode in here, due to Hugo's limitation, you
**must** use HTML format.
{{</* /note */>}}

## Back to Markdown
```

This will render the output as:

```html {linenos=table,hl_lines=["4-21"],linenostart=1}
<h1>This is Markdown Heading</h1>
<p>Your normal text here.</p>

{{% note "info" "Important Note" %}}
This is the content of the note. By default, it is rendered as Markdown. Should
any HTML codes is detected, the shortcode with switch to render safeHTML
instead. Hence, please choose wisely.

Also, if you are using shortcode in here, due to Hugo's limitation, you
**must** use HTML format.
{{% /note %}}

<h2>Back to Markdown></h2>
```




## Conclusion
That's all about `note` shortcode in Bissetii. If you need more feature or
need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
