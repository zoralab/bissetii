+++
date = "2020-12-12T13:00:02+08:00"
title = "translate Shortcode"
description = """
`translate` shortcode is a Bissetii specific tool to obtain a particular text or
partials from a language dictionary inside `data/` directory. The design allows
the use of various format to render the final results.
"""
keywords = ["translate", "shortcode", "hugo", "bissetii"]
draft = false
type = ""
redirectURL="/en/components/i18n/"
layout = "redirect"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
]


[creators.ZORALab]
type = "Organization"
name = "ZORALab Team"

[creators.KeanHo]
type = "Person"
name = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "disabled"
name = "translate (BETA)"
pre = "🍱"
weight = 1


[schema]
selectType = "WebPage"
+++

# `translate` Shortcode
{{% param "description" %}}

Example usage:

```markdown {linenos=table,hl_lines=[1,"2-3"],linenostart=1}
{{</* translate "bissetii.i18n" "this" "list.Section.Description" */>}}
{{</* translate "bissetii.i18n" "zh-hans" "list.Section.Description" */>}}
```

This shortcode is available starting from `v1.13.0`.

{{< note "warning" "Beta Testing Notice" >}}
This is a fairly new component and it is still under user-level API beta
testing. Hence, there may be some major updates in the future that are
**not backward compatible**. Please **ONLY use at your own risk**.
{{< /note >}}




## Heads-Up
While the shortcode is useful, **it breaks the Markdown syntax conventions
due to Hugo specific shortcode feature**. Therefore, when using other Markdown
renderer, these shortcodes would be rendered as text instead of HTML code
fragments.

Also, be careful and stay safe while using this shortcode. It allows any form
of HTML to be embedded into your Markdown page.




## Using `translate`
`translate` shortcode is making use of Hugo's `data/` directory instead of the
original Hugo
[i18n](https://gohugo.io/content-management/multilingual/#reference-the-translated-content)
feature. You are free to use that original feature instead of this shortcode.

The reason behind this shortcode is it allows one to develop a more organized
way to organize the dictionary inside `data/` directory while maintaining the
translation flexiblity from Hugo i18n feature.

Another good reason is that this shortcode allows various rendering format. The
parsing format sequences are:

1. It first reads the value as a partial filepath. If the partial file is found,
   it will parse the partial file instead. Example `my-partial.html` for
   `layouts/partials/my-partial.html`.
2. If sequence 1 fails, then it will parse the content as Markdown format.
3. If there are reported HTML errors in sequence 2, then it will finally parse
   the content as HTML format.

It is the content creators' responsibility to ensure the contents are compatible
across multiple output formats (e.g. Accelerated Mobile Page: `amp`).



### Shortcode Parameters
Since this is a HTML renderer in Markdown file, you want to call in the
shortcode using the `<` symbol instead of `%`. The brackets are as follow:

The shortcode follows the following parameters pattern:

```md
{{</* translate "<dict>" "<lang>" "<query>" */>}}
```


#### Dictionary (`<dict>`)
The 1st parameter is the source of your multi-language dictionary. The 1st-level
sub-directory **MUST** be language oriented. For example, Bissetii's default
`bissetii/i18n` dictionary is arranged as follows:

```
data/bissetii/i18n/
├── en/
│   └── list.toml
└── zh-hans/
    └── list.toml
```


#### Language (`<lang>`)
The 2nd parameter is the selected language. This will select the 1st-level
sub-directory from the given dictionary. For content compatibility, this
parameter accepts a special value called `"this"` which in turns, select the
page's current language instead.

You can freely provide your own language directory. Although given with that
freedom, Bissetii recommends you stick to:

1. [ISO 639-1](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes)
2. [ISO 3166-1 Alpha 2](https://www.iban.com/country-codes)
3. [ISO 15924](http://unicode.org/iso15924/iso15924-codes.html)

If we follow the example above, the value can be any of the following:

1. `en` = explictly select `en` language directory.
2. `zh-hans` = explictly select `zh-hans` langauge directory.
3. `this` = select the language directory using the current page's language
   code.


#### Query (`<query>`)
The 3rd parameter is your translation query. This will select the data after
the selected 1st-level sub-directory (language). Here, you are free to use your
query based on your defined data TOML file.

The TOML data file and data structure **MUST** be consistent across langauges.
If we continue the example above, inside:

`data/bissetii/i18n/en/list.toml` we got:

```
[Section]
Title = "Available Contents"
Description = """
In case you are looking up for something in this topic, here are some of the
available content ready for you:
"""
```

`data/bissetii/i18n/zh-hans/list.toml` we got:

```
[Section]
Title = "内容目录"
Description = """
如果您想在这个话题里寻找一些仔细的内容，这里准备了一些简单目录让您寻找和过目：
"""
```

This allows us to query `list.Section.Description` and `list.Section.Title`
across both `en` and `zh-Hans` languages.



### Examples
Here are some of the examples for using the shortcode using the example data
above:

```markdown {linenos=table,hl_lines=[1,"2-3"],linenostart=1}
# This is Markdown Heading
Your normal text here.

{{</* translate "bissetii.i18n" "this" "list.Section.Description" */>}}

{{</* translate "bissetii.i18n" "zh-hans" "list.Section.Description" */>}}

## Back to Markdown
```

This will render the output as:

```html {linenos=table,hl_lines=[1,"2-3"],linenostart=1}
<h1>This is Markdown Heading</h1>
<p>Your normal text here.</p>

{{% translate "bissetii.i18n" "this" "list.Section.Description" %}}

{{% translate "bissetii.i18n" "zh-hans" "list.Section.Description" %}}

<h2>Back to Markdown></h2>
```




## Conclusion
That's all about `translate` shortcode in Bissetii. If you need more feature or
need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
