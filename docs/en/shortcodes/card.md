+++
date = "2020-08-08T14:10:08+08:00"
title = "card Shortcode"
description = """
`card` shortcode is a renderer that renders **a specific card data** easily
into card format with various designs. This shortcode is mainly makes things
easier to use since it involves many integrated data together.
"""
keywords = ["card", "shortcode", "hugo", "bissetii"]
draft = false
type = ""
redirectURL="/en/components/card/"
layout = "redirect"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "sidebar",
]


[creators.ZORALab]
type = "Organization"
name = "ZORALab Team"

[creators.KeanHo]
type = "Person"
name = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "disabled"
name = "card (BETA)"
pre = "🔖"
weight = 1


[schema]
selectType = "WebPage"
+++

# `card` Shortcode
{{% param "description" %}}

Example usage:

```txt {linenos=table,hl_lines=[],linenostart=1}
{{</* card "bissetii.data.cards" "grid" "embed" */>}}
```

This shortcode is available starting from `v1.12.1`.

If you are looking to render multiple cards in a deck at once, see
[Cards Shortcode]({{< link "/shortcodes/cards" "this" "url-only" />}})
(notice the plural form).


{{< note "warning" "Beta Testing Notice" >}}
This is a fairly new shortcode and it is still under user-level API beta
testing. Hence, there may be some **major updates in the future that are not
backward compatible**. Please use **ONLY at your own risk**.
{{< /note >}}




## Heads-Up
While the shortcode is useful, **it breaks the Markdown syntax conventions
due to Hugo specific shortcode feature**. Therefore, when using other Markdown
renderer, these shortcodes would be rendered as text instead of HTML code
fragments.

Also, be careful and stay safe while using this shortcode. It allows any form
of HTML to be embedded into your Markdown page.




## Using `card`
`card` shortcode relies completely on data file to render every cards. You
should create the component listed here starting from top to bottom.



### Create Card Contents
There are 3 different methods to create the card contents. You may use only 1
method as per your use cases. Here, the methods are arranged in their parsing
sequences.

Bissetii parse your card content is the following priority:

1. If the content is a partial path and the file is available, the partial
   file will be parsed instead.
2. Else, Bissetii will parse the content as a direct Markdown format.
3. If any HTML codes are detected, Bissetii performs the last resort which
   is to parse the entire content as a direct HTML format.



### Create Every Cards' Data File
Inside the card data deck directory, you should create each data file for every
single card.

You can name if however you want as the shortcode will not use it. However,
Bissetii recommends `TOML` format in order to keep things simple.

The full file pattern is shown as follow:

```toml {linenos=table,hl_lines=["1-3","10-16", "27-36"],linenostart=1}
Width = "500px"
Height = "500px"
Content = """
### My Card Title Here
This can be written in Markdown where it will be processed into HTML
automatically.
"""


[CTA.1]
URL = "/"
Content = "Home"
ID = "card1-cta-home"
Class = ""
Download = false
Target = ""

[CTA.2]
URL = "/"
Content = "Buy"
ID = "card1-cta-buy"
Class = "pinpoint"
Download = false
Target = ""


[Thumbnails.1]
AltText = "Card Info 1"
SourceURL = "/img/thumbnails/default-1200x1200.png"
Width = "500px"
Height = "263px"
IsMap = "false"
LoadingMode = "lazy"
SourceSet = ""
Layout = "responsive"
Class = ""

[Thumbnails.2]
AltText = "Card Info 1a"
SourceURL = "/img/thumbnails/default-1200x1200.png"
Width = "500px"
Height = "263px"
IsMap = "false"
LoadingMode = "lazy"
SourceSet = ""
Layout = "responsive"
Class = ""
```

* **Line 1-2**    - specifies the card `width` and `height`. The value `auto` is
                    allowed.
* **Line 3**      - the rendering for the `card-content`. You can provide any
                    one of the following values:
  1. Partial parsing (E.g. `layouts/partials/bissetii-only/card/sample.html`).
  2. If fails, directly process with markdown format.
  3. If fails or HTML codes is detected, direct process with HTML format.
* **Line 10**     - creating optional Call-To-Action (CTA) links. Each CTA has
                    its own positioning tags. CTA main keyword **MUST** be
                    `CTA`.
* **Line 11-16**  - CTA link properties compliant to [Link Shortcode]({{< link
                  "/shortcodes/link" "this" "url-only" />}}) parameters.
* **Line 27**     - creating optional cards' image thumbnails. Each thumbnail
                    has its own position tags. Thumbnails main keyword **MUST**
                    be `Thumbnails`.
* **Line 28-36** - Thumbnail image properties compliant to
                   [Image Shortcode]({{< link "/shortcodes/image" "this"
                   "url-only" />}}) parameters.

{{< note "info" "For Your Information" >}}
Multiple thumbnails is planned but yet to be implemented using carousel
presentation. There is no definite scheduled development for it yet.

As of version `v1.12.1`, the shortcode will pick the **first** thumbnail as its
primary display image.
{{< /note >}}



### Calling Shortcode
```txt {linenos=table,hl_lines=[],linenostart=1}
{{</* card "<deckPath>" "<outputType>" "<embedMode>" */>}}
```
`card` shortcode currently accepting the following parameters.


#### `deckPath` Parameter
This is the card deck path for rendering all the cards inside it.

Example, for the deck directory located in `data/bissetii/data/cards`, the
`deckPath` is `bissetii.data.cards`.


#### `outputType` Parameter
This is to instruct shortcode to generate the card deck alongside with grid
system or just the card itself. Currently, it accepts the following values:

0. `""` - empty value default to `card`.
1. `card` - just to generate the card HTML code (default).
2. `grid` - generate the deck along with the grid layout.



#### `embedMode` Parameter
Set this value to `embed` if you want the card not to have shadow and animations
but embedded into the page instead. Currently it accepts the following values:

0. `""` - empty value default to normal mode.
1. `embed` - embed into the page with no shadow and movements.



### Example Usage
Since this is a HTML renderer in Markdown file, you want to call in the
shortcode using the `<` symbol instead of `%`.

The example usage is as follows:

```txt {linenos=table,hl_lines=[4],linenostart=1}
# This is Markdown Heading
Your normal text here.

{{</* card "bissetii.data.examples.cards.card1" "card" */>}}

## Back to Markdown
```

This will render the output as:

```html {linenos=table,hl_lines=[],linenostart=1}
<h1>This is Markdown Heading</h1>
<p>Your normal text here.</p>

{{% card "bissetii.data.examples.cards.card1" "card" %}}

<h2>Back to Markdown></h2>
```




## Conclusion
That's all about `card` shortcode in Bissetii. If you need more feature or
need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
