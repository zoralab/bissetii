+++
date = "2020-06-27T14:16:22+08:00"
title = "image Shortcode"
description = """
"image" shortcode is to facilitate an unified call to facilitate a detailed
image HTML tag instead of the conventional "<img>" tag. Also, this shortcode
facilitates multiple outputs rendering like "amp.html" and "html" outputs.
"""
keywords = ["image", "shortcode", "hugo", "bissetii"]
draft = false
type = ""
redirectURL="/en/components/image/"
layout = "redirect"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[amp]
modules = [
        # Example: "amp-sidebar",
]


[creators.ZORALab]
type = "Organization"
name = "ZORALab Team"

[creators.KeanHo]
type = "Person"
name = "Holloway Chew Kean Ho"


[thumbnail]
url = "/img/thumbnails/default.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "disabled"
name = "image"
pre = "📷"
weight = 1


[schema]
selectType = "WebPage"
+++

# `image` Shortcode
`image` shortcode is to facilitate an unified call to facilitate a detailed
image HTML tag instead of the conventional `<img>` tag. Also, this shortcode
facilitates multiple outputs rendering like `amp.html` and `html` outputs.

Example usage:

```markdown
{{</* image "Bissetii Project | Welcome"
	"/img/logo/2500x1250-color.png"
	"2500"
	"1250"
	"false"
	"lazy"
	""
	"responsive"
	"dark-color"
*/>}}
```

This shortcode is available starting from `v1.12.0`.




## Heads-Up
While the shortcode is useful, **it breaks the Markdown syntax conventions
due to Hugo specific shortcode feature**. Therefore, when using other Markdown
renderer, these shortcodes would be rendered as text instead of HTML code
fragments.

Also, be careful and stay safe while using this shortcode. It allows any form
of HTML to be embedded into your Markdown page.




## Using `image`
`image` is a single Bissetii rendering API that renders images on a website.
It uses these tags depending on the output type:

1. `html` - `<img>`
2. `amp.html` - `<amp-img>`

Since this is a HTML renderer in Markdown file, you want to call in the
shortcode using the `<` symbol instead of `%`. The brackets are as follow:

```markdown
# This is Markdown Heading
Your normal text here.

{{</* image "[Title] Title of the Image that serves as alternative text"
	"[SourceURL] URL to source image"
	"[Width] width of the image to be displayed"
	"[Height] height of the image to be displayed"
	"[IsMap] isMap boolean to indicate <img> is wrapped by a <a> link"
	"[LoadingMode] loading type between eager or lazy"
	"[SourceSet] srcset a set of image sources for browser to choose"
	"[Layout] layout mode, like responsive, responsive-amp-only, ..."
	"[Class] all class tags"
*/>}}

## Back to Markdown
```
All parameters are compulsory. The following sub-sections explains each
parameters in detail.


### `Title` Parameter
`Title` is used primarily as the value for `alt=` attribute. We call it a title
to ensure that you prioritize this attribute before placing an image.

It will be filled in as a string. Example values:

```Markdown
"blue ocean photo"
```


### `SourceURL` Parameter
`SourceURL` is used primarily as the value for `src=` attribute. It loads the
image source from a given URL. Both absolute and relative URLs are acceptable.

It will be filled in as a string. Example values:

```Markdown
"https://www.example.com/img/logo/2500x1250-color.png"
"/img/logo/2500x1250-color.png"
```


### `Width` Parameter
`Width` is used primarily as the value for `width=` attribute. Its loading
depends entirely on `Layout` parameter and the output types. In plain HTML,
`Width` only loads when `Layout` is set to `responsive-amp-only` and
`responsive-height-only`. However, in AMP HTML, `Width` is always loaded.

It will be filled in as a string. Example values:

```Markdown
"2048"
"1024"
```

You should use the desired rendering width on the page, **NOT** the actual image
width. However, it's good to ensure the actual width is the same as your value.


### `Height` Parameter
`Height` is used primarily as the value for `height=` attribute. Its loading
depends entirely on `Layout` parameter and the output types. In plain HTML,
`Width` only loads when `Layout` is set to `responsive-amp-only` and
`responsive-width-only`. However, in AMP HTML, `Height` is always loaded.

It will be filled in as a string. Example values:

```Markdown
"1152"
"600"
```

You should use the desired rendering width on the page, **NOT** the actual image
width. However, it's good to ensure the actual width is the same as your value.


### `IsMap` Parameter
`IsMap` is used primarily as the trigger to include `ismap` attribute. This
parameter is only used for plain HTML where the `<img>` is a sub-child/wrapped
by a link.

This parameter will be ignored in AMP HTML output.

It is a boolean string. Example values:

```Markdown
"false"
"true"
```

The recommended value is to set it to `"false"` if you are not using it.


### `LoadingMode` Parameter
`LoadingMode` is used primarily as the value for `loading=` attribute in `<img>`
tag. It is meant for informing the browser to load the image immediately or to
lazy-load by view. Visit
[here](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/img) for more
info.

This parameter will be ignored in AMP HTML output.

It is a string. Example values:

```Markdown
"eager"
"lazy"
```


### `SourceSet` Parameter
`SourceSet` is used primarily as the value for `srcset=` attribute in `<img>`.
It is meant for browser to select the appropriate image source based on screen
conditions.

This parameter will be ignored in AMP HTML output.

It is a string. Example values:

```Markdown
"www.example.com/img/bg/img_480w.svg 480w,www.example.com/img/bg/img_800w.svg 800w,www.example.com/img/bg/img_1024w.svg 1024w,/img/bg/img_2048w.svg 2048w,/img/bg/img_4096w.svg 4096w"
```

If you do not want to use it, set it to empty string (`""`).

Due to Hugo's shortcode quote limitation and no line breaks, you need to
consolidate all of them before setting this field.


### `Layout` Parameter
`Layout` is used primarily as the value for `layout=` attribute in `<amp-img>`.
This is a shortcode specific parameter that configures the `Width` and `Height`
parameters to appear in AMP HTML and Plain HTML accordingly.

There are 4 values in string for this parameter:

```Markdown
"responsive"             -- ensure both AMP HTML and Plain HTML are responsive.
                            Width and Height are ignored in Plain HTML.
"responsive-amp-only"    -- ensure AMP HTML is responsive. Plain HTML retains
                            its Width and Height attributes when set.
"responsive-height-only" -- ensure AMP HTML is responsive. Plain HTML retains
                            its Width attribute when set while Height is
                            ignored.
"responsive-width-only"  -- ensure AMP HTML is responsive. Plain HTML retains
                            its Height attribute when set while Width is
                            ignored.
```

You must always set this to ensure the image is rendered properly.



### `Class` Parameter
`Class` is used primarily as the value for `class=` attribute in both `<img>`
and `<amp-img>` tags. It is a full long string.



### `Sizes` Parameter
`Sizes` is used to specify the image sizes based on specific media query on the
fly. Similar to `SourceSet` parameter, it is a single long string. Example
values:

If you do not want to use it, set it to empty string (`""`).

{{< note "info" "Availability" >}}
This parameter is only available starting from version `v1.13.0`.
{{< /note >}}

```Markdown
(max-width: 480px) 20vw,(max-width: 900px) 75vw
```



### `Caption` Parameter
`Caption` is used to specify figure caption alongside image. It will appear at
the bottom of the image.

If you do not want to use it, set it to empty string (`""`).

{{< note "info" "Availability" >}}
This parameter is only available starting from version `v1.13.0`.
{{< /note >}}

```Markdown
Figure 1.1 - My Picture Caption Description Here
```
