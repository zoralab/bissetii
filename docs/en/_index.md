+++
date = "2019-03-10T13:16:02+08:00"
title = "Bissetii Hugo Theme and Go Template Module"
description = """
Bissetii is an "All-in-One" Hugo Theme and Go Template Module for static website
content creation and quick web application development. It is designed with
minimalist direction and keeping things simple, functioning, and nice to use.
"""
keywords = [ "bissetii", "hugo", "site", "website", "theme", "go package" ]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[amp]
modules = [
	# Example: "amp-sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
type = "image/png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
type = "image/png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
name = "Home"
parent = ""
pre = "🏡"
weight = 1


[schema]
selectType = "bissetii.data.schema.SoftwareApplication"
+++

# {{% param "title" %}}
{{< image "Bissetii Project | Welcome"
	"/img/logo/s2048-color.webp"
	"150"
	"150"
	"false"
	"lazy"
	""
	""
>}}

{{% param "description" %}}




## Why Choose Bissetii
Here are some of the great reasons to choose Bissetii as your website
development Hugo theme module or Go web styling package:

{{< cards "features" "grid" "embed" >}}




## Roadmap
From time to time, Bissetii identified and executed a wide varieties of features
implementations for better use in the future. Here are some of our recognizable
features implemented across Bissetii versions.

| Name                                        | Status      | Starting Version |
|:--------------------------------------------|:------------|:-----------------|
| Mobile-first Design                         | ✅Completed | `v1.0.0`         |
| Sass-Driven Styling                         | ✅Completed | `v1.0.0`         |
| Social Network Open Graph Supoort           | ✅Completed | `v1.0.0`         |
| Css-Only Styling Direction                  | ✅Completed | `v1.0.0`         |
| Integrate [Milligram](https://milligram.io/) | ✅Completed | `v1.0.0`        |
| Removed Snapcraft Supports                  | ✅Completed | `v1.9.0`         |
| RenderHTML Shortcodes                       | ✅Completed | `v1.9.0`         |
| PayPalME Shortcodes                         | ✅Completed | `v1.9.0`         |
| Support Emoji Rendering                     | ✅Completed | `v1.9.0`         |
| Support Code Syntax Highlighting            | ✅Completed | `v1.9.0`         |
| Support DEB Server Release                  | ✅Completed | `v1.9.0`         |
| Support `repo-docs` File Structure          | ✅Completed | `v1.10.0`        |
| Documents All HTML Components               | ✅Completed | `v1.11.0`        |
| Integrate [Shields.io](https://shields.io/) Badges with Shortcode | ✅Completed | `v1.11.0` |
| Support Definition List:`<dl>`              | ✅Completed | `v1.11.0`        |
| Support Custom `404.html` for Hugo          | ✅Completed | `v1.11.0`        |
| Support Print Rendering                     | ✅Completed | `v1.11.0`        |
| Support Sass Config-Driven Design           | ✅Completed | `v1.11.0`        |
| Support SEO Robots Handling                 | ✅Completed | `v1.12.0`        |
| Integrate [Accelerated Mobile Pages](https://amp.dev/)  | ✅Completed | `v1.12.0` |
| Integrate [Google Analytics](https://analytics.google.com)  | ✅Completed | `v1.12.0` |
| Integrate [Schema.org](https://schema.org/) | ✅Completed | `v1.12.0`        |
| Facilitate Social Media Sharing Outlook Management | ✅Completed | `v1.12.0` |
| Single SHELL Script Automated Setup         | ✅Completed | `v1.12.0`        |
| Support Card Component                      | ✅Completed | `v1.12.1`        |
| Support `<iframe>` Component                | ✅Completed | `v1.12.1`        |
| Support Carousel Component                  | ✅Completed | `v1.12.1`        |
| Support Note Component                      | ✅Completed | `v1.12.1`        |
| Decentralized CI Development                | ✅Completed | `v1.12.4`        |
| Non-Extended Hugo Support                   | ✅Completed | `v1.12.4`        |
| Dart Sass Compilation                       | ✅Completed | `v1.12.4`        |
| Thumbnail Page Listing                      | ✅Completed | `v1.12.4`        |
| CDN Facilitated CSS Files                   | ✅Completed | `v1.13.0`        |
| Component-Driven Development                | ✅Completed | `v1.13.0`        |
| CSS Variables Customization                 | ✅Completed | `v1.13.0`        |
| Integrate [PWA](https://web.dev/progressive-web-apps/) | 📅Working | ... |
| Integrate Go Template Module | 📅Working | ... |
| Support Cookie Notices | 📅Working | ... |
| Support RPM Server Release | 📅Working | ... |
| Documents How-to use Bissetii for hosting DEB Server  | 💡idea | `N/A` |

For latest information, please check out the Issues board at:
https://gitlab.com/ZORALab/bissetii/issues




## Powered by Simple Tools
Bissetii is powered by many simple tools integrated together in order to produce
great results. Here are some of the core technologies:

{{< svg/shieldTag "core language" "html" "#1313c3" >}}
{{< svg/shieldTag "core language" "css" "#1313c3" >}}
{{< svg/shieldTag "core language" "javascript" "#1313c3" >}}
{{< svg/shieldTag "core language" "sass" "#1313c3" >}}
{{< svg/shieldTag "core language" "AMP-html" "#1313c3" >}}
{{< svg/shieldTag "core language" "hugo" "#1313c3" >}}
{{< svg/shieldTag "core language" "bash" "#1313c3" >}}
{{< svg/shieldTag "core language" "go" "#1313c3" >}}
{{< svg/shieldTag "core tool" "BaSHELL" "#004d40" >}}




## Quality Assured Via Continuous Integrations
To ensure that our customers are getting updates in an incremental and stable
way, Bissetii uses [GitLab CI](https://docs.gitlab.com/ee/ci/quick_start/)
technologies to manage its quality assurance from release to even developer
experimentation stage.

Feel free to check out their current status here:

| Branch   | Test Status | Test Coverage |
|:---------|:------------|:--------------|
| `main` | {{< image "Main Pipeline Status"
	"https://gitlab.com/ZORALab/bissetii/badges/main/pipeline.svg"
	"116"
	"20"
	"false"
	"lazy"
	""
	""
>}} | {{< image "Main Coverage Report"
	"https://gitlab.com/ZORALab/bissetii/badges/main/coverage.svg"
	"120"
	"20"
	"false"
	"lazy"
	""
	""
>}} |
| `staging` | {{< image "Staging Pipeline Status"
	"https://gitlab.com/ZORALab/bissetii/badges/staging/pipeline.svg"
	"116"
	"20"
	"false"
	"lazy"
	""
	""
>}} | {{< image "Staging Coverage Report"
	"https://gitlab.com/ZORALab/bissetii/badges/staging/coverage.svg"
	"120"
	"20"
	"false"
	"lazy"
	""
	""
>}} |
| `next` | {{< image "Next Pipeline Status"
	"https://gitlab.com/ZORALab/bissetii/badges/next/pipeline.svg"
	"116"
	"20"
	"false"
	"lazy"
	""
	""
>}} | {{< image "Next Coverage Report"
	"https://gitlab.com/ZORALab/bissetii/badges/next/coverage.svg"
	"120"
	"20"
	"false"
	"lazy"
	""
	""
>}} |

To find Bissetii source codes, it is available at the following repositories:

| Name          | Location                            |
|:--------------|:------------------------------------|
| GitLab.com    | https://gitlab.com/ZORALab/bissetii |




## Contributors
Bissetii is not just a single person work but a culmulative of large amount of
knowledge and wisdom. Here are some of the critical contributors for making
Bissetii a success.


### Maintainers
Here are the maintainers who manage Bissetii's code releases and its health.

1. (Holloway) Chew, Kean Ho (`kean.ho.chew@zoralab.com`) - 2019 to present


### Developers
Here are the developers who works tirelessly to enable and keeping Bissetii
up-to-date.

1. (Holloway) Chew, Kean Ho (`kean.ho.chew@zoralab.com`) - 2019 to present


### Knowledge Experts
Here are the knowledge experts contributed insights and wisdom for Bissetii to
be successful.

1. [Milligram Project](https://github.com/milligram/milligram) - 2019
2. [CSS Tricks](https://css-tricks.com/) - 2019
3. [Sass](https://sass-lang.com/) - 2019
4. [AMP](https://amp.dev/) - 2020
5. [Schema.org](https://schema.org/) - 2020
6. [Jonathan Snook's Rem Unit](https://snook.ca/archives/html_and_css/font-size-with-rem) - 2021



### Our Sponsors
Here are the sponsors that provide financial supports to enable Bissetii team
to continue keeping Bissetii alive:

{{< cards "sponsors" "grid" "default" >}}




## Where to Go Next?
Interested to use Bissetii for your next project? Feel free to:


{{< link "/getting-started" "this" "" "" "button" >}}
Get Started
{{< /link >}}
