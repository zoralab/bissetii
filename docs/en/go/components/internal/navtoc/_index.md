+++
# WARNING: This markdown is autogenerated by bot. Do not edit it manually!
date = "2021-09-01T21:41:26+08:00"
title = "navtoc"
description = """
Package navtoc is for rendering table of contents navigation list.

The HTML template codes are written in Go's string format under the
corresponding group. This allows one to embed the HTML codes instead of
parsing it from file.

"""
keywords = [ "navtoc", "go", "bissetii", "docs" ]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Go Documents"

[thumbnails.1]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Go Documents"


[menu.main]
parent = "Interfaces | Go"
name = "components/internal/navtoc"
pre = "📘"
weight = 1


[schema]
selectType = "WebPage"
+++

# Package navtoc
```go
import "gitlab.com/zoralab/bissetii/pkg/components/internal/navtoc"
```
Package navtoc is for rendering table of contents navigation list.

The HTML template codes are written in Go's string format under the
corresponding group. This allows one to embed the HTML codes instead of
parsing it from file.





## Constants
All Bissetii Compressed Compiled CSS
```go
const (
	CSSAMP = `nav.nav-toc{width:var(--nav-toc-width);padding:var(--nav-toc-padding);border:var(--nav-toc-border);border-radius:var(--nav-toc-border-radius);background:var(--nav-toc-background)}nav.nav-toc .title{margin:var(--nav-toc-title-margin);font-size:var(--nav-toc-title-font-size);text-align:var(--nav-toc-title-text-align);font-weight:var(--nav-toc-title-font-weight)}nav.nav-toc ol{margin:var(--nav-toc-list-margin);padding:var(--nav-toc-list-padding);counter-reset:nav-toc-item;list-style-type:none}nav.nav-toc ol>li{margin:var(--nav-toc-li-margin);counter-increment:nav-toc-item}nav.nav-toc ol>li p,nav.nav-toc ol>li a{display:inline}nav.nav-toc ol>li:before{content:counters(nav-toc-item, ".") ".";margin:0 1rem 0 0}`

	CSSCritical = `nav.nav-toc{width:var(--nav-toc-width);padding:var(--nav-toc-padding);border:var(--nav-toc-border);border-radius:var(--nav-toc-border-radius);background:var(--nav-toc-background)}nav.nav-toc .title{margin:var(--nav-toc-title-margin);font-size:var(--nav-toc-title-font-size);text-align:var(--nav-toc-title-text-align);font-weight:var(--nav-toc-title-font-weight)}nav.nav-toc ol{margin:var(--nav-toc-list-margin);padding:var(--nav-toc-list-padding);counter-reset:nav-toc-item;list-style-type:none}nav.nav-toc ol>li{margin:var(--nav-toc-li-margin);counter-increment:nav-toc-item}nav.nav-toc ol>li p,nav.nav-toc ol>li a{display:inline}nav.nav-toc ol>li:before{content:counters(nav-toc-item, ".") ".";margin:0 1rem 0 0}`

	CSSTablet = ``

	CSSDesktop = ``

	CSSWidescreen = ``

	CSSPrint = `nav.nav-toc{break-inside:avoid}nav.nav-toc li{break-inside:avoid}`

	CSSVariables    = ``

	CSSVariablesAMP = ``
)
```
Constants for managing the package as a whole.
```go
const (
	// Name is the block name (`{{- define "this-name" -}}`).
	Name = "nav-toc"

	// AMPName is the AMP HTML block name.
	AMPName = internal.AMPPrefix + Name

	// ParametersComment are the list of comment headers
	// describing inputs.
	ParametersComment = `
{{- /* .Class            = Nav-TOC class attribute.                   */ -}}
{{- /* .ID               = Nav-TOC id attribute.                      */ -}}
{{- /* .Title            = Nav-TOC title in HTML format.              */ -}}
{{- /* .List             = Nav-TOC primary list.                      */ -}}
{{- /*   .HTML           = HTML content for the item.                 */ -}}
{{- /*   .Items          = Sub list. Repeat List structure. Optional. */ -}}
`
)
```
Dependencies are the dependent codes for building independent template.

This is useful for rendering portable template usage like Hugo partials
where they do not share a common definition source.
```go
const ( (
	// DepHTML is the vanilla HTML output type.
	DepHTML = `
{{- define "` + Name + "-item" + `" -}}
<li {{- if .ID }} id="{{- .ID -}}"{{- end -}}

	{{- if .Class }} class="{{- .Class -}}"{{- end -}}

	{{- if .Style }} style="{{- .Style -}}"{{- end -}}>

	{{- if ne (len .Items) 0 }}

		<ol>

			{{- range .Items }}

				{{ template "` + Name + `-item" . -}}

			{{- end -}}

		</ol>

	{{- else }}

		{{ .HTML }}

	{{- end }}
</li>
{{ end -}}
`

	// DepAMPHTML is the Accelerated Mobile Pages HTML output
	// type.
	DepAMPHTML = `
{{- define "` + AMPName + "-item" + `" -}}
<li {{- if .ID }} id="{{- .ID -}}"{{- end -}}

	{{- if .Class }} class="{{- .Class -}}"{{- end -}}

	{{- if .Style }} style="{{- .Style -}}"{{- end -}}>

	{{- if ne (len .Items) 0 }}

		<ol>

			{{- range .Items }}

				{{ template "` + AMPName + `-item" . -}}

			{{- end -}}

		</ol>

	{{- else }}

		{{ .HTML }}

	{{- end }}
</li>
{{ end -}}
`
)
```
Full HTML codes for rendering templates without needing to parse file.
```go
const (
	// HTML is the vanilla HTML output type.
	HTML = `
<nav class="nav-toc {{- if .Class }} {{ .Class -}}{{- end -}}"
{{- if .ID }}

	id="{{- .ID -}}"
{{- end -}}>

	{{- if .Title }}{{- .Title -}}{{- end }}

	<ol>

		{{- range .List.Items }}

			{{ template "` + Name + `-item" . -}}

		{{- end -}}

	</ol>
</nav>
`

	// AMPHTML is the Accelerated Mobile Pages HTML output type.
	AMPHTML = `
<nav class="nav-toc {{- if .Class }} {{ .Class -}}{{- end -}}"
{{- if .ID }}

	id="{{- .ID -}}"
{{- end -}}>

	<ol>

		{{- range .List.Items }}

			{{ template "` + AMPName + `-item" . -}}

		{{- end -}}

	</ol>
</nav>
`
)
```




## Data
```go
type Data struct {
	// Class is the additional CSS class tag for NavTOC.
	Class string

	// ID is the ID tag for NavTOC.
	ID string

	// List is the main list of items.
	List *nav.Item

	// Title is the HTML format title for the NavTOC list.
	Title string
}
```
Data is the data structure for rendering the component.
