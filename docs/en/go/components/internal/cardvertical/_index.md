+++
# WARNING: This markdown is autogenerated by bot. Do not edit it manually!
date = "2021-09-01T21:41:26+08:00"
title = "cardvertical"
description = """

"""
keywords = [ "cardvertical", "go", "bissetii", "docs" ]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Go Documents"

[thumbnails.1]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Go Documents"


[menu.main]
parent = "Interfaces | Go"
name = "components/internal/cardvertical"
pre = "📘"
weight = 1


[schema]
selectType = "WebPage"
+++

# Package cardvertical
```go
import "gitlab.com/zoralab/bissetii/pkg/components/internal/cardvertical"
```





## Constants
All Bissetii Compressed Compiled CSS
```go
const (
	CSSAMP = `.Card.vertical{--card-grid:"thumbnail" minmax(0, max-content) "content" auto "cta" minmax(0, max-content)/minmax(0, max-content);-webkit-transition:var(--animation-timing-fast);-ms-transition:var(--animation-timing-fast);-moz-transition:var(--animation-timing-fast);-o-transition:var(--animation-timing-fast);transition:var(--animation-timing-fast)}.Card.vertical .content{--card-content-padding:1rem}.Card.vertical:hover,.Card.vertical:focus,.Card.vertical:focus-within{--card-filter:drop-shadow(0 0 1rem var(--color-grey-700))}`

	CSSCritical = `.Card.vertical{--card-grid:"thumbnail" minmax(0, max-content) "content" auto "cta" minmax(0, max-content)/minmax(0, max-content);-webkit-transition:var(--animation-timing-fast);-ms-transition:var(--animation-timing-fast);-moz-transition:var(--animation-timing-fast);-o-transition:var(--animation-timing-fast);transition:var(--animation-timing-fast)}.Card.vertical .content{--card-content-padding:1rem}.Card.vertical:hover,.Card.vertical:focus,.Card.vertical:focus-within{--card-filter:drop-shadow(0 0 1rem var(--color-grey-700))}`

	CSSTablet = ``

	CSSDesktop = ``

	CSSWidescreen = ``

	CSSPrint = ``

	CSSVariables    = ``

	CSSVariablesAMP = ``
)
```