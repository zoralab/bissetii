+++
date = "2020-09-11T20:52:06+08:00"
title = "Getting Started"
description = """
Let's get started with deploying Bissetii in your next development. Bissetii
itself comprises of many core components integrated together using various core
technologies. That way, Bissetii can deliver modular results with common
packing distributions.
"""
keywords = ["getting", "started", "hugo", "bissetii"]
draft = false
type = ""
redirectURL="/en/hugo/getting-started/"
layout = "redirect"

[section]
sortBy = ""


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/getting-started/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/getting-started/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"



[menu.main]
parent = ""
name = "Getting Started"
pre = "🛫"
weight = 2


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}




## Factors to Consider
Before using Bissetii, you must consider the following factors with readiness
in mind.



### 1. Bissetii Is An Actively Developed Hugo Theme and Go Template Module
Bissetii is specifically designed for 3 primary roles:

1. As a Hugo Theme Module
2. As a Go template design module
3. As a CSS framework module

When used as a Hugo Theme module, web developers and designers can independently
design the Sass and HTML components or building a static website without needing
to learn Go programming language.

When used as a Go template design module, Go developer can develop web interface
quickly without needing to write and design user interface modules from scratch.

When used as a CSS framework module, it allows developers to use Bissetii user
interface design outside of both Hugo and Go interfaces.

That being said, Bissetii module would be updated in an extra ordinary manner.
Hence, you should always keep track of them and update your applications from
time-to-time.



### 2. Not For Beginner
Although Bissetii keeps its design to be simple and nice, it is not for
beginner. Bissetii consistently being upgraded with respect to the latest search
engine optimization (SEO) implementations and W3C amendment.

Hence, when using Bissetii, you **MUST** already mastered your SEO skills and
W3C knowledge.



### 3. Minimalist Design
Bissetii is a complete rewritten project referenced from
[Milligram](https://milligram.io/) CSS Framework Project. The goal is to create
a maintainable, compliance to W3C standard, minimialistic web styling framework
for both static website generation and Go application.

To date, Bissetii only uses HTML and CSS to create its user interface component.
Hence, it frees the entire Javascript for designer or app developer to fully
maximize their app design.



### 4. Snapcraft and Ubuntu PPA Support Removed
As of version `v1.9.0`, both [Snapcraft.io](https://snapcraft.io/bissetii) and
[Ubuntu PPA](https://launchpad.net/~zoralab/+archive/ubuntu/bissetii)
distribution providers are no longer supported but can't be removed by the
platform providers.

Hence, you will not get any support if you use any of them to install Bissetii.




## Installing Bissetii Program
Regardless of your use intentions, there is a chance you need to use the
Bissetii program to manage the complicated "Hugo and Theme" setup. Bissetii
specifically designed a program to keep things sane and consistent to use.
Here are some of the ways to install Bissetii program.




## Epilogue
Once done, please feel free to explore other sections to construct your website.
Thank you for choosing Bissetii.
