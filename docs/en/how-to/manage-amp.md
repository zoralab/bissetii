+++
date = "2020-06-25T12:44:54+08:00"
title = "How To Manage AMP As Hugo Theme"
description = """
Bissetii supports Accelerated Mobile Pages (AMP) by default. This helps SEO and
earns the lightning badge on search result list and improves mobile visitors'
user experience.
"""
keywords = ["manage", "amp", "hugo", "bissetii"]
draft = false
type = ""
redirectURL="/en/hugo/manage-amp/"
layout = "redirect"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "sidebar",
]


[creators.ZORALab]
type = "Organization"
name = "ZORALab Team"

[creators.KeanHo]
type = "Person"
name = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/how-to/manage-amp/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii AMP"

[thumbnails.1]
url = "/img/thumbnails/how-to/manage-amp/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii AMP"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "disabled"
name = "Manage AMP (OLD)"
pre = "⚡"
weight = 1


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}

Starting from version `v0.12.0`, Accelerated Mobile Pages (AMP) is supported and
enabled by default.




## How Bissetii Works with AMP
By default, Bissetii configured Hugo to use the `.amp.html` file extension for
AMP rendering as per documentation instead of a dedicated `amp` directory. That
also means that for each page, you get 2 html files as follow:

```
path/
    index.html
    index.amp.html
```

Through this way, user will only need to remember the same sitemap and only
query the extension format to switch between vanilla HTML and AMP HTML.




## Enable/Disable AMP Support
By default, AMP is enabled using the following config:

```
pattern : <engine>/config/_default/outputs.toml
local   :          config/_default/outputs.toml
repo-doc:   .sites/config/_default/outputs.toml
```

with the following setup to **ENABLE** AMP:
```toml
home = [ "HTML", "AMP" ]
page = [ "HTML", "AMP" ]
section = [ "HTML", "AMP" ]
taxonomy = [ "HTML", "AMP" ]
term = [ "HTML", "AMP" ]
```

To **DISABLE** it, remove all the `"AMP"` items. Example:
```toml
home = [ "HTML" ]
page = [ "HTML" ]
section = [ "HTML" ]
taxonomy = [ "HTML" ]
term = [ "HTML" ]
```




## Configure Hugo's Meta Settings
{{< note "warning" "Warning" >}}
You must ensure that you know what you're doing. Otherwise, any
misconfigurations can break AMP with disasterous effect.
{{< /note >}}


Bissetii set AMP configurations in 2 configuration files:

1. `mediaTypes.toml`
2. `outputFormats.toml`


### `mediaTypes.toml`
The location for this configuration is located at:

```
pattern : <engine>/config/_default/mediaTypes.toml
local   :          config/_default/mediaTypes.toml
repo-doc:   .sites/config/_default/mediaTypes.toml
```

This file is responsible for defining the AMP file specification. By default,
Bissetii only defined a new `text/amphtml` media type in order to facilitate
`amp.html` file extension. Here is the default configurations:

```toml
["text/amphtml"]
suffixes = [ "amp.html" ]
```

You can modify accordingly based on Hugo's specification available here:
https://gohugo.io/templates/output-formats/


### `outputFormats.toml`
The location for this configuration is located at:

```
pattern : <engine>/config/_default/outputFormats.toml
local   :          config/_default/outputFormats.toml
repo-doc:   .sites/config/_default/outputFormats.toml
```

This file is responsible for binding the output types to the media types. For
AMP, there is a strict requirement for specifying and configuring the
relationship across both vanilla HTML and AMP HTML. This file is also
responsible to remove the `amp` pathing, allowing the `index.amp.html` to be
generated next to `index.html`.

Here is the default configurations:

```toml
[HTML]
rel="canonical"

[AMP]
mediaType = "text/amphtml"
path = ""
```

You can modify accordingly based on Hugo's specification available here:
https://gohugo.io/templates/output-formats/




## Adding AMP Extension Javascript Modules
To add AMP extension Javascript modules, you can add it via 2 ways depending on
the area of effect. Starting form version `v1.12.0`, Bissetii facilitates
modules inclusion in a single
`layouts/partials/amp/metas/modules/extensions.html`. You just need to include
the proper `custom-element` as the listing value. Example, for `amp-sidebar`,
it will generate:

```html
<script async
	custom-element="amp-sidebar"
	src="https://cdn.ampproject.org/v0/amp-sidebar-XXXX.js">
</script>
```



### Add to All Pages
To add AMP module globally, you can add it into the `modules` list in
`includes.AMP` TOML map table. The file is located in:

```
pattern  :          data/bissetii/amp.toml
directory:    docs/.data/
repo-doc :    docs/.data/bissetii/amp.toml
```

Once added, the modules will be included in **ALL** AMP pages.

Example:

```toml {linenos=table,hl_lines=["3-5"],linenostart=1}
# All styling, javascripts and assets inclusions for AMP HTML

modules = [                               #--> include AMP Javascript Module
        "amp-sidebar"
]

...
```



### Add to One Specific Page
**This is our recommended path per AMP specifications**. You can add page
specific AMP module inside the page's front-matter, under `[amp]` map table's
`modules` array variable. Here is an example using `toml` front-matter format:

```toml {linenos=table,hl_lines=["3-9"],linenostart=1}
+++
...
[amp]
modules = [
        # Example: "amp-sidebar",
	"amp-analytics",
	"amp-form",
	...
]

...
+++
```

Once added, the modules will be included in that specific AMP page.
