+++
date = "2020-01-30T13:39:38+08:00"
title = "How To Include External CSS File"
description = """
Apart from using Bissetii styling, you can also include your very own CSS files
for additional stylings. In most cases, you can include an external CSS designs
add-on to extend from Bissetii's simplified inclusion mechanism.
"""
keywords = ["include", "css", "hugo", "bissetii"]
draft = false
type = ""
redirectURL="/en/hugo/include-css/"
layout = "redirect"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[amp]
modules = [
        # Example: "amp-sidebar",
]


[creators.ZORALab]
type = "Organization"
name = "ZORALab Team"

[creators.KeanHo]
type = "Person"
name = "Holloway Chew Kean Ho"


[thumbnail]
url = "/img/thumbnails/default.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "disabled"
name = "Include CSS into Hugo"
pre = "🌈"
weight = 1


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}

This feature is available starting from `v1.11.0` onwards.




## Understand How Bissetii Loads CSS
Depending on your Bissetii version, the CSS loading varies accordingly.

### Version `v1.13.0` and above
Bissetii now fully loads all CSS files (including its `bissetii-main.min.css`)
**ASYNChonously**.


### Version `v1.12.5` and below
{{< note "warning" "Important Notice" >}}
Support for CSS inclusion version `v1.12.5` and below is scheduled to be
stopped starting **June 30, 2021**. While the codes remain available, the team
will no longer answer any questions related to the matter.

**Please DO your CSS includes configurations migrations as soon as possible.**
{{< /note >}}
Bissetii loads each CSS components both synchonous and asynchnonous manner.
The sequences are as follows:

1. Main Sass Codes (**SYNChnonous loading**)
2. Plain CSS Inclusion (**ASYNChonous loading**)
3. Asset Concatenation (**ASYNChonous loading**)



## Main Sass/Scss Configuration Files
{{< note "error" "HEADS UP" >}}
Starting from version `v1.12.5` and above, Bissetii uses external software
`dart-sass` to compile its Sass/Scss files.

That means that Bissetii **no longer use Hugo extended feature to do the job**.

Hence,

1. Altering the Sass/Scss codes while Hugo is running will not reflect on
your page.
2. Hugo extended will report a lot of errors as its internal `libsass` Sass
features are overly lagging behind `dart-sass`.
{{< /note >}}

Bissetii compile `assets/main.sass` and `assets/amp.sass` using its library
`assets/css/bissetii`. Hence, **you should leave them as it is**.



## Including CSS
Depending on your Bissetii version, the configurations varies accordingly.


### Version `v1.13.0` onwards
Starting from `v1.13.0`, Bissetii uses a single data file located in
`data/bissetii/assets/css.toml` to organize the CSS file inclusion. This is
mainly to facilitate additional security features like SRI integrity checking
and cross-origin ID.

Each CSS file metadata and how it should be loaded is stated in each entry
complying to the following pattern:

```toml {linenos=table,hl_lines=[],linenostart=1}
[0]
Algorithm = "include"
URL = "/css/myCSS.min.css"
CrossOrigin = "anonymous"
Integrity = "sha512-awraerrhe....reagaeg"
Disabled = false
Media = "all"
OnLoad = 'this.media="all"'           # be careful with the quote
```

* **[Basic Index ID]** - Keep it numerical for positioning purposes.
* **Algorithm** - The selected processing algorithm to handle this CSS. The
                  values can be the following:
    1. `compress` will concatenate all CSS files with the same process
        algorithm into a single large CSS file.
    2. `include` will add each CSS files with the same process algorithm with
       dedicated `<link>` tag.
* **URL** - the CSS URL source. The URL can be Bissetii's compatible relative
            URL. Depending on the **Algorithm**, your CSS file should be stored
            in different directory:
    1. for `compress`, place it inside `assets/` directory.
    2. for `include`, place it inside `static/` directory.
* **CrossOrigin** - Cross Origin ID. The ID for cross-origin verification when
            downloading the CSS from other origin. This requires a valid
            **Integrity** in order to work. The possible values are:
    1. `anonymous` - anyone can download via cross-origin as long as **SRI**
                     is verified.
    2. `use-credentials` - use request credentials for verifications.
* **Integrity** - The algorithmic shasum value for cross-origin request file
            verification. It need a valid **CrossOrigin** in order to work.
* **Disabled** - The `disabled` attribute flag. Set `true` to disable this
            CSS to load.
* **Media** - The `media=` attribute for downloading and loading the stylesheet.
* **OnLoad** - Javascript `onload=` HTML attribute. This allows one to
               asynchonously load the CSS as long as the process algorithm
               permits.




### Version `v1.12.0` to Version `v1.12.5`
{{< note "warning" "Important Notice" >}}
Support for CSS inclusion version `v1.12.5` and below is scheduled to be
stopped starting **June 30, 2021**. While the codes remain available, the team
will no longer answer any questions related to the matter.

**Please DO your CSS includes configurations migrations as soon as possible.**
{{< /note >}}

Starting from `v1.12.0` to `v1.12.5`, Bissetii uses the TOML map table to
organize the configuration parameters inside `/data/bissetii` toml data file.

The responsible file for including the list of CSS / Sass / Scss files is
located in:

```
pattern  :                data/bissetii/includes.toml
directory:          docs/.data/bissetii/
repo-docs:          docs/.data/bissetii/includes.toml
```

#### Plain CSS Inclusion
To include plain CSS, you add the URL into the `includes.CSS` map table list.

Example:

```toml {linenos=table,hl_lines=["1-2"],linenostart=1}
[CSS]
files  = [                                   #--> just include the CSS-es
       #"https://cdn.example.com/myCSS.css"
]
```



#### Asset Concatenation
To include the Sass/Scss asset concatenations, you add the relative URL into
the `includes.CSS` map table list.

Example:

```toml {linenos=table,hl_lines=["1-2"],linenostart=1}
[CSS]
assets = [                                 #--> compile into 1 CSS
       #"css/myCSS.css"                    #--> $assetDir/css/myCSS.css
]
```




## Epilouge
Now that you know how to include external CSS into your website. If you have any
questions, feel free to raise them in our
[Issues Section](https://gitlab.com/zoralab/bissetii/-/issues).
