+++
date = "2020-01-31T10:18:21+08:00"
title = "How To Include Javascript As Hugo Theme"
description = """
While Bissetii is solely a HTML and CSS Hugo theme module, it still allows
Javascript usage. In fact, it freed up the Javascript for you to use from the
get-go. Bissetii simplified the location for such inclusion so creators only
focus on contents creation.
"""
keywords = ["include", "js", "hugo", "bissetii"]
draft = false
type = ""
redirectURL="/en/hugo/include-js/"
layout = "redirect"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[amp]
modules = [
        # Example: "amp-sidebar",
]


[creators.ZORALab]
type = "Organization"
name = "ZORALab Team"

[creators.KeanHo]
type = "Person"
name = "Holloway Chew Kean Ho"


[thumbnail]
url = "/img/thumbnails/default.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "disabled"
name = "Include JS (Old)"
pre = "💫"
weight = 1


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}

However, please keep in mind that **Javascript will not work for
Accelerated Mobile Pages (AMP) as part of its main requirement**. Hence,
please ensure your critical components do not depend on Javascript if AMP is
enabled.




## How Bissetii Loads Javascript
Javascript, especially dealing with its frameworks, usually comes in large
files. This can block downloading bandwidth when they are downloaded
synchonously with the main HTML and CSS body. Fortunately, modern browsers now
support `defer` and `async` features and
**Bissetii use both of them heavily to the point of NO synchonous loading**.


### Version `1.13.0` and above
Bissetii loads all Javascripts in the end of the `<body>` sections regardless
of its `async` and `defer` attributes.
**You still need them to manage their respective initializations**.


### Version `1.12.5` and Below
{{< note "warning" "Important Notice" >}}
Support for JS inclusion version `v1.12.5` and below is scheduled to be
stopped starting **June 30, 2021**. While the codes remain available, the team
will no longer answer any questions related to the matter.

**Please DO your JS includes configurations migrations as soon as possible.**
{{< /note >}}

Bissetii loads the `defer` Javascripts in the `<head>` section while
`async` in the end of the `<body>` section.



## Javascript Inclusion
Depending on Bissetii version, the configuration varies accordingly.



### Version `v1.13.0` Onwards
Bissetii uses a single data file located in `data/bissetii/assets/js.toml` to
organize the JS file inclusion. This is mainly to facilitate additional
security features like SRI integrity checking and cross-origin ID.

Each JS file metadata and how it should be loaded is stated in each entry
complying to the following pattern:

```toml {linenos=table,hl_lines=[],linenostart=1}
[0]
Algorithm = "compress"
ID = "script-myJS"            # file in 'assets/js/myJS.min.js'
URL = "/js/myJS.min.js"
Type = "application/x-javascript"
LoadMode = "defer"
CrossOrigin = "anonymous"
Integrity = "sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg=="
#Content = """ """
```

* **[Index ID]** - Keep it numerical for positioning purposes.
* **Algorithm** - The algorithm to process the script. It can be the following:
    1. `compress` will concatenate all CSS files with the same process
        algorithm into a single large CSS file.
    2. `include` will add each CSS files with the same process algorithm with
       dedicated `<link>` tag.
* **ID** - The optional `id=` value for HTML ID attribute.
* **URL** - The Bissetii compatible relative URL. The files location varies
           depending on the progress algorithm.
    1. for `compress`, place it inside `assets/` directory.
    2. for `include`, place it inside `static/` directory.
* **Type** - The optional `type=` value for HTML TYPE attribute.
* **LoadMode** - The script loading mode. This allows one to asynchonously load
  Javascript. It can be:
    1. `""` (nothing) - load synchnonously and execute immediately
    2. `"async"` - load asynchonously and execute immediately
    3. `"defer"` - load asynchonously and execute after the page is loaded.
* **CrossOrigin** - The ID for cross-origin request download verification. It
  need a valid **Integrity**. The value can be any of the following:
    1. `anonymous` - anyone can download via cross-origin as long as
                     **Integrity** is verified.
    2. `use-credentials`  - use credentials to perform the request alongside
                            **Integrity**.
* **Integrity** - The algorithmic shasum value for cross-origin request file
  verification. It need a valid **Cross Origin ID**.
* **Content** - The inline content of the script. This only works if **URL** is
  disabled or empty.



### Version `v1.12.0` to `v1.12.5`
{{< note "warning" "Important Notice" >}}
Support for JS inclusion version `v1.12.5` and below is scheduled to be
stopped starting **June 30, 2021**. While the codes remain available, the team
will no longer answer any questions related to the matter.

**Please DO your JS includes configurations migrations as soon as possible.**
{{< /note >}}

Bissetii relies on the 4 map table variables to configure Javascript.
The file that is responsible for configuring Javascript is located in the
following file:

```bash
pattern  :           data/bissetii/includes.toml
directory:     docs/.data/
repo-docs:     docs/.data/bissetii/includes.toml
```


#### Plain Javascript Inclusion
To include the JS, simply add the URL into `asyncFiles` or `deferFiles` under
`[includes.JS]` map table.

Example:

```toml {linenos=table,hl_lines=["1-2", "8-9"],linenostart=1}
[JS]                                      #--> vanilla HTML's Javascript assets
asyncFiles = [                            #--> just include and async-load
       # "js/myLib.js"                    #--> $assetDir/js/myLib.js
]

# ---OR---

[JS]                                      #--> vanilla HTML's Javascript assets
deferFiles = [                            #--> just include and defer-load
       # "js/myLib.js"                    #--> $assetDir/js/myLib.js
]
```


#### Asset Concatenation
To include the JS, simply add the URL into `asyncAssets` or `deferAssets` under
`[includes.JS]` map table.

Example:

```toml {linenos=table,hl_lines=["1-2", "8-9"],linenostart=1}
[JS]                                      #--> vanilla HTML's Javascript assets
asyncAssets = [                           #--> compile into 1 JS and async-load
       # "js/myLib.js"                    #--> $assetDir/js/myLib.js
]

# ---OR---

[JS]                                      #--> vanilla HTML's Javascript assets
deferAssets = [                           #--> compile into 1 JS and defer-load
       # "js/myLib.js"                    #--> $assetDir/js/myLib.js
]
```




## Epilouge
Now that you know how to include external JS into your website. If you have any
questions, feel free to raise them in our
[Issues Section](https://gitlab.com/zoralab/bissetii/-/issues).
