+++
date = "2020-02-14T18:05:54+08:00"
title = "How To Customize 404 Page As Hugo Theme"
description = """
Hugo supplies a method to create a fully customizable 404 page using its
internal feature. That also means that Bissetii has such support for creating
a fully customized 404 inheritly page as well.
"""
keywords = ["custom", "404", "hugo", "bissetii"]
draft = false
type = ""
redirectURL="/en/hugo/manage-404/"
layout = "redirect"

[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[amp]
modules = [
        # Example: "amp-sidebar",
]


[creators.ZORALab]
type = "Organization"
name = "ZORALab Team"

[creators.KeanHo]
type = "Person"
name = "Holloway Chew Kean Ho"


[thumbnail]
url = "/img/thumbnails/default.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "disabled"
name = "Custom 404 (Disabled)"
pre = "🚧"
weight = 1


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}

This feature is available since `v1.11.0`.




## Known Limitation
It is not Hugo's duty to serve `404.html` but the reverse proxy/front-end facing
server. These are the known limitations:

1. Hugo generates multi-languages `404.html` in each language directories.
   However, on common platform like Github or Gitlab, they only recognizes
   a single `404.html` at **root** directory only.
2. Hugo has no way to auto-select a specific language's `404.html` as the root
   `404.html`.




## Enable Custom 404
By default, Bissetii workrounds these limitations using the `manager.sh` script.
To enable a fully functional `404.html` page, you need to perform the following
steps.

This function is only available for `v1.7.0` `manager.sh` script and above.


### Language Selection
The manager automatically reads the **first** language code tag from your local
engine's `languages.toml` configuration file as its selection choice. The
pathing is:

```bash
config/_default/languages.toml
```

If you need to select a specific language, place the language configurations
as the first choice. Example, to select `zh-cn`'s `404.html` over `en-us`,
the `languages.toml` should have `[zh-cn]` comes first:

```toml {linenos=table,hl_lines=[1],linenostart=1}
[zh-cn]
languageCode = "zh-cn"
languageName = "Chinese (Simplified)"
contentDir = "../docs/zh-cn"
title = "Bissetii"

[en-us]
languageCode = "en-us"
languageName = "English"
contentDir = "../docs/en-us"
title = "Bissetii"
```




## Customizing 404 Page
The location of the 404 page is located at:
```bash
layouts/404.html
```

You should only supply `main` code blocks will do. The rest would follow
Bissetii's templates. Due to the limitations introduced by Hugo above, it's
better to be very creative with the design without locking yourself to a
specific language.




## Conclusion
That's all for rendering custom `404.html` page.
