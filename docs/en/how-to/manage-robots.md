+++
date = "2020-06-22T21:30:06+08:00"
title = "How To Manage Crawler Robots As Hugo Theme"
description = """
Like all SEO enabled sites, there should be a default robots management across
each page and via the singular "robots.txt" for crawlers to crawl in. Starting
from version `v1.12.0`, Bissetii supports both page-level meta robot tag and
the single "robots.txt".
"""
keywords = ["manage", "robots", "hugo", "bissetii"]
draft = false
type = ""
redirectURL="/en/hugo/manage-crawler/"
layout = "redirect"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "sidebar",
]


[creators.ZORALab]
type = "Organization"
name = "ZORALab Team"

[creators.KeanHo]
type = "Person"
name = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "disabled"
name = "Manage Robots (Disabled)"
pre = "🤖"
weight = 1


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}




## Customizing `robots.txt`
Depending on Bissetii version, Bissetii renders `robots.txt` differently.


### Version `v1.13.0` and Above
Having Hugo fixed its `robots.txt` placement in
https://github.com/gohugoio/hugo/issues/5160 (tested with Hugo version
`v0.78.2`), Bissetii can now safely reverting back to using Hugo renderer to
create `robots.txt` as documented in Hugo.

To customize `robots.txt`, you can create your "User Agent" data file inside
your `data/bissetii/robots` data directory. The pathing, depending on your
configurations, is as follow:

```
filepath pattern:             data/bissetii/robots/<User-Agent>.toml
repo-docs:              docs/.data/bissetii/robots/<User-Agent>.toml
```

The filename serves as the value for `User-agent` field. The only exception is
`all` where it will be renamed as `*` (`User-agent: *`).

You can create as many `<User-Agent>.toml` as you want
(e.g. `all.toml`, `GoogleBot.toml`, etc). They will all be processed into a
single `robots.txt` file.

By default, Bissetii supplies `all.toml` to define sitemap location as
required by most search engines optimization.


#### Defining Rules for An User-Agent
You can define the rules for that particular crawler robot using TOML format.
For relative URL construction, Bissetii supplies `{{ .BaseURL }}` as a
placeholder to be replaced with the actual `.Site.BaseURL`.

```toml {linenos=table,hl_lines=[2, "7-10", "15-18", "22"],linenostart=1}
# Relative to .Site.BaseURL (placeholder {{ .BaseURL }}) for "Sitemap" field.
Sitemap = "{{ .BaseURL }}/sitemap.xml"


# Multi-values "Allow" field. The value will be placed in as it is.
# Placeholder can be used.
Allow = [
	"/",
	"/en/",
]


# Multi-values "Disallow" field. The value will be placed in as it is.
# Placeholder can be used.
Disallow = [
	"/en-us/",
	"/zh-cn/",
]


# "Crawl-delay" field.
Crawl-delay = 5
```

As an example, say the filename is `GoogleBot.toml`, the above will be converted
into:

```txt {linenos=table,hl_lines=[],linenostart=1}
User-agent: GoogleBot
Allow: /
Allow: /en/
Crawl-delay: 5
Disallow: /en-us/
Disallow: /zh-cn/
Sitemap: {{< link "/sitemap.xml" "this" "url-only" />}}
```



### Version `v1.12.0` to `v1.12.5`
{{< note "warning" "Important Notice" >}}
Support for managing robot version `v1.12.0` to `v1.12.5` is scheduled to be
stopped starting **June 30, 2021**. While the codes remain available, the team
will no longer answer any questions related to the matter.

**Please DO your configurations migrations as soon as possible.**
{{< /note >}}
Bissetii facilitates `robots.txt` in `static/robots.txt`. The location is at
static root directory. By default, Bissetii has the following content:

```txt
User-agent: *
```

To override Bissetii's default file, you can create the same `robots.txt` in
the same path.

The `enableRobotsTXT` will be disabled due to Hugo's multi-language bug in
`config/_default/config.toml`. Hence, the guide for `robots.txt` if Hugo's main
documentations no applied into these Bissetii versions.




## Meta Robot Tag
Bissetii also supports page specific meta tag for robot management. To add a
robot rules tag, you need to add each robot's rules into the `[robots]`
front-matter TOML map-table. Example:

```toml {linenos=table,hl_lines=["3-6", "8-10"],linenostart=1}
+++
...
[robots]
[robots.googleBot]
name = "googleBot"
content = "noindex, nofollow"

[robots.twitterBot]
name = "twitterBot"
content = "noindex"

...
+++
```

This will render the HTML as:

```html
<meta name="googleBot" content="noindex,nofollow" />
<meta name="twitterBot" content="noindex" />
```

You can create multiple robot tags for this specific page.

{{< note "warning" "Be Careful" >}}
Bissetii does not run through each robot's rules especially dealing with
`nofollow`. You need to be careful as it might create conflicts with sitemap
page definitions. To be on the safe side, use `robots.txt`.
{{< /note >}}




## Epilogue
That's all for how to manage robot crawlers for your website as Bissetii
Hugo Theme user. If you have any questions, please feel free to place your
query in our [Issues Section](https://gitlab.com/zoralab/bissetii/-/issues).
