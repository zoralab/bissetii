+++
date = "2020-07-10T18:16:15+08:00"
title = "How To Manage Social Sharing Display in Bissetii Hugo Theme Module"
description = """
All website in 21st century must be very user friendly in terms of sharing
across social media. Bissetii integrated most of the common social network
requirements into a single management tool. This allows user to easily configure
the sharing outlook without needing to read a lot of technical specifications.
"""
keywords = ["manage", "social", "sharing", "hugo", "bissetii"]
draft = false
type = ""
redirectURL="/en/hugo/manage-social-network/"
layout = "redirect"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[amp]
modules = [
        # Example: "amp-sidebar",
]


[creators.ZORALab]
type = "Organization"
name = "ZORALab Team"

[creators.KeanHo]
type = "Person"
name = "Holloway Chew Kean Ho"


[thumbnail]
url = "/img/thumbnails/default.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "disabled"
name = "Manage Social Sharing Display (disabled)"
pre = "🤝"
weight = 1


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}

This feature is available since version `v1.12.0`.




## Validation Linters
Social sharing display can be tested in various tools depending on which social
network. For starters:

1. Twitter Card - https://cards-dev.twitter.com/validator
2. Facebook Share - https://developers.facebook.com/tools/debug




## Thumbnail Images
Thumbnail images are used mainly to display the small thumbnails during sharing.
This is applied to `og:image`.


### Storage Location
The default location for saving thumbnail images are located in the following:

```
pattern  :                    static/img/thumbnails/<name>.png
directory:                    static/img/thumbnails/
repo-docs:                    static/img/thumbnails/<name>.png
url      :       https://domain.name/img/thumbnails/<name>.png
```

Bissetii facilitates its default image in: `static/img/thumbnails/default.png`.


### Requirements
The thumbnail should be **strictly**:

1. In `JPEG` or `PNG` format.
2. In `1200x1200` pixels (**recommended**) or `1200x628` pixels.


### Configure Site Thumbnails
The default location for the default thumbnail configurations is located in:

```
pattern  :                    data/bissetii/thumbnails.toml
directory:              docs/.data/
repo-docs:              docs/.data/bissetii/thumbnails.toml
```

Inside the configuration file, you can fill in the thumbnail image properties
as follows:

```toml {linenos=table,hl_lines=["3-13"],linenostart=1}
# All Social Media Thumbnail Image Configurations

[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"
```

For **internal relative URL**, you need to provide the relative URL beginning
with `/`.


### Customize Page Specific Thumbnails
You can also specify page-specific thumbnails. To do that, you can fill in the
thumbnail image properties as follows into the page's front-matter:

```toml {linenos=table,hl_lines=["4-14"],linenostart=1}
+++
...

[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

...
+++
```




## Twitter Sharing Cards
Twitter sharing cards are used mainly to provide Twitter the necessary user data
of the website. The configuration data file is located in:

```
pattern   :        data/bissetii/vendors/Twitter.toml
directory :  docs/.data/
repo-docs :  docs/.data/bissetii/vendors/Twitter.toml
```


### Change Twitter Sharing Type
To set the Twitter sharing card type, you can change this setting by updating
the `card` field with the available
[Twitter Card Types](https://developer.twitter.com/en/docs/tweets/optimize-with-cards/overview/abouts-cards).

By default, Bissetii uses `summary_large_image` card type.

```toml {linenos=table,hl_lines=[5],linenostart=1}
# All Twitter Configurations

[Cards]
Card = "summary_large_image"
Username = "zoralab" # Twitter handle without '@'
```

To set the post owner, You can change this setting by updating the `Username`
with your Twitter handle (without the `@`). By default, Bissetii set the field
as empty.




## Epilogue
That's all for how to manage social sharing display for Bissetii Hugo Theme. If
you have any question, please feel free to reach us at our
[Issues Section](https://gitlab.com/zoralab/bissetii/-/issues).
