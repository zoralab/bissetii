+++
date = "2019-12-18T13:57:50+08:00"
title = "How To Highlight Code Syntaxes As Hugo Theme"
description = """
Thanks to Hugo's built-in support of using its Chroma to highlight code syntaxes,
Bissetii can safely supports it inheritly starting from version "v1.9.0". This
allows various useful code snippets to be rendered with colorful highlighting
for better visual perception.
"""
keywords = ["highlight", "code", "syntaxes", "syntax", "hugo", "bissetii"]
draft = false
type = ""
redirectURL="/en/components/gochroma/"
layout = "redirect"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[amp]
modules = [
        # Example: "amp-sidebar",
]


[creators.ZORALab]
type = "Organization"
name = "ZORALab Team"

[creators.KeanHo]
type = "Person"
name = "Holloway Chew Kean Ho"


[thumbnail]
url = "/img/thumbnails/default.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "redirect"
name = "Highlight Code Syntaxes"
pre = "🖍️"
weight = 1


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}




## Patching Configuration File
Bissetii added a new configuration file for Code Syntaxes Highlighter. If your
Hugo engine is not up-to-date to minimum `v1.9.0` version, you will need to
add this configuration file manually to this location:

```
local directory    :       /config/_default/markup.toml
repo-docs directory: .sites/config/_default/markup.toml
```

Otherwise, you cannot control the highlighter and may break your Bissetii
default styling.

The file is available at:
https://gitlab.com/zoralab/bissetii/-/blob/master/config/_default/markup.toml




## Enable/Disable Code Syntaxes Highlighting
Unfortunately, there is no way to enable/disable this feature.




## Composing Code Snippet with Highlighter
There are 2 ways to do it:

1. Using code fencing (recommended)
2. Using Hugo shortcode


### Using Code Fencing (Recommended)
You do not need to modify your `markup.toml` configuration file just for
rendering all the time. Code fencing is something like:

````md {linenos=table,hl_lines=[1],linenostart=1}
```go {linenos=table,hl_lines=[4,"7-9"],linenostart=1}
package main

import (
	"fmt"
)

func main() {
	fmt.Printf("Hello World, %s\n", "Cheri")
}
```
````

That renders:

```go {linenos=table,hl_lines=[4,"7-9"],linenostart=1}
package main

import (
	"fmt"
)

func main() {
	fmt.Printf("Hello World, %s\n", "Cheri")
}
```

That being said, you can style your snippet on-the-spot. These are the
available options for code fencing:

{{< note "default" "Available Options" >}}
1. `linenos` = one of this: `true`, `false`, `table`, or `inline`
   (**NOTE**: `table` is copy-paste friendly)
2. `hl_lines` = array of numbers (can be digit or range) in string
3. `linenostart` = starting number in digits
{{< /note >}}



### Using Hugo Shortcode
Hugo provides shortcode as an alternative. You may refer to this
link: https://gohugo.io/content-management/syntax-highlighting/#highlight-shortcode




## Customizing Highlighting Style
Inside the `markup.toml`, there is a field called `style`. For Bissetii, it
is default to `"monokailight"`. Of course, if you want to alter it, simply
browse the gallery here and change it to that title.

1. [Summary Text Styling](https://xyproto.github.io/splash/docs/all.html)
2. [Full Text Styling](https://xyproto.github.io/splash/docs/longer/all.html)
