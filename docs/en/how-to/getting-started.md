+++
date = "2020-09-11T14:11:50+08:00"
title = "Getting Started For Hugo"
description = """
Bissetii natively supports Go Hugo as a theme module. In fact, Bissetii started
off as a theme module before expanded to many of its other deployments. With
Go Hugo as its primary development environment, developers can easily construct,
test, and reuse web components without too much dependencies.
"""
keywords = ["hugo", "getting", "started", "getting started", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "sidebar",
]


[creators.ZORALab]
type = "Organization"
name = "ZORALab Team"

[creators.KeanHo]
type = "Person"
name = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/how-to/getting-started/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/how-to/getting-started/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "disabled"
name = "Getting Started | Hugo"
pre = "🛫"
weight = 1


[schema]
selectType = "WebPage"
+++

# How to {{% param "title" %}} As Bissetii Module
{{% param "description" %}}




## Factors to Consider
Apart from the general consideration factors, before using Bissetii, you must
consider the following Hugo-specific factors with readiness in mind.



### 1. Fixed Bissetii File Structure
Unlike standard Hugo, Bissetii uses the following file structure:

```bash {linenos=table,hl_lines=["2-4",6,8,10],linenostart=1}
.                              # your git root repository
├── docs/                      # hugo docs/ directory
│   ├── .data/                 # hugo data/ directory
│   │   └── bissetii           # bissetii local settings
│   │        └── ...
│   ├── en-us/                 # default United States English docs/ directory
│   │   └── ...
│   └── zh-cn/                 # default China Mandarin docs/ directory
│       └── ...
└── .sites/                    # local hugo directory (a.k.a. "engine")
    ├── archetypes
    ├── config
    ├── ...
    ├── static
    └── themes
```

The 2 key directories are:

1. `docs` - holding the Hugo `data` and `content` directories.
2. `.sites` - the Hugo engine for generating your Hugo static websites.

This setup is also known as **Repository Documentation (`repo-docs`)**. It
organizes and reuses `docs/` directory as its content source, allowing user
to create contents once and works everywhere. Instead of clogging the repository
with Hugo internal contents, Bissetii grouped them into `.sites` "engine"
directory instead, making the repository free for other development.



### 2. Breaking the Standard Markdown Convention
Due to Hugo's multiple output types and standard Markdown convention
limitations, Bissetii can only comply to them and forces everyone to use
Bissetii's shortcodes whenever possible.

The take-away is that Hugo is capable of rendering a single Markdown content
into multiple outputs like vanilla HTML, AMP HTML, RSS, etc without worries.

The give-back is you need to break the standard Markdown writing convention with
Bissetii's Hugo shortcodes.

Long story short, if you use Bissetii's shortcodes in your Markdown (which you
have to), you will eventually create dependencies on Hugo and Bissetii Theme
module and your Markdown will no longer be portable across other Hugo themes.




## 1. Install Dependencies
Bissetii would not be able to work without its dependencies. Hence, you need
to install all of them into your system.



### 1.1. Hugo
Starting from version `1.12.4`, Bissetii can use non-extended Hugo as its
primary engine. For earlier versions, Bissetii
**STRICTLY REQUIRES HUGO EXTENDED** version to operate properly.

To install one, you can donwload it from its
[Github Release Section](https://github.com/gohugoio/hugo/releases).
The end-result is that `hugo` command is available at terminal. Example:

```bash {linenos=table,hl_lines=[2],linenostart=1}
$ hugo version
Hugo Static Site Generator v0.XX.YY/extended linux/amd64 BuildDate: ...
```

The minimum supporting version is `0.73.X`.



### 1.2. Git
Bissetii requires `git` made available for downloading the theme module and
obtain pages' last-modified timestamp.

The end-result is that `git` command is available at terminal and
`git version` reports the extended version. Example:

```bash {linenos=table,hl_lines=[2],linenostart=1}
$ git version
git version XX.YY.ZZ
```




## 2. Setup Hugo CMS Repository
There are a few ways to setup a Hugo repository with Bissetii `repo-docs` file
structures. Here are some of the methods available at your disposal.



### Using Bissetii Program
If you have bissetii Program installed, you can easily setup Hugo repository by
issuing the following command:

```bash {linenos=table,hl_lines=[1,5],linenostart=1}
$ bissetii --install
```



### Quick Setup
To make a quick setup without Bissetii program, you can use the following
command depending on `curl` or `wget` availability.

```bash {linenos=table,hl_lines=[1,5],linenostart=1}
$ curl "https://zoralab.gitlab.io/bissetii/bissetii.sh" | bash -s -- --install

# OR

$ wget -qO - 'https://zoralab.gitlab.io/bissetii/bissetii.sh' | bash /dev/stdin --install
```



### Manual Setup
To setup your Hugo repository with Bissetii as a module, you can simply
`git clone` or `git submodule add` it into the Hugo's `theme/` directory.
The following commands works:

```bash {linenos=table,hl_lines=[1,5],linenostart=1}
$ git clone https://gitlab.com/zoralab/bissetii.git ./path/to/theme

# OR

$ git submodule add https://gitlab.com/zoralab/bissetii.git ./path/to/theme
```

Then, you need to ensure your config file lists bissetii as one of the theme
module. Example, for `config/_default/config.toml`:

```toml {linenos=table,hl_lines=[2],linenostart=1}
...
theme = [ "bissetii" ]
...
```




## Wrapping Up
Now that you have your Hugo setup accordingly, you are ready to develop your
Hugo static website with Bissetii. Once ready, you may proceed to the next
section:

{{< link "/how-to/create-new-content" "this" "" "" "button pinpoint" >}}
Create Your New Content
{{< /link >}}
