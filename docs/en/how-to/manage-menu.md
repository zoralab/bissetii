+++
date = "2019-10-14T13:35:34+08:00"
title = "How To Manage Menu As Hugo Theme"
description = """
Managing navigation menu for Bissetii Hugo Theme module is quite a flexible
and configurable task. Bissetii prepared some data-driven configuration files
to configure not only the menu items but their corresponding icons too.
"""
keywords = ["organize", "menu", "hugo", "bissetii" ]
draft = false
type = ""
redirectURL="/en/hugo/manage-menu/"
layout = "redirect"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[amp]
modules = [
        # Example: "amp-sidebar",
]


[creators.ZORALab]
type = "Organization"
name = "ZORALab Team"

[creators.KeanHo]
type = "Person"
name = "Holloway Chew Kean Ho"


[thumbnail]
url = "/img/thumbnails/default.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "disabled"
name = "Manage Menu (Disabled)"
pre = "📓"
weight = 1


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}



## Changing Menu Icons
Starting from version `v1.12.0` onwards,  all icons embedded inside Sass were
removed and revert to using HTML and images. This includes the navigation bar
menu (`☰`) and close icon (`✕`).

To change those icons, simply set the relative path in both `openIcon` and
`closeIcon` inside `data/bissetii/navbars.toml`'s `[sidebar]` map table:

Example:

```toml {linenos=table,hl_lines=["3-5"],linenostart=1}
# all navbars related configurations

[sidebar]
openIcon = "/img/css/menu-icon.svg"
closeIcon = "/img/css/close-icon.svg"
```

The default values (if the parameters are not available) would be:

```
# Close Icon (✕)
URL Path:               /img/css/close-icon.svg
Storage Location: static/img/css/close-icon.svg

# Menu Icon (☰)
URL Path:               /img/css/menu-icon.svg
Storage Location: static/img/css/menu-icon.svg
```



## Add Icon to Menu Items
Starting from version `v1.12.0` onwards, all itemized icons can be added using
the `pre =` variable in the page's front-matter. Default is none. Here is an
example:

```toml {linenos=table,hl_lines=[6],linenostart=1}
+++
...
[menu.main]
parent = "B) How-to (Hugo)"
# name = "{{- $title -}}"
pre = "✤"
...
+++
```

If the variable is not available, simply add one under the `[menu.main]`
section. You should be able to see the menu.

Currently, **only Unicode symbols or emoji are supported**.




## Organize Menu in Hugo
There are 2 types of menus in Hugo:

1. Basic title menu
2. "Parent" menu

By default, given every items in the menu having same weight, they are arranged
in an **alphabetically order** manner.

### Basic Title Menu
To order a custom menu order, you can manipulate the weight number in the
`front matter` of the post:

```toml
[menu.main]
parent = "B) How-to"
weight = 1
```
The lower the weight number, the higher the order. Example, for a list below:

```
Alpha     = 2
Bravo     = 1
Charlie   = 4
Delta     = 3
```

It would be rendered as:

```
Brovo
Alpha
Delta
Charlie
```

### Parent Menu
Parent menu are the "grouped" menu items. Currently, Hugo does not offer any
method for organizing parent menu. iOne has to develop a quite sophisicated
renderer for such menu ordering using "params".

Hence, as a quick workaround, you can order the parent menu easily using the
alphabet as the index. Example, for the list below:

```
Alpha   --> B) Alpha
Bravo   --> A) Bravo
Charlie --> D) Charlie
Delta   --> C) Delta
```

It would be rendered as:

```
A) Bravo
B) Alpha
C) Delta
D) Charlie
```


### Blocklisting Menu Item
Starting from version `v1.12.1`, Bissetii allows one to block main menu list
items. To block an item, you need to add the item into
`data/bissetii/navbars.toml`'s `[blocklist]` listing. Then, set its value into
`true`.

Here is an example: Bissetii blocks all item name's `redirect` since it makes no
sense to render them on the main navigation bar. Also, the user wants to block
an item called `Nav List Item Name` so the `toml` data file should have
something like:


```toml {linenos=table,hl_lines=["5-7"],linenostart=1}
# all navbars related configurations

...

[blocklist]
"redirect" = true
"Nav List Item Name" = true
```




## As A Summary
These are the known methods for organizing menus without complicated plugins.
If you need one, feel free to inform Hugo team at our
[Issues Section](https://github.com/gohugoio/hugo/issues)!
