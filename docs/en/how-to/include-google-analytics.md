+++
date = "2020-06-22T23:07:43+08:00"
title = "How To Include Google Analytics As Hugo Theme"
description = """
Google Analytics has long became the No.1 tool for web traffic analysis.
Starting from version `v1.12.0`, depending on output format, Bissetii supports
Google Analytics as per its specifications.
"""
keywords = ["include",
	"google",
	"analytics",
	"google-analytics",
	"hugo",
	"bissetii"]
authors = ["Bissetii Team"]
draft = false
type = ""
redirectURL="/en/hugo/include-google-analytics/"
layout = "redirect"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[amp]
modules = [
        # Example: "amp-sidebar",
]


[creators.ZORALab]
type = "Organization"
name = "ZORALab Team"

[creators.KeanHo]
type = "Person"
name = "Holloway Chew Kean Ho"


[menu.main]
parent = "disabled"
name = "Include Google Analytics (old)"
pre = "📈"
weight = 1


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}

Based on the specification, Bissetii complies to its requirements by including
its Javascript in the `<head>` section.




## Adding Google Analytics ID
To enable Google Analytics, simply add your Google Analytics ID into
`docs/.data/bissetii/vendors/Google.toml` data file inside your `data/`
directory.

{{< note "note" "Note" >}}
Bisetii's default data directory is set to `docs/.data/`. Please check your
Hugo config for pathing.
{{< /note >}}

The field you're looking for is called `[Analytics].ID`. Here is an example:

```toml {linenos=table,hl_lines=[4],linenostart=1}
# All Google Related Configurations

[Analytics]
ID = "UA-1234565789"  # GA-ID
```

Setting this field as blank results with not including Google Analytics into
your website.




## Google Analytics for AMP
At this point of time, Google Analytics for AMP is overly customized for
Bissetii. We have **yet to support this feature for Hugo**.




## Epilogue
That's all for including Google Analytics into the page. If you have any query,
please feel free to reach us at our
[Issues Section](https://gitlab.com/zoralab/bissetii/-/issues).
