+++
date = "2020-07-22T13:56:36+08:00"
title = "How to Create New Content As Hugo Theme"
description = """
With a properly configured Bissetii, one can now focuses on writing the first
content efficiently. By default, Bissetii chooses United States English as its
primary language and packages all the contents under a single directory. Then,
one can distribute this primary content for multi-lingual translations.
"""
keywords = ["create", "page", "content", "hugo", "bissetii"]
draft = false
type = ""
redirectURL="/en/hugo/manage-page/"
layout = "redirect"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "sidebar",
]


[creators.ZORALab]
type = "Organization"
name = "ZORALab Team"

[creators.KeanHo]
type = "Person"
name = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "disabled"
name = "Create New Content (disabled)"
pre = "📝"
weight = 1


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}

{{< note "warning" "Disclaimer" >}}
This page assumed you already know how to use
[Hugo program](https://gohugo.io/) like configurations and content writing.
{{< /note >}}




## 1. Cross-Checking Configurations
Although not needed, it is always good to check the current repository's Hugo
configurations to know its current conditions and setup. That way, we can ensure
the contents we creating is aligned to the repository settings.

Please rest assured that this cross-checking only needed to be done once per
Bissetii release.



### 1.1. Corrects Your Hugo Configurations
Firstly, you need to check the base URL is the correct one. This URL is the
final publication website. The location would be:

```bash
pattern  :         config/_default/config.toml
directory:  .sites/config/
repo-docs:  .sites/config/_default/config.toml
```

You need to verify the information are correct, such that:

1. `baseURL` is the publication base URL.
2. `theme` has `bissetii` as one of the listed item.
3. `archetypeDir` is pointing to the desired location (default is `archetypes`).
4. `dataDir` is pointing to the desired location (default is `../docs/.data`).
5. `staticDir` is pointing to the correct directory (default is `staticDir`).
6. `layoutDir` is pointing to the correct directory (default is `layouts`).
7. `themesDir` is pointing to the correct directory (default is `themes`).
8. `publishDir` is pointing to the correct directory (default is `public`).
9. `i18nDir` is pointing to the correct directory (default is `i18n`).

{{< note "note" "Note" >}}
Pathing is relative from inside `.sites`. So if the path is `archetypes`, it
refers to `.sites/archetypes`.
{{< /note >}}

Example:

```bash {linenos=table,hl_lines=[1,3,"5-12"],linenostart=1}
baseURL = "https://www.mycompany.com"
...
theme = [ "bissetii" ]
...
archetypeDir = "archetypes"
assetDir = "assets"
dataDir = "../docs/.data"
staticDir = ["static"]
layoutDir = "layouts"
themesDir = "themes"
publishDir = "public"
i18nDir = "i18n"
```

If the `baseURL` is incorrect, you might want to change it to the final
publication version.



### 1.2. Correct Language Configurations
{{< note "error" "Important" >}}
Starting from version `v1.13.0`, Bissetii team corrected her mistake by removing
regional code fragments from the language codes. **This breaks all older
versions if they are not updated accordingly**.

The original intention for default was to serve as language. Please note that
**you can still create regional code fragments** like:

1. `en-us` - English for United States only
2. `zh-Hans-CN` - Simplified Chinese for China only
{{< /note >}}

By default, Bissetii set the default content location in the root repository's
`docs` directory with `en` (English) set as default and `zh-hans`
(Simplified Chinese) as the second option. The location would be:

```bash
pattern  :         config/_default/languages.toml
directory:  .sites/config/
repo-docs:  .sites/config/_default/languages.toml
```

You need to verify the information are correct, such that:

1. `langaugeCode` is pointing to the correct symbol.
2. `languageName` is labelled with English translated name.
3. `contentDir` is the location of the language-specific contents.
4. `title` is the website title appeared in that specific language.

```toml {linenos=table,hl_lines=["2-5","8-11"],linenostart=1}
[en]
languageCode = "en"
languageName = "English"
contentDir = "../docs/en"
title = "My New Hugo Site"

[zh-hans]
languageCode = "zh-hans"
languageName = "Chinese (Simplified)"
contentDir = "../docs/zh-hans"
title = "我的Hugo网站"
```



### 1.3. Configure Additional Bissetii
Bissetii packed with a lot of advanced features focusing on SEO. Please refers
to each feature from [How-to (Hugo)]({{< link "/how-to" "this" "url-only" />}})
list for effective configurations.




## 2. Launch A Development Server
The next thing to do is to launch the development server to render your content.
In a Bissetii generated repository via our installation instructions
(called `repo-docs`), you can easily launch a server. To do that, open a new
terminal or command prompt and use the conventional Hugo command:

```bash {linenos=table,hl_lines=["2-6"],linenostart=1}
$ cd .sites                           # enters the engine
$ hugo server --buildDrafts \
        --disableFastRender \
        --bind "localhost" \
        --baseURL "localhost" \
        --port 8080
```

Alternatively, available starting form version `v1.12.0`, if you happen to
install `bissetii` package into your operating system, you can use `bissetii`
automated command right at the root repository:

```bash {linenos=table,hl_lines=[1],linenostart=1}
$ bissetii -r
```

For version `v1.11.0` and earlier, you can make use of the deprecated local
`manager.sh` script. Example:

```bash {linenos=table,hl_lines=[2],linenostart=1}
$ cd .sites                           # enters the engine
$ ./manager.sh -r
...
$ cd ..                               # exit the engine
```




## 3. Create New Page
Now that the configurations are set correctly and the development server is
running, it's time to create the content page.



### 3.1. Generate New Page From Engine
The first thing to do is enter the local Hugo site and create one using
`hugo new` command. If you create the repository using `bissetii.sh` (or
`bissetii` program), the engine is located inside `.sites` directory.

The command is as follows:

```bash {linenos=table,hl_lines=[2,3],linenostart=1}
$ cd .sites                                 # enter the local hugo engine
$ hugo new path/to/url-pathing-name.md
/...<your workspace>.../docs/en-us/path/to/url-pathing-name.md created
$ cd ..                                     # exit the local hugo engine
```

{{< note "note" "Note" >}}
Use dash for filename since it will formulate the URL location. If we follow
the example above, it would be:
`https://your.domain.com/en-us/path/to/url-pathing-name/`.
{{< /note >}}

Alternatively, you can use `bissetii` command helper to quickly create the page
at root repository (available since `v1.12.1`):

```bash {linenos=table,hl_lines=[1],linenostart=1}
$ bissetii -cmd new path/to/url-pathing-name.md
/...<your workspace>.../docs/en-us/path/to/url-pathing-name.md created
```



### 3.2. Edit The New Page
Your new page is located as specified. Head over to edit it with your favourite
editor.


#### 3.2.1. Fill In The Page Front-Matter
Before charging into writing the content, you need to make sure the page's
front-matter's values are filled correctly. This helps you organize the purpose
of the page and fulfilling the SEO requirements. A few things to take note are:

1. Create a sensible `title`. Your site title will be appended behind this
   `title` and will replace `{{%/* param "title" */%}}` automatically if being
   used in the page.
2. Sort out the page's `keywords`. This set your content priority and focus.
3. Write a sensible page `description` (around 150-300 characters). This will
   be used across all description meta tags and as a first paragraph, replacing
   `{{%/* param "description" */%}}` automatically if being used in the page.
4. Set the `[creators.name]` information correctly. This will be appended or
   be overwitten from the site creators.
5. Set the `[menu.main]` information correctly. You might want to update:
   1. `parent` group.
   2. `pre` for icon.
6. Set `[thumbnail]` and `[schema]` for page sharing configurations.
7. Set `draft` to `false` once you are ready to publish.
8. Select the correct [Schema]({{< link "/how-to/customize-schema-org/"
   "this" "url-only" />}}) for page data populations.

Here's an example for this page:

```toml {linenos=table,hl_lines=[],linenostart=1}
+++
date = "2020-07-22T13:56:36+08:00"
title = "How to Create New Content As Hugo Theme Module"
description = """
With a properly configured Bissetii, one can now focuses on writing the first
content efficiently. By default, Bissetii chooses United States English as its
primary language and packages all the contents under a single directory. Then,
one can distribute this primary content for multi-lingual translations.
"""
keywords = ["create", "page", "content", "hugo", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[amp]
modules = [
        # Example: "amp-sidebar",
]


[creators.ZORALab]
type = "Organization"
name = "ZORALab Team"

[creators.KeanHo]
type = "Person"
name = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "B) How-to (Hugo)"
name = "Create New Content"
pre = "X"
weight = 1


[schema]
selectType = "WebPage"
+++
```


#### 3.2.2. Write Your Contents
Once done, you can now write your page content using the Markdown. Some
important note to remember is:

1. Make use all Bissetii's
   [Shortcodes]({{< link "/shortcodes" "this" "url-only" />}}) as much as
   possible. Some compulsory elements like `link`, `form`, and `image` are
   vital to use mainly because Bissetii generates multiple outputs like vanilla
   HTML, AMP HTML, and so on simultenously and each of them has its own unique
   HTML codes.



### 3.3. Commit Your Content
Once you're stastifed with your page creation, you may proceed to commit and
publish it using your `git`.




## 4. Building Static Website Artifact
To build the static website artifact, you can use the following standard
commands:

```bash {linenos=table,hl_lines=[2],linenostart=1}
$ cd .sites                           # enters the engine
$ hugo --minify
...
$ cd ..                               # exit the engine
```

Alternatively, available starting form version `v1.12.0`, if you happen to
install `bissetii` package into your operating system, you can use `bissetii`
automated command right at the root repository:

```bash {linenos=table,hl_lines=[1],linenostart=1}
$ bissetii -B
```

You should get a new content directory specified by your `publishDir` path
(default is `public` directory outside engine).

{{< note "default" "Backward Compatibilities" >}}
For version `v1.11.0` and earlier, you can make use of the deprecated local
`manager.sh` script. Example:

```bash {linenos=table,hl_lines=[2],linenostart=1}
$ cd .sites                           # enters the engine
$ ./manager.sh -B
...
$ cd ..                               # exit the engine
```
{{< /note >}}




## 5. Publish to Server
Once you have your artifact, you can publish it directly to your hosting server
based on our supported platforms. Currently, we are supporting:

1. [GitHub Pages](https://docs.github.com/en/github/working-with-github-pages)
2. [GitLab Pages](https://about.gitlab.com/blog/2016/04/07/gitlab-pages-setup/)



### 5.1. Create Publish Branch
{{< note "warning" "Heads Up" >}}
**Skip this step** if:
1. You already done it.
{{< /note >}}

Bissetii recommends using the dedicated publication branch method for the
following reasons:

1. avoid duplicated artifacts git commit and has the ability to keep 1 artifact
   per project.
2. avoid conflicting with `docs/` path for local project documentations.
3. easy and clear instruction.

Hence, this is a one-time setup for the publish branch. To do that, all you need
is to create the publication branch using the following commands:

```bash {linenos=table,hl_lines=[],linenostart=1}
$ git checkout --orphan <publication branch name>
$ git reset --hard
$ git clean -fd
$ git commit --allow-empty -m "Init"
$ git push origin <publication branch name>:<publication branch name>
$ git checkout <back to your work branch>
```

{{< note "warning" "GitHub Pages" >}}
For [Github Pages](https://docs.github.com/en/github/working-with-github-pages),
you **MUST** create your branch name as `gh-pages`. Hence, your commands are:

```bash {linenos=table,hl_lines=[],linenostart=1}
$ git checkout --orphan gh-pages
$ git reset --hard
$ git clean -fd
$ git commit --allow-empty -m "Init"
$ git push origin gh-pages:gh-pages
$ git checkout <back to your work branch>
```
{{< /note >}}

{{< note "default" "GitLab Pages" >}}
For [GitLab Pages](https://about.gitlab.com/blog/2016/04/07/gitlab-pages-setup/),
to keep things simple and unified, Bissetii
**recommends to follow GitHub Pages**.

Bissetii will generate its own `.gitlab-ci.yml` for the publish branch so you
do not need to manually create one for it.
{{< /note >}}



### 5.2. Publish This Artifact
With a publication branch ready, you may proceed to publish the artifact.
Starting from version `1.12.4`, Bissetii facilitates a single line command to
execute the publications. The command pattern is as follows:

```bash {linenos=table,hl_lines=[],linenostart=1}
$ bissetii --publish <OPTIONAL:action> --target "<your branch name>"
```

There are a number of `<OPTIONAL:action>` available at your disposal.


#### Nothing Provided and Default
The default or when nothing is provided as `<OPTIONAL:action>`, Bissetii
resorts to retain historical commits and add the new publication on top of it.

For example, if your branch name is `gh-pages`, then your command would be:

```bash {linenos=table,hl_lines=[],linenostart=1}
$ bissetii --publish --target "gh-pages"
```


#### Clean Action
If `clean` is provided as  `<OPTIONAL:action>`, Bissetii will clean the entire
publication branch historical commits and keep the new publication as a single
commit on top of it.

This is especially useful for those *who can firmly reproduce older publications
via git tagging and wanted to conserve storage spaces from unnecessary
duplications*.

For example, if your branch name is `gh-pages`, then your command would be:

```bash {linenos=table,hl_lines=[],linenostart=1}
$ bissetii --publish clean --target "gh-pages"
```




## 6. Wrapping Up
That’s all for creating a new content from scratch using Bissetii as Hugo Theme
module. You can repeatedly do this guide for every new content without needing
to worry about breaking things up. If you have any questions, please feel free
to ask in our [Issues Section](https://gitlab.com/zoralab/bissetii/-/issues).
