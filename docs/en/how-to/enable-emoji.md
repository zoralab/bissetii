+++
date = "2019-12-18T11:43:52+08:00"
title = "How to Enable Emoji Rendering As Hugo Theme"
description = """
Bissetii supports emoji rendering starting from version v1.9.0 when used as Hugo
Theme. It is enabled by default and uses 'Noto Color Emoji' font set as its
primary artwork.
"""
keywords = ["enable", "emoji", "hugo", "bissetii"]
draft = false
type = ""
redirectURL="/en/components/root/#emoji"
layout = "redirect"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "sidebar",
]


[creators.ZORALab]
type = "Organization"
name = "ZORALab Team"

[creators.KeanHo]
type = "Person"
name = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "disabled"
name = "disabled"
pre = "😄"
weight = 1


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}

Currently, Bissetii only serves `Noto Color Emoji` either sourcing from:

1. **CDN Network** - requires network connection; always up-to-date. Fallback
to this mode if localized version is unavailable.
2. **Localized Version** - works locally; may be outdated. Manual maintenance
and updating efforts required.

For CDN Network, the file is available at:
```html
https://gitcdn.xyz/repo/googlefonts/noto-emoji/master/fonts/NotoColorEmoji.ttf
```




## Enable/Disable Rendering
There are different mechanism to enable/disable emoji rendering depending on
your Bissetii version.

### Version `v1.13.0` and above
Enable/Disable rendering is entirely depends on the data located inside
`data/bissetii/emoji.toml`. By default, it defines a single child that has
something as such:

```toml {linenos=table,hl_lines=["1-4"],linenostart=1}
[0]
Name = "emoji"
Src = [
	"/fonts/NotoColorEmoji.ttf",
	"https://gitcdn.xyz/repo/googlefonts/noto-emoji/master/fonts/NotoColorEmoji.ttf",
]
```

1. **LINE 1** - the items are arranged in array form. Keep the numeric naming.
2. **LINE 2** - `Name`'s value is always hardcoded to `emoji`.
3. **LINE 3** - `Src` is the list of source URLs for your emoji fonts.
4. **LINE 4** - Bissetii compatible relative URL is accepted for local linking.
                You are free to add as many as you want with your own URLs.

By default, Bissetii keeps and serves a localized version in
`static/fonts/NotoColorEmoji.ttf`.


### Version `v1.12.5` and below
{{< note "warning" "Important Notice" >}}
Support for Sass emoji is scheduled to be stopped starting **June 30, 2021**.
While the codes remain available, the team will no longer answer any questions
related to the matter.
{{< /note >}}

There are 3 steps to fully enable emoji rendering. You will need to enable both
of them in order to have good experience.

For Localized Path, the file should be made available at:
```
Local directory    :              static/css/fonts/NotoColorEmoji.ttf
Repo-docs directory:       .sites/static/css/fonts/NotoColorEmoji.tff
URL                : https://domain.name/css/fonts/NotoColorEmoji.ttf
```

To enable/disable it, you can refer to
[Emoji Component - Enabling Emoji]({{< link "/components/emoji#enabling-emoji"
"this" "url-only" />}})



### Markdown Encoding Configurations
Hugo provided a configuration to enable emoji encoding. You need to add:
```toml
enableEmoji = true
```
into your local `config/_default/config.toml`. This allows you to write
emoji codes in your Markdown without needing to extract their unicodes
explictly.

To disable it, set it to `false`:
```toml
enableEmoji = false
```




## Composing Emoji
To write emoji, simply use the `:emoji:` pattern in your Markdown. You may
refers to the
[Emoji Cheat Sheet](https://www.webfx.com/tools/emoji-cheat-sheet/) or
[Emoji Manual](https://unicode.org/emoji/charts/full-emoji-list.html) for some
guidance. Example:

| Code                   | Unicode                        | Emoji                |
|:----------------------:|:------------------------------:|:--------------------:|
| :`grinning`:           | `U+1F600`                      | :grinning:           |
| :`white_check_mark`:   | `U+2705`                       | :white_check_mark:   |
| :`x`:                  | `U+274C`                       | :x:                  |
| :`woman_raising_hand`: | `U+1F64B U+200D U+2640 U+FE0F` | :woman_raising_hand: |
| :`man_raising_hand`:   | `U+1F64B U+200D U+2642 U+FE0F` | :man_raising_hand:   |

Other than that, `try and error` would be your best friend.
