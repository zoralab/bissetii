+++
date = "2019-12-23T16:42:01+08:00"
title = "How To Hugo Shortcode As Hugo Theme"
description = """
Occassionally, you would want to print the shortcode instructions into your
content as example. Fortunately, Hugo has a built-in method to perform such
shortcode escaping and Bissetii supports such feature alongside with Hugo.
"""
keywords = ["escape", "hugo", "shortcode", "hugo", "bissetii"]
draft = false
type = ""
redirectURL = "/en/hugo/escape-shortcode/"
layout = "redirect"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[amp]
modules = [
        # Example: "amp-sidebar",
]


[creators.ZORALab]
type = "Organization"
name = "ZORALab Team"

[creators.KeanHo]
type = "Person"
name = "Holloway Chew Kean Ho"


[thumbnail]
url = "/img/thumbnails/default.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "disabled"
name = "Escaping Hugo Shortcode (old)"
pre = "🏕️"
weight = 1


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}

To escape Hugo shortcode, you need to append the Go's comment syntax (`/*` and
`*/`) after/before the shortcode caller like this `{{</*`, `*/>}}`.




## Example
Here is an example, say the shortcode is this:

```markdown
{{</* link "/img/sample.png" "this" "url-only" */>}}
```

You do this:
```markdown
{{</*/* link "/img/sample.png" "this" "url-only" */*/>}}
```
and the entire shortcode is escaped. You can also append the comment syntax
one more time to print-out the inceptional shortcode, like the one above.
