+++
date = "2020-07-11T10:56:52+08:00"
title = "How To Manage Authorship As Hugo Theme"
description = """
When a page consists of various contributors, writers, and editors, it can be
messy to manage the authorship and copyright references. Bissetii sorted it out
for Hugo using Map data based on Schema.org structured data.
"""
keywords = ["manage", "authorship", "hugo", "bissetii"]
draft = false
type = ""
redirectURL="/en/hugo/manage-authorship/"
layout = "redirect"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[amp]
modules = [
        # Example: "amp-sidebar",
]


[creators.ZORALab]
type = "Organization"
name = "ZORALab Team"

[creators.KeanHo]
type = "Person"
name = "Holloway Chew Kean Ho"


[thumbnail]
url = "/img/thumbnails/default.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "disabled"
name = "Manage Authorship (OLD)"
pre = "👨‍👩‍👧‍👦"
weight = 1


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}

This feature is available since version `v1.12.0`.




## How it Works: Using Hugo's TOML Map Table
After researching for scalability and trying to resolve many authorship
problems, Bissetii decided to opt for using TOML Map Table inside Hugo's
configurations file and page's front-matter. The pattern is something as
follows:

```toml
[john-smith]
type = "Person"
name = "John Smith"

[success-team]
type = "Organization"
name = "Success Team"
```

By using map table, both site-level and page-level authors are uniquely sorted
out automatically, saving the problem of repetition.

{{< note "default" "Note" >}}
Use lowercase for the map key. Like the example above, it is `success-team`
instead of `"SuccessTeam"`.
{{< /note >}}



### Supported Format
The fields inside each creator is strictly defined by
[Schema.org](https://schema.org/author). Some important fields are:

1. `type` - either `"Person"` or `"Organization"`.
2. `name` - the full name in string.



### Targeted Output
Bissetii renders the list of authors to many SEO components like meta tags,
[Schema.org](https://schema.org) LD+JSON script, twitter authors, open graph
tags, and etc.




## Setting Authorships
There are 2 ways to setup the authors depending on your desired area of effect.



### Site-Level Authorship
These settings affects every pages in the Hugo content. Should the any
Page-level authorships is defined, the Site-level authorships will be
overwritten by it.



#### Location
To set Site-Level Authorship, you declare the creators map tables inside the
following:

```
pattern   :           data/creators.toml
directory :     docs/.data/
repo-docs :     docs/.data/creators.toml
```

One example would be:

```toml {linenos=table,hl_lines=["3-13"],linenostart=1}
# creators/authorship list

[bissetii]
"@type" = "Organization"
"name"  = "Bissetii Team"
"slogan" = "Design with Simplicity"
```



### Page-Level Authorship
For page specific authorship, one can set it inside the page's front matter.
If the creator map name (e.g. `[creators.JohnSmith]`) is the same as the
Site-Level Authorship, the latter will be overwritten.

Example:

```toml {linenos=table,hl_lines=["7-13"],linenostart=1}
+++
...
# redirectURL=""
layout = "single"


[creators.JohnSmith]
type = "Person"
name = "John Smith"

[creators.SuccessTeam]
type = "Organization"
name = "Success Team"


[menu.main]
...
+++
```
