+++
date = "2020-06-22T18:01:31+08:00"
title = "How To Create Redirect As Hugo Theme"
description = """
Starting from version `v1.12.0`, Bissetii integrated the meta-tag with redirect
capability into all pages. This allows anyone to perform page redirect easily
without needing to move the documentation sources around.
"""
keywords = ["create", "redirect", "hugo", "bissetii"]
draft = false
type = ""
redirectURL="/en/hugo/manage-redirect/"
layout = "redirect"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
]


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[creators.ZORALab]
type = "Organization"
name = "ZORALab Team"

[creators.KeanHo]
type = "Person"
name = "Holloway Chew Kean Ho"


[menu.main]
parent = "disabled"
name = "Create Redirect (Disabled)"
pre = "🚥"
weight = 1


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}

{{< note "note" "Important Note" >}}
This feature does not have any defenses against infinity looping. Please ensure
your redirected URL will not cause such incident before enabling.
{{< /note >}}




## The Page `redirectURL`
To enable page redirect, there are 2 steps to deal with:


### Specify destination URL
You can specify your destination URL into `redirectURL` variable in your page's
front-matter parameter list. Here is an example using `toml` front-matter:

```md {linenos=table,hl_lines=[4,11],linenostart=1}
+++
...

redirectURL = "https://example.com"

...

[menu.main]
parent = ""
name = "Target Page"
url = "https://example.com"
pre = "💠"
weight = 1

...

+++
```

* **Line 4**  - specify the target link into the `redirectURL` field.
* **Line 11** - overwrite the main menu navigation item's `url` with the target
                link. This is to prevent unnecessary redirect chain.

Bissetii also interprets relative URL (to your base URL) just in case you want
to redirect the page to other relative pages.

{{< note "note" "Note" >}}
Starting form version `v1.13.0`, the relative URL for redirect requires you to
prepend any language prefix into the URL due to the `link` shortcode changes.

Otherwise, the relativity is alaways to the root of the base URL where one can
redirect it to any static files.
{{< /note >}}


### Activate/Deactivate Redirect
To activate or deactivate the redirect mechanism, you can set the `layout` in
your page's front-matter. Here is an example using `toml` front-matter:

```md {linenos=table,hl_lines=["5"],linenostart=1}
+++
...

redirectURL = "https://example.com"
layout = "redirect"

...
+++
```

Setting `redirect` will render that page to perform redirect meta page and the
redirecting mechanism happens immediately (`0` second).

Setting something else (default is `single` from Bissetii) will render that page
as it is.


### Hide From Main Menu
To hide from main menu, set `parent` inside `[menu.main]` field as `"redirect"`.
Bissetii will filter these pages away from menu building automatically.

Example:

```toml {linenos=table,hl_lines=[2],linenostart=1}
[menu.main]
parent = "redirect"
# name = "Custom title"
pre = "🚥"
weight = 1
```

{{< note "note" "For Your Information" >}}
Starting from version `v1.13.0`, these filtering keywords are governed by a
data file located in your data directory (e.g. `docs/.data`). The usual pathing
is `{data_directory}/bissetii/blocklist.toml`. The default version is viewable
[HERE](https://gitlab.com/zoralab/bissetii/-/blob/next/data/bissetii/blocklist.toml)
.
{{< /note >}}




## Epilogue
That's all for how to create redirect for existing pages when using Bissetii
as Hugo theme module. If you have any questions, please feel free to raise your
queries in our [Issues Section](https://gitlab.com/zoralab/bissetii/-/issues).
