+++
date = "2020-07-14T16:00:48+08:00"
title = "How To Customize Schema.Org Data As Hugo Theme"
description = """
Schema.org is the next advancement to make website parsing easier for robots.
It has been used across many search engine listing. Bissetii has this feature
supported to ensure it is competitive in the SEO industry.
"""
keywords = ["customize", "schema.org", "hugo", "bissetii"]
draft = false
type = ""
redirectURL="/en/hugo/manage-schema/"
layout = "redirect"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "sidebar",
]


[creators.ZORALab]
type = "Organization"
name = "ZORALab Team"

[creators.KeanHo]
type = "Person"
name = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/how-to/customize-schema-org/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii with Schema.org Integration"

[thumbnails.1]
url = "/img/thumbnails/how-to/customize-schema-org/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii with Schema.org Integration"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii with Schema.org Integration"


[menu.main]
parent = "disabled"
name = "Customize Schema Data Structure (disabled)"
pre = "🔬"
weight = 1


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}

This feature is available since version `v1.12.0`.

Here is an example (for education purposes only, Bissetii generates a
compressed version):

```html
<script type="application/ld+json">
{
	"@context": "https://schema.org/",
	"@type": "SoftwareApplication",
	"applicationcategory": ["Hugo Theme Module", "Go Template Module"],
	"author": [{
			"@type": "Organization",
			"name": "Bissetii Team",
			"slogan": "Design with Simplicity"
		},
		[{
			"name": "Holloway Chew Kean Ho",
			"type": "Person"
		}],
		[{
			"name": "ZORALab Team",
			"type": "Organization"
		}]
	],
	"datePublished": "2019-03-10T13:16:02+08:00",
	"description": "Bissetii is an \"All-in-One\" templating module for both Hugo Theme and Go Programming Language. It is designed to bootstrap both Go application and Hugo static website in a simple and nice manner. Come on in to learn more.",
	"image": "https://zoralab.gitlab.io/bissetii/img/thumbnails/default.png",
	"name": "Bissetii Hugo Theme and Go Template Module",
	"offers": {
		"@type": "Offer",
		"price": "0",
		"pricecurrency": "USD"
	},
	"operatingsystem": ["Windows", "Debian", "OSX"]
}
</script>
```




## Customize Schema Structure
While Bissetii provides a default set of schema, in most cases, you will want
to customize them according to your use cases. Here are some of the ways to do
it easily.



### Customizing Default Schemas
The default schemas are defined in the following pattern and location:

```
pattern   :          data/bissetii/schema/<type>.toml
directory :    docs/.data/
repo-docs :    docs/.data/bissetii/schema/<type>.toml
```

Since Bissetii uses `data/` directory, all you need to do is to define your
own structure and place it in your **repo-docs** `data/` directory with the
same relative pathing.

Example, based on the pattern above, if you want to use your own default
`WebPage.toml`, you can create a copy in
`docs/.data/bissetii/schema/WebPage.toml`.

Customizing default schemas will indefinitely affects all the corresponding
pages.



### Customzing Specific Schema Sources
Starting from Bissetii version `v1.13.0`, you can place specific schema file
anywhere inside your `data/` directory. If you do not have any idea, we
recommend you to place them inside: `data/schema/` pathing for organization
purposes.

These schemas can be specific to a page or group of pages based on your page
selection.




## Selecting A Schema
To select a schema, you feed the selection inside the page's front-matter as
follow:

```toml {linenos=table,hl_lines=[4,5],linenostart=1}
+++

...
[schema]
selectType = "SoftwareApplication"
```

The value must be of the filename. You can supply your custom schema and specify
it inside the `selectType`. Example:

| `selectType` Values      | Target                                          |
|:-------------------------|:------------------------------------------------|
| `"SoftwareApplication"`  | `data/bissetii/schema/SoftwareApplication.toml` |
| `"MyCustom"`             | `data/bissetii/schema/MyCustom.toml`            |
| `WebPage` or default     | `data/bissetii/schema/WebPage.toml`             |

If the schema is not found, Bissetii will fallback to use the default schema
(`WebPage`).



### Select Specific Schema Sources
{{< note "Note" "Note" >}}
This feature is only available starting from `v1.13.0` onwards.
{{< /note >}}

To select a specific custom schema sources, you need to supply the full Hugo
compatible data pathing to your schema file. This is done by replacing your
`data/` and each directory slash with a period (`.`), and remove the TOML
file extension.

Example: here is a schema data file located in the following path:

| Attributes          | Description                          |
|:--------------------|:-------------------------------------|
| file location       | `docs/.data/schema/LandingPage.toml` |
| `data/` directory   | `docs/.data/`                        |
| Schema Select Value |           `.schema.LandingPage`      |

Your front-matter will be as the following:

```toml {linenos=table,hl_lines=[5],linenostart=1}
+++

...
[schema]
selectType = ".schema.LandingPage"
```




## Building Your Own Schema
To build your very own schema data structure, you **MUST** to refer to:

1. [Schema.org Parameters](https://schema.org/docs/full.html) and construct the
   elements accordingly.

Additionally, to support specific types of parameters for search engine, you
can refer to:

1. [Google Search Gallery](https://developers.google.com/search/docs/guides/search-gallery)



### Parsing Format
Bissetii uses `toml` map table to structure the entire
[Schema.org parameter schemes](https://schema.org/docs/full.html). To construct
the data structure, you just need to define it accordingly using TOML format.


#### Dynamic Page Data
Bissetii also allows dynamic value parsing from existing Page data structure
using the period (`.`) notation. Due to Go's templating limitation, this ability
**only affects FIRST degree data fields**. Anything deeper will be treated as
value. Example:

```toml {linenos=table,hl_lines=[],linenostart=1}
"description" = ".Page.Description" # Will Work : 1st-level (.description)

[offer]
"description" = ".Page.Description" # WON'T work: 2nd-level (.offer.description)
```

The data structure (those `type NAME struct` definition) is available at:
[Go Meta Component]({{< link "/go/internal/components/meta/" "this"
"url-only" />}}).



### Validate Output
Lastly, to validate the generated output, you can use JSON linter website like:
https://jsonlint.com/. Alternatively, for published website, you can test the
website output using
[Google Rich Results Test](https://search.google.com/test/rich-results).




## Epilogue
That's all for how to customize schema.org data structure as Bissetii Hugo
theme module. If you have any questions or query, please feel free to file an
issue at our [Issue Section](https://gitlab.com/zoralab/bissetii/-/issues). We
will be happy to response to your question.
