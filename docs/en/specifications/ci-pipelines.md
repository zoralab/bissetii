+++
date = "2019-03-13T07:19:31+08:00"
title = "CI Pipelines"
description = """
Bissetii relies on various continuous integrations to ensure it is tested and
verified. They are automated and operate under a single process chain for easy
development. Come in to learn more.
"""
keywords = ["Bissetii", "continuous integration", "specification"]
draft = false
type = ""
# thumbnailURL = "#"
robots = [
        # Example: "robotname|noindex,nofollow,...",
]
includeAMPModules = [
        # Example: "amp-sidebar",
]


# redirectURL=""
layout = "single"


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[menu.main]
parent = "Z) Specifications"
# name = "CI Pipelines"
pre = "⛓️"
weight = 1
+++

# CI Pipelines
Bissetii relies on various continuous integrations to ensure it is tested and
verified. They are automated and operate under a single process chain for easy
development. This is for quality assurance.

Bissetii only uses CI pipelines to manage releases and to test its application.
As for human perception design testing, manual documentations are used to test
all the visual output.

Bissetii uses ZORALab's [Fennec Project](https://zoralab.gitlab.io/fennec) to
unilaterally execute its GitLab CI automation. Hence, most of the deployment
complies to Fennec specifications.




## Release Strategy
Bissetii uses point-release strategy to manage its releases progressively.

{{< image "Point Release Strategy"
	"/img/specs/point_release_strategy.png"
	"920"
	"383"
	"false"
	"lazy"
	""
	"responsive-amp-only"
	""
>}}



### Tagging Definitions
Its tags system follows [semantic versioning](https://semver.org/) with starting
letter `v`. Example:

```
v0.1.0
v1.0.0
```

Tags with tailing `+` is reserved for packaging releases by builds. Usually,
this type of tagging (with or without `+` sign) happens on `master` branch.
Example:

```
v1.0.0+b1
```

Tags with tailing `~` is reserved for packaging release candicates. Usually,
this type of tagging happens on `staging` branch. Example:

```
v1.0.0~rc1
```

Tag with tailing `-` is reserved for package maintainer only (e.g. Debian
developers/maintainer). Hence, **we should not use it to avoid
unnecessary collision**. Example:

```
v1.0.0-sp1
```




## Branch Management
Bissetii follows the 3 branches (`stable`, `testing`, and `unstable`) system
and progressive development. This means that we do not perform git revert
or rebase to break any timelines.

{{< note "default" "Rationale" >}}
Not many people understand how time works, espcially with `git rebase` that
allows time travel and alternation. Hence, to keep things simple, we move
forward in one direction progressively.
{{< /note >}}


### Master
This branch is known as the **main product line**. It is protected and
recognized as the final product.

**ONLY** `staging` branch is allowed to merge into `master` branch and there
**SHOULD NOT** be any development on this branch.


### Staging
This branch is known as the **next product release**. It holds all the commit
patches for the next release and is currently under testing.

It should carries the same commit timeline as `master` branch and holding
the future commits to be merged into it.

**ONLY** maintainers are allowed to commit and cherry-pick commit patches into
this branch.


### Next
This branch is known as the **edge**. It contains all the latest and greatest
development. Any contributors can contribute here.

As for the website publishing, it is tracked under this branch for
documentation flexibility without triggering a minor release.
