+++
date = "2019-03-12T09:32:34+08:00"
title = "Design Concept, Design Principles, and User Experience  Specification"
description = """
Like all design related modules, Bissetii has its own design concept. The idea
is primarily focusing on rapid prototyping a web interface without relying
on framework so the developer would just focusing on enabling the functions.
"""
keywords = ["hugo", "bissetii", "zoralab", "concept", "design"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[amp]
modules = [
        # Example: "amp-sidebar",
]


[thumbnail]
url = "/img/thumbnails/default.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[menu.main]
parent = "Z) Specifications"
name = "Design Concept"
pre = "⛩️"
weight = 1

[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}

{{< image "Using Bamboo to Build Hong Kong Skyscrapper"
	"/img/specs/bambooHK-1280px.jpg"
	"543"
	"407"
	"false"
	""
	"responsive-amp-only"
	"" >}}

Bissetii, also known as **Phyllostachys Bissetii** was inspired by big,
beautiful, simple, hardiest, and strong bamboo plant that stands out in both the
garden and forest. Bamboo serves as the scaffolding for skysrapper construction,
especially in Hong Kong.

Relating it back to the scaffolding role, Bissetii serves as the same types of
bamboo scaffolding material for one to build building beyond imaginations.





## Values and Motivations
Here are the values and motivations that kick-started Bissetii project.


### The Story Behind
At first, Bissetii is designed as a Hugo theme module. It was meant to honor
majority of the HTML partial templates and its associated settings. After
observing Hugo's practices and source codes, it is suggested that Go programming
language is able to interpolate easily with the module.

Henceforth, the idea of **single web design template module** is born. Bissetii
quickly got itself into a position where it is best suited to serve as a
single theme-based design template module.


### Resources Consolidations
One benefit via consolidation is to produce a **single, focused, and pinpoint
web template module**. This consolidation allows us  to **easily standardize
scaffolding design across different website or web-app, creating consistency for
user experience.**

Another benefit is we are able to **isolate technological dependencies for both
designers and developers for their respective work**. Web designers can work
independently with the designs in Bissetii itself using Hugo while Go developers
can focus on developing the applications.




## Design Principles
Based on the design's values and motivations, we derived a list of design
principles that we must strictly adhere with.



### Principle #1 - Follows the Standards
**Adhere internet standards like [www.w3.org](https://www.w3.org),
[Go Programming Paradigm](https://golang.org/doc/effective_go.html),
and [Hugo Programming Paradigm](https://gohugo.io/getting-started/)**.

{{< note "default" "Rationale" >}}
Just like the bamboo scaffolding, we take each bamboos to outline a structure.

A bamboo is a bamboo: **it does not need wood, or leaves, or special types of
chemical just to use it**.
{{< /note >}}



### Principle #2 - Keep it Go and Hugo Compatible
**Keep the module compatible for both Hugo and Go**. That way, we maintain a
single template module.

{{< note "default" "Rationale" >}}
Serving 2 different software in the same programming paradigm is complicated
enough already. Hence, we should not complicate it further by having specific
purposes that breaks Bissetii's values and motivations.
{{< /note >}}



### Principle #3 - Keep Things Simple and Practical
**Keep the module simple and practical**. This is an artistic tool, not a fully
aristic piece of artwork. It has a purpose to scaffold an idea.

{{< note "default" "Rationale" >}}
Just like a bamboo for scaffolding, it is a dried sugar-free long, strong stick.
Do not offer bamboo with reinforced concrete, steel coating or whatsoever. It
makes no sense.
{{< /note >}}



### Principle #4 - Prioritize Hugo over Go
**The primary customer is still Hugo**. Go is a programming language which has
flexibility and versatility to overcome obstacles compared to Hugo.

{{< note "default" "Rationale" >}}
Hugo is a widely deployed product. Hence, respecting its theme module is a
primary requirement.
{{< /note >}}




## User Experiences
Bissetii is designed based on the Hugo's theme module. In other words, it has
all the basic elements of Hugo themes like `archetypes`, `i18n`, `layouts`, etc.

In this section, we will specify more about how user uses Bissetii as an
overall.



### Bisseti Must Be A Direct Hugo Theme Module
The very basic function user **MUST** be able to do is `git clone` Bissetii
into the Hugo local `themes` directory without any custom configurations.

```bash
$ git clone https://gitlab.com/ZORALab/bissetii.git
```

This is a **NON-COMPROMISING** user experience.



### Go Compatible Import for Layouts Partials
All HTML or contents templates should resides under a single `layouts`
directory. The `layouts/partials` directory is something you must pay attention:

1. `layouts/partials` must be simple enough for Go to import with the exception
   to `layouts/partials/hugo-only` directory (the name said it all).

Anything else like `layouts/_defaults`, and `layouts/shortcodes` are more for
Hugo-only use cases.



### Go Shares the Same Hugo Theme File Structures
Since Hugo dominated the file structure, the easiest way is to adopt its file
structures from the get-go.




## Conclusion
That's all for Bissetii's design concept, design principles, and user experienc
specification. Feel free to use the navigation menu to explore other contents.
