+++
date = "2019-03-13T10:36:31+08:00"
title = "Logo Specification"
description = """
Bissetii has its own product logo to represent itself. They come with specific
dimension and coloring for protecting its identity. Therefore, there is a
strict specification for using logo and banner in your content.
"""
keywords = ["bissetii", "logo", "banner"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Z) Specifications"
name = "Logo"
pre = "💠"
weight = 1


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}




## Do and Do Not
Here are some do and don't when using Bissetii's Logo and Banner.


### DO
You are **ALLOWED**:

1. To propotionally scale the graphics to meet the requirement size.
2. To use it for attribution and referencing.


### DO NOT
You are **NOT ALLOWED**:

1. To recolor, to edit, or to cut the logo or banner.
2. To resize the logo or banner into dispropotions.
3. Use it to mis-represent or endorse your products without written approval.
4. To do anything else outside of the allowed list.




## Image Sources
Here are the image sources for download (please avoid hotlinking them).



### Logo
This is the logo used in the banner. Although the image is prepared with the
largest size possible, you need to resize it according to your layout design.

| Attributes   | Prepared Values    |
|:-------------| :------------------|
| Width        | `2048px`           |
| Height       | `2048px`           |
| Color        | `8-bit gamma sRGB` |
| Resolution   | `300 x 300 ppi`    |



| Image | WebP Format | PNG Format | JPEG Format |
|:------|:------------|:-----------|:------------|
| {{< image "Square Logo" "/img/logo/s2048-color.webp" "100" "100" "false"
"lazy" "" >}} | {{< link "/img/logo/s2048-color.webp"
"" "" "" "" "" "download" >}}
Download
{{< /link >}} | {{< link "/img/logo/s2048-color.png"
"" "" "" "" "" "download" >}}
Download
{{< /link >}} | {{< link "/img/logo/s2048-color.jpg"
"" "" "" "" "" "download" >}}
Download
{{< /link >}} |




## Epilogue
That's all for Bissetii's Logo and banner. If you have any queries, please feel
free to raise them at our
[Issues Section](https://gitlab.com/zoralab/bissetii/-/issues).
