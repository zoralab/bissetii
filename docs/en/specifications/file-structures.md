+++
date = "2019-03-12T18:12:41+08:00"
title = "File Structures Specification"
description = """
Since Bissetii is a multi-role module, the file structures must be documented
specifically for future engineering betterment. That way, communications between
developers can be aligned correctly for synchonous contributions.
"""
keywords = ["Bissetii", "file structures", "specification"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[amp]
modules = [
        # Example: "amp-sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnail]
url = "/img/thumbnails/default.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Z) Specifications"
name = "File Structures"
pre = "🗄️"
weight = 1


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}

Below are the specification for the key file structures in Bissetii Hugo Theme
and Go Template Module.


## app
```bash
app/
    └── bissetii.sh
```
`app` directory holds the source codes for Bissetii application program.




## archetypes
```bash
archetypes/
    ├── default.md
    └── redirect.md
```
This serves Hugo's default page creation template. For bissetii, a `default.md`
is in placed to handle all the default values.




## assets
```bash
assets/
├── css/
│   ├── bissetii/
│   │   ├── _Render.sass
│   │   ├── ...
│   │   ├── templates
│   │   │   ├── ...
│   │   │   └── ...
│   │   └── modules/
│   │       ├── core/
│   │       │    ├── _Base.scss
│   │       │    ├── _Blockquote.scss
│   │       │    └── ...
│   │       ├── emoji/
│   │       │    ├── _CustomEmoji.scss
│   │       │    ├── _NotoColorEmoji.scss
│   │       │    └── ...
│   │       └── ...
│   ├── desktop.sass
│   ├── main.sass
│   ├── ...
│   └── print.sass
└── js/
    ├── coreLib/
    │   ├── core.js
    │   └── ...
    └── ...
```
These are the Sass and Javascript asset components for Bissetii.

This directory must be compatible for:
1. Hugo Theme (`assets/`).
2. Sass Engine.

The `css` directory holds the master `main.sass` that serves as the primary css
configurations for a Hugo engine. the `bissetii` inside this directory holds
the Sass files for generating the final CSS output.

The `js` directory holds all Javascripts, ready to be fused and minified
together.




## config
```bash
config/
    └── _default/
        ├── config.toml
        ├── ...
        └── params.toml
```
`config` is the master template for `.sites` engine's (or Hugo's) `config`
directoy. It is used to quickly setup a local Hugo repository fully configured
for Bisseti.

This directory must be compatible for:

1. Hugo themes (`config/`)




## .configs
```bash
bissetii/
├── .configs/
│   └── ...
...
```
`.configs` directory holds all configurations for Bissetii's GitLab CI
automation purposes.



## data
```bash
data/
├── bissetii/
│   ├── ...
│   ├── data/
│   │   └── ...
│   ├── defaults.toml
│   ├── includes.toml
│   ├── navbars.toml
│   ├── schema/
│   │   ├── SoftwareApplication.toml
│   │   └── ...
│   ├── thumbnails.toml
│   └── vendors/
│       ├── google.toml
│       └── ...
└── creators.toml
```
Holds data essentials for Hugo Theme with local dataset and bissetii Hugo
Theme configuration files. For all Bissetii configurations, it is located in
`data/bissetii/` directory.

Anything outside of Bissetii configurations directory is meant to serve as
the local dataset for users to be overwritten.


This directory must be compatible with:
1. Hugo Theme (`data/`).
2. Go Programming Language.




## docs
```bash
docs/
    ├── en-us/
    │   └── ...
    └── zh-cn/
        └── ...
```
Holds Bissetii's documentations. It is powered by the local `.sites` website
generator.

This directory must be compatible for:
1. Hugo Theme (`docs/`).




## .fennec
```bash
.fennec/
    └── ...
```
Bissetii's GitLab CI support module. Bissetti uses
[Fennec](https://zoralab.gitlab.io/fennec) to drive most of its continuous
integration works.




## i18n
```bash
i18n/
    ├── en-us.toml
    └── ...
```
`i18n` holds Bissetii's i18n translation dictionaries as Hugo theme module.

This directory must be compatible for:
1. Hugo Theme (`i18n/`).




## images
```bash
bissetii/
└── images/
    ├── screenshot.png
    └── tn.png
```
Required Hugo Theme directory for dislaying screenshot and thumbnail on Hugo's
theme store. You should not include anything else aside both of the listed files
above.




## layouts
```bash
layouts/
├── 404.html
├── _default/
│   ├── baseof.html
│   └── ...
├── partials/
│   ├── amp/
│   │   └── ...
│   ├── hugo-only/
│   │   ├── css/
│   │   │   └── ...
│   │   ├── js/
│   │   │   └── ...
│   │   └── ...
│   │
│   ├── footers/
│   │   └── ...
│   ├── .../
│   │   └── ...
│   └── svg/
│       └── ...
└── shortcodes/
    ├── absLangLink.html
    ├── ...
    └── sponsors/
        ├── PayPalME.html
        └── ...
```
`layouts` directory holds various partial templates and some Hugo-specific
components.

There is one particular directory should be taken with extreme care:
`layouts/partials`. This directory (with exception to
`layouts/partials/hugo-only`) must be compatible for:

1. Hugo theme.
2. Go template imports.

The rest of the directories and files are only compatible for:

1. Hugo themes (`layouts/`).




## release
```bash
release/
   └── ...
```
`release` directory is a reserved directoy for the repository to deposit all
the compiled packages. Hence, nothing should inherit this working directory.




## .scripts
```bash
.scripts/
    └── ...
```
`scripts` holds the Bissetii repository's scripts for automated executions.
These are normally package builder and manager, tester, and other operational
scripts.




## .sites
```bash
.sites/
├── archetypes/
│   └── ...
├── config/
│   └── _default/
│       ├── config.toml
│       └── ...
├── ...
├── resources/
│   └── ...
├── static/
│   └── ...
└── themes/
    ├── bissetii/
    │    └── config.toml
    └── ...
```
Bissetii local Hugo engine. This engine has several functions:

1. Works as a local Hugo development point.
2. Works as Bissetti website contents generator.
3. Hosts release pacakges in its local `static/releases` directory.

This directory must be compatible for:

1. Hugo themes (`static/`).




## static
```bash
static/
├── android-icon-144x144.png
├── css/
│   ├── ...
│   └── fonts/
│       └── ...
├── img/
│   ├── logo/
│   │   ├── 2500x1250-color.png
│   │   ├── ...
│   │   └── s2048-color.svg
│   └── sponsors/
│       ├── any.jpg
│       ├── ...
│       └── meal.jpg
└── ...
```
`static` holds all the static files to be loaded directly into the server
via Bissetii's theme module.

This directory must be compatible for:

1. Hugo themes (`static/`).
2. Go Programming Language.




## tests
```bash
tests/
    ├── scripts/
    │   └── ...
    └── temp/
        └── ...
```
Bissetii's local BaSHELL test framework for testing bash scripts or other
automation work in the repository.




## tmp
```bash
tmp/
    └── ...
```
`tmp` directory is a reserved directory for the repository to perform automated
operations. Hence, nothing should inherit this working directory.
