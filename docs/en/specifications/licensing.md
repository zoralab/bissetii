+++
date = "2019-03-13T10:09:16+08:00"
title = "Licensing Specification"
description = """
Although Bissetii is licensed under Apache 2.0, there are license's
specification to ensure it always safe to at outbound license. That way, we can
keep Bissetii from legal troubles.
"""
keywords = ["licensing", "bissetii", "contribute"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[amp]
modules = [
        # Example: "amp-sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnail]
url = "/img/thumbnails/default.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Z) Specifications"
name = "Licensing Specification"
pre = "⚖️"
weight = 1


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}

Let's us look at licensing criterias below.




## Known Incompatible Licenses
These are the known incompatible inbound licenses that will get rejected:

1. Proprietary Licenses
2. GPL Version 2 License and similar Copyleft Licenses
3. Missing License
4. Unknown/Undetermined License

{{< note "default" "Note" >}}
You must always understand the origin of your idea/code/design before you apply
into this project. If the idea is yours and you wish to contribute it to
Bissetii, it will be licensed using Apache 2.0 outbound license.
{{< /note >}}




## Media Files Licensing
Media files such as but not limited to music, audio, speech, voice, video,
copyrighting, and images used on Bissetii website publications **MUST** comply
to their original licenses. The files **MUST ONLY** be used within their
licensed jurisdictions:

1. [Creative Common Licenses](https://creativecommons.org/licenses/)
2. [Public Domain or CC0](https://creativecommons.org/share-your-work/public-domain/cc0/)

If there is/are person/entity identifiable elements in the image (e.g. human
model with clear face, brands, etc.), these documents must be presented
alongside the license:

1. a written, certified, and legally-binded `model/location/entity release form`
   for accepted concensus.
2. a written, ceritifed, and legally-binded `privacy notice agreement` for
   human identifable media.

**ANYTHING ELSE IS FORBIDDEN**




## That's All
That's all for "{{% param "title" %}}". If you're unsure or having doubt, please
**[raise an issue and ASK](https://gitlab.com/ZORALab/bissetii/issues)**.
