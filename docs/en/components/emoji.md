+++
date = "2020-02-13T20:46:29+08:00"
title = "Emoji Component"
description = """
"Emoji", known as e (絵, "picture") + moji (文字, "character") from Japanese
Kanji is a key feature for web content. With emoji supports, a lot of icons
can be replaced easily. Bissetii does support "Emoji" seemlessly with an
optional enabling trigger.
"""
keywords = ["emoji", "components", "html", "w3c", "bissetii"]
draft = false
type = ""
redirectURL="/en/components/root/"
layout = "redirect"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[amp]
modules = [
        # Example: "amp-sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnail]
url = "/img/thumbnails/default.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "disabled"
name = "Emoji (disabled)"
pre = "😄"
weight = 1


[schema]
selectType = "WebPage"
+++

# `Emoji` Component
`Emoji`, known as e (絵, "picture") + moji (文字, "character") from Japanese
Kanji is a key feature for web content. With emoji supports, a lot of icons
can be replaced easily. Bissetii does support `Emoji` seemlessly with an
optional enabling trigger.

This component is available starting from `v1.11.0`.




## Sass Styling Parts
Sass styling parts varies depending on the version you use.



### Version `v1.13.0` and above
Starting from version `v1.13.0`,
**emoji no longer requiring dedicated Sass files**. It will be parts of
[Root Component]({{< link "/components/root" "this" "url-only" />}}).

By default, the emoji uses its original name "`Noto Color Emoji`" and will use
local system-UI version if available.

Otherwise, it falls back to finding its local font hosting at
`/fonts/NotoColorEmoji.ttf` pathing.




### Version `v1.12.5` and before
{{< note "warning" "Important Notice" >}}
Support for Sass emoji is scheduled to be stopped starting **June 30, 2021**.
While the codes remain available, the team will no longer answer any questions
related to the matter.
{{< /note >}}

The Sass scripts responsible for styling emoji are located at:

```bash
assets/css/bissetii/modules/emoji/*
```

Bissetii is currently using external emoji font graphics like
`Noto Color Emoji` for page rendering.


#### Enabling Emoji
To enable emoji, you need to supply the type of emoji styling using its Bissetii
emoji code name. To disable emoji completely, simply supply `false` as falue.
The config is:
```sass
$config-emoji: code-name
```

##### Custom Sourcing URL Paths
Bissetii also supply optional local URL and remote URL sourcing path. If URLs
are specified, it will overrides the default URL. To disable them and use the
default URLs, simply supply an empty array (`[]`). The configs are:

```sass
$config-emoji-local-url: [ "font/myEmojiFile.tff" ]
$config-emoji-remote-url: [ "site.com/emoji.tff", "site.com/emoji.woff" ]
```


#### Custom Font
To supply custom font of your own, simply supply the codename as:
```sass
$config-emoji: custom-emoji
```

You **MUST** specify both local and remote URLs for sourcing the font files.
Otherwise, the compiler returns an error.


#### Noto Color Emoji
[Noto Color Emoji](https://www.google.com/get/noto/help/emoji/) is the Noto
font family emoji. The graphics font is available at its
[Github repository](https://github.com/googlefonts/noto-emoji). To enable this
font, simple supply the codename as:

```sass
$config-emoji: noto-color-emoji
```

By default, this font sources the font file from the following locations:

```html
Local URL : fonts/NotoColorEmoji.tff
Remote URL: https://gitcdn.xyz/repo/googlefonts/noto-emoji/master/fonts/NotoColorEmoji.ttf
```




## Javascript Parts
This component does not depend on any Javascript.




## HTML Parts
There are multiple aspects you need to manage for HTML part.



### Import Emoji Font
Starting from version `v1.13.0`, you need import your emoji font using inline
styling (`<style>`).

This applies to AMP output format where you compress the codes together
into your `<style amp-custom>`.

The CSS is **hard-coded** to use the name `emoji`. Hence, you just need to
specify the source URL list using the `@font-face` attribute . Here is an
example:

```css {linenos=table,hl_lines=[],linenostart=1}
@font-face {
	font-family: emoji;  /* keep this hard-coded */
	src: url(//localhost:8080/fonts/NotoColorEmoji.ttf),
		url(https://cdn.com/fonts/NotoColorEmoji.ttf),
		...;
}
```



### Creating Emoji
There are many ways to create emoji. There are minimum 3 ways to do it:

1. By shortcode (**HIGHLY RECOMMENDED**)
2. By HTML Encoded UTF-8
3. By UTF-8 Symbols directly

You may refers to the
[Emoji Cheat Sheet](https://www.webfx.com/tools/emoji-cheat-sheet/) or
[Emoji Manual](https://unicode.org/emoji/charts/full-emoji-list.html) for some
guidance. Bissetii currently renders `Emoji` as follows:

{{< renderHTML html >}}
<table>
<thead>
	<tr>
		<th>Emoji</th>
		<th>Unicode</th>
		<th>By Shortcode</th>
		<th>By HTML UTF-8</th>
		<th>By Symbol</th>
		<th>Render (Shortcode)</th>
		<th>Render (HTML UTF-8)</th>
		<th>Render (Symbol)</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>Grinning</td>
		<td><code>U+1F600</code></td>
		<td><code>&colon;grinning&colon;</code></td>
		<td><code>&#38;#x1F600</code></td>
		<td><code>😀</code></td>
		<td><p>:grinning:</p></td>
		<td><p>&#x1F600</p></td>
		<td><p>😀</p></td>
	</tr>
	<tr>
		<td>Check Mark Button</td>
		<td><code>U+2705</code></td>
		<td><code>&colon;white_check_mark&colon;</code></td>
		<td><code>&#38;#x2705</code></td>
		<td><code>✅</code></td>
		<td><p>:white_check_mark:</p></td>
		<td><p>&#x2705</p></td>
		<td><p>✅</p></td>
	</tr>
	<tr>
		<td>Cross Mark</td>
		<td><code>U+274C</code></td>
		<td><code>&colon;x&colon;</code></td>
		<td><code>&#38;#x274C</code></td>
		<td><code>❌</code></td>
		<td><p>:x:</p></td>
		<td><p>&#x274C</p></td>
		<td><p>❌</p></td>
	</tr>
	<tr>
		<td>Woman Rising Hand</td>
		<td><code>U+1F64B U+200D U+2640 U+FE0F</code></td>
		<td><code>&colon;woman_raising_hand&colon;</code></td>
		<td><code>&#38;#x1F64B&#38;#x200D&#38;#x2640&#38;#xFE0F</code></td>
		<td><code>🙋‍♀️</code></td>
		<td><p>:woman_raising_hand:</p></td>
		<td><p>&#x1F64B&#x200D&#x2640&#xFE0F;</p></td>
		<td><p>🙋‍♀️</p></td>
	</tr>
	<tr>
		<td>Man Rising Hand</td>
		<td><code>U+1F64B U+200D U+2642 U+FE0F</code></td>
		<td><code>&colon;man_raising_hand&colon;</code></td>
		<td><code>&#38;#x1F64B&#38;#x200D&#38;#x2642&#38;#xFE0F</code></td>
		<td><code>🙋‍♂️</code></td>
		<td><p>:man_raising_hand:</p></td>
		<td><p>&#x1F64B&#x200D&#x2642&#xFE0F;</p></td>
		<td><p>🙋‍♂️</p></td>
	</tr>
</tbody>
</table>
{{< /renderHTML >}}
