+++
date = "2021-04-22T12:24:14+08:00"
title = "nav-sidebar Component"
description = """
This page specify how to create `nav-sidebar` component using Bissetii styling.
It serves as the component specification and a manual on interacting with its
generator for content generation.
"""
keywords = ["nav-sidebar", "components", "html", "w3c", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Nav - Sidebar (BETA)"
pre = "🧭"
weight = 1


[schema]
selectType = "WebPage"
+++

# `nav-sidebar` Component
{{% param "description" %}}

This component is available starting from `v1.13.0`.

{{< note "warning" "Beta Testing Notice" >}}
This is a fairly new component and it is still under user-level API beta
testing. Hence, there may be some major updates in the future that are
**not backward compatible**. Please **ONLY use at your own risk**.
{{< /note >}}




## Hugo
When using Bissetii as Hugo theme, this component implements the main navigation
system using `nav-sidebar` seamlessly starting from version `v1.13.0` and above.

By design, although this component allows multiple sidebars, due to limitation
with Go's template data processing, Bissetii
**restricts the theme to 1 sidebar** at a time. However, Bissetii allows some
configurations to that sidebar itself.



### Module Extension
To use `nav-sideebar` component, you need to include `"nav-sidebar"` extension
module into your front-matter or the global `amp.toml` data file. The code
pattern is as follows:

```toml {linenos=table,hl_lines=[5],linenostart=1}
+++
...
[modules]
extensions = [
	"sidebar",
]
...
+++
```

Bissetii will automatically includes the extension module seamlessly in
accordance to the output format (e.g. `amp-sidebar` extension module for
AMPHTML).



### Data Customizations and Configurations
Hugo parses customizations data and configurations data from the following
locations, with priority (means first found will be used) and in sequence
(means will following source if current is unavailable):

```
1. data/bissetii/nav-sidebar/<LANGUAGE_CODE>.toml
2. data/bissetii/nav.toml
```

`nav-sidebar` will always parse language-code specific configuration file first
before parsing the global navigation configuration file. Their formats are the
same.


There are a number of customizations and configurations available for
deployment.


#### Customize Title
You can customize the `nav-sidebar` title using HTML or AMPHTML codes under
`[Title]` table. Example:

```toml
[Title]
HTML = "<p>My Custom Title</p>"
AMPHTML = "<p>My Custom Title</p>"
```

Leaving them blank (default) will set the value back to using site title.


#### Customize Menu Trigger Icon
You can customize the `nav-sidebar` menu icon on the trigger using HTML or
AMPHTML code under `[Menu]` table. By default, the code is wrapped inside a
`<span>` HTML syntax for consistency. Here are the default values in `nav.toml`
file:

```toml
[Menu]
HTML = "<span class='nav-sidebar-icon nav-sidebar-menu'>☰</span>"
AMPHTML = "☰"  # content inside <button>
```


#### Customize Close Trigger Icon
You can customize the `nav-sidebar` close icon on the trigger using HTML or
AMPHTML code under `[Close]` table. By default, the code is wrapped inside a
`<span>` HTML syntax for consistency. Here are the default values in `nav.toml`
file:

```toml
[Close]
HTML = "<span class='nav-sidebar-icon nav-sidebar-close'>𝖷</span>"
AMPHTML = "𝖷" # content inside <button>
```


#### Customize Submenu Icon
You can customize the `nav-sidebar` submenu icon in the sidebar using HTML or
AMPHTML code under `[Submenu]` table. Here are the default values in `nav.toml`
file:

```toml
[Submenu]
HTML = "<span class='nav-sidebar-submenu-icon'>➣</span>"
AMPHTML = "<span class='nav-sidebar-submenu-icon'>➣</span>"
```

By default, Bissetii uses the `nav-sidebar-submenu-icon` css class to quickly
style the given icon. Since that css class rotates the icon by 90 degree with
animation, it's recommended you provide a plain square icon (be it image or
text).


#### Customize Logo Image
You can customize the `nav-sidebar` logo image in the sidebar using HTML or
AMPHTML code under `[Logo]` table. Here are the default values in `nav.toml`
file:

```toml
[Logo]
HTML = ""
AMPHTML = ""
URL = "/img/thumbnails/default-1200x1200.png"
Width = "160"  # px unit
Height = "160" # px unit
AltText = ""
Class = "nav-sidebar-logo"
```

IF `HTML` or `AMPHTML` is available, they will be used directly instead in
their respective rendering.

Otherwise, Bissetii will process the image `URL`, `Width`, `Height`, `AltText`,
and `Class` accordingly. The maximum width is the width of the sidebar and
recommended width is `160px` or `200px` square.

For animated images, you're recommended to use animated image files like
`webp`, `gif`, or `svg`.

Setting all `HTML`, `AMPHTML`, and `URL` empty will disable the logo display.


#### Customize Sidebar Location
By default, Bissetti uses the default settings from `nav-sidebar`. You can
configure your customizations accordingly in the `[Sidebar]` table. Here are
the default values in `nav.toml`:

```toml
[Sidebar]
RightSide = false
DrawerModeOnly = false
```

##### `RightSide` (boolean)
By default, Bissetii uses left side `nav-sidebar`. To switch to the right side,
set this field to `true`.

##### `DrawerModeOnly` (boolean)
By default, on wider screen like desktop and widescreen category, the sidebar
is set to be always expanded. If you wish to retain the drawer form like the
mobile version, set this to true.



## Go
Coming Soon.




## HTML
This section covers how to deploy the component using various HTML codes.
Depending on the type of HTML you're working on, please view the sub-sections
accordingly.



### Vanilla HTML5
In general, `nav-sidebar` relies on the `<input type=[checkbox]>` and its
`<label>` trick to perform actionable CSS toggling without relying on
Javascript. Hence, the overall pattern looks something as such:

```html {linenos=table,hl_lines=[],linenostart=1}
<nav class="nav-sidebar">
	<input id="nav-sidebar-[shasumUID]"
		class="nav-sidebar-trigger"
		type="checkbox">
	<label class="nav-sidebar-trigger" for="nav-sidebar-[shasumUID]">
		<span class="nav-sidebar-icon nav-sidebar-menu">
			... menu button HTML code here ...
		</span>
		<span class="nav-sidebar-icon nav-sidebar-close">
			... close button HTML code here ...
		</span>
	</label>
	<label class="nav-sidebar-shade" for="nav-sidebar-[shasumUID]">
	</label>
	<ul class="nav-sidebar-list">
		... list items here ...
	</ul>
</nav>
```

Basically, both the triggering buttons and shading layout are `<label>` to the
corresponding checkbox via the `for=` HTML attribute targeting the `<input>`'s
`id=` attributes.

Each `nav-sidebar` must have its unique ID (the `[shasumID]`) in order to
differenciate itself from one another.

Also, the positioning for the all the following HTML syntax **must strictly
conform to the hirarchy shown above** as  Bissetii uses relative sibling
to style them against one another:

1. `<input class="nav-sidebar-trigger">`
2. `<label class="nav-sidebar-trigger">`
3. `<label class="nav-sidebar-shade">`
4. `<ul class="nav-sidebar-list>`


#### Multiple Sidebars
For the time being, Bissetii only tested a maximum of 2 sidebars at a time
(left and right). As long as every `nav-sidebar` has its own unique ID for
operations, both sidebars should be working fine.

To go beyond 2 sidebars, it's at your own risk since Bissetii page layouts does
not offer a lot of options to render the extra sidebars beyond left and right.

Maintaining maximum of 2 sidebars is also complaint to AMP HTML counterpart
since they too have layout restrictions for those build website using
"single content, multiple outputs" rendering.



#### Right Side
Bissetii does prepare styling for right side in case there is a need. To do
that, simply add `.right-side` css class tag onto the `<nav>` HTML syntax, such
that:

```html {linenos=table,hl_lines=[],linenostart=1}
<nav class="nav-sidebar right-side">
	...
</nav>
```

This will "flip" te sidebar from the default left side to the right side.


#### Maintain Drawer Effect For Larger Screen
Bissetii by default auto-expand all sidebars on larger screen like larger
tablet, desktop, and widescreen. If you wish to maintain the drawer effect like
the mobile version, simply add `.drawer` css class tag onto the `<nav>` HTML
syntax, such that:


```html {linenos=table,hl_lines=[],linenostart=1}
<nav class="nav-sidebar drawer">
	...
</nav>
```


#### Trigger Button Positioning
The trigger button positioning is default to top-left edge or top-right edge.
It is always in fixed position. To customize its vertical position and its
design, simply adjust it using the CSS variables accordingly. See CSS section
for more info.


#### List Items
`nav-sidebar` uses conventional list item `<li>` to populate your list. Although
you're free to develop your own listing and styling, Bissetii made 3 types of
styling classes for those who wants quick and seamless styling.

##### Title and Logo
The first styling class is the title and logo list item. The HTML code is
as follows:

```html {linenos=table,hl_lines=[],linenostart=1}
<li class="nav-sidebar-title">
	<img class="nav-sidebar-logo" ... >
	<p>Title Here</p>
</li>
```

The list item `<li>` must have the `.nav-sidebar-title` css class tag in order
for Bissetii to style upon.

The logo image should have the the `.nav-sidebar-logo` css class tag in order
to restrict the image dimension properly.

As for the title wording, **please use paragraph `<p>`** instead of any level
of heading since it's not part of the content.

##### Link
The second styling class is the simple link list item. The HTML code is as
follows:

```html {linenos=table,hl_lines=[],linenostart=1}
<li class="nav-sidebar-link">
	<a href="...">
		<span class="pre">icon</span>
		<span>Link Title Here</span>
	</a>
</li>
```

The list item must have the `.nav-sidebar-link` css class tag in order to allow
Bissetii to know where to style it.

Immediately inside shall be a single anchor HTML syntax (`a`) defining your
hyperlink. Nothing else should be inside it.

As for the content of the link, set a `<span class="pre">` or `<pre>` allows
you to define your list item icon. You're free to place anything inside ranging
from emoji to image.

As for the remaining title, wrapping it with a `<span>` syntax is recommended.

##### Sub-Menu
The third list item is the sub-menu, allowing you to populate additional
sub-list recursively. The HTML code is as follows:

```html {linenos=table,hl_lines=[],linenostart=1}
<li class="nav-sidebar-submenu">
	<input id="nav-sidebar-submenu-[shasumUID]" type="checkbox">
	<label for="nav-sidebar-submenu-[shasumUID]">
		<span class="nav-sidebar-submenu-icon">ICON HERE</span>
		<span>List Title Here</span>
	</label>
	<ul id="nav-sidebar-submenu-[shasumUID]">
		... repeat all link child recursively ...
	</ul>
</li>
```

The list item must have `nav-sidebar-submenu` css class tag for Bissetii to
apply sub-menu styling and animation.

Similar to the parent, sub-menu needs to have its own checkbox input field
and its associated label acting as a trigger. All `<input>`, `<label>`, and
list (`<ul>` or `<ol>`) must have unique ID as its `id=` HTML attributes.

Optionally, if you want to facilitate a sub-menu icon but use Bissetii prepared
animation and styling, wrap it in a `<span>` syntax with
`.nav-sidebar-submenu-icon` CSS class syntax. Bissetii animates it by rotating
the icon 90 degrees anti-clockwise when open and vice versa.

Inside the list (`<ul>` or `<ol>`), you can populate the link items `<li>` again
recursively.

However, please be reminded to check with the latest SEO recommendations where
there should be a limit of depth in your sitemap layouts.



### Accelerated Mobile Pages (AMP)
As for AMPHTML codes, the `<amp-sidebar>` is used so you will need to import
that component in before use. Please check the
[documentation](https://amp.dev/documentation/components/amp-sidebar/?format=websites)
for its latest guide. It should look something as such:

```html {linenos=table,hl_lines=[],linenostart=1}
<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
```

Unlike Vanilla HTML5, the AMP requires the entire sidebar to be at the `<body>`
level. The AMPHTML code is as follows:

```html {linenos=table,hl_lines=[],linenostart=1}
<body>
... YOUR PAGE CONTENT HERE ...

<nav class="nav-sidebar">
	<button class="nav-sidebar-icon nav-sidebar-menu"
		on="tap:nav-sidebar-[shasumUID].open">
		... MENU ICON HERE ...
	</button>
</nav>
<amp-sidebar id="nav-sidebar-[shasumUID]"
	class="nav-sidebar"
	layout="nodisplay"
	side="left"
	role="menu"
	tabindex="-1">
	<button id="nav-sidebar-[shasumUID]"
		class="nav-sidebar-icon nav-sidebar-close"
		on="tap:nav-sidebar-[shasumUID].close"
		... CLOSE ICON HERE ...
	</button>
	<ul class="nav-sidebar-list">
		... LIST ITEM HERE ...
	</ul>
</amp-sidebar>
</body>
```

The first difference is that the close button resides inside the sidebar as
compared to vanilla HTML5. The button's Javascript trigger point must acts onto
the `<amp-sidebar>`'s `id=` HTML attribute. Hence, all buttons and sidebar
**MUST** have the same unique ID (`[shasumUID]`) in order to work properly.
Due to the split buttons, the animation is different in AMP due to the use
of its component Javascript as required.

The second difference is it has lesser HTML code inside the `<amp-sidebar>`
since the AMP Sidebar Javascript did all the heavy lifting for triggering
and animation.

**All the list items is the same as Vanilla HTML5**.
Please do note that *the sub-menu list item is still relying on the checkbox
input implementations to work properly.*


#### Right Side
To set the sidebar towards the right side, apart from adding `.right-side` css
class tags into `<nav>` and `<amp-sidebar> AMP HTML syntax, you also need to set
the `side=` attribute to `"right"` inside `<amp-sidebar>`. The code changes are
as follow:

```html {linenos=table,hl_lines=[],linenostart=1}
<nav class="nav-sidebar right-side">
	...
</nav>
<amp-sidebar ...
	class="nav-sidebar right-side"
	...
	side="right"
	...>
		...
</amp-sidebar>
</body>
```


#### Multiple Sidebars
It is unclear whether AMP supports multiple sidebars natively since AMP's
objective is to keep things simple while multiple sidebars simply means the
opposite. Hence, you can try at your own risk.


#### Vanilla HTML5 Comaptibility
In any cases, **the Vanilla HTML5 version `nav-sidebar` is not compatible with
AMPHTML**. Hence, you need to plan out your contents appropriately abide to
AMP standards.




## CSS
This section covers all the CSS component customization using
[CSS variables](https://www.w3.org/TR/css-variables-1/). The customization
allows you to inline the settings in your HTML code without altering the
compiled CSS assets to meet your user interface requirements.

Depending on how "globally" you plan to apply your customization, Bissetii
recommends apply the variable changes inline with `style=` HTML attributes to
specific HTML syntax respectively.

Example, to change a particular `nav-sidebar` width to `80%` and background to
`aquamarine`, we apply it at the `<nav>` and `<amp-sidebar>` accordingly:

```html {linenos=table,hl_lines=[2],linenostart=1}
<nav class="nav-sidebar"
	style="--nav-sidebar-width: 80%; --nav-sidebar-background: aquamarine;">
	...
</nav>
```

There are a bunch of known sub-components that have their respective
customization CSS variables.



### Overall Sidebar
These CSS variables affect the overall sidebar layouts.


#### `--nav-sidebar-width`
Affects the expanded sidebar width. Acceptable values must comply to `width:`
and `max-width:` CSS fields. The default is `75%`.


#### `--nav-sidebar-width-tablet`
Affects the expanded sidebar width when presented in tablet screen dimension.
Acceptable values must comply to `width:` and `max-width:` CSS fields. The
default is `var(--sidebar-width)`.


#### `--nav-sidebar-width-desktop`
Affects the expanded sidebar width when presented in desktop screen dimension
and the sidebar is in drawer mode (has `.drawer` css class). Acceptable values
must comply to `width:` and `max-width:` CSS fields. The default is
`var(--sidebar-width)`.


#### `--nav-sidebar-width-widescreen`
Affects the expanded sidebar width when presented in widrescreen screen
dimension and the sidebar is in drawer mode (has `.drawer` css class).
Acceptable values must comply to `width:` and `max-width:` CSS fields. The
default is `var(--sidebar-width)`.


#### `--nav-sidebar-height`
Affects the expanded sidebar height. Acceptable value must comply to `height:`
and `max-height:` CSS fields. The default values are:

1. mobile: `100vh`
2. tablet: `100vh`
3. desktop: `100%`
4. widescreen: `100%`


#### `--nav-sidebar-z-index`
Affects the sidebar z-index vertical layering. Acceptable values must comply to
`z-index:` CSS field. The default is `var(--z-index-nav)`.


#### `--nav-sidebar-background`
Affects the sidebar's overall background. The acceptable values must comply to
`background:` CSS field. The default is `rgba(0,0,0,.9)`.


#### `--nav-sidebar-padding`
Affects the sidebar's overall padding. Acceptable values must comply to
`padding:` CSS field. The default is `0`.


#### `--nav-sidebar-justify-content`
Affects the sidebar's list vertical alignment using column flex display
mechanism. Acceptable values must comply to `justify-content:` CSS field.
The default is `flex-start`.


{{< note "note" "IMPORTANT" >}}
When using `center` and `flex-end` (towards right), your list **MUST** be small
enough to fit into the smallest device screen. Otherwise, it will grow
off-screen until it is unscrollable.

For long list, stick to `flex-start` (towards left).
{{< /note >}}


#### `--nav-sidebar-grid-area`
Affects the sidebar's layout location. The acceptable values shall be the one
facilitated by the overall grid definitions. Bissetii supplies the following
values under its own default layouts:

1. `sidebar` (default)
2. `rsidebar` (when `.right-side` is enabled)

If you're using your own layout, you need to specify your grid-area name where
a given sidebar shall be rendered onto.


#### `--nav-sidebar-transition`
Affects the sidebar animation timing. The acceptable values shall comply to
`transition:` CSS field. The default value is: `var(--animate-fast)`.



### Sidebar Trigger
The following variables shall affect the trigger button that opens and closes
the sidebar drawer.


#### `--nav-sidebar-trigger-color`
Affects the trigger's text coloring for text-based icons. Acceptable values
shall comply with `color:` CSS field. The default is `var(--color-grey-50)`.

Image based icons are not affected by this field.


#### `--nav-sidebar-trigger-font-size`
Affects the trigger's font size for text-based icons. Acceptable values shall
comply with `font-size:` CSS field. The default is `x-large`.

Image based icons are not affected by this field.


#### `--nav-sidebar-trigger-font-weight`
Affects the trigger's font weight for text based icons. Acceptable values shall
comply with `font-weight:` CSS field. The default is `bold`.

Image based icons are not affected by this field.


#### `--nav-sidebar-position-top`
Affects the top positioning of the trigger. Acceptable value shall comply with
`top:` CSS field. The default is `0`.


#### `--nav-sidebar-trigger-dimension`
Affects the trigger's overall size. Acceptable values shall comply with both
`width:` and `height:` CSS fields, forming a square dimension. The default is
`5rem`.


#### `--nav-sidebar-trigger-transform`
Affects the trigger's animation transform type when triggered. Acceptable values
shall comply with `transform:` CSS field. The default is `rotate(360deg)`.


#### `--nav-sidebar-trigger-z-index`
Affects the trigger's z-index layering dimension. Acceptable values shall comply
with `z-index:` field. The default is `var(--z-index-max)`.

{{< note "note" "Note" >}}
`var(--z-index-max)` is configurable via [Root Component]({{< link
"/components/root" "this" "url-only" />}}) Layout CSS variables.
{{< /note >}}


#### `--nav-sidebar-trigger-border-radius`
Affects the trigger's button border radius roundness shaping. Acceptable values
shall comply with `border-radius:` CSS field. Default is: `0 0 50% 0`.



### Shade Triggering Layer (a.k.a. Overlay)
The following variables affects the triggering shade layer right beneath the
sidebar when opened. Usually it is the opaque white or opaque black. The purpose
is to capture user pressing or clicking on background to return back to main
content.


#### `--nav-sidebar-shade-background`
Affects the triggering shade's background. Acceptable value shall comply with
`background:` CSS field. The default is `rgba(255,255,255,.8)`.


#### `--nav-sidebar-shade-z-index`
Affects the triggering shade's z-index layering dimension. Acceptable values
shall comply with `z-index:` CSS field. The default is
`var(--z-index-nav-overlay)`.



### `.nav-sidebar-title` List Item
The following variables affects the list item (`<li>`) carrying
`.nav-sidebar-title` class.


#### `--nav-sidebar-title-color`
Affects the text color inside the list item. Acceptable value shall comply with
`color:` CSS field. The default is `var(--color-grey-50)`.


#### `--nav-sidebar-title-font-size`
Affects the text font size inside the list item. Acceptable values shall comply
with `font-size:` CSS field. The default is `xx-large`.


#### `--nav-sidebar-title-text-align`
Affects the horizontal text alignment inside the list item. Acceptable values
shall comply with `text-align:` CSS field. The default is `center`.


#### `--nav-sidebar-title-text-transform`
Affects the text styling transformation inside the list item. Acceptable values
shall comply with `text-transform:` CSS field. The default is `uppercase`.


#### `--nav-sidebar-title-font-weight`
Affects the text font weight inside the list item. Acceptable values shall
comply with `font-weight:` CSS field. The default is `300`.


#### `--nav-sidebar-title-line-height`
Affects the text line height inside the list item. Acceptable values shall
comply with `line-height:` CSS field. The default is `auto`.



### `.nav-sidebar-logo` Logo Image
The following variables affect the logo image tag with `.nav-sidebar-logo`
css class tag.


#### `--nav-sidebar-logo-margin`
Affects the logo 4 directional margin spacing. Acceptable values shall comply
with `margin:` CSS field. The default is `3rem auto 0 auto`.



### `.nav-sidebar-link` List Item
The following variables affect the list item `<li>` holding the
`.nav-sidebar-link` css class tag.


#### `--nav-sidebar-link-line-height`
Affects the text line height in the list item. Acceptable values shall comply
with `line-height:` CSS field. The default is `3.5rem`.


#### `--nav-sidebar-link-padding`
Affects the padding spacing of the list item. Acceptable values shall comply
with `padding:` CSS field. The default is `.8rem`.


#### `--nav-sidebar-link-background`
Affects the background of the list item. Acceptable values shall comply with
`background:` CSS field. The default is `transparent`.


#### `--nav-sidebar-link-justify-content`
Affects the horizontal alignment of the list item. Acceptable values shall
comply with `justify-content:` CSS field. The default is `flex-start`.

{{< note "info" "Direction" >}}
1. `flex-start` is left-aligned
2. `center` is center-aligned
3. `flex-end` is right-aligned.
{{< /note >}}


#### `--nav-sidebar-link-align-items`
Affects the vertical alignment of the list item. Acceptable values shall comply
with `align-items:` CSS field. The default is `center`.

{{< note "info" "Direction" >}}
1. `flex-start` is top-aligned
2. `center` is middle-aligned
3. `flex-end` is bottom-aligned.
{{< /note >}}


#### `--nav-sidebar-link-color`
Affects the text color of the list item. Acceptable value shall comply with
`color:` CSS field. The default is `var(--color-grey-50)`.


#### `--nav-sidebar-link-text-transform`
Affects the text transformation of the list item. Acceptable value shall comply
with `text-transform:` CSS field. The default is `captialize`.


#### `--nav-sidebar-link-vertical-align`
Affects the text vertical alignment of the list item. Acceptable value shall
comply with `vertical-align:` CSS field. The default is `middle`.


#### `--nav-sidebar-link-pre-spacing`
Affects the spacing between `pre` icon and the content of the list item.
Acceptable values shall comply with `border-right:` CSS field. The default
value is `1rem`.


#### `--nav-sidebar-link-pre-padding`
Affects the padding spacing for the `pre` icon inside the list item's content.
Acceptable values shall comply with `padding:` CSS field. The default value
is `0 var(--nav-sidebar-link-pre-spacing) 0 0`.


#### `--nav-sidebar-link-pre-margin`
Affects the margin spacing for the `pre` icon inside the list item's content.
Acceptable values shall comply with `margin:` CSS field. The default value is
`0`.


#### `--nav-sidebar-link-pre-background`
Affects the background for the `pre` icon inside the list item's content.
Acceptable values shall comply with `background:` CSS field. The default value
is `transparent`.


#### `--nav-sidebar-link-active-color`
Affects the text color of the list item when it is tagged with `.active` css
class. Acceptable value shall comply with `color:` CSS field. The default value
is `var(--color-c700)`.


#### `--nav-sidebar-link-focus-color`
Affects the text color of the list item when it being focused by user cursor.
Acceptable value shall comply with `color:` CSS field. The default value is
`var(--color-c500)`.


#### `--nav-sidebar-link-hover-color`
Affects the text color of the list item when it being hovered by user cursor.
Acceptable value shall comply with `color:` CSS field. The default value is
`var(--color-c500)`.


#### `--nav-sidebar-link-active-background`
Affects the background of the link item when it is tagged with `.active` css
class. Acceptable values shall comply with `background:` CSS field. The default
value is: `rgba(0,0,0,.35)`.


#### `--nav-sidebar-link-focus-background`
Affects the background of the link item when it is being focused by user cursor.
Acceptable values shall comply with `background:` CSS field. The default value
is: `rgba(0,0,0,.75)`.


#### `--nav-sidebar-link-hover-background`
Affects the background of the link item when it is being hovered by user cursor.
Acceptable values shall comply with `background:` CSS field. The default is
`rgba(0,0,0,.75)`.


#### `--nav-sidebar-link-transition`
Affects the animation timing of the link item. Acceptable values shall comply
with `transition:` CSS field. The default is `var(--animated-short)`.


#### `--nav-sidebar-link-separator-border`
Affects the border top or border bottom that separates each list item.
Acceptable values shall comply with `border-top:` and `border-bottom:` CSS
fields. The default is `1px solid var(--color-grey-700)`.


#### `--nav-sidebar-link-border-left`
Affects the border left of the list item. Acceptable values shall comply with
`border-left:` and `border-right` CSS fields. The default value is `none`.

{{< note "note" "NOTE" >}}
If the sidebar is configured with right side class (`.right-side`), then this
value will be used onto `border-right:` instead for reflection.
{{< /note >}}


#### `--nav-sidebar-link-border-right`
Affects the border right of the list item. Acceptable values shall comply with
`border-left:` and `border-right:` CSS fields. The default value is
`4px solid var(--color-p300)`.

{{< note "note" "NOTE" >}}
If the sidebar is configured with right side class (`.right-side`), then this
value will be used onto `border-left:` instead for reflection.
{{< /note >}}


#### `--nav-sidebar-link-text-align`
Affects the text alignment of the list item. Acceptable values shall comply with
`text-align:` CSS field. The default value is `left`.



### `.nav-sidebar-submenu` List Item
The following variables affect the list item (`<li>`) having the
`.nav-sidebar-submenu` css class tag.


#### `--nav-sidebar-submenu-margin`
Affects the margin for the submenu list item. Acceptable values shall comply
with `margin:` CSS field. The default is `1rem 0 1rem 1rem`.


#### `--nav-sidebar-submenu-padding`
Affects the padding for the submenu list item. Acceptable values shall comply
with `padding:` CSS field. The default is `.8rem 0 .8rem .8rem`.


#### `--nav-sidebar-submenu-animation`
Affects the animation definition for the submenu list item. Acceptable values
shall comply with `animation:` CSS field. The default is
`nav-sidebar-submenu var(--animate-rapid) both`.

{{< note info "Note" >}}
If you're using your own animation, you need to define your own animation name
in your custom css file before applying this animation to the list item.
{{< /note >}}


#### `--nav-sidebar-submenu-icon-animation`
Affects the animation definition for the submenu list item's `pre` icon.
Acceptable values shall comply with `animation:` CSS field. The default value
is `nav-sidebar-submenu-icon var(--animate-rapid) both`.

{{< note info "Note" >}}
If you're using your own animation, you need to define your own animation name
in your custom css file before applying this animation to the list item.
{{< /note >}}


#### `--nav-sidebar-print-display`
Affects the sidebar display during print mode. Acceptable values shall comply
with `display:` CSS field. The default value is `none`.




## Javascript
This component does not use any Javascript.




## Sass
Bissetii uses Dart Sass to compile the styling Sass codes into CSS file. This
component's Sass codes is available at the following location:

```
pkg/internal/navsidebar
```

Bissetii does not package and ship Sass codes explictly. Unless CSS variables
customization is insufficient and in absolute necessary, please view them via
the git repository.




## Researches
Here are the researches done to ensure `Image` component meets the necessarily
quality assurances:

1. **Navigation Sidebar Web User Interface Architectural Design Analysis**
(https://doi.org/10.5281/zenodo.4744173)




## Epilogue
That is all about `nav-sidebar` component in Bissetii. If you need more
feature or encountered a bug, please feel free to contact us by filing an issue
ticket at our [Issue Section](https://gitlab.com/zoralab/bissetii/-/issues). We
will be glad to help you.
