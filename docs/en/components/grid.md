+++
date = "2020-02-06T16:41:59+08:00"
title = "Grid Component"
description = """
`grid` component is vital for organizing HTML elements horizontally. Without
`grid`, every contents are arranged vertically with  100% width as default.
Bissetii uses the CSS Flexbox to perform its dynamic wrapping and the latest can
now work without hard-coded mathematics (e.g. 12 columns or fixed percent).
"""
keywords = ["grid", "components", "html", "w3c", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "grid"
pre = "🗂️"
weight = 1


[schema]
selectType = "WebPage"
+++

# `grid` Component
{{% param "Description" %}}

This component is available starting from `v1.11.0`.

Example usage:

```html {linenos=table,hl_lines=[],linenostart=1}
<div class="row">
	<div class="column">
		...
	</div>
	...
</div>
```




## Hugo
Bissetii supports Hugo interfaces to make content creation easier depending on
Bissetii version.



### Shortcodes
Bissetii prepared a list of shortcodes to make website creation a lot easier.
Here are some of the available shortcodes related to `Grid` component.


#### `grid` Shortcode
The `grid` shortcode allows you to create a `.row` or a `.column` containers
easily when using Bissetii in Hugo.

This shortcode is available starting from version `v1.13.1`.

The format is as follows:

```markdown {linenos=table,hl_lines=[],linenostart=1}
{{</* grid [TYPE] [STYLE] [CLASS] [ID] */>}}
	[CONTENT]
{{</* /grid */>}}
```

1. `TYPE` - **COMPULSORY**
	1. Denotes the sub-component. Either `"row"` or `"column"`.
2. `STYLE` - **OPTIONAL**
	1. The `style=` attribute for the sub-component.
3. `CLASS` - **OPTIONAL**
	1. The `class=` attribute for the sub-component.
4. `ID` - **OPTIONAL**
	1. The `id=` attribute for the sub-component.
5. `CONTENT` - **COMPULSORY**
	1. The content inside the sub-component.

To use it, simply call it inside the markdown. Example:

```markdown {linenos=table,hl_lines=[],linenostart=1}
{{</* grid "row" "--grid-column-margin: 2px;" */>}}
	{{</* grid "column" */>}}
		COLUMN 1
	{{</* /grid */>}}

	{{</* grid "column" */>}}
		COLUMN 2
	{{</* /grid */>}}
{{</* /grid */>}}

		⥥

{{% grid "row" "--grid-column-margin: 2px;" %}}
	{{% grid "column" %}}
		COLUMN 1
	{{% /grid %}}

	{{% grid "column" %}}
		COLUMN 2
	{{% /grid %}}
{{% /grid %}}
```




## Go
Coming Soon.




## HTML
Bissetii supports its grid system seamlessly and similarly in multiple output
formats. Additionally, starting from version `v1.13.0`, the use of CSS variables
is vital for its upgrade from `v1.12.5`.

Bissetii HTML codes are the same for Vanilla HTML5 and AMPHTML. The grid system
takes in the form of `.row` and multiple `.column`s using `<div>` HTML tag. Here
is the main pattern:

```html {linenos=table,hl_lines=[],linenostart=1}
<div class="row">
	<div class="column">
		...
	</div>
	<div class="column">
		...
	</div>
	...
</div>
```

There is no limit for how many `.column` in a `.row` as long as it is sensible.
The rest of the customizations are depending on the Bissetii version.



### Quantified Re-Sizing
To keep things familar across Bissetii's competitors, Bissetii supplies the
porpotional sizing for columns.


#### Version `1.13.0` and Above
Bissetii uses simple multiplication to provide the column's `BASE` width and
its `MULTIPLIER` using the following formula:

```
flex-grow: calc(BASE *  MULTIPLIER);
```

Where:
1. `BASE` must be the same across all columns in a `.row`
2. `BASE` > `MULTIPLIER`
3. Default `BASE` = `10`
4. Default `MULTIPLIER` = `1`
5. If the total width is not permittable (e.g. hitting max width of a `.row`),
the growth and shrink will be halted all together.

via the following CSS variables (see [CSS section]({{< link "#css" "this"
"url-only" />}}) for more info):

```CSS
--grid-column-base: 10;      /* BASE - set at .row */
--grid-column-multiplier: 1; /* MULTIPLIER - set at .column */
```

The HTML pattern is as such:

```html {linenos=table,hl_lines=[6],linenostart=1}
<div class="row" style="--grid-column-base: 12">
	<div class="column" style="--grid-column-multiplier: 2">
		...
	</div>
	...
</div>
```

As an example, the above will render as the following:

{{< renderHTML html amp >}}
<div class="row" style="border: 2px solid red; text-align: center;">
	<div class="column" style="background-color: grey; border-radius: 5px;">
		<p>B = default | M = default</p>
	</div>

	<div class="column" style="background-color: grey; border-radius: 5px;">
		<p>B = default | M = default</p>
	</div>
</div>

<div class="row" style="border: 2px solid red; text-align: center;">
	<div class="column" style="background-color: grey; border-radius: 5px;">
		<p>B = default | M = default</p>
	</div>

	<div class="column" style="--grid-column-multiplier: 2;
		background-color: grey;
		border-radius: 5px;">
		<p>B = default | M = 2</p>
	</div>
</div>

<div class="row" style="border: 2px solid red; text-align: center;">
	<div class="column" style="background-color: grey; border-radius: 5px;">
		<p>B = default | M = default</p>
	</div>

	<div class="column" style="--grid-column-multiplier: 3;
		background-color: grey;
		border-radius: 5px;">
		<p>B = default | M = 3</p>
	</div>
</div>

<div class="row" style="border: 2px solid red;
		text-align: center;
		--base-grid-column: 5;">
	<div class="column" style="--grid-column-multiplier: 3;
		background-color: grey;
		border-radius: 5px;">
		<p>B = default | M = 3</p>
	</div>

	<div class="column" style="--grid-column-multiplier: 2;
		background-color: grey;
		border-radius: 5px;">
		<p>B = default | M = 2</p>
	</div>
</div>
{{< /renderHTML >}}


#### Version `1.12.5` And Below
{{< note "warning" "Important Notice" >}}
Support for `.grid-2x .grid-3x ...` css classes for `v1.12.5` and below is
scheduled to be stopped and removed starting **July 31, 2021**. While the codes
remain available for now, the team will no longer answer any questions related
to the matter.

**Please DO your migrations as soon as possible.**
{{< /note >}}
Bissetii uses a constant `10` as `BASE` value and supplies `grid-2x`, `grid-3x`,
... up to `grid-10x` as their respective MULTIPLIER.

Here is an example of the HTML code:

```html {linenos=table,hl_lines=[6,16,26,36,46,56,66,76,86],linenostart=1}
<div class="row">
	<div class="column">
		<p>Column 1</p>
	</div>

	<div class="column grid-2x">
		<p>Column 2</p>
	</div>
</div>
```



### Hide a Column
Bissetii allows a column to remain hidden at will. The implementation depends
on which version you deployed.


#### Version `1.13.0` and Above
To hide a `.column`, simply set the `--grid-column-visibility: hidden;` onto
that column's inline CSS style. Example:

```html {linenos=table,hl_lines=[],linenostart=1}
<div class="row">
	<div class="column">
		...
	</div>
	<div class="column" style="--grid-column-visibility: hidden;">
		...
	</div>
	<div class="column">
		...
	</div>
</div>
```

This will render as:

{{< renderHTML html amp >}}
<div class="row" style="border: 2px solid red; text-align: center; color: white;">
	<div class="column" style="background: grey;">
		<p>Column 1</p>
	</div>
	<div class="column" style="background: grey; --grid-column-visibility: hidden;">
		<p>Column 2</p>
	</div>
	<div class="column" style="background: grey;">
		<p>Column 3</p>
	</div>
</div>
{{< /renderHTML >}}


#### Version `1.12.5` and Below
{{< note "warning" "Important Notice" >}}
Support for `.offset` css class for `v1.12.5` and below is scheduled to be
stopped and removed starting **July 31, 2021**. While the codes remain
available for now, the team will no longer answer any questions related to the
matter.

**Please DO your migrations as soon as possible.**
{{< /note >}}

For version `1.12.5` and below, apply `.offset` class tag to the column.
Example:

```html {linenos=table,hl_lines=[],linenostart=1}
<div class="row">
	<div class="column">
		...
	</div>
	<div class="column offset">
		...
	</div>
	<div class="column">
		...
	</div>
</div>
```



### Align Vertically
Bissetii grid system permits vertical alignment for all columns' content in the
entire row using the Flexbox's `align-items:` CSS field. Depending on Bissetii's
version, the implementation can be done differently.


#### Version `1.13.0` and Above
To set the column's content vertical alignment, set the `--grid-align-items`
CSS variable provided by Bissetii at the `.row` container. For more info about
`--grid-align-items`, please see the [CSS section]({{< link "#css"
"this" "url-only" />}}). Here is the HTML code pattern:

```html {linenos=table,hl_lines=[],linenostart=1}
<div class="row" style="--grid-align-items: center;">
	<div class="column">
		...
	</div>
	...
</div>
```

This will render as such:

{{< renderHTML html amp >}}
<div class="row" style="border: 2px solid red; text-align: center;
		--grid-align-items: center;">
	<div class="column" style="background-color: grey; border-radius: 5px;">
		<p style="color: white; text-align: center;">center</p>
		<p style="color: white; text-align: center;">center</p>
		<p style="color: white; text-align: center;">center</p>
	</div>
	<div class="column" style="background-color: grey; border-radius: 5px;">
		<p style="color: white; text-align: center;">center</p>
	</div>
	<div class="column" style="background-color: grey; border-radius: 5px;">
		<p style="color: white; text-align: center;">center</p>
		<p style="color: white; text-align: center;">center</p>
		<p style="color: white; text-align: center;">center</p>
		<p style="color: white; text-align: center;">center</p>
		<p style="color: white; text-align: center;">center</p>
	</div>
</div>
{{< /renderHTML >}}


#### Version `1.12.5` and Below
{{< note "warning" "Important Notice" >}}
Support for `.align-top .align-middle ...` css classes for `v1.12.5` and below
is scheduled to be stopped and removed starting **July 31, 2021**. While the
codes remain available for now, the team will no longer answer any questions
related to the matter.

**Please DO your migrations as soon as possible.**
{{< /note >}}
For version `1.12.5` and below, apply `.align-[DIRECTION]` class tags to the
column.

Example:

```html {linenos=table,hl_lines=[],linenostart=1}
<div class="row align-top">
	...
</div>

<div class="row align-middle">
	...
</div>

<div class="row align-bottom">
	...
</div>

<div class="row align-baseline">
	...
</div>
```




## CSS
Bissetii provided a list of CSS variables dedicated for grid styling alteration
without needing to recompile Sass. These variables are best applied directly to
the Grid HTML tags. Example:

```html {linenos=table,hl_lines=[],linenostart=1}
<div class="row" style="--grid-column-base: 12;">
	<div class="column" style="--grid-column-multiplier: 3;">
		...
	</div>
	...
</div>
```



### `--grid-width`
Set the width of the column. The acceptable value shall be compatible with
`width:` CSS fields. The default value is `100%`.



### `--grid-justify-content`
Set the spacing between the columns. The acceptable value shall be compatible
with `justify-content:` CSS field. The default value is `space-around`.



### `--grid-align-items`
Set the alignment of the columns. The acceptable value shall be compatible
with `align-items:` CSS field. The default value is `stretch`.



### `--grid-flex-wrap`
Set the flex wrapping for all the columns. The acceptable value shall be
compatible with `flex-wrap:` CSS field. The default value is `wrap`.



### `--grid-column-display`
Set the display type for the column. The acceptable value shall be compatible
with `display:` CSS field. The default value is `block`.



### `--grid-column-max-width`
Set the column's max width. The acceptable value shall be compatible with
`max-width:` CSS field. The default value is `100%`.



### `--grid-column-margin`
Set the column's margin. The acceptable value shall be compatible with `margin:`
CSS field. The default value is `.3rem`.



### `--grid-column-padding`
Set the column's padding. The acceptable value shall be compatible with
`padding:` CSS field. The default value is `0`.

This is only available for Bissetii version `v1.13.1` and above.



### `--grid-column-visibility`
Set the column's visibility. The acceptable value shall be compatible with
`visibility:` CSS field. The default value is `visible`.



### `--grid-column-base`
Set the total columns in a row as base number. The acceptable value shall be
a round number compatible for `calc()` CSS input calculations function. The
default is `10`.

{{< note info "Info" >}}
The minimum is `1`. For bootstrap grid system, the base is `12`.
{{< /note >}}



### `--grid-column-multiplier`
Set the column width to the multiplication of `--grid-column-base`. The
acceptable value shall be a round number compatible for `calc()` CSS input
calculations function. The default is `1`.

{{< note info "Note" >}}
The minimum is `1`. For `grid-2x` (`2x`), then the multiplier is `2`.
{{< /note >}}



### `--grid-column-flex-basis`
Set the column flexibility to responsive screen. The acceptable value shall be
compatible with `flex-basis:` CSS field. The default is `auto`.




## Javascript
This component does not depend any Javascript.




## Sass
Depending on release version, the Sass files work differently. Bissetii does not
package Sass codes explictly so please view them via the git repository.



### `v1.13.0` and Above
Bissetii uses Dart Sass to compile the styling Sass codes into CSS file. This
component's Sass codes are available at the following location:

```
pkg/components/internal/image
```



### `v1.12.5` and Before
{{< note "warning" "Deprecation Notice" >}}
Starting from version `v1.13.0`, this version of Sass is no longer used and the
support is scheduled to be removed on **July 31, 2021**.
{{< /note >}}

The Sass scripts responsible for styling image are located at:

```
assets/css/bissetii/modules/core/_Grid.scss
```




## Researches
Here are the researches done to ensure `Grid` component meets the necessary
quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `grid` component in Bissetii. If you need more feature or
need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
