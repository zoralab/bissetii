+++
date = "2020-02-02T18:12:59+08:00"
title = "Blockquote Component"
description = """
`Blockquote` is the component for adding a quote from a reference sources.
Bissetii uses the standard W3C HTML tag for rendering `Blockquote`'s content.
"""
keywords = ["blockquote", "components", "html", "w3c", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Blockquote"
pre = "💬"
weight = 1


[schema]
selectType = "WebPage"
+++

# `Blockquote` Component
{{% param "description" %}}

This component is available starting from `v1.11.0`.




## Hugo
Hugo supports list seamlessly so no special development is required.




## Go
Coming Soon.




## HTML
Bissetii supports `List` component seamlessly across multiple output formats.
Additionally, starting from version `v1.13.0`, the use of CSS variable is vital
for its upgrade from `v1.12.5`.

Bissetii HTML codes for this component are the same for Vanilla HTML5 and
AMPHTML.

The common `Blockquote` HTML code pattern is as follows:

```html {linenos=table,hl_lines=[],linenostart=1}
<blockquote cite="https://loremipsum.io/">
	<p><b>Lorem Ipsum</b></p>
	<p>
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
culpa qui officia deserunt mollit anim id est laborum.
	</p>
	<cite>John Smith, 2021</cite>
</blockquote>
```

This will render as:

{{< renderHTML "html" "amp" >}}
<blockquote cite="https://loremipsum.io/">
	<p><b>Lorem Ipsum</b></p>
	<p>
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
culpa qui officia deserunt mollit anim id est laborum.
	</p>
	<cite>John Smith, 2021</cite>
</blockquote>
{{< /renderHTML >}}




## CSS
Bissetii provided a list of CSS variables dedicated for blockquote styling
alteration without needing to recompile Sass. These variables are best applied
directly to the blockquote HTML tags. Example:

```html {linenos=table,hl_lines=[],linenostart=1}
<blockquote style="--blockquote-background-color: none;">
	...
	<cite style="--blockquote-cite-font-style: bold;">
		...
	</cite>
</blockquote>
```



### `--blockquote-color`
Affects the text color inside the entire `<blockquote>`. The acceptable value
shall be compatible with `color:` CSS field. The default is
`var(--color-grey-800)`.



### `--blockquote-background-color`
Affects the background color of the entire `<blockquote>`. The acceptable value
shall be compatible with `background-color` CSS field. The default is
`var(--color-grey-50)`.



### `--blockquote-border`
Affects the border of the entire `<blockquote>` in all directions. The
acceptable value shall be compatible with `border:` CSS field. The default is
`0.1rem solid var(--color-grey-800)`.



### `--blockquote-border-left`
Affects the border of the entire `<blockquote>` only in left direction. The
acceptable value shall be compatible with `border-left:` CSS field. The default
is `0.5rem solid var(--color-grey-800)`.



### `--blockquote-border-radius`
Affects the edge roundness of the entire `<blockquote>`. The acceptable value
shall be compatible with `border-radius:` CSS field. The default is
`0.4rem`.



### `--blockquote-margin`
Affects the margin spacing of the entire `<blockquote>`. The acceptable value
shall be compatible with `margin:` CSS field. The default is `2.1rem 0`.



### `--blockquote-padding`
Affects the padding spacing of the entire `<blockquote>`. The acceptable value
shall be compatible with `padding:` CSS field. The default is `1rem 1.5rem`.



### `--blockquote-font-style`
Affects the font style of the entire `<blockquote>`. The acceptable value shall
be compatible with `font-style:` CSS field. The default is `italic`.



### `--blockquote-print-break-inside`
Affects the page breaking inside the `<blockquote>`. The acceptable value shall
be compatible with `break-inside:` CSS field. The default is `avoid`.



### `--blockquote-cite-display`
Affects the display type of the `<cite>` inside `<blockquote>`. The acceptable
value shall be compatible with `display:` CSS field. The default is `block`.



### `--blockquote-cite-margin`
Affects the margin spacing of the `<cite>` inside `<blockquote>`. The acceptable
value shall be compatible with `margin:` CSS field. The default is `1rem auto`.



### `--blockquote-cite-font-size`
Affects the font size of the `<cite>` inside `<blockquote>`. The acceptable
value shall be compatible with `font-size:` CSS field. The default is `larger`.



### `--blockquote-cite-font-style`
Affects the font style of the `<cite>` inside `<blockquote>`. The acceptable
value shall be compatible with `font-style:` CSS field. The default is `italic`.



### `--blockquote-cite-text-align`
Affects the text alignment of the `<cite>` inside `<blockquote>`. The acceptable
value shall be compatible with `text-align:` CSS field. The default is `center`.




## Javascript
This component does not rely on any Javascript.




## Sass
Depending on release version, the Sass files work differently. Bissetii does not
package Sass codes explictly so please view them via the git repository.



### `v1.13.0` and Above
Bissetii uses Dart Sass to compile the styling Sass codes into CSS file. This
component's Sass codes are available at the following location:

```
pkg/components/internal/blockquote
```



### `v1.12.5` and Before
{{< note "warning" "Deprecation Notice" >}}
Starting from version `v1.13.0`, this version of Sass is no longer used and the
support is scheduled to be removed on **July 31, 2022**.
{{< /note >}}
The Sass scripts responsible for styling the component are located in:

```
assets/css/bissetii/modules/core/_Blockquote.scss
```




## Researches
Here are the researches done to ensure `Blockquote` component meets the
necessary quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `Blockquote` component in Bissetii. If you need more feature or
need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
