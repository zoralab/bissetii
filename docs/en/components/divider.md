+++
date = "2020-02-03T08:49:56+08:00"
title = "Divider Component"
description = """
Bissetii provides the `Divider` component as per HTML spec seamlessly. Although
Bissetii sections each of the contents via headers automatically, there are
some use cases where a manual horizontal divider bar will be best used.
"""
keywords = ["divider", "components", "html", "w3c", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Divider"
pre = "🗡️"
weight = 1


[schema]
selectType = "WebPage"
+++

# `Divider` Component
{{% param "description" %}}

This component is available starting from `v1.11.0`.




## Hugo
Hugo supports `Divider` seamlessly so no special development is required.




## Go
Coming Soon.




## HTML
Bissetii supports `Divider` component seamlessly across multiple output formats.
Additionally, starting from version `v1.13.0`, the use of CSS variable is vital
for its upgrade from `v1.12.5`.

Bissetii HTML codes for this component are the same for Vanilla HTML5 and
AMPHTML.

An example HTML pattern Bissetii recommends would be:

```html {linenos=table,hl_lines=[],linenostart=1}
<hr/>
```

This will render as:

{{< renderHTML html amp >}}
<hr/>
{{< /renderHTML >}}




## CSS
Bissetii provided a list of CSS variables dedicated for divider styling
alteration without needing to recompile Sass. These variables are best applied
directly to the HTML tags. Example:

```html {linenos=table,hl_lines=[],linenostart=1}
<hr style="--divider-margin: 10rem 0;" />
```



### `--divider-overflow`
Set the divider overflow behavior. The acceptable value shall be compatible with
`overflow:` CSS field. The default is `visible`.



### `--divider-margin`
Set the margin spacing of the divider. The acceptable value shall be compatible
with `margin:` CSS field. The default is `5.0rem 0`.



### `--divider-break-inside`
Set the behavior for page break inside the divider. The acceptable value shall
be compatible with `break-inside:` CSS field. The default is `avoid`.




## Javascript
This component does not rely on any Javascript.




## Sass
Depending on release version, the Sass files work differently. Bissetii does not
package Sass codes explictly so please view them via the git repository.



### `v1.13.0` and Above
Bissetii uses Dart Sass to compile the styling Sass codes into CSS file. This
component's Sass codes are available at the following location:

```
pkg/components/internal/divider
```



### `v1.12.5` and Before
{{< note "warning" "Deprecation Notice" >}}
Starting from version `v1.13.0`, this version of Sass is no longer used and the
support is scheduled to be removed on **July 31, 2022**.
{{< /note >}}
The Sass scripts responsible for styling the component are located in:

```
assets/css/bissetii/modules/core/_Divider.scss
```




## Researches
Here are the researches done to ensure `Divider` component meets the necessary
quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `Divider` component in Bissetii. If you need more feature or
need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
