+++
date = "2020-02-11T22:18:40+08:00"
title = "Navbar Component"
description = """
"Navbar" is the navigation menus for users to navigate across the websites. To
avoid using Javascript, Bissetii supports most of its navigation styling using
specific HTML pattern especially when using the "<input type='checkbox'>"
toggling trick.
"""
keywords = ["navbar", "components", "html", "w3c", "bissetii"]
draft = false
type = ""
indexLink = "#html-parts"
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Navbar (DEPRECATED)"
pre = "❗"
weight = 1


[schema]
selectType = "WebPage"
+++

# `Navbar` Component
`Navbar` is the navigation menus for users to navigate across the websites. To
avoid using Javascript, Bissetii supports most of its navigation styling using
specific HTML pattern especially when using the `<input type='checkbox'>`
toggling trick.

This component is available starting from `v1.11.0`.

{{< note "warning" "DEPRECATION NOTICE" >}}
This component is deprecated starting from version `v1.13.0` and it is created
from scratch into [Nav - Sidebar Component]({{< link "/components/nav-sidebar"
"this" "url-only" />}}). All `navbar` shall migrate into that `nav-sidebar`
component for its HTML and CSS flexibilities.

It will be rescinded on **July 31, 2022**.
{{< /note >}}




## Javascript Parts
This component does not depend on any Javascript.




## Sass Styling Parts
The Sass styling scripts are located at:

```
assets/css/bissetii/modules/core/_Nav.scss
assets/css/bissetii/modules/nav/*
```

The index script (`_.scss`) is a mixins compiler where it takes in a number
of configs to setup the correct mixin groups to include in. The configs will
be detailed in the HTML Parts sub-sections.

The base script (`core/_Nav.scss`) governs the module's private variables and
fallback design when user forgets to specify the navbar types via class or ID
tags. This script will automatically be included when one or many types of
navbars are included together.

The sidebar script (`_Sidebar.scss`) is responsible for styling sidebar types
navbar.


### Changing Bullets
There are 2 different implementations depending on the version used.


#### Version `v1.12.0` Onwards
From version `v1.12.0`, the Sass embedded default symbols were completely
removed to facilitate flexiblity. The list items must comply to the
**2 `<span>` format**: 1 for bullet and tab spacing, and 1 for text content.

To add symbol, you can add it into the `<span class"nav-symbol>` text. The new
coding pattern for items with example are shown below:

```html {linenos=table,hl_lines=[2,6,10],linenostart=1}
<li><a href="https://www.example.com">
	<span class="nav-symbol">⌁</span>
	<span>Item 1</span>
</a></li>
<li><a href="https://www.example.com">
	<span class="nav-symbol">✤</span>
	<span>Item 2</span>
</a></li>
<li><a href="https://www.example.com">
	<span class="nav-symbol"></span>
	<span>Item 3</span>
</a></li>
```
You should add the hyperlink coverage to the desired component on your side
with absolute freedom.

As for navbar, it is added into the `<label>` tag:

```html {linenos=table,hl_lines=["4-9"],linenostart=1}
<label id="navside-button-sidebar-222"
	class="navbar-button"
	for="nav-main-sidebar-222">
	<img class="navbar-button-open"
		alt="Open"
		src="https://www.domain.com/img/css/menu-icon.svg" />
	<img class="navbar-button-close"
		alt="Close"
		src="https://www.domain.com/img/css/close-icon.svg" />
</label>
```


#### Version `v1.11.0` and Below
For version `v1.11.0`, to alter the bullets, you can supply the following
configurations before importing/using Bissetii core modules:

```sass
$nav-bullet: "✤"
$nav-bullet-active: "⌁"
$nav-bullet-menu: "☰"
$nav-bullet-close: "✕"
```





## HTML Parts
Bissetii introduces a number of navbars available for use.

* [Basic HTML Layouts](#basic-html-layouts)
* [Sidebar](#sidebar)
* [Table of Contents](#table-of-contents)


### Basic HTML Layouts
It can be quite taxing for HTML to cater navigation bar without using any
Javascript. Hence, there are some basic rules for you to comply with in order
to make Bissetii works.


#### Navbar First, First Content Next
This refers to the overall layout where `<nav>` always leads before `<main>` or
equivalent content HTML semantics. One example is:

```html {linenos=table,hl_lines=["6-11"],linenostart=1}
<html>
<head>
	...
</head>
<body>
	<nav>
		...
	</nav>
	<main>
		...
	</main>
	<footer>
		...
	</footer>
</body>
</html>
```


#### Tagging: class for CSS
Bissetii's navbar applies via the class tag. Example, the following works:

```html
<nav class="Sidebar">
	...
</nav>
```

{{< note "default" "Important Note" >}}
Starting from `v1.12.0`, Bissetii's Sidebar no longer complies to `id=Sidebar`.
This is to strictly separate Javascript and styling callers where styling only
uses class tag.
{{< /note >}}


#### Checkbox Input Toggle
Bissetii uses checkbox input to perform toggling action. It heavily relies on
the associated `<label>` to provide good user experience actions. This section
covers only the vanilla HTML. For AMP HTML, you must refer to the specific
navbar.


#### There Is Always A Fallback Mode
Just in case, navbar always has a fallback mode for default styling in situation
where designers forgotten to add the class tag. The fallback mode is rendered as
a "Table of Contents" type of navigation. The Sass script for handling such
fallback mode is `_Base.scss`.

[Back to Index]({{< param "IndexLink" >}})


----


### Sidebar
A very common mode is the `Sidebar` type. Sidebar attaches itself to the left
side of the parent (e.g. `<body>` or `<div>`). On mobile and tablet, `Sidebar`
is rendered as hamburger drawer menu from the left. On bigger screen like
desktop and beyond, it is the "always open" left sidebar that occupies 20%
of the parent width.


#### Enable Sidebar (Sass)
In your Sass main script, you must supply the following config set to `1` before
importing/using Bissetii Sass nav module:

```sass
$config-nav-sidebar: 1
```


#### Deploy Sidebar (HTML)
To enable `Sidebar`, attach the CSS class tag `Sidebar` to the `<nav>` tag.
Example:

```html
<nav class="Sidebar">
...
</nav>
```


#### Vanilla HTML Layout
For vanilla HTML, the default HTML layout is used and shown below. Each
important points are highlighted and described.

{{< note "default" "important note" >}}
Starting from version `v0.12.0`, you **MUST** apply `sidebar-mainmenu` to the
main `<ul>` in order to render the list.
{{< /note >}}

```html {linenos=table,hl_lines=[1,2,"5-10",11,17,24,28,29,34,35,42,45,52,53]}
<nav class="Sidebar">
	<header class="site-title">
		<p>Site Title</p>
	</header>
	<input id="nav-main-sidebar-UNIQUEID"
		class="navbar-button"
		type="checkbox" />
	<label id="navside-button-sidebar-UNIQUEID"
		class="navbar-button"
		for="nav-main-sidebar-UNIQUEID">
		<img src="{{< link "/img/css/menu-icon.svg" "this" "url-only" />}}"
			class="navbar-button-open"
			alt= "Open"
			width="33"
			height="35"
			loading="lazy" />
		<img src="{{< link "/img/css/close-icon.svg" "this" "url-only" />}}"
			class="navbar-button-close"
			alt= "Close"
			width="33"
			height="35"
			loading="lazy" />
	</label>
	<label id="navbar-shade-sidebar-UNIQUEID"
		class="nav-sidebar-shade"
		for="nav-main-sidebar-UNIQUEID">
	</label>
	<section>
		<header>
			<p>Menu Title</p>
		</header>
		<ul class="sidebar-mainmenu">
			<li><a href="#">
				<span class="nav-symbol">⌁</span>
				<span>Item 1</span>
			</a></li>
			<li><a href="#">
				<span class="nav-symbol">✤</span>
				<span>Item 2</span>
			</a></li>
			<li>
				<input id="submenu-SUBMENU-ID1"
					class="submenu"
					type="checkbox" />
				<label id="submenu-SUBMENU-ID1"
					class="submenu">
					<span class="nav-symbol">
						✤
					</span>
					<span>Sub-Category</span>
				</label>
				<ul id="submenu-SUBMENU-ID1"
					class="submenu">
					<li><a href="#">
						<span class="nav-symbol">⌁</span>
						<span>Item 1</span>
					</a></li>
					<li><a href="#">
						<span class="nav-symbol">✤</span>
						<span>Item 2</span>
					</a></li>
					<li><a href="#">
						<span class="nav-symbol"></span>
						<span>Item 3</span>
					</a></li>
				</ul>
			</li>
			<li><a href="#">
				<span class="nav-symbol"></span>
				<span>Item 3</span>
			</a></li>
		</ul>
	</section>
</nav>
```

Some noticable parts would be:

* [**1**] - Ensure you apply `Sidebar` class tag to the `<nav>` HTML tag.
* [**2**] - Site title for always appearing display. You should use `<p>` tag
            to avoid `<h1>` SEO conflicting usage.
* [**5-10**] - Use of checkbox input and labels for toggling. This is the
               trigger point for open/close sidebar. You should supply an
               UNIQUEID (e.g. `ea1231adv`) for it.
* [**11,17**] - Use of images for rendering button icons. There should be 1
                for open with class tag (`navbar-button-open`) and 1 for closing
                with class tag (`navbar-button-close`). Both images must be
                the children for the corresponding open/close toggling
                `<label>`.
* [**24**] - Create corresponding shade `<label>` for mobile user experience.
             This is for closing the sidebar when user presses the background
             instead of the toggling button.
* [**28**] - The body of the sidebar. Use `<section>` tag.
* [**29**] - Menu title when the drawer is opened. use `<p>` tag for text.
* [**32**] - Main menu listing. `sidebar-mainmenu` class tag must be applied.
* [**34**] - Bullet inclusion. `nav-symbol` class tag must be applied. Leave it
             empty if unused.
* [**35**] - List item text.
* [**42,45**] - Optional submenu toggling. It uses the same concept as the main
                menu. Reserved for future nested sidebar development. Likewise,
                each sub-menu must have its own UNIQUEID to avoid conflicting
                toggling.
* [**52-53**] - Sub-menu example. The list must have `submenu` class tag.


#### AMP HTML Layout
For AMP sidebar, its AMPHTML is entirely different from the vanilla HTML.

You must import `amp-sidebar` to facilitate the sidebar operations in the meta
tag as documented in
[amp-sidebar](https://amp.dev/documentation/components/amp-sidebar/).

Compared to vanilla HTML, AMPHTML uses less codes since most of the rendering
are done by the `amp-sidebar` extension module. Hence, they are incompatible
to each other.

The AMPHTML for Sidebar is as follows:

```html {linenos=table,hl_lines=[1,2,"5-7",8,16,20,27,30,32,33,40,43,50,51]}
<nav class="Sidebar">
	<header class="site-title">
		<p>Site Title</p>
	</header>
	<button id="navside-button-222"
		class="navbar-button clean"
		on="tap:"UNIQUEID".toggle">
		<amp-img alt="Open"
			width="35"
			height="35"
			src="{{< link "/img/css/menu-icon.svg"
				"this"
				"url-only" />}}"
			layout="responsive">
		</amp-img>
	</button>
</nav>
<amp-sidebar id="222" layout="nodisplay" side="left">
	<button id="navside-button-222"
		class="navbar-button clean"
		on="tap:"UNIQUEID".close">
		<amp-img alt="Close"
			width="35"
			height="35"
			src="{{< link "/img/css/close-icon.svg"
				"this"
				"url-only" />}}"
			layout="responsive">
		</amp-img>
	</button>
	<header>
		<p>Menu Title</p>
	</header>
	<ul class="sidebar-mainmenu">
		<li><a href="#">
			<span class="nav-symbol">⌁</span>
			<span>Item 1</span>
		</a></li>
		<li><a href="#">
			<span class="nav-symbol">✤</span>
			<span>Item 2</span>
		</a></li>
		<li>
			<input id="submenu-SUBMENU-ID1"
				class="submenu"
				type="checkbox" />
			<label id="submenu-SUBMENU-ID1"
				class="submenu">
				<span class="nav-symbol">
					✤
				</span>
				<span>Sub-Category</span>
			</label>
			<ul id="submenu-SUBMENU-ID1"
				class="submenu">
				<li><a href="#">
					<span class="nav-symbol">⌁</span>
					<span>Item 1</span>
				</a></li>
				<li><a href="#">
					<span class="nav-symbol">✤</span>
					<span>Item 2</span>
				</a></li>
				<li><a href="#">
					<span class="nav-symbol"></span>
					<span>Item 3</span>
				</a></li>
			</ul>
		</li>
		<li><a href="#">
			<span class="nav-symbol"></span>
			<span>Item 3</span>
		</a></li>
	</ul>
</amp-sidebar>
```

Some noticable parts would be:

* [**1**] - Ensure you apply `Sidebar` class tag to the `<nav>` HTML tag.
* [**2**] - Site title for always appearing display. You should use `<p>` tag
            to avoid `<h1>` SEO conflicting usage.
* [**5-7**] - Use of button and AMP built-in toggle function. This is the
               trigger point for opening sidebar. You should supply an UNIQUEID
               (e.g. `ea1231adv`) for it alongside its `on=` trigger.
* [**8**]     - Use of image for rendering menu icon. The image must use
                `<amp-img>` HTML tag and it must contain the
                `navbar-button-open` class tag.
* [**16**] - The body of the sidebar. Use `<amp-sidebar>` tag with
             `id=UNIQUEID`, `layout="nodisplay"`, and `side="left"`.
* [**20**]     - Use of image for rendering close icon. The image must use
                `<amp-img>` HTML tag and it must contain the
                `navbar-button-close` class tag.
* [**27**] - Menu title when the drawer is opened. use `<p>` tag for text.
* [**30**] - Main menu listing. `sidebar-mainmenu` class tag must be applied.
* [**32**] - Bullet inclusion. `nav-symbol` class tag must be applied. Leave it
             empty if unused.
* [**33**] - List item text.
* [**40,43**] - Optional submenu toggling. It uses the same concept as the main
                menu. Reserved for future nested sidebar development. Likewise,
                each sub-menu must have its own UNIQUEID to avoid conflicting
                toggling.
* [**50-51**] - Sub-menu example. The list must have `submenu` class tag.

[Back to Index]({{< param "IndexLink" >}})


----

### Table of Contents
"Table of Contents" navigation is the fundamental and fallback navigation
rendering in Bissetii. It basically renders the entire list as a list table.


#### Enable Table of Contents (Sass)
Should any specific navigation bar is imported, "Table of Contents" will be
included automatically as a fallback mode. Unless otherwise necessary, you can
**optionally**, explictly include it by setting the following config to `1`
before importing/using Bissetii Sass nav module:

```sass
$config-nav: 1
```

The Sass script responsible for rendering "Table of Contents" is located at:
`assets/css/bissetii/modules/nav/_Base.scss`.


#### Deploy Table of Contents (HTML)
"Table of Contents" requires no class tag attached to `<nav>` tag. A simple
`<nav>` tag is suffice.

```html
<nav>
...
</nav>
```


#### Vanilla HTML Layout
A Basic table of contents structure would be:

```html {linenos=table,hl_lines=[1,2,3,6,8,9]}
<nav>
	<section>
		<header>
			<p>Menu Title</p>
		</header>
		<ul>
			<li><a href="#">
				<span class="nav-symbol">⌁</span>
				<span>Item 1</span>
			</a></li>
			<li><a href="#">
				<span class="nav-symbol">✤</span>
				<span>Item 2</span>
			</a></li>
			<li><a href="#">
				<span class="nav-symbol"></span>
				<span>Item 3</span>
			</a></li>
		</ul>
	</section>
</nav>
```

Some noticable highlights are:

* [**1**] - Ensure you apply `Sidebar` class tag to the `<nav>` HTML tag.
* [**2**] - `<section>` tag to ensure fallback compatibilities from other navbar
            types.
* [**3**] - Site title for always appearing display. You should use `<p>` tag
            to avoid `<h1>` SEO conflicting usage.
* [**6**] - Main menu listing. no class tag is needed.
* [**8**] - Bullet inclusion. `nav-symbol` class tag must be applied. Leave it
             empty if unused.
* [**9**] - List item text.


Here is an example of fallback rendering:

{{< renderHTML "html" "amp" >}}
<div style="height: 500px;
	max-width: 500px;
	overflow: auto;
	border: 2px solid red;
	margin: 0 auto;"><div style="width: 100%;">
	<nav>
		<section>
			<header>
				<p>Menu Title</p>
			</header>
			<ul>
				<li><a href="#">
					<span class="nav-symbol">⌁</span>
					<span>Item 1</span>
				</a></li>
				<li><a href="#">
					<span class="nav-symbol">✤</span>
					<span>Item 2</span>
				</a></li>
				<li><a href="#">
					<span class="nav-symbol"></span>
					<span>Item 3</span>
				</a></li>
			</ul>
		</section>
	</nav>
</div></div>
<br/>
{{< /renderHTML >}}


#### AMP HTML Layout
AMP HTML for "table of contents" is the same as **Vanilla HTML Layout**. No
AMP-specific codes are needed.

[Back to Index]({{< param "IndexLink" >}})
