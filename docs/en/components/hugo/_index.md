+++
date = "2021-08-08T11:36:03+08:00"
title = "Hugo-Only Component"
description = """
Unlike all other components, this component is ONLY for hugo specific
tools and applications. It does not have any other interfaces and only houses
these tools and applications until an appropriate component is ready for
parking.
"""
keywords = ["hugo", "hugo-only", "only", "components", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "amp-sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Hugo Only"
pre = "🦚"
weight = 1


[schema]
selectType = "WebPage"
+++

# `Hugo-Only` Component
{{% param description %}}

This component is available starting from `v1.13.0`.




## Hugo
Bissetii prepared a number of Hugo interfaces to manage `Hugo-Only` component
seamlessly across all output formats.



### Shortcodes
By default, it is hard to process single or multiple card data across multiple
outputs while maintaining a single input format. Hence, Bissetii prepared the
following shortcodes to standardize the `i18n` rendering. These shortcodes are
specific to Bissetii which works differently from Hugo.


#### `renderHTML` Shortcode
To render HTML codes inside the Markdown.

Available since version: `v1.19.0`

```md {linenos=table,hl_lines=[],linenostart=1}
{{</* renderHTML "[OUTPUT_FORMAT_1]" "[OUTPUT_FORMAT_2]" ... */>}}
	<!-- Your HTML Codes Here */ -->
{{</* /renderHTML */>}}
```

1. `[OUTPUT_FORMAT_N]` - **COMPULSORY**
	1. To instruct Hugo to render the HTML codes in specific output formats.
	2. The parameters are variadic.


Example usage:

```md {linenos=table,hl_lines=[],linenostart=1}
{{</* renderHTML html amp */>}}
...
{{</* /renderHTML */>}}
```




## Go
This is a Hugo-Only component. Hence, there is no Go interface.




## HTML
This is a Hugo-Only component. Hence, there is no HTML interface.




## CSS
This is a Hugo-Only component. Hence, there is no CSS interface.




## Javascript
This is a Hugo-Only component. Hence, there is no Javascript interface.




## Sass
This is a Hugo-Only component. Hence, there is no Sass interface.




## Researches
Here are the researches done to ensure `Hugo-Only` component meets the necessary
quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `Hugo-Only` component in Bissetii. If you need more feature or
need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
