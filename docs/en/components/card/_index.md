+++
date = "2020-08-06T17:42:55+08:00"
title = "Card Component"
description = """
`Card` component is widely used in many information and summary display. Among
the use cases are presenting itemized products in e-commerece, data from search
list, and etc. Bissetii supports a number of card styling seemlessly.
"""
keywords = ["card", "components", "html", "w3c", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Card (BETA)"
pre = "🔖"
weight = 1


[schema]
selectType = "WebPage"
+++

# `Card` Component
{{% param "description" %}}

This component is available starting from `v1.12.1`.

{{< note "warning" "Beta Testing Notice" >}}
This is a fairly new shortcode and it is still under user-level API beta
testing. Hence, there may be some **major updates in the future that are not
backward compatible**. Please use at your own risk.
{{< /note >}}



## Supported Card Designs
Bissetii currently supports a number of card designs via its sub-components.
Feel free to explore each of them and deploy accordingly:

{{< cards "bissetii.data.cards.design.en" >}}




## Hugo
Bissetii prepared a number of Hugo interfaces to generate `Card` component
seamlessly across all output formats.



### Card Datafile
Due to the massive data contents for a `Card` component, it was decided to
save and load it from a datafile (e.g. `card1.toml`) directly. There are
multiple parts vital to facilitate a proper card data file.


#### General Card Data
The datafile first starts with the overall card attributes. The following
would be the minimum general card data:

```toml {linenos=table,hl_lines=[],linenostart=1}
Type = "vertical"
Class = "additional-css-class"
Style = "--my-variable: 2px;"
ID = "special-card-01"
Width = "max-content"
Height = "max-content"
```

* **Type** - the type of card to render with.
* **Class** - the card's Class attribute. Leave it empty if not used.
* **Style** - the card's Style attribute. Leave it empty if not used.
* **ID** - the card's ID attribute. Leave it empty if not used.
* **Width** - the card's width size. If undetermined, set it to `max-content`.
* **Height** - the card's height size. If undetermined, set it to `max-content`.


#### Content Data
Then, the datafile defines the `<div class="content">` container's contents. The
following would be the minimum content data:

```toml {linenos=table,hl_lines=[],linenostart=7}
[Content]
HTML = """
### My Ideal Card
This can be written in Markdown or HTML which will then be processed by Bissetii
into Vanilla HTML.
"""
AMPHTML = """
### My Ideal Card
This can be written in Markdown or HTML which will then be processed by Bissetii
into AMPHTML.
"""
```

* **[Content]** - indicates the following will be parsed as Content data.
* **HTML**	- content data that will be deployed to Vanilla HTML5. The
		  format can be Markdown or HTML5 but not mixed. For parsing,
		  Bissetii will try with Markdown format. If fails, HMTL5 format
		  will be the final fallback.
* **AMPHTML**	- content data that will be deployed to AMPHTML. The
		  format can be Markdown or HTML5 but not mixed. For parsing,
		  Bissetii will try with Markdown format. If fails, HMTL5 format
		  will be the final fallback.


#### CTA Data
The datafile then defines the `<div class="cta">` container's contents. The
following would be the minimum CTA data:

```toml {linenos=table,hl_lines=[],linenostart=18}
[CTA]
HTML = """
"""
AMPHTML = """
"""

[CTA.Links.1]
URL = "/"
HTML = "Home"
AMPHTML = "Home"
ID = "card1-cta-ideal-card-home"
Class = "button clean"
Rel = ""
Download = false
Target = ""

[CTA.Links.2]
URL = "/"
HTML = "Buy"
AMPHTML = "Buy"
ID = "card1-cta-ideal-card-buy"
Class = "button"
Rel = ""
Download = false
Target = ""
```

* **[CTA]**
	* indicates the following will be parsed as CTA data.
* **HTML**
	* content data that will be deployed to Vanilla HTML5.
	* The format can be Markdown or HTML5 but not mixed. For parsing,
		Bissetii will try with Markdown format. If fails, HMTL5 format
		will be the final fallback.
	* Leave this blank if quick links feature is used.
* **AMPHTML**
	* content data that will be deployed to AMPHTML.
	* The format can be Markdown or HTML5 but not mixed. For parsing,
		Bissetii will try with Markdown format. If fails, HMTL5 format
		will be the final fallback.
	* Leave this blank if quick links feature is used.
* **[CTA.Links.N]**
	* Quick CTA link feature that uses the default CTA styling instead of
		manually creating the list using `HTML` or `AMPHTML`.
	* If `HTML` or `AMPHTML` are not blank, they will be used fo rendering
	  instead of this CTA link feature, effectively overriding it.
	* The position of the link is dictated by the numbering `N` in
	  ascending order.
	* The data structure must contain:
		* **URL**
			* The link URL. Relative URL shall be converted to
				absolute URL automatically.
		* **HTML**
			* link content that will be deployed to Vanilla HTML5.
			* The format can be Markdown or HTML5 but not mixed.
			  For parsing, Bissetii will try with Markdown format.
			  If fails, HMTL5 format will be the final fallback.
		* **AMPHTML**
			* link content that will be deployed to AMPHTML.
			* The format can be Markdown or HTML5 but not mixed.
			  For parsing, Bissetii will try with Markdown format.
			  If fails, HMTL5 format will be the final fallback.
		* **ID**
			* The link `id=` attribute.
		* **Class**
			* The link `class=` attribute.
		* **Rel**
			* The link `rel=` attribute.
		* **Download**
			* The link `download` attribute.
		* **Target**
			* The link `target=` attribute.


#### Thumbnail Data
Lastly, the datafile defines the content of the `<div class="thumbnail">`
container. The following would be the minimum thumbnail data:

```toml {linenos=table,hl_lines=[],linenostart=43}
[Thumbnail]
HTML = """
"""
AMPHTML = """
"""
Picture = "media.images.myAlbum01.IMG01"

[Thumbnail.Image]
AltText = "Vertial Layout"
URL = "/img/thumbnails/default-1200x628.png"
Width = "350"
Height = "700"
IsMap = "false"
LoadingMode = "lazy"
SrcList = """
"""
Layout = ""
Class = ""
Sizes = """
"""
```

* **[Thumbnail]**
	* indicates the following will be parsed as Thumbnail data.
* **HTML**
	* content data that will be deployed to Vanilla HTML5.
	* The format can be Markdown or HTML5 but not mixed. For parsing,
		Bissetii will try with Markdown format. If fails, HMTL5 format
		will be the final fallback.
	* Leave this blank if quick picture feature is used.
* **AMPHTML**
	* content data that will be deployed to AMPHTML.
	* The format can be Markdown or HTML5 but not mixed. For parsing,
		Bissetii will try with Markdown format. If fails, HMTL5 format
		will be the final fallback.
	* Leave this blank if quick picture feature is used.
* **Picture**
	* Quick picture feature to quickly include an image without manually
	  construct the codes using `HTML` and `AMPHTML`.
	* If `HTML`, `AMPHTML`, and `[Thumbnail.Image]` are not blank or absent,
	  they will be used for rendering instead of this quick picture feature,
	  effectively overriding it.
	* The `picture` shortcode (See [Image Component]({{< link
	  "/components/image/#picture-shortcode" "this" "url-only" />}})) is
	  used so the value **MUST** match the shortcode's input alongside its
	  picture datafile.
* **[Thumbnail.Image]**
	* Quick image feature to quickly include an image using the same card
	  datafile and without manually construct the code using `HTML` and
	  `AMPHTML`.
	* This entire section is optional and can be absent for other feature
	  usage.
	* If `HTML` or `AMPHTML`, they will be used for rendering instead of
	  this CTA link feature, effectively overriding it.
	* If `Picture` is not blank, it will be ignored and this quick image
	  featue shall overrides it.
	* The `image` shortcode (See [Image Component]({{< link
	  "/components/image/#image-shortcode" "this" "url-only" />}})) is
	  used so the fields and values **MUST** match the shortcode's list of
	  inputs. They are:
		* **AltText** - image `alt=` attribute.
		* **URL** - image source URL (`src=`) attribute.
		* **Width** - image `width=` attribute.
		* **Height** - image `height=` attribute.
		* **IsMap** - image `ismap=` attribute.
		* **LoadingMode** - image `loading=` attribute.
		* **SrcList** - image multiple source URLs list.
		* **Layout** - image `layout=` attribute.
		* **Class** - image `class=` attribute.
		* **Sizes** - image `sizes=` attribute.



### Shortcodes
By default, it is hard to process single or multiple card data across multiple
outputs while maintaining a single input format. Hence, Bissetii prepared the
following shortcodes to standardize the card rendering. These shortcodes are
specific to Bissetii which works differently from Hugo.


#### `card` Shortcode
Renders a single card from a given data path.

```md {linenos=table,hl_lines=[],linenostart=1}
{{</* card "[DATAPATH]" */>}}
```

1. `DATAPATH` - **COMPULSORY**. The Hugo's pathing to the card data file.

Example usage:

```md {linenos=table,hl_lines=[],linenostart=1}
{{</* card "bissetii.data.examples.cards.card1" */>}}
```


#### `cards` Shortcode
Renders a deck of cards from a given data file directory path and organize them
using [Grid Component]({{< link "/components/grid/" "this" "url-only" />}}).

```md {linenos=table,hl_lines=[],linenostart=1}
{{</* cards "[DATAPATH]" "[STYLE]" "[CLASS]" "[ID]" */>}}
```

* `DATAPATH` - **COMPULSORY**
	1. The Hugo's pathing to the directory containing all the cards.
* `STYLE` - **OPTIONAL**
	1. `style=` attribute at deck level which will be added to `.row` Grid
	container.
* `CLASS` - **OPTIONAL**
	1. `class=` attribute at deck level which will be added to `.row` Grid
	container.
* `ID` - **OPTIONAL**
	1. `id=` attribute at deck level which will be added to `.row` Grid
	container.

Example usage:

```md {linenos=table,hl_lines=[],linenostart=1}
{{</* cards "bissetii.data.examples.cards"
	"--card-margin: .5rem 0;"
	"my-custom-class"
	"mydeck-01" */>}}
```




## Go
Coming Soon.




## HTML
Bissetii supports `Card` component seamlessly across multiple output formats.
Additionally, starting from version `v1.13.0`, the use of CSS variable is vital
for its upgrade from `v1.12.5`.

Bissetii HTML codes for this component are the same for Vanilla HTML5 and
AMPHTML. Due to the undefined condition from W3C manuals, Bissetii is currently
using class styling tag to structure the HTML component. The HTML key structure
would be as follows:


```html {linenos=table,hl_lines=[1,2,5,8],linenostart=1}
<article class="Card TYPE" style="--card-width: 250px;">
	<div class="thumbnail">
		<!-- OPTIONAL. Thumbnail placement here. -->
	</div>
	<div class="content">
		<!-- COMPULSORY. Main card content here. -->
	</div>
	<div class="cta">
		<!-- OPTIONAL. List of call-to-action (CTA) here. -->
	</div>
</article>
```

* Line 1 - **COMPULSORY**
	1. Use `<article>` HTML syntax since the content are usually outside of
		main content.
	2. Enable the component by supplying the `.Card` CSS class tag and its
		corresponding design `TYPE` (e.g. `.vertical`). By default, the
		card is **rendered in an error way** so that you can rectify
		your selection.
	3. Optionally, you can customize CSS styling using the `style=`
		attributes.
* Line 2 - **OPTIONAL**
	1. A `<div>` container with `.thumbnail` CSS class tag to wrap all
		thumbnails part.
	2. If the container is empty or do not exist, `.Card` will set its
		height to `0`, allowing `.content` container to takes over the
		spacing.
* Line 5 - **COMPULSORY**
	1. A `<div>` container with `.content` CSS class tag to wrap all
		main contents part.
	2. Should the card has width and height constraints (e.g. massive
		contents for small cards), this container shall overflow with
		scrolling in both direction.
* Line 7 - **OPTIONAL**
	1. A `<div>` container with `.cta` CSS class tag to wrap all
		call-to-action interaction elements.
	2. If needed, the immediate [List Component]({{< link "/components/list/"
		"this" "url-only" />}}) can have a `.default` CSS class to
		quickly layout all CTA elements.

Example code:

```html {linenos=table,hl_lines=[],linenostart=1}
{{% card "bissetii.data.examples.cards.card1" %}}
```




## CSS
Bissetii provided a list of CSS variables dedicated for list styling alteration
without needing to recompile Sass. These variables are best applied directly to
the `Card` HTML tags. Example:

```html {linenos=table,hl_lines=[],linenostart=1}
<article class="Card vertical" style="--card-width: 500px">
	...
</article>
```



### `--card-grid`
Affects the grid layout of the card. The acceptable value shall be compatible
with `grid:` CSS field. The default is `unset` which is intentionally causing
rendering error while the `Card` sub-components shall define its own default
value.



### `--card-overflow`
Affects the overflow behavior of the entire card. The acceptable value shall
be compatible with `overflow:` CSS field. The default is `hidden`.



### `--card-width`
Affects the width of the entire card. The acceptable value shall be compatible
with `width:` CSS field. The default is `max-content`.



### `--card-max-width`
Affects the maximum width of the entire card. The acceptable value shall be
compatible with `max-width:` CSS field. The default is `100%`.



### `--card-height`
Affects the height of the entire card. The acceptable value shall be compatible
with `height:` CSS field. The default is `fit-content`.



### `--card-border-radius`
Affects the edge roundness of the entire card. The acceptable value shall be
compatible with `border-radius:` CSS field. The default is `1rem`.



### `--card-padding`
Affects the padding spacing of the entire card. The acceptable value shall be
compatible with `padding:` CSS field. The default is `0`.



### `--card-margin`
Affects the margin spacing of the entire card. The acceptable value shall be
compatible with `margin:` CSS field. The default is `.5rem`.



### `--card-border`
Affects the border styling of the entire card. The acceptable value shall be
compatible with `border:` CSS field. The default is `unset`.



### `--card-background`
Affects the background styling of the entire card. The acceptable value shall
be compatible with `background:` CSS field. The default is
`var(--color-grey-50)`.



### `--card-filter`
Affects the filter styling of the entire card. The acceptable value shall be
compatible with `filter:` CSS field. The default is
`drop-shadow(0 0 .1rem var(--color-grey-900))`.



### `--card-thumbnail-overflow`
Affects the overflow behavior for the `<div class="thumbnail">` container. The
acceptable value shall be compatible with `overflow:` CSS field. The default
is `hidden`.



### `--card-thumbnail-height`
Affects the height of the `<div class="thumbnail">` container. The acceptable
value shall be compatible with `height:` CSS field. The default is `100%`.



### `--card-thumbnail-padding`
Affects the padding spacing of the `<div class="thumbnail">` container. The
acceptable value shall be compatible with `padding:` CSS field. The default is
`0`.



### `--card-thumbnail-margin`
Affects the margin spacing of the `<div class="thumbnail">` container. The
acceptable value shall be compatible with `margin:` CSS field. The default is
`0`.



### `--card-content-overflow`
Affects the overflow behavior for the `<div class="content">` container. The
acceptable value shall be compatible with `overflow:` CSS field. The default
is `auto`.



### `--card-content-height`
Affects the height of the `<div class="content">` container. The acceptable
value shall be compatible with `height:` CSS field. The default is `unset`.



### `--card-content-padding`
Affects the padding spacing of the `<div class="content">` container. The
acceptable value shall be compatible with `padding:` CSS field. The default is
`0`.



### `--card-content-margin`
Affects the margin spacing of the `<div class="content">` container. The
acceptable value shall be compatible with `margin:` CSS field. The default is
`0`.



### `--card-cta-overflow`
Affects the overflow behavior for the `<div class="cta">` container. The
acceptable value shall be compatible with `overflow:` CSS field. The default
is `visible`.



### `--card-cta-height`
Affects the height of the `<div class="cta">` container. The acceptable value
shall be compatible with `height:` CSS field. The default is `unset`.



### `--card-cta-padding`
Affects the padding spacing of the `<div class="cta">` container. The acceptable
value shall be compatible with `padding:` CSS field. The default is `0`.



### `--card-cta-margin`
Affects the margin spacing of the `<div class="cta">` container. The acceptable
value shall be compatible with `margin:` CSS field. The default is `0`.



### `--card-cta-list-flex-direction`
Affects the direction cta items arrangement for List with `.default` CSS class.
The acceptable value shall be compatible with `flex-direction:` CSS field. The
default is `row`.



### `--card-cta-list-align-items`
Affects the cta items vertical alignment inside the List with `.default` CSS
class. The acceptable value shall be compatible with `align-items:` CSS field.
The default is `center`.



### `--card-cta-list-justify-content`
Affects the cta items horizontal alignment inside the List with `.default` CSS
class. The acceptable value shall be compatible with `justify-content:` CSS
field. The default is `center`.



### `--card-cta-list-align-content`
Affects the cta items overall content alignment inside the List with `.default`
CSS class. The acceptable value shall be compatible with `align-content:` CSS
field. The default is `center`.




## Javascript
This component does not rely on any Javascript.




## Sass
Depending on release version, the Sass files work differently. Bissetii does not
package Sass codes explictly so please view them via the git repository.



### `v1.13.0` and Above
Bissetii uses Dart Sass to compile the styling Sass codes into CSS file. This
component's main Sass codes are available at the following location:

```
pkg/components/internal/card
```

Bissetii splits different card types into its own sub-components which has their
own Sass codes.



### `v1.12.5` and Before
{{< note "warning" "Deprecation Notice" >}}
Starting from version `v1.13.0`, this version of Sass is no longer used and the
support is scheduled to be removed on **July 31, 2022**.
{{< /note >}}

The Sass styling scripts that are responsible for styling cards are located in

```
assets/css/bissetii/modules/card
```

This is an optional module so one must include the following config into your
own Sass configuration script:

```sass
$config-card: 1
```

Since `card` component is usually deployed inside some kind of table or
[Grid Component]({{< link "/components/grid/" "this" "url-only" />}}) layout,
It only uses `margin-top` for its hovering animation.




## Researches
Here are the researches done to ensure `Card` component meets the necessary
quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `Card` component in Bissetii. If you need more feature or
need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
