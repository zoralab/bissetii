+++
date = "2021-07-31T21:53:16+08:00"
title = "Vertical Layout Card Sub-Component"
description = """
`Vertical Card Layout` component is the `Card` component design that holds
vertical layout where thumbnail is at the top, content is at the center, and
CTA is at the bottom of the card.
"""
keywords = ["card", "vertical design", "components", "html", "w3c", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Card (BETA) | Vertical"
pre = "🔖"
weight = 1


[schema]
selectType = "WebPage"
+++

# `Vertical Layout Card` Sub-Component
{{% param "description" %}}

This component is available starting from `v1.13.0`.

{{< note "warning" "Beta Testing Notice" >}}
This is a fairly new shortcode and it is still under user-level API beta
testing. Hence, there may be some **major updates in the future that are not
backward compatible**. Please use at your own risk.
{{< /note >}}

Here is a rendering example:

{{< card "bissetii.data.examples.cards.card1" >}}




## Hugo
All Hugo interface is supported directly from [Card Component]({{< link
"/components/card/" "this" "url-only" />}}).

### Card Datafile
For deploying this sub-component to style a particular card, the `Type` field
must be set to `vertical`.

```toml {linenos=table,hl_lines=[],linenostart=1}
Type = "vertical"
...
```




## Go
All Go interface is supported directly from [Card Component]({{< link
"/components/card/" "this" "url-only" />}}). No additional feature is added.




## HTML
Extending from [Card Component]({{< link "/components/card/" "this"
"url-only" />}}), Bissetii styles the `Card` component with this sub-component
via the `.vertical` class tag.

Bissetii HTML codes for this component are the same for Vanilla HTML5 and
AMPHTML. Due to the undefined condition from W3C manuals, Bissetii is currently
using class styling tag to structure the HTML component. The HTML key structure
would be as follows:


```html {linenos=table,hl_lines=[1,2,5,8],linenostart=1}
<article class="Card vertical" style="--card-width: 250px;">
	...
</article>
```

* Line 1 - **COMPULSORY**
	1. Set the `.vertical` CSS class tag alongside `.Card` CSS class tag
		will automatically styles the card with `Vertical Layout Card`
		design.




## CSS
All CSS variables are directly supported directly from [Card Component]({{< link
"/components/card/" "this" "url-only" />}}). There is no additional feature or
variable.




## Javascript
All Javascript are directly supported directly from [Card Component]({{< link
"/components/card/" "this" "url-only" />}}). There is no additional feature or
variable.




## Sass
Depending on release version, the Sass files work differently. Bissetii does not
package Sass codes explictly so please view them via the git repository.



### `v1.13.0` and Above
Bissetii uses Dart Sass to compile the styling Sass codes into CSS file. This
component's main Sass codes are available at the following location:

```
pkg/components/internal/cardvertical
```




## Researches
Here are the researches done to ensure `Vertical Layout Card` component meets
the necessary quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `Vertical Layout Card` component in Bissetii. If you need more
feature or need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
