+++
date = "2020-07-29T16:56:51+08:00"
title = "Carousel Component"
description = """
`Carousel` component is a dynamic display of content over a same view port. A
normal deployment would be album carousel, text based cards carousel, or
video carousel. Bissetii supports this component using CSS only animation.
"""
keywords = ["carousel", "components", "html", "w3c", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	"carousel",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Carousel (BETA)"
pre = "🗂️"
weight = 1


[schema]
selectType = "WebPage"
+++

# `Carousel` Component
{{% param "description" %}}

This carousel design was inspired by
[Chris Coyier](https://css-tricks.com/author/chriscoyier/)'s article about
[CSS-Only Carousel](https://css-tricks.com/css-only-carousel/) where the CSS
implementation was introduced by Christian Schaefer. Its HTML codes were
cross-checked with
[W3C Carousel Code Structure](https://www.w3.org/WAI/tutorials/carousels/structure/)
before adopt-and-modify into Bissetii.

This component is available starting from `v1.12.1`.

{{< note "warning" "Beta Testing Notice" >}}
This is a fairly new component and it is still under user-level API beta
testing. Hence, there may be some **major updates in the future that are not
backward compatible**. Please use at your own risk.
{{< /note >}}




## Hugo
Bissetii prepared a number of Hugo interfaces to generate `Carousel` component
seamlessly across all output formats.



### Module Extension
To use `Carousel` component, you need to include `"carousel"` extension module
into your front-matter or the global `amp.toml` data file. The code pattern is
as follows:

```toml {linenos=table,hl_lines=[5],linenostart=1}
+++
...
[modules]
extensions = [
	"carousel",
]
...
+++
```

Bissetii will automatically includes the extension module seamlessly in
accordance to the output format (e.g. `amp-carousel` extension module for
AMPHTML).



### Carousel Datafile
Due to the massive data contents for a `Carousel` component, it was decided to
save and load it from a datafile (e.g. `mycarousel.toml`). The full contents
of the datafile is as follows:

```toml {linenos=table,hl_lines=[],linenostart=1}
ID          = "my-gallery-slide"
Class       = ""
Type        = "horizontal-slide"
Timing      = "5000"
HideNav     = false
ShowPrint   = true

[HTML]
Autoplay    = true
Width       = "auto"
Height      = "500px"
Layout      = "responsive"
Next        = """
&#x203A;
"""
Previous    = """
&#x2039;
"""

[AMP]
Autoplay    = true
Width       = "1200px"
Height      = "500px"
Layout      = "responsive"
Next        = """
&#x203A;
"""
Previous    = """
&#x2039;
"""




[slides.1]
HTML = """
<div style="background: crimson;
		width: 100%;
		height: 100%;
		display: flex;
		align-items: center;
		justify-content: center;"
>
	<p style="font-size: 5rem">Slide 1</p>
</div>
"""
AMPHTML     = """
<div style="background: crimson;
		width: 100%;
		height: 100%;
		display: flex;
		align-items: center;
		justify-content: center;"
>
	<p style="font-size: 5rem">Slide 1</p>
</div>
"""
[slides.1.Thumbnail]
HTML = """
<p>Slide 1</p>
"""
AMPHTML = """
<p>Slide 1</p>
"""

...
```



### Shortcodes
By default, it is hard to process images across multiple outputs while
maintaining a single input format. Hence, Bissetii prepared the following
shortcodes to standardize the image rendering. These shortcodes are specific to
Bissetii which works differently from Hugo.


#### `carousel` Shortcode
The `carousel` shortcode is for creating a `Carousel` component quickly. The
shortcode pattern is as follows:

```md {linenos=table,hl_lines=[],linenostart=1}
{{</* carousel "[DATAPATH]" */>}}
	[CONTENT]
{{</* /carousel */>}}
```

1. `DATAPATH` - **COMPULSORY**. The Hugo's pathing to the carousel data file.

Example usage:

```md {linenos=table,hl_lines=[],linenostart=1}
{{</* carousel "bissetii.data.examples.carousels.picture-gallery" */>}}
```




## Go
Coming soon.




## HTML
Bissetii supports `Carousel` component using different HTML5 codes for different
output formats. Additionally, starting from version `v1.13.0`, the use of CSS
variable is vital for its upgrade any previous versions.



### Output Formats
The `Carousel` component's HTML codes are different depending on the selected
output format. Bissetii is able to style all supported formats accordingly.


#### AMPHTML
For AMPHTML to use `Carousel` component, the AMP `amp-carousel` library is
required and shall be imported inside the `<head>` section as instructed by
[AMP Specification](https://amp.dev/documentation/components/amp-carousel/).


```html {linenos=table,hl_lines=["3-4"],linenostart=1}
<head>
	...
	<script async custom-element="amp-carousel"
		src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
	...
</head>
```

Then, the `<amp-carousel>` slide version is used extensively to create the
entire `Carousel` component. Bissetii supported the following `<amp-carousel>`
AMPHTML structure:

```html {linenos=table,hl_lines=[],linenostart=1}
<amp-carousel width="1200px"
	height="500px"
	layout="responsive"
	type="slides"
	autoplay
	controls
	delay="5000">
	<article>
		...AMPHTML Slide Content 1...
	</article>
	<article>
		...AMPHTML Slide Content 2...
	</article>
	<article>
		...AMPHTML Slide Content 3...
	</article>
	...
</amp-carousel>
```

The specification is available in the AMP official website available at
[AMP Carousel Spec](https://amp.dev/documentation/components/amp-carousel/).


#### Vanilla HTML5
For Vanilla HTML5, Bissetii uses its own HTML5 to generate the `Carousel`
component structure.

The full Vanilla HTML5 code structure is as follows:

```html {linenos=table,hl_lines=[],linenostart=1}
<section id="my-carousel-id"
	class="Carousel
		print
		autoplay"
	style="--carousel-height: 500px;
		--carousel-width: auto;
		--carousel-total: 3;
		--carousel-autoplay-timing: 5000;">
	<ol class="viewport">
		<li id="my-gallery-slide-1">
			<div class="controller"></div>
			<article>
				<!-- Slide Content 1 -->
			</article>
			<nav>
				<a class="previous" href="#my-gallery-slide-N">
					<!-- Previous Button Icon -->
				</a>
				<a class="next" href="#my-gallery-slide-2">
					<!-- Next Button Icon -->
				</a>
			</nav>
		</li>
		<li id="my-gallery-slide-2">
			...
			<nav>
				<a class="previous" href="#my-gallery-slide-1">
					<!-- Previous Button Icon -->
				</a>
				<a class="next" href="#my-gallery-slide-3">
					<!-- Next Button Icon -->
				</a>
			</nav>
		</li>
		...
	</ol>
</section>
```

* *Line 1* - **COMPULSORY** - Carousel must use `<section>` and must have an
                              unique `id=` attribute.
* *Line 2* - **COMPULSORY** - Carousel must include `.Carousel` CSS class tag.
* *Line 3* - **OPTIONAL**   - Adding `print` CSS class tag allows Carousel
                              to render itself in print mode.
* *Line 4* - **OPTIONAL**   - Adding `autoplay` CSS class tag allows Carousel
                              to perform animation automatically.
* *Line 5* - **COMPULSORY** - Specify Carousel height. Otherwise, it will not
                              appear.
* *Line 6* - **OPTIONAL**   - Specify Carousel weight.
* *Line 7* - **COMPULSORY** - Specify Carousel total slides number.
* *Line 8* - **OPTIONAL**   - Specify Carousel autoplay timing.
* *Line 9* - **COMPULSORY** - Use a list (`<ol>` or `<ul>`) with `.viewport`
                              CSS class tag to display viewable region.
* *Line 10* - **COMPULSORY** - List item `<li>` **MUST** have an unique slide
                               ID (e.g. `{Carousel-ID}-{Slide#}` pattern is
                               used above).
* *Line 11* - **COMPULSORY** - An invisibile `<div>` with `.controller` CSS
                               css class tag for `.autoplay` to control the
                              `Carousel` automated animation.
* *Line 12* - **COMPULSORY** - Use `<article>` to contain the slide contents.
* *Line 15* - **COMPULSORY** - Use `<nav>` to contain navigation links.
* *Line 16* - **COMPULSORY** - Use `<a>` with `.previous` CSS class tag and the
                               jump link (`#`) using the slide ID from the slide
                               before the current. For first slide, use the last
                               slide's slide ID.
* *Line 19* - **COMPULSORY** - Use `<a>` with `.next` CSS class tag and the
                               jump link (`#`) using the slide ID from the slide
                               after the current. For last slide, use the first
                               slide's slide ID.



### Animation
Currently, **only horizontal sliding animation is supported** in the main
`Carousel` component.

{{< note "info" "Note" >}}
Research work is still carrying out for supporting other CSS-only `Carousel`
animations. The HTML codes section is still yet to be finalized due to the
absent of vertical and horizontal scalability expansion at the moment.
{{< /note >}}

The horizontal sliding animation relies heavily on anchor jump links (`#ID`)
that enables it to scroll across slides. Hence, each slide **MUST** have an
unique and predictable `id=` attributes. For example, the ID table can be built
using the main `Carousel` ID attribute:

| Description | Position | ID           | Next ID      | Previous ID      |
|:------------|:--------:|:------------:|:------------:|:----------------:|
| Carousel    |  N/A     | `my-slide`   |  N/A         |  N/A             |
| Slide 1     | `1`      | `my-slide-1` | `my-slide-2` | `my-slide-N`     |
| Slide 2     | `2`      | `my-slide-2` | `my-slide-3` | `my-slide-2`     |
| Slide 3     | `3`      | `my-slide-3` | `my-slide-4` | `my-slide-3`     |
| ...         | ...      | ...          | ...          | ...              |
| Slide N     | `N`      | `my-slide-N` | `my-slide-1` | `my-slide-{N-1}` |

* `N/A` = Not Applicable



### Autoplay
The autoplay feature is currently at its experiementals stage where it uses
positioning animation to navigate from left to right.

To enable autoplay in Vanilla HTML5, simply attach `.autoplay` CSS class tag
into `<section class="Carousel">` HTML5 tag.

{{< note "warning" "Warning" >}}
Autoplay is not functioning in Firefox-based browser. See
[Issue 118](https://gitlab.com/zoralab/bissetii/-/issues/118).
{{< /note >}}

To enable autoplay in AMPHTML, simply attach `autoplay` HTML attribute
into `<amp-carousel>` AMPHTML tag.



### Printing
Since `Carousel` component itself is hiding information in nature, `Carousel`
is disabled during print mode to save ink and paper spaces.

Should there a need to print `Carousel` component (in which each slide will be
listed out accordignly) in Vanilla HTML5, simply add `print` CSS class tag into
`<secton class="Carousel">` HTML5 tag.

As for printing `Carousel` component in AMPHTML, please follow
[AMP Carousel Spec](https://amp.dev/documentation/components/amp-carousel/).



### Rendering Example
Here is an example of the a carousel rendering.

{{< carousel "bissetii.data.examples.carousels.picture-gallery" >}}




## CSS
Bissetii provided a list of CSS variables dedicated for `Carousel` styling
alteration without needing to recompile Sass. Depending on the area of effects,
these variables are best applied directly to the `Carousel` HTML tag. Example:

```html {linenos=table,hl_lines=[],linenostart=1}
<section id="my-gallery-slide"
	class="Carousel print autoplay"
	style="--carousel-height: 500px;
		--carousel-width: auto;
		--carousel-total: 3;
		--carousel-autoplay-timing: 5000;">
	...
</section>
```

{{< note warning "Notice" >}}
Due to the HTML codes are not finalized yet (as in supporting other animation
types), the CSS variables are subjected to change. Hence, for the time being,
the CSS variables will not be published for now.
{{< /note >}}




## Javascript
This component does not rely on any Javascript.




## Sass
Depending on release version, the Sass files work differently. Bissetii does not
package Sass codes explictly so please view them via the git repository.



### `v1.13.0` and Above
Bissetii uses Dart Sass to compile the styling Sass codes into CSS file. This
component's Sass codes are available at the following location:

```
pkg/components/internal/carousel
```




## Researches
Here are the researches done to ensure `Carousel` component meets the necessary
quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `Carousel` component in Bissetii. If you need more feature or
need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
