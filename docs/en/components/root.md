+++
date = "2021-04-30T17:11:59+08:00"
title = "Root Component"
description = """
This page specify how `Root` component is used in Bissetii styling. It serves
as the foundation of all other components including their respectives CSS
variables for content styling and code generations.
"""
keywords = ["root", "components", "html", "w3c", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "root (BETA)"
pre = "🧬"
weight = 1


[schema]
selectType = "WebPage"
+++

# `Root` Component
{{% param "description" %}}

This component is available starting from `v1.13.0`.

{{< note "warning" "Beta Testing Notice" >}}
This is a fairly new component and it is still under user-level API beta
testing. Hence, there may be some major updates in the future that are
**not backward compatible**. Please **ONLY use at your own risk**.
{{< /note >}}




## Hugo
Here are `Root` components' Hugo-specific matters.



### No Shortcode
Since this component is mainly for CSS to establish its styling foundation,
**there are no shortcode** available to use. Hugo uses the compiled Bissetii
CSS seamlessly.



### Emoji
Hugo supports emoji using `Noto Color Emoji` font as its default rendering.
Emoji allows any content creators to easily and to quickly generate common
emotional imagery expression without relying too much on image resources.


#### Local Font File
By default, Bissetii bundled a local copy ot `Noto Color Emoji` located inside
`static/fonts/NotoColorEmoji.ttf`.

You can roll your own font file update as long as it complies to the pathing
correctly.


#### Emoji Markdown Encoding Configurations
Hugo provided a configuration to enable emoji encoding. To ensure emoji works
for you, you need to add the following config:

```toml {linenos=table,hl_lines=[],linenostart=1}
enableEmoji = true
```

into your local `config/_default/config.toml` or similar pathing file. Setting
the value to `false` disables the emoji encoding.


#### Composing Emoji
To write emoji, simply use the `:emoji:` pattern in your Markdown. You may
refers to the
[Emoji Cheat Sheet](https://www.webfx.com/tools/emoji-cheat-sheet/) or
[Emoji Manual](https://unicode.org/emoji/charts/full-emoji-list.html) for some
guidance. Example:

| Code                   | Unicode                        | Emoji                |
|:----------------------:|:------------------------------:|:--------------------:|
| :`grinning`:           | `U+1F600`                      | :grinning:           |
| :`white_check_mark`:   | `U+2705`                       | :white_check_mark:   |
| :`x`:                  | `U+274C`                       | :x:                  |
| :`woman_raising_hand`: | `U+1F64B U+200D U+2640 U+FE0F` | :woman_raising_hand: |
| :`man_raising_hand`:   | `U+1F64B U+200D U+2642 U+FE0F` | :man_raising_hand:   |



### Layout Controls
Hugo uses a different layout controls using `data/bissetii/layouts.toml` data
file.

Instead of using the default Bissetii layouts, due to the fact that currently
Bissetii Hugo interface only supports 1 sidebar, Hugo interface uses a different
layout settings.

In short, the layout values (see CSS section for details) are:

| Screen Width   | Nickname     | Variable Name            | Variable Value               |
|:---------------|:-------------|:-------------------------|:-----------------------------|
| `<60rem`       | Mobile       | `--body-grid`            | `var(--layout-vertical)`     |
| `>60rem`       | Tablet       | `--body-grid-tablet`     | `var(--layout-left-and-top)` |
| `>80rem`       | Desktop      | `--body-grid-desktop`    | `var(--layout-left-and-top)` |
| `>120rem`      | Widescreen   | `--body-grid-widescreen` | `var(--layout-left-and-top)` |

The layout will automatically change from `var(--layout-left-and-top)` to
`var(--layout-right-and-top)` when the `nav-sidebar` is set to `RightSide`.


#### Configure Custom Layout
You can configure your own layout based on screen width breakpoint. Inside
`data/bissetii/layouts.toml`, they are written in this format:

```toml {linenos=table,hl_lines=[],linenostart=1}
[<Nickname>]
Grid = ''
Name = '<name without layout>'
```

Example for `Mobile` layout, it is:

```toml {linenos=table,hl_lines=[],linenostart=1}
[Mobile]
Grid = ''
Name = 'vertical'
```

If you define a custom grid layout, such that:

```toml {linenos=table,hl_lines=[],linenostart=1}
[Mobile]
Grid = '''
"sidebar		topbar" min(0, max-content)
"rsidebar		content" min(0, max-content)
/ minmax(auto, 30%)	minmax(auto, 70%)
'''
Name = 'vertical'
```

Then your `Grid` definition will be used and the `Name` field shall be ignored.



### Font Files
Bissetii's Hugo interface serves localized `Noto Sans` fonts inside the
following pathing for font-styling consistency:

```
static/fonts/NotoSans-Bold.ttf
static/fonts/NotoSans-BoldItalic.ttf
static/fonts/NotoSans-Italic.ttf
static/fonts/NotoSans-Regular.ttf
```





## Go
Coming Soon.




## HTML
Bissetii first designed its HTML parts using HTML5 (called `Vanilla HTML5`) and
later [Accelerated Mobile Pages HTML](https://amp.dev/) (called `AMPHTML` in
Bissetii official term). Due to `AMPHTML` is using its specialized HTML syntax,
Bissetii took it as a development architecture variable and comply to multiple
HTML outputs. Depending on your output types, you should comply to their
respective foundational HTML layout codes.



### Syntax Structure
Bissetii uses various documentations to build its opinionated structure used
across multiple outputs. Among referenced documentations are:

1. [Google Web Fundamentals](https://developers.google.com/web/fundamentals/)
2. [W3C](https://www.w3.org/standards/webdesign/)
3. [Accelerated Mobile Pages](https://amp.dev/)
4. [Search Engine Journal](https://www.searchenginejournal.com/)

For AMPHTML, Bissetii complies to its
[specification](https://amp.dev/documentation/guides-and-tutorials/start/create/basic_markup/).
Love it or hate it, AMPHTML did put some creative thinking process to slim down
the HTML codes and CSS assets accordingly.

For Vanilla HTML5, Bissetii uses the following layout:

```html
<!DOCTYPE html>
<html>
<head>
	... meta and titles ...
</head>
<body>
	<main>
		... the page content ...
	</main>
	<...>
		... anything else that is not the main content like sidebar ...
	</...>
</body>
</html>
```

Bissetii recommends the above layout because when CSS is not available by any
reason, the HTML-only display is still consumable as the page contents are
served first. Anything else would be later.

Should CSS is available, each component will then be mapped into its defined
CSS layout accordingly.



### Section and Article
Bissetii has yet to plan out how to implement the section (`<section>`) and
article (`<article>`) syntaxes. They will eventually be used for context
grouping especially dealing with CSS printer friendly styling in near future.



### Creating Emoji
There are many ways to create emoji. There are minimum 3 ways to do it:

1. By HTML Encoded UTF-8
2. By UTF-8 Symbols directly

You may refers to the
[Emoji Cheat Sheet](https://www.webfx.com/tools/emoji-cheat-sheet/) or
[Emoji Manual](https://unicode.org/emoji/charts/full-emoji-list.html) for some
guidance. Bissetii currently renders `Emoji` as follows:

{{< renderHTML html >}}
<table>
<thead>
	<tr>
		<th>Emoji</th>
		<th>Unicode</th>
		<th>By HTML UTF-8</th>
		<th>By Symbol</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>Grinning</td>
		<td><code>U+1F600</code></td>
		<td><code>&#38;#x1F600</code></td>
		<td><code>😀</code></td>
	</tr>
	<tr>
		<td>Check Mark Button</td>
		<td><code>U+2705</code></td>
		<td><code>&#38;#x2705</code></td>
		<td><code>✅</code></td>
	</tr>
	<tr>
		<td>Cross Mark</td>
		<td><code>U+274C</code></td>
		<td><code>&#38;#x274C</code></td>
		<td><code>❌</code></td>
	</tr>
	<tr>
		<td>Woman Rising Hand</td>
		<td><code>U+1F64B U+200D U+2640 U+FE0F</code></td>
		<td><code>&#38;#x1F64B&#38;#x200D&#38;#x2640&#38;#xFE0F</code></td>
		<td><code>🙋‍♀️</code></td>
	</tr>
	<tr>
		<td>Man Rising Hand</td>
		<td><code>U+1F64B U+200D U+2642 U+FE0F</code></td>
		<td><code>&#38;#x1F64B&#38;#x200D&#38;#x2642&#38;#xFE0F</code></td>
		<td><code>🙋‍♂️</code></td>
	</tr>
</tbody>
</table>
{{< /renderHTML >}}




## CSS
This section covers all the CSS parts for `Root` component. Please go through
each sub-section carefully as they affects Bissetii styling globally.



### Layout
These CSS variables affects the layouts of the page.


#### Z-Index
The following variables affect the z-axis layering values for various
`z-index`es. Bissetii always use the highest level of `z-index` as the starting
point. Then, it works itself downwards. Here are the declared values in
decending decrement by `1`:

1. `--z-index-max` (`2147483647`) - for dialog or modal presentation usage.
2. `--z-index-max-overlay` (Max - 1) - for dialog or model presentation overlay
   usage.
3. `--z-index-nav-topping` (Max - 2) - for navigation layer triggers.
4. `--z-index-nav` (Max - 3) - for navigation layer.
5. `--z-index-nav-overlay` (Max - 4) - for navigation layer overlay.
6. `--z-index-main-topping` (Max - 5) - for main content actionable triggers.
7. `--z-index-main` (Max - 6) - for main content layer.
8. `--z-index-main-overlay` (Max - 7) - for main content overlay.
9. `--z-index-hidden` (`0`) - for hidden layer using z-axis.


#### Layout Spacing
The following variables affect the layout spacing used by the `<body>`'s grid
system before styling `<body>` HTML tag.

##### `--sidebar-width`
The layout spacing for a single sidebar. The value shall be compatible with
CSS grid column `minmax()` and `calc()` functions. The default is `22%`.

##### `--body-full-width`
The complete width of the `<body>` tag with its unit. The value shall be
compatible with CSS grid column `minmax()` function, `calc()` function, and
having the same unit as `--sidebar-width`. The default is `100%`.


#### Grid Setting for `<body>`
The following variables affect the `grid` display settings for `<body>`. By
default, the grid system defines the following areas:

1. `sidebar` - main and left navigation sidebar
2. `topbar` - vertical navigation topbar
3. `content` - main content
4. `rsidebar` - right navigation sidebar

To maximize the screen space usage, the `grid-gap:` is always set to `0` in
favor of its child elements' `margin:` and `padding:` styling.

##### Default layouts
Bissetii defined a minimum of 5 default layouts under their respective CSS
variable names. You do not need to redefine any of them.
They are defined in the following patterns:

`--layout-vertical`
```css {linenos=table,hl_lines=[],linenostart=1}
"content" auto
"topbar" minmax(0, auto)
"sidebar" minmax(0, auto)
"rsidebar" minmax(0, auto)
/ var(--body-full-width)
```

`--layout-left-and-top`
```css {linenos=table,hl_lines=[],linenostart=1}
"sidebar				topbar"		minmax(0, max-content)
"sidebar				content"	auto
/ minmax(0, var(--sidebar-width))	minmax(auto, calc(var(--body-full-width) - var(--sidebar-width)))
```

`--layout-right-and-top`
```css {linenos=table,hl_lines=[],linenostart=1}
"topbar									rsidebar"	minmax(0, max-content)
"content								rsidebar"	auto
/ minmax(auto, calc(var(--body-full-width) - var(--sidebar-width)))	minmax(0, var(--sidebar-width))
```

`--layout-both-and-top`
```css {linenos=table,hl_lines=[],linenostart=1}
"sidebar				topbar									rsidebar" minmax(0, max-content)
"sidebar				content									rsidebar" auto
/ minmax(0, var(--sidebar-width))	minmax(auto, calc(var(--body-full-width) - 2*var(--sidebar-width)))	minmax(0, var(--sidebar-width))
```

`--layout-top-and-both`
```css {linenos=table,hl_lines=[],linenostart=1}
"topbar					topbar									topbar" minmax(0, max-content)
"sidebar				content									rsidebar" auto
/ minmax(0, var(--sidebar-width))	minmax(auto, calc(var(--body-full-width) - 2*var(--sidebar-width)))	minmax(0, var(--sidebar-width))
```

##### Control Variables
These variables are the control variables that handles `<body>`'s `grid:` field.
They use the default layout values above depending on screen size.

| Screen Width   | Nickname     | Variable Name            | Variable Value               |
|:---------------|:-------------|:-------------------------|:-----------------------------|
| `<60rem`       | mobile       | `--body-grid`            | `var(--layout-vertical)`     |
| `>60rem`       | tablet       | `--body-grid-tablet`     | `var(--layout-both-and-top)` |
| `>80rem`       | desktop      | `--body-grid-desktop`    | `var(--layout-top-and-both)` |
| `>120rem`      | widescreen   | `--body-grid-widescreen` | `var(--layout-top-and-both)` |

These variable changes are best defined at `<body>` syntax inline styling.
Example:

```html {linenos=table,hl_lines=[],linenostart=1}
<body style="--body-grid-tablet: var(--layout-vertical);">
	...
</body>
```



### Main Content Styling
Bissetii place main contents inside the universal `<main>` HTML syntax. Hence,
there are a number of variables available to control its common styling.

These variable changes are best defined at `<main>` syntax inline styling.
Example:

```html {linenos=table,hl_lines=[],linenostart=1}
<main style="--main-max-width: 80%; --main-margin: 2rem;">
	...
</main>
```

#### `--main-max-width`
Defines the main content's CSS value for `max-width:` field. By default, it is
`100%`.


#### `--main-margin`
Defines the main content's CSS value for `margin:` field. By default, it is
`0`.


#### `--main-padding`
Defines the main content's CSS value for `padding:` field. By default, it is
`3.5rem`.



### HTML Layout Styling
Bissetii place some default HTML layout styling inside the `<html>` HTML syntax.

These variable changes are best defined at `<html>` syntax inline styling.
Example:

```html {linenos=table,hl_lines=[],linenostart=1}
<html style="--box-sizing-html: content-box;">
	...
</html>
```


#### `--box-sizing-html`
Defines the html's CSS value for `box-sizing:` field. By default, it is
`border-box`.



### Font Management
Bissetii defined a number of settings to constriant its font rendering. There
are a large number of variables that control each font settings differently.


#### Control Value for `rem`
Bissetii set `rem` value `font-size:` at `<html>` HTML syntax level. The default
value is `62.5%` where `1.6rem = 16px` using
[Johnathan Snook's rem unit](https://snook.ca/archives/html_and_css/font-size-with-rem).

This way, it is safe to use `rem` with consistent styling conversion across
major browsers.


#### Local Font Files Relative URL Pathing
Bissetii set some default font-facing pathing for its primary default
`Noto Sans` and `Noto Color Emoji` fonts. The font faces tries to load existing
local system fonts first if available. Otherwise, they will seek the local
font-faces fonts using local pathing. There are the relative pathing from base
URL for loading the font files:

```css {linenos=table,hl_lines=[],linenostart=1}
url("/fonts/NotoSans-Bold.ttf");
url("/fonts/NotoSans-BoldItalic.ttf");
url("/fonts/NotoSans-Italic.ttf");
url("/fonts/NotoSans-Regular.ttf");
url("/fonts/NotoColorEmoji.ttf");
```

If both `Noto Sans` are missing, then the site falls back to the web-safe fonts
available on devices. However, if `Noto Color Emoji` font file is missing, it
breaks emoji fonts, giving you jibbrish or tofu characters.


#### General Fonts Settings
Thse are the general font settings set into `<body>` HTML syntax. They act as
the foundation of font across the content.

These variable changes are best defined at `<body>` syntax inline styling.
Example:

```html {linenos=table,hl_lines=[],linenostart=1}
<body style="--font-size: 1.4rem;">
	...
</body>
```

##### `--font-size`
The base text font size CSS value for `font-size:` field. Default is `1.8rem`.

##### `--font-weight`
The base text font weight CSS value for `font-weight:` field. Default is
`normal`.

##### `--font-char-spacing`
The base text character spacing CSS value for `letter-spacing:` field. Default
is `.01rem`.

##### `--font-line-height`
The base text font height CSS value for `line-height:` field. Default is
`1.6`.

##### `--font-family`
The base text font family CSS value for `font-family:` field. Default is
`"Noto Sans", "Noto Serif", "Noto Color Emoji", "Roboto", "Helvetica Neue, "sans-serif"`.

##### `--font-align`
The base text font alignment value for `text-align:` field. Default is
`justify`.


#### Header Font Settings
Bissetii styled a a number of headers upto 6 levels (`h1`, `h2`, ... , `h6`)
as per [SEO best practices](https://www.searchenginejournal.com/on-page-seo/header-tags/).
If you need more than 6 levels of heading, please take it as a warning that you
have content structuring problem.

These variable changes are best defined at `<body>` syntax inline styling. If
you need to specific, you can apply it towards the heading syntax.
Example:

```html {linenos=table,hl_lines=[],linenostart=1}
<body style="--h2-before-content: '>';">
	<h2>This will be affected by changes done on body</h2>
	...

	<h4 style="h4-font-size: 2rem;">This affects only this heading</h4>
	...
</body>
```

All headings have their respective set of variables enabled as per styling
needs. In this documentation, we will represents all level numbering as `N`.
Hence, for `hN` and you want level 2 (N = 2), the resultant would be `h2`.

##### `--hN-font-size`
Defines the CSS value for the heading's `font-size:` field. The default values
are as follow:

| `hN` |  `h1`  |  `h2`  |   `h3 `  |   `h4`   |   `h5`   |   `h6`   |
|:-----|:-------|:-------|:---------|:---------|:---------|:---------|
| HTML | `4rem` | `3rem` | `2.5rem` | `2.2rem` | `1.8rem` | `1.6rem` |
| AMP  | `4rem` | `3rem` | `2.5rem` | `2.2rem` | `1.8rem` | `1.6rem` |

##### `--hN-line-height`
Defines the CSS value for the heading's `line-height:` field. The default values
are as follow:

| `hN` | `h1`   | `h2`   | `h3`  | `h4`   |   `h5`   | `h6` |
|:-----|:-------|:-------|:------|:-------|:---------|:-----|
| HTML | `1.2`  | `1.25` | `1.3` | `1.35` | `1.5`    | `0`  |
| AMP  | `1.2`  | `1.25` | `1.3` | `1.35` | `1.5`    | `0`  |

##### `--hN-char-spacing`
Defines the CSS value for the heading's `letter-spacing:` field. The default
values are as follow:

| `hN` | `h1`      | `h2`     | `h3`     | `h4`      |   `h5`    | `h6` |
|:-----|:----------|:---------|:---------|:----------|:----------|:-----|
| HTML | `-.1rem`  | `-.1rem` | `-.1rem` | `-.08rem` | `-.05rem` | `0`  |
| AMP  | `-.1rem`  | `-.1rem` | `-.1rem` | `-.08rem` | `-.05rem` | `0`  |

##### `--hN-margin`
Defines the CSS value for the heading's `margin:` field. The default
values are as follow:

| `hN` | `h1`        | `h2`        | `h3`       | `h4`         |   `h5`       | `h6`          |
|:-----|:------------|:------------|:-----------|:-------------|:-------------|:--------------|
| HTML | `2rem 0 0`  | `10rem 0 0` | `8rem 0 0` | `4.5rem 0 0` | `4.5rem 0 0` | `4.5rem 0 0`  |
| AMP  | `2rem 0 0`  | `10rem 0 0` | `8rem 0 0` | `4.5rem 0 0` | `4.5rem 0 0` | `4.5rem 0 0`  |

##### `--hN-before-content`
Defines the CSS value for the heading's `hN:before { content: }` field. The
default values are as follow:

| `hN` | `h1` | `h2` | `h3` | `h4` | `h5` | `h6` |
|:-----|:-----|:-----|:-----|:-----|:-----|:-----|
| HTML | N/D  | `§`  | `¶`  | N/D  | N/D  | N/D  |
| AMP  | N/A  | N/A  | N/A  | N/A  | N/A  | N/A  |

Legends:
* `N/D` = Not defined.
* `N/A` = Not available.

##### `--hN-before-color`
Defines the CSS value for the heading's `hN:before { color: }` field. The
default values are as follow:

| `hN` | `h1` | `h2` | `h3` | `h4` | `h5` | `h6` |
|:-----|:-----|:-----|:-----|:-----|:-----|:-----|
| HTML | N/D  | `var(#{Color.$Primary-500})`  | `var(#{Color.$Primary-500})`  | N/D  | N/D  | N/D  |
| AMP  | N/A  | N/A  | N/A  | N/A  | N/A  | N/A  |

Legends:
* `N/D` = Not defined.
* `N/A` = Not available.

##### `--hN-before-margin`
Defines the CSS value for the heading's `hN:before { margin: }` field. The
default values are as follow:

| `hN` | `h1` | `h2` | `h3` | `h4` | `h5` | `h6` |
|:-----|:-----|:-----|:-----|:-----|:-----|:-----|
| HTML | N/D  | `0 2rem 0 0`  | `0 2rem 0 0`  | N/D  | N/D  | N/D  |
| AMP  | N/A  | N/A  | N/A  | N/A  | N/A  | N/A  |

Legends:
* `N/D` = Not defined.
* `N/A` = Not available.

##### `--hN-text-deco`
Defines the CSS value for the heading's `text-decoration:` field. The
default values are as follow:

| `hN` | `h1`         | `h2` | `h3` | `h4` | `h5` | `h6` |
|:-----|:-------------|:-----|:-----|:-----|:-----|:-----|
| HTML | `underline`  | N/D  | N/D  | N/D  | N/D  | N/D  |
| AMP  | `underline`  | N/D  | N/D  | N/D  | N/D  | N/D  |

Legends:
* `N/D` = Not defined.

##### `--hN-text-deco-color`
Defines the CSS value for the heading's `text-decoration-color:` field. The
default values are as follow:

| `hN` | `h1`                          | `h2` | `h3` | `h4` | `h5` | `h6` |
|:-----|:------------------------------|:-----|:-----|:-----|:-----|:-----|
| HTML | `var(#{Color.$Primary-500})`  | N/D  | N/D  | N/D  | N/D  | N/D  |
| AMP  | `var(#{Color.$Primary-500})`  | N/D  | N/D  | N/D  | N/D  | N/D  |

Legends:
* `N/D` = Not defined.

##### `--hN-border-bottom`
Defines the CSS value for the heading's `border-bottom:` field. The
default values are as follow:

| `hN` | `h1` | `h2`                                   | `h3` | `h4` | `h5` | `h6` |
|:-----|:-----|:---------------------------------------|:-----|:-----|:-----|:-----|
| HTML | N/D  | `1px solid var(#{Color.$Primary-300})` | N/D  | N/D  | N/D  | N/D  |
| AMP  | N/D  | `1px solid var(#{Color.$Primary-300})` | N/D  | N/D  | N/D  | N/D  |

Legends:
* `N/D` = Not defined.



### Color Management
Bissetii also manages its color schemes inside `Root` component. Bissetii uses
the
[Material.IO Color Naming Convention](https://material.io/resources/color/#!/?view.left=0&view.right=0) to determine the color names and toning identifications.

These variable changes are best defined at `<body>` syntax inline styling.
Example:

```html {linenos=table,hl_lines=[],linenostart=1}
<body style="--color-p500: #12ab2a;">
	...
</body>
```

To conserve the compiled CSS size without bloating it with a huge list
of color defined CSS variables, **not ALL toning ID are made available**.
If a particular toning ID is missing and you need it, please raise a feature
request at our [Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).


#### Variable Naming Convention
Bissetii standardizes the CSS variable naming convention for colors as follows,
with examples:

```css
--color-[NAME]-[TONE ID]

--color-primary-500
--color-blue-300
--color-red-300
```


#### Primary Colors
Primary colors are the single color that defines your theme. The name is called
`primary`. Bisettii set the following toning values by default:

```css
--color-primary-300: #ccff90;
--color-primary-500: #1ea82b;
--color-primary-700: #012104;
```


#### Contrast Colors
Contrast colors are attention seeking color that defines your complimentary
theme color. The name is called `contrast`. Bissetii set the following toning
values by default:

```css
--color-contrast-300: #fff59d;
--color-contrast-500: #ffeb3b;
--color-contrast-700: #f9c22d;
```


#### Red Colors
Red colors are the color red that defines glarying alert attention usually used
as indication. The name is called `red`. Bissetii set the following toning
values by default:

```css
--color-red-100: #ffe5e5;
--color-red-300: #ff6666;
--color-red-500: #ff0000;
--color-red-700: #b20000;
--color-red-900: #330000;
```


#### Yellow Colors
Yellow colors are the color yellow that defines noticable attention usually used
as mild level of indication. The name is called `yellow`. Bissetii set the
following toning values by default:

```css
--color-yellow-100: #ffffe5;
--color-yellow-300: #ffff66;
--color-yellow-500: #ffff00;
--color-yellow-700: #ffea00;
--color-yellow-900: #ffcc00;
```


#### Green Colors
Green colors are the color green that defines good attention usually used as
good level of indication. The name is called `green`. Bissetii set the following
toning values by default:

```css
--color-green-100: #e6fae6;
--color-green-300: #6ce569;
--color-green-500: #0ad406;
--color-green-700: #079404;
--color-green-900: #022a01;
```


#### Blue Colors
Blue colors are the color blue that defines simple attention usually used as
noticable level of indication. The name is called `blue`. Bissetii set the
following toning values by default:

```css
--color-blue-100: #e5e5ff;
--color-blue-300: #6666ff;
--color-blue-500: #0000ff;
--color-blue-700: #0000b2;
--color-blue-900: #000033;
```


#### Grey Colors
Grey colors are the color grey that defines the level of black and white usually
used in many applications especially typography. The name is called `grey`.
Bissetii set the following toning values by default:

```css
--color-grey-50: #fafafa;
--color-grey-100: #f5f5f5;
--color-grey-300: #e0e0e0;
--color-grey-400: #bdbdbd;
--color-grey-500: #9e9e9e;
--color-grey-700: #616161;
--color-grey-800: #4d4d4d;
--color-grey-900: #212121;
```



### Animation
Bissetii prepared some variables for defining CSS animation properties. These
controls the global animation settings like timing and etc.


#### Timing
The common animation setup would be timing. Bissetii prepares a number of
animation timing with its nicknames. There are as follows:

```css
--animation-timing-rapid: .2s;
--animation-timing-fast: .3s;
--animation-timing-normal: .4s;
--animation-timing-slow: .5s;
--animation-timing-slowest: .8s;
```




## Javascript
This component does not use any Javascript.




## Sass
Bissetii uses Dart Sass to compile the styling Sass codes into CSS file. This
component's Sass codes is available at the following location:

```
pkg/components/internal/root
```

Bissetii does not package Sass codes explictly so please view them via the
git repository.



### Version `v1.12.5` and before
{{< note "warning" "Important Notice" >}}
Support for Sass emoji is scheduled to be stopped starting **July 31, 2022**.
While the codes remain available, the team will no longer answer any questions
related to the matter.
{{< /note >}}

The Sass scripts responsible for styling emoji are located at:

```bash
assets/css/bissetii/modules/emoji/*
```

Bissetii is currently using external emoji font graphics like
`Noto Color Emoji` for page rendering.


#### Enabling Emoji
To enable emoji, you need to supply the type of emoji styling using its Bissetii
emoji code name. To disable emoji completely, simply supply `false` as falue.
The config is:
```sass
$config-emoji: code-name
```

##### Custom Sourcing URL Paths
Bissetii also supply optional local URL and remote URL sourcing path. If URLs
are specified, it will overrides the default URL. To disable them and use the
default URLs, simply supply an empty array (`[]`). The configs are:

```sass
$config-emoji-local-url: [ "font/myEmojiFile.tff" ]
$config-emoji-remote-url: [ "site.com/emoji.tff", "site.com/emoji.woff" ]
```


#### Custom Font
To supply custom font of your own, simply supply the codename as:
```sass
$config-emoji: custom-emoji
```

You **MUST** specify both local and remote URLs for sourcing the font files.
Otherwise, the compiler returns an error.


#### Noto Color Emoji
[Noto Color Emoji](https://www.google.com/get/noto/help/emoji/) is the Noto
font family emoji. The graphics font is available at its
[Github repository](https://github.com/googlefonts/noto-emoji). To enable this
font, simple supply the codename as:

```sass
$config-emoji: noto-color-emoji
```

By default, this font sources the font file from the following locations:

```html
Local URL : fonts/NotoColorEmoji.tff
Remote URL: https://gitcdn.xyz/repo/googlefonts/noto-emoji/master/fonts/NotoColorEmoji.ttf
```




## Epilogue
That is all about `Root` component in Bissetii. If you need more
feature or encountered a bug, please feel free to contact us by filing an issue
ticket at our [Issue Section](https://gitlab.com/zoralab/bissetii/-/issues). We
will be glad to help you.
