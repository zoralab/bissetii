+++
date = "2020-02-10T12:25:18+08:00"
title = "Typography Component"
description = """
Typography in web design is a critical component responsible for styling text
from font size to font style. Bissetii renders its typography in its specific
manner for multiple outputs.
"""
keywords = ["typography", "components", "html", "w3c", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[amp]
modules = [
        # Example: "amp-sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnail]
url = "/img/thumbnails/default.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Typography (DEPRECATED)"
pre = "❗"
weight = 1


[schema]
selectType = "WebPage"
+++

# `Typography` Component
Typography in web design is a critical component responsible for styling text
from font size to font style. Bissetii renders its typography in its specific
manner for multiple outputs.

This component is available starting from `v1.11.0`.

{{< note "warning" "DEPRECATION NOTICE" >}}
This component is deprecated starting from version `v1.13.0` and its font
management is merged into [Root Component]({{< link "/components/root"
"this" "url-only" />}}).

It will be rescinded on **July 31, 2022**.
{{< /note >}}



## Sass Styling Parts
The Sass styling script responsible for `Typography` component is located at:

```bash
assets/css/bissetii/modules/core/_Base.scss
assets/css/bissetii/modules/core/_Typography.scss
```

1. `_Base.scss` defines the default font styling and font types for the entire
page.
2. `_Typography.scss` defines the different font sizes for different texts.


## Javascript Parts
This component does not depend on any Javascript.


## HTML Parts
Bissetii complies to the headers' priority levels where `<h1>` is the
"main title of the page", `<h2>` is the section title of the page, etc.

This is for 2 objectives:

1. organize your page contents effectively.
2. comply to SEO scanning requirements.

Technically, each page should have:

1. **ONE** `<h1>` tag for page title.
2. **MANY** `<h2>` tags for sectioning you page.
3. **MANY** `<h3>`, `<h4>`, `<h5>`, and `<h6>` tags for sectioning you sections.

{{< note "default" "Note" >}}
A good content organizing does not go beyond `<h4>`. If you need `<h5>` and
beyond, you should consider splitting the page topic and link them purposefully.
{{< /note >}}


Bissetii currently renders `Typography` as the following:

{{< renderHTML "html" "amp" >}}
<h1>Main Page Title H1</h1>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit
convallis orci. Praesent vel nulla nec nulla semper finibus quis ut magna.</p>

<h2>Section Title 1 H2</h2>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit
convallis orci. Praesent vel nulla nec nulla semper finibus quis ut magna.
Nulla ut tortor non lorem sodales blandit. Nunc quis nunc quis odio lobortis
ornare. Donec nulla velit, accumsan a ultricies quis, viverra eu mauris. In
hac habitasse platea dictumst. Nam eu urna porttitor tellus imperdiet mattis
ut id diam. Praesent a interdum nisi, sed facilisis lectus. Nunc congue
tincidunt neque sed scelerisque. Pellentesque dignissim finibus est id egestas.
Fusce viverra eget sapien vel dignissim. Phasellus ipsum justo, commodo a
pharetra quis, mattis in quam. In sit amet urna volutpat, porta lacus quis,
hendrerit ante. Sed placerat magna pulvinar nisl egestas, id posuere elit
dictum.</p>

<h2 class="clean">Clean Class Section Title 2 H2</h2>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit
convallis orci. Praesent vel nulla nec nulla semper finibus quis ut magna.</p>

<h2>Section Title 2 H2</h2>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit
convallis orci. Praesent vel nulla nec nulla semper finibus quis ut magna.</p>

<h3>Sub-Section Title 1 H3</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit
convallis orci. Praesent vel nulla nec nulla semper finibus quis ut magna.</p>

<h3 class="clean">Clean Class Sub-Section Title 1 H3</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit
convallis orci. Praesent vel nulla nec nulla semper finibus quis ut magna.</p>

<h3>Sub-Section Title 2 H3</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit
convallis orci. Praesent vel nulla nec nulla semper finibus quis ut magna.</p>

<h4>Minor title 1 H4</h4>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit
convallis orci. Praesent vel nulla nec nulla semper finibus quis ut magna.</p>

<h4>Minor title 2 H4</h4>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit
convallis orci. Praesent vel nulla nec nulla semper finibus quis ut magna.</p>

<h5>Even Minor title 1 H5</h5>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit
convallis orci. Praesent vel nulla nec nulla semper finibus quis ut magna.</p>

<h5>Even Minor title 2 H5</h5>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit
convallis orci. Praesent vel nulla nec nulla semper finibus quis ut magna.</p>

<h6>Final Tiny title 1 H6</h6>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit
convallis orci. Praesent vel nulla nec nulla semper finibus quis ut magna.</p>

<h6>Final Tiny title 2 H6</h6>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit
convallis orci. Praesent vel nulla nec nulla semper finibus quis ut magna.</p>
{{< /renderHTML >}}
