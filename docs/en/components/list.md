+++
date = "2020-01-31T13:06:19+08:00"
title = "List Component"
description = """
List is a common component used in web pages. There are 2 common lists from W3C
HTML: ordered list (`ol`) and unordered list (`ul`). Bissetii supports both
list types seemlessly.
"""
keywords = ["list", "components", "html", "w3c", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "List"
pre = "🧮"
weight = 1


[schema]
selectType = "WebPage"
+++

# `List` Component
{{% param "description" %}}

This component is available starting from `v1.11.0`.




## Hugo
Hugo supports list seamlessly so no special development is required.




## Go
Coming soon.




## HTML
Bissetii supports `List` component seamlessly across multiple output formats.
Additionally, starting from version `v1.13.0`, the use of CSS variable is vital
for its upgrade from `v1.12.5`.

Bissetii HTML codes for this component are the same for Vanilla HTML5 and
AMPHTML.


### Ordered List
Bissetii supports ordered list (usually numeric) using the following HTML
pattern:

```html
<ol>
	<li>
		...
	</li>
	<li>
		...
	</li>
	...
</ol>
```
Here is an example:

{{< renderHTML html amp >}}
<ol>
	<li><p>1st item</p></li>
	<li><p>2nd item</p></li>
	<li><p>3rd item</p></li>
</ol>
{{< /renderHTML >}}



### Unordered List
Bissetii supports unordered list (usually disc bullet) using the following HTML
pattern:

```html
<ul>
	<li>
		...
	</li>
	<li>
		...
	</li>
	...
</ul>
```

Here is an example:

{{< renderHTML html amp >}}
<ul>
	<li><p>1st item</p></li>
	<li><p>2nd item</p></li>
	<li><p>3rd item</p></li>
</ul>
{{< /renderHTML >}}




## CSS
Bissetii provided a list of CSS variables dedicated for list styling alteration
without needing to recompile Sass. These variables are best applied directly to
the List HTML tags. Example:

```html
<ul style="--list-position: fixed; --li-print-break-inside: initial;">
	<li>
		...
	</li>
	<li>
		...
	</li>
	...
</ul>
```



### `--list-position`
Set the list layout position. The acceptable value shall be compatible with
`position:` CSS field. The default value is `relative`.



### `--list-padding-left`
Set the list distance from the left using padding. The acceptable value shall be
compatible with `padding-left:` CSS field. The default is `2.5rem`.



### `--list-margin`
Set the list distance using margin. The acceptable value shall be compatible
with `margin:` CSS field. The default is `1rem 1rem 1rem 3rem`.



### `--list-style-type`
Set the list icon type. The acceptable value shall be compatible with
`list-style-type:` CSS field. The default are:

1. `auto` as default.
2. `decimal` for ordered list (`<ol>`)
3. `disc` for unordered list (`<ul>`)



### `--list-style-position`
Set the list style position. The acceptable value shall be compatible with
`list-style-position:` CSS field. The default is `outside`.



### `--list-style-image`
Set the list style image. The acceptable value shall be compatible with
`list-style-image:` CSS field. The default is `none`.



### `--li-print-break-inside`
Set the print behavior for breaking the content within the list item `<li>`. The
acceptable value shall be compatible with `break-inside:` CSS field. The default
is `avoid`.




## Javascript
This component does not rely on any Javascript.




## Sass
Depending on release version, the Sass files work differently. Bissetii does not
package Sass codes explictly so please view them via the git repository.



### `v1.13.0` and Above
Bissetii uses Dart Sass to compile the styling Sass codes into CSS file. This
component's Sass codes are available at the following location:

```
pkg/components/internal/list
```



### `v1.12.5` and Before
{{< note "warning" "Deprecation Notice" >}}
Starting from version `v1.13.0`, this version of Sass is no longer used and the
support is scheduled to be removed on **July 31, 2022**.
{{< /note >}}
The Sass scripts responsible for styling the component are located in:

```
assets/css/bissetii/modules/core/_List.scss
```




## Researches
Here are the researches done to ensure `List` component meets the necessary
quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `List` component in Bissetii. If you need more feature or
need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
