+++
date = "2020-02-12T16:58:31+08:00"
title = "Shields.io Badge Component"
description = """
`Shields.io Badge` (a.k.a `shieldsbadge`) is the component to re-create
Shields.io SVG badges using only HTML+CSS codes instead of rendering SVG file.
The sole purpose is to avoid depending on external server rendering critical
content.
"""
keywords = ["shields-badge", "components", "html", "w3c", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Shields.io Badge"
pre = "🔰"
weight = 1


[schema]
selectType = "WebPage"
+++

# `Shields.io Badge` Component
{{% param "description" %}}

This component is available starting from `v1.11.0`.




## Hugo
To make things easier for content creation in Hugo, Bissetii supplies a number
of Hugo specific features to create `Shields.io Badge` component easily.



### Shortcodes
There are a number of shortcodes available at your disposal.


#### `shieldsbadge` Shortcode
`shieldsbadge` shortcode is available starting from `v1.13.0` and above for
replacing `svg/shieldTag` due to naming convention and parameter rearrangement.
The shortcode accepts the following pattern:

```html {linenos=table,hl_lines=[],linenostart=1}
{{</* shieldsbadge "[LABEL]" "[VALUE]" "[COLOR]" "[TEXT_COLOR]" */>}}
```

1. `LABEL` - **COMPULSORY** - The text for the badge label.
2. `VALUE` - **COMPULSORY** - The text for the badge data.
3. `COLOR` - **OPTIONAL** - The color of the badge data. Default is primary
                            color at `500` toning.
4. `TEXT_COLOR` - **OPTIONAL** - The color of the badge data's text. Default is
                            grey at `50` toning (near white).

Example usage:

```html {linenos=table,hl_lines=[],linenostart=1}
{{</* shieldsbadge "LANGUAGE" "HTML5" "#0011DC" "ABABAB" */>}}
		⥥
{{% shieldsbadge "LANGUAGE" "HTML5" "#0011DC" "ABABAB" %}}
```




#### `svg/shieldTag` Shortcode
{{< note "warning" "Deprecation Notice" >}}
Starting from version `v1.13.0`, the `svg/shieldTag` shortcode is deprecated
and the support is scheduled to be rescinded on **July 31, 2021**.

Please migrate to `shieldsbadge` shortcode instead.
{{< /note >}}

`svg/shieldTag` shortcode is available from `v1.12.5` and below. The shortcode
accepts the following pattern:

```html {linenos=table,hl_lines=[],linenostart=1}
{{</* shieldsbadge "[LABEL]" "[VALUE]" "[COLOR]" "[TEXT_COLOR]" "[URL]" */>}}
```

1. `LABEL` - **COMPULSORY** - The text for the badge label.
2. `VALUE` - **COMPULSORY** - The text for the badge data.
3. `COLOR` - **OPTIONAL** - The color of the badge data. Default is primary
                            color at `500` toning.
4. `TEXT_COLOR` - **OPTIONAL** - The color of the badge data's text. Default is
                            grey at `50` toning (near white).
3. `URL` - **DEPRECATED** - Optional URL link overlaying the badge.
                            **DO NOT USE**.



## Go
Coming Soon.




## HTML
Bissetii supports `Shields.io Badge` component seamlessly across multiple output
formats. Additionally, starting from version `v1.13.0`, the use of CSS variable
is vital for its upgrade from `v1.12.5`.

Bissetii HTML codes for this component are the same for Vanilla HTML5 and
AMPHTML. It uses the [Definition List]({{< link "/components/definitions-list/"
"this" "url-only" />}}) component as its primary code structure and
**ONLY ALLOWED 1 `<dt>` and 1 `<dd>` tags**.

{{< note "info" "IMPORTANT NOTE" >}}
Although the default definition list (`<dl>`) permits multiple label (`<dt>`)
and definition (`<dd>`) tags,
**it is strictly NOT supported for Bissetii's `Shields.io Badge` component**.
{{< /note >}}

The HTML code pattern for Bissetii's `Shields.io Badge` is as follows:

```html {linenos=table,hl_lines=[],linenostart=1}
<dl class="shields-badge">
	<dt>Single Line</dt>
	<dd>Value 1, Value 2, Value 3</dd>
</dl>
```

This will render as:

{{< renderHTML "html" "amp" >}}
<dl class="shields-badge">
	<dt>Single Line</dt>
	<dd>Value 1, Value 2, Value 3</dd>
</dl>
{{< /renderHTML >}}





## CSS
Bissetii provided a list of CSS variables dedicated for `Shields.io Badge`
styling alteration without needing to recompile Sass. These variables are best
applied directly to the HTML tags. Example:

```html {linenos=table,hl_lines=[],linenostart=1}
<dl class="shields-badge"
	style="--shieldsbadge-dd-background: #ff5500;
		--shieldsbadge-dd-color: yellow;">
	<dt>Inline Styling</dt>
	<dd>
		Value 1, Value 2, Value 3
	</dd>
</dl>
```

This will render away from the default as:

{{< renderHTML "html" "amp" >}}
<dl class="shields-badge"
	style="--shieldsbadge-dd-background: #ff5500;
		--shieldsbadge-dd-color: yellow;">
	<dt>Inline Styling</dt>
	<dd>
		Value 1, Value 2, Value 3
	</dd>
</dl>
{{< /renderHTML >}}



### `--shieldsbadge-border`
Set the badge's border styling. The acceptable value shall be compatible with
`border:` CSS field. The default is `none`.



### `--shieldsbadge-border-radius`
Set the badge's edge roundness. The acceptable value shall be compatible with
`border-radius:` CSS field. The default is `.4rem`.



### `--shieldsbadge-margin`
Set the badge's margin spacing. The acceptable value shall be compatible with
`margin:` CSS field. The default is `.4rem`.



### `--shieldsbadge-padding`
Set the badge's padding spacing. The acceptable value shall be compatible with
`padding:` CSS field. The default is `0`.



### `--shieldsbadge-dt-min-width`
Set the badge's label minimal width. The acceptable value shall be compatible
with `min-width:` CSS field. The default is `10.5rem`.



### `--shieldsbadge-dt-padding`
Set the badge's label padding spacing. The acceptable value shall be compatible
with `padding:` CSS field. The default is `0`.



### `--shieldsbadge-dt-font-size`
Set the badge's label font size. The acceptable value shall be compatible with
`font-size:` CSS field. The default is `1.4rem`.



### `--shieldsbadge-dt-font-weight`
Set the badge's label font weight. The acceptable value shall be compatible with
`font-weight:` CSS field. The default is `bold`.



### `--shieldsbadge-dt-color`
Set the badge's label content and text color. The acceptable value shall be
compatible with `color:` CSS field. The default is `var(--color-grey-50)`.



### `--shieldsbadge-dt-background`
Set the badge's label background styling. The acceptable value shall be
compatible with `background:` CSS field. The default is
`var(--color-primary-500)`.



### `--shieldsbadge-dd-white-space`
Set the badge's data white-space behavior. The acceptable value shall be
compatible with `white-space:` CSS field. The default is `normal`.



### `--shieldsbadge-dd-padding`
Set the badge's data padding spacing. The acceptable value shall be compatible
with `padding:` CSS field. The default is `.8rem`.



### `--shieldsbadge-dd-font-size`
Set the badge's data font size. The acceptable value shall be compatible with
`font-size:` CSS field. The default is `1.4rem`.



### `--shieldsbadge-dd-font-weight`
Set the badge's data font weight. The acceptable value shall be compatible with
`font-weight:` CSS field. The default is `bold`.



### `--shieldsbadge-dd-color`
Set the badge's data content and text color. The acceptable value shall be
compatible with `color:` CSS field. The default is `var(--color-grey-50)`.



### `--shieldsbadge-dd-background`
Set the badge's data background  styling. The acceptable value shall be
compatible with `background:` CSS field. The default is
`var(--color-primary-500)`.




## Javascript
This component does not rely on any Javascript.




## Sass
Depending on release version, the Sass files work differently. Bissetii does not
package Sass codes explictly so please view them via the git repository.



### `v1.13.0` and Above
Bissetii uses Dart Sass to compile the styling Sass codes into CSS file. This
component's Sass codes are available at the following location:

```
pkg/components/internal/shieldsbadge
```



### `v1.12.5` and Before
{{< note "warning" "Deprecation Notice" >}}
Starting from version `v1.13.0`, this version of Sass is no longer used and the
support is scheduled to be removed on **July 31, 2022**.
{{< /note >}}
The Sass scripts responsible for styling the component are located in:

```
assets/css/bissetii/modules/badges/_Shields.scss
```

To enable `Shields.io Badge`, you need to set the following config to 1:

```sass
$config-badges-shields: 1
```




## Researches
Here are the researches done to ensure `Shields.io Badge` component meets the
necessary quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `Shields.io Badge` component in Bissetii. If you need more
feature or need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
