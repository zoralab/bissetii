+++
date = "2021-08-30T09:38:01+08:00"
title = "Align Component"
description = """
Align component is a module to render a certain content with a specific
alignment in the page. This component is to ensure the content alignment is
always consistent across Bissetii's deployment.
"""
keywords = ["align", "components", "html", "w3c", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Align (BETA)"
pre = "🎏"
weight = 1


[schema]
selectType = "WebPage"
+++

# `Align` Component
{{% param "description" %}}

This component is available starting from `v1.13.1`.

{{< note "warning" "Beta Testing Notice" >}}
This is a fairly new component and it is still under user-level API beta
testing. Hence, there may be some **major updates in the future that are not
backward compatible**. Please use at your own risk.
{{< /note >}}




## Hugo
Bissetii supports Hugo interfaces to make content creation easier depending on
Bissetii version.



### Shortcodes
Bissetii prepared a list of shortcodes to make website creation a lot easier.
Here are some of the available shortcodes related to `Align` component.


#### `align` Shortcode
The `align` shortcode allows you to create a container with the CSS alignment.

This shortcode is available starting from version `v1.13.1`.

The format is as follows:

```markdown {linenos=table,hl_lines=[],linenostart=1}
{{</* align [ALIGN] [VALIGN] [STYLE] [CLASS] [ID] */>}}
	[CONTENT]
{{</* /align */>}}
```

* `ALIGN` - **COMPULSORY**
	1. Denotes the horizontal alignment. The values are as follows:
		1. `left` - set to left-aligned.
		2. `center` - set to center-aligned.
		3. `right` - set to right-aligned.
		4. `justify` - set to justify-aligned.
* `VALIGN` - **COMPULSORY**
	1. Denotes the vertical alignment. The values are as follows:
		1. `top` - set to top-aligned.
		2. `middle` - set to middle-aligned.
		3. `bottom` - set to bottom-aligned.
		4. `stretch` - set to stretch-aligned.
		5. `baseline` - set to baseline-aligned.
* `STYLE` - **OPTIONAL**
	1. The `style=` attribute for the container.
* `CLASS` - **OPTIONAL**
	1. The `class=` attribute for the container.
* `ID` - **OPTIONAL**
	1. The `id=` attribute for the container.
* `CONTENT` - **COMPULSORY**
	1. The content inside the container.

To use it, simply call it inside the markdown. Example:

```markdown {linenos=table,hl_lines=[],linenostart=1}
{{</* align "center" "bottom"
	"height: 80px; background: var(--color-grey-300)"
	"custom-class"
	"my-custom-id-1"
*/>}}
	This content will be horizontally centered, vertically bottom aligned.
{{</* /align */>}}

		⥥

{{% align "center" "bottom"
	"height: 80px; background: var(--color-grey-300)"
	"custom-class"
	"my-custom-id-1"
%}}
	This content will be horizontally centered, vertically bottom aligned.
{{% /align %}}
```

This will render as :

{{< align "center" "bottom"
	"height: 80px; background: var(--color-grey-300)"
	"custom-class"
	"my-custom-id-1" >}}
	This content will be horizontally centered, vertically bottom aligned.
{{< /align >}}




## Go
Coming soon.




## HTML
Bissetii supports `Align` component seamlessly across multiple output formats.
However, in most implmenetation (where you have access to HTML), this component
is entirely optional and you would not be needing it since it is essentially a
`flexbox wrapper`.

However, just in case for unknown reason where you really need to use this
component, simply apply the required CSS classes tags into a `<div>` container.
Example:

```html
<div class="align center middle" ...>
	<!-- YOUR CENTERED CONTENT HERE -->
</div>
```

The `.align` class tag is **compulsory** where it set the container as an
alignment container. This will reset its spacing related CSS fields like
`margin:` and `padding:` to `0` to ensure it does not introduce any spacing
related adjustment. It also set the `display:` mode into `flex` in order to
configure the given alignments accurately.

For horizontal alignment, these are the available CSS class tags to append in
the tail:

1. `left` - align the entire content towards left.
2. `center` - align the entire content towards center.
3. `right` - align the entire content towards right.
4. `justify` - align the entire content towards justify styling.

For vertical alignment, these are the avaialble CSS class tags to append in the
tail:

1. `top` - align the entire content towards the top.
2. `middle` - align the entire content towards the middle.
3. `bottom` - align the entire content towards the bottom.
4. `stretch` - align the entire content by stretching towards top and bottom.
4. `baseline` - align the entire content towards baselining.




## CSS
CSS implementation is not applicable for this component.




## Javascript
This component does not rely on any Javascript.




## Sass
Depending on release version, the Sass files work differently. Bissetii does not
package Sass codes explictly so please view them via the git repository.



### `v1.13.1` and Above
Bissetii uses Dart Sass to compile the styling Sass codes into CSS file. This
component's Sass codes are available at the following location:

```
pkg/components/internal/align
```




## Researches
Here are the researches done to ensure `Align` component meets the necessary
quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `Align` component in Bissetii. If you need more feature or
need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
