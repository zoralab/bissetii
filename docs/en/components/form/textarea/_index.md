+++
date = "2021-07-10T17:02:00+08:00"
title = "Textarea"
description = """
`Textarea` component is an input field for capturing a longer and larger text
input from user. Bissetii did a number of researches on its own and decided to
only provide styling for proper HTML5 semantic syntax defined by w3.org.
"""
keywords = ["form",
	"input",
	"textarea",
	"components",
	"html",
	"w3c",
	"bissetii",
]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
	"form",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Form | Textarea"
pre = "⌨"
weight = 1


[schema]
selectType = "WebPage"
+++

# `{{% param "title" %}}` Component
{{% param "description" %}}

This component is available starting from `v1.12.1`.




## Hugo
All `{{% param "title" %}}` Hugo interface is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Go
All `{{% param "title" %}}` Go interface is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## HTML
Expanding from  [Form Component]({{< link "/components/form/" "this"
"url-only" />}}), `{{% param title %}}` component is to facilitate an input
for entering a longer and larger text.

An all compatible attributes HTML syntax is shown as follows:

```html {linenos=table,hl_lines=[],linenostart=1}
<label for="input-javascript-id">Text</label>
<textarea
	id="input-javascript-id"
	name="example"
	cols="50"
	rows="50"
	wrap="hard"
	minlength="8"
	maxlength="40"
	spellcheck
	placeholder="...YOUR PLACEHOLDER..."
	required
	disabled
	autofocus
	autocomplete
	readonly
></textarea>
```

Here is an example:

{{< renderHTML html amp >}}
{{< form "" "post" "#" >}}
<fieldset class="border">
	<fieldset>
		<label for="form-textarea-required">
			Required
		</label>
		<textarea id="form-textarea-required"
			name="example-textarea-required"
			placeholder="Enter your statement here..."
			minlength="250"
			maxlength="2500"
			required
			spellcheck></textarea>
	</fieldset>
	<fieldset>
		<label for="form-textarea-required-filled">
			Required and Filled
		</label>
		<textarea id="form-textarea-required-filled"
			name="example-textarea-required-filled"
			placeholder="Enter your statement here..."
			minlength="250"
			maxlength="2500"
			row="2"
			required
			spellcheck>
A Quick Brown Fox Jumps Over The Lazy Dog.
</textarea>
	</fieldset>
	<fieldset>
		<label for="form-textarea-readonly-filled">
			Read Only and Filled
		</label>
		<textarea id="form-textarea-readonly-filled"
			name="example-textarea-readonly-filled"
			placeholder="Enter your statement here..."
			minlength="250"
			maxlength="2500"
			row="2"
			readonly
			spellcheck>
A Quick Brown Fox Jumps Over The Lazy Dog.
</textarea>
	</fieldset>
	<fieldset>
		<label for="form-textarea-disabled-filled">
			Disabled and Filled
		</label>
		<textarea id="form-textarea-disabled-filled"
			name="example-textarea-disabled-filled"
			placeholder="Enter your statement here..."
			minlength="250"
			maxlength="2500"
			row="2"
			disabled
			spellcheck>
A Quick Brown Fox Jumps Over The Lazy Dog.
</textarea>
	</fieldset>
	<fieldset>
		<label for="form-textarea-disabled-required-filled">
			Disabled, Required, and Filled
		</label>
		<textarea id="form-textarea-disabled-required-filled"
			name="example-textarea-disabled-required-filled"
			placeholder="Enter your statement here..."
			minlength="250"
			maxlength="2500"
			row="2"
			disabled
			required
			spellcheck>
A Quick Brown Fox Jumps Over The Lazy Dog.
</textarea>
	</fieldset>
</fieldset>
{{< /form >}}
{{< /renderHTML >}}




## CSS
All `{{% param "title" %}}` CSS variables is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Javascript
All `{{% param "title" %}}` Javascript is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Sass
All `{{% param "title" %}}` Sass is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Researches
Here are the researches done to ensure `{{% param "title" %}}` component meets
the necessary quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `{{% param "title" %}}` component in Bissetii. If you need more
feature or need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
