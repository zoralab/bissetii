+++
date = "2021-07-08T15:57:54+08:00"
title = "Color Input"
description = """
`Color Input` component is an input field for capturing a color data from known
red+blue+green color profile. Bissetii did a number of researches on its own and
decided to only provide styling for proper HTML5 semantic syntax defined by
w3.org.
"""
keywords = ["form",
	"input",
	"color",
	"components",
	"html",
	"w3c",
	"bissetii",
]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
	"form",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Form | Color"
pre = "⌨"
weight = 1


[schema]
selectType = "WebPage"
+++

# `{{% param "title" %}}` Component
{{% param "description" %}}

This component is available starting from `v1.12.1`.




## Hugo
All `{{% param "title" %}}` Hugo interface is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Go
All `{{% param "title" %}}` Go interface is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## HTML
Expanding from  [Form Component]({{< link "/components/form/" "this"
"url-only" />}}), Color is meant for capturing red, blue, green (RGB) color data
from user.

Due to the design of the `<input type="color">` where the unfilled value is
always `#000` (black), there is no way to indicate a `required` field that is
unfilled black is also a popular selection.

An all compatible attributes HTML syntax is shown as follows:

```html {linenos=table,hl_lines=[],linenostart=1}
<label for="input-javascript-id">Color</label>
<input type="color"
	id="input-javascript-id"
	name="example"
	value="#fefefe"
	required
	disabled
	autofocus
	autocomplete
	list="datalist-id"
/>
<datalist id="datalist-id">
	<option value="#515151" />
	...
</datalist>
```

Here is an example:

{{< renderHTML html amp >}}
{{< form "" "post" "#" >}}
<fieldset class="border">
	<fieldset>
		<label for="form-input-color-required">Required</label>
		<input type="color"
			id="form-input-color-required"
			name="example-color"
			required
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-color-required-filled">
			Required and Filled
		</label>
		<input type="color"
			id="form-input-color-required-filled"
			name="example-color"
			value="#ab23fe"
			required
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-color-list">
			List Selector
		</label>
		<input type="color"
			id="form-input-color-list"
			name="example-color"
			list="form-input-color-datalist"
		/>
		<datalist id="form-input-color-datalist">
			<option value="#515151" label="Team A" />
			<option value="#005100" label="Team B" />
			<option value="#000051" label="Team C" />
			<option value="#FF0000" label="Team D" />
			<option value="#00FF00" label="Team E" />
			<option value="#0000FF" label="Team F" />
		</datalist>
	</fieldset>
	<fieldset>
		<label for="form-input-color-disabled-filled">
			Disabled and Filled
		</label>
		<input type="color"
			id="form-input-color-disabled-filled"
			name="example-color"
			value="#ab23fe"
			disabled
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-color-required-disabled-filled">
			Required and Disabled
		</label>
		<input type="color"
			id="form-input-color-required-disabled-filled"
			name="example-color"
			value="#ab23fe"
			disabled
			required
		/>
	</fieldset>
</fieldset>
{{< /form >}}
{{< /renderHTML >}}




## CSS
All `{{% param "title" %}}` CSS variables is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Javascript
All `{{% param "title" %}}` Javascript is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Sass
All `{{% param "title" %}}` Sass is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Researches
Here are the researches done to ensure `{{% param "title" %}}` component meets
the necessary quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `{{% param "title" %}}` component in Bissetii. If you need more
feature or need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
