+++
date = "2021-07-11T11:58:10+08:00"
title = "File Input"
description = """
`File Input` component is an input field for designating file upload or
interacting with local capturing devices like camera from user. Bissetii did a
number of researches on its own and decided to only provide styling for proper
HTML5 semantic syntax defined by w3.org.
"""
keywords = ["form",
	"input",
	"file",
	"components",
	"html",
	"w3c",
	"bissetii",
]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
	"form",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Form | File"
pre = "⌨"
weight = 1


[schema]
selectType = "WebPage"
+++

# `{{% param "title" %}}` Component
{{% param "description" %}}

This component is available starting from `v1.12.1`.




## Hugo
All `{{% param "title" %}}` Hugo interface is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Go
All `{{% param "title" %}}` Go interface is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## HTML
Expanding from  [Form Component]({{< link "/components/form/" "this"
"url-only" />}}), `{{% param title %}}` is meant for designating file upload or
interacting with a local capturing devices like camera depending on the browser
support.

Should you need to specify specific files, use `accept=` attribute and specify
the supported file types. You need to search your list of MIME types
from [known sources](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types).
Below are some examples.

For PNG graphic files, the attribute can be any of the following:

```html {linenos=table,hl_lines=[],linenostart=1}
accept="image/png"
accept=".png"
```

For JPEG graphic files, the attribute can be any of the following:

```html {linenos=table,hl_lines=[],linenostart=1}
accept="image/jpeg"
accept=".jpg,.jpeg"
```

For both PNG and JPEG graphic files, the attribute is:

```html {linenos=table,hl_lines=[],linenostart=1}
accept="image/png,image/jpeg"
accept=".png,.jpg,.jpeg"
```

For any graphic files, the attribute is:

```
accept="image/*"
```

{{< note info "For Your Info" >}}
The `accept="image/*"` on most mobile devices sometimes allow user to take a
picture from camera when used.
{{< /note >}}

An all compatible attributes HTML syntax is shown as follows:

```html {linenos=table,hl_lines=[],linenostart=1}
<label for="input-javascript-id">File Upload</label>
<input type="file"
	id="input-javascript-id"
	name="example"
	accept="...YOUR FILTER..."
	capture
	multiple
	required
	disabled
	autofocus
	autocomplete
/>
```

{{< note warning "IMPORTANT" >}}
As of July 11, 2021, for unknown reason, `capture` is **DISALLOWED in AMPHTML**.
{{< /note >}}

Here is an example:

{{< renderHTML html amp >}}
{{< form "" "post" "#" >}}
<fieldset class="border">
	<fieldset>
		<label for="form-input-file">Capture</label>
		<input type="file"
			id="form-input-file"
			name="example-file"
			capture
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-file-required">
			Required, Multiple
		</label>
		<input type="file"
			id="form-input-file-required"
			name="example-file"
			multiple
			required
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-file-disabled">
			Disabled
		</label>
		<input type="file"
			id="form-input-file-disabled"
			name="example-file"
			disabled
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-file-required-disabled">
			Required and Disabled
		</label>
		<input type="file"
			id="form-input-file-required-disabled"
			name="example-file"
			disabled
			required
		/>
	</fieldset>
</fieldset>
{{< /form >}}
{{< /renderHTML >}}




## CSS
All `{{% param "title" %}}` CSS variables is directly supported by
[Button Component]({{< link "/components/button/" "this" "url-only" />}}) and
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Javascript
All `{{% param "title" %}}` Javascript is directly supported by
[Button Component]({{< link "/components/button/" "this" "url-only" />}}) and
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Sass
All `{{% param "title" %}}` Sass is directly supported by
[Button Component]({{< link "/components/button/" "this" "url-only" />}}) and
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Researches
Here are the researches done to ensure `{{% param "title" %}}` component meets
the necessary quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `{{% param "title" %}}` component in Bissetii. If you need more
feature or need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
