+++
date = "2021-07-08T18:24:37+08:00"
title = "Password Input"
description = """
`Password Input` component is an input field for capturing sensitive short text
from user. Bissetii did a number of researches on its own and decided to only
provide styling for proper HTML5 semantic syntax defined by w3.org.
"""
keywords = ["form",
	"input",
	"password",
	"components",
	"html",
	"w3c",
	"bissetii",
]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
	"form",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Form | Password"
pre = "⌨"
weight = 1


[schema]
selectType = "WebPage"
+++

# `{{% param "title" %}}` Component
{{% param "description" %}}

This component is available starting from `v1.12.1`.




## Hugo
All `{{% param "title" %}}` Hugo interface is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Go
All `{{% param "title" %}}` Go interface is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## HTML
Expanding from  [Form Component]({{< link "/components/form/" "this"
"url-only" />}}), `{{% param title %}}` component is to facilitate an input
for entering a short data-sensitive text.



### Patterns
To ensure the accepted text is complying to your specific pattern, use a regular
expression pattern for `pattern=` attribute. Most browsers will perform native
validation and Bissetii styles each of its state seamlessly.

For numeric pattern (e.g. PIN number):

1. 8 characters minimum: `([0-9]){8,}`
2. 16 characters maximum:`([0-9]){,16}`
3. 8 minimum and 16 maximum: `([0-9]){8,16}`

For alphanumeric with space pattern 8 characters minimum and 16 characters
maximum (E.g. OTP Vertification codes):

```html {linenos=table,hl_lines=[],linenostart=1}
([A-z0-9À-ž\s]){8,16}
```

For pattern accepting:
1. minimum 8 and 16 maximum characters
2. at least 1 lowercase a-Z
3. at least 1 uppercase A-Z
4. at least 1 number from 0-9
5. at least 1 symbol

```html {linenos=table,hl_lines=[],linenostart=1}
^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\(!@#\$%\^&\*\)]).{8,16}
```



### Helper Labels
Bissetii prepared a number of quick helpers CSS classes for quickly and easily
notify or educate users. There are 2 types: `.info` and `.notice` CSS classes.
All the contents within are exactly shown as it is.

`.info` CSS class onto `<label>` generates a small label information board
designed for education. This is very useful for stating essential information
such as your password requirements. Example:

```html {linenos=table,hl_lines=[],linenostart=1}
<label for="..." class="info">
<b>RULE:</b> Password must at least has:
	I. <b>ONE</b> lowercase (a-z)
	II. <b>ONE</b> uppercase letter (A-Z or À-ž)
	III. <b>ONE</b> number (0-9)
	IV. <b>ONE</b> symbol (!@#$%^&*()[])
	V. <b>Minimum 8</b> characters
</label>
<input type="password" ... />
```

`.notice` CSS class onto `<label>` with and without `.info` CSS class will
highlight the content into high attention (e.g. colored red) notices. This is
useful for reporting error for the field. Example:

```html {linenos=table,hl_lines=[],linenostart=1}
<label class="info notice"> ... </label>
<input type="password" ... />
<label class="notice">Bad Password. Plaese comply to the rules above.</label>
```

It is advisable to place the `.info` before the `<input>` and `.notice` after
it. That way, it simplifies the rendering and keeping the user interface clean
and simple.




### Codes
An all compatible attributes HTML syntax is shown as follows:

```html {linenos=table,hl_lines=[],linenostart=1}
<label for="input-javascript-id">Text</label>
<input type="password"
	id="input-javascript-id"
	name="example"
	value="...YOUR TEXT VALUE..."
	pattern="...YOUR PATTERN..."
	minlength="8"
	maxlength="40"
	placeholder="...YOUR PLACEHOLDER..."
	required
	disabled
	autofocus
	readonly
/>
```

Here is an example:

{{< renderHTML html amp >}}
{{< form "" "post" "#" >}}
<fieldset class="border">
	<fieldset>
		<label for="form-input-password-required">
			Required
		</label>
		<label for="form-input-password-required"
			class="info">
<b>RULE:</b> Password must at least has:
	I. <b>ONE</b> lowercase (a-z)
	II. <b>ONE</b> uppercase letter (A-Z or À-ž)
	III. <b>ONE</b> number (0-9)
	IV. <b>ONE</b> symbol (!@#$%^&*()[])
	V. <b>Minimum 16</b> characters
		</label>
		<input type="password"
			id="form-input-password-required"
			name="example-password-required"
			minlength="16"
			pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\(!@#\$%\^&\*\)]).{16,}"
			required
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-password-pin-required">
			Pin Number
		</label>
		<label for="form-input-password-pin-required"
			class="info">
<b>RULE:</b> Pin must at least has:
	I. <b>8 digits</b> number (0-9)
		</label>
		<input type="password"
			id="form-input-password-pin-required"
			name="example-password-pin-required"
			minlength="8"
			maxlength="8"
			pattern="([0-9]){8,8}"
			required
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-password-required-filled">
			Required and Filled
		</label>
		<label for="form-input-password-required-filled"
			class="info">
<b>RULE:</b> Password must at least has:
	I. <b>ONE</b> lowercase (a-z)
	II. <b>ONE</b> uppercase letter (A-Z or À-ž)
	III. <b>ONE</b> number (0-9)
	IV. <b>ONE</b> symbol (!@#$%^&*()[])
	V. <b>Minimum 8</b> characters
		</label>
		<input type="password"
			id="form-input-password-required-filled"
			name="example-password-required-filled"
			minlength="8"
			pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\(!@#\$%\^&\*\)]).{8,}"
			required
			value="MotivÀtor123&^%"
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-password-readonly-filled">
			Read Only and Filled
		</label>
		<label for="form-input-password-readonly-filled"
			class="info">
<b>RULE:</b> Password must at least has:
	I. <b>ONE</b> lowercase (a-z)
	II. <b>ONE</b> uppercase letter (A-Z or À-ž)
	III. <b>ONE</b> number (0-9)
	IV. <b>ONE</b> symbol (!@#$%^&*()[])
	V. <b>Minimum 8</b> characters
		</label>
		<input type="password"
			id="form-input-password-readonly-filled"
			name="example-password-readonly-filled"
			minlength="8"
			pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\(!@#\$%\^&\*\)]).{8,}"
			readonly
			value="MotivÀtor123&^%"
		/>
		<label class="notice">Locked</label>
	</fieldset>
	<fieldset>
		<label for="form-input-password-disabled-filled">
			Disabled and Filled
		</label>
		<label for="form-input-password-disabled-filled"
			class="info">
<b>RULE:</b> Password must at least has:
	I. <b>ONE</b> lowercase (a-z)
	II. <b>ONE</b> uppercase letter (A-Z or À-ž)
	III. <b>ONE</b> number (0-9)
	IV. <b>ONE</b> symbol (!@#$%^&*()[])
	V. <b>Minimum 8</b> characters
		</label>
		<input type="password"
			id="form-input-password-disabled-filled"
			name="example-password-disabled-filled"
			minlength="8"
			pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\(!@#\$%\^&\*\)]).{8,}"
			disabled
			value="MotivÀtor123&^%"
		/>
		<label class="notice">Disabled</label>
	</fieldset>
	<fieldset>
		<label for="form-input-password-disabled-required-filled">
			Disabled, Required, and Filled
		</label>
		<label for="form-input-password-disabled-required-filled"
			class="info">
<b>RULE:</b> Password must at least has:
	I. <b>ONE</b> lowercase (a-z)
	II. <b>ONE</b> uppercase letter (A-Z or À-ž)
	III. <b>ONE</b> number (0-9)
	IV. <b>ONE</b> symbol (!@#$%^&*()[])
	V. <b>Minimum 8</b> characters
		</label>
		<input type="password"
			id="form-input-password-disabled-required-filled"
			name="example-password-disabled-required-filled"
			minlength="8"
			pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\(!@#\$%\^&\*\)]).{8,}"
			disabled
			required
			value="MotivÀtor123&^%"
		/>
		<label class="notice">Disabled</label>
	</fieldset>
</fieldset>
{{< /form >}}
{{< /renderHTML >}}




## CSS
All `{{% param "title" %}}` CSS variables is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Javascript
All `{{% param "title" %}}` Javascript is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Sass
All `{{% param "title" %}}` Sass is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Researches
Here are the researches done to ensure `{{% param "title" %}}` component meets
the necessary quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `{{% param "title" %}}` component in Bissetii. If you need more
feature or need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
