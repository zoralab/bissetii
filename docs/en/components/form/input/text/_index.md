+++
date = "2021-07-08T18:24:37+08:00"
title = "Text Input"
description = """
`Text Input` component is an input field for capturing a short text input from
user. Bissetii did a number of researches on its own and decided to only provide
styling for proper HTML5 semantic syntax defined by w3.org.
"""
keywords = ["form",
	"input",
	"text",
	"components",
	"html",
	"w3c",
	"bissetii",
]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
	"form",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Form | Text"
pre = "⌨"
weight = 1


[schema]
selectType = "WebPage"
+++

# `{{% param "title" %}}` Component
{{% param "description" %}}

This component is available starting from `v1.12.1`.




## Hugo
All `{{% param "title" %}}` Hugo interface is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Go
All `{{% param "title" %}}` Go interface is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## HTML
Expanding from  [Form Component]({{< link "/components/form/" "this"
"url-only" />}}), `{{% param title %}}` component is to facilitate an input
for entering a short text.

To ensure the accepted text is complying to your specific pattern, use a regular
expression pattern for `pattern=` attribute. Most browsers will perform native
validation and Bissetii styles each of its state seamlessly.

Example, for alphanumeric with space pattern:

1. 8 characters minimum: `([A-z0-9À-ž\s]){8,}`
2. 16 characters maximum:`([A-z0-9À-ž\s]){,16}`
3. 8 minimum and 16 maximum: `([A-z0-9À-ž\s]){8,16}`

Another example, for alphanumeric and symbol pattern (e.g. use in password)
with minimum 8 and 16 maximum characters, 1 lowercase a-Z, 1 uppercase A-Z,
1 number, and 1 symbol:

```html {linenos=table,hl_lines=[],linenostart=1}
^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\(!@#\$%\^&\*\)]).{8,16}
```

An all compatible attributes HTML syntax is shown as follows:

```html {linenos=table,hl_lines=[],linenostart=1}
<label for="input-javascript-id">Text</label>
<input type="text"
	id="input-javascript-id"
	name="example"
	value="...YOUR TEXT VALUE..."
	pattern="...YOUR PATTERN..."
	minlength="8"
	maxlength="40"
	spellcheck
	placeholder="...YOUR PLACEHOLDER..."
	required
	disabled
	autofocus
	autocomplete
	readonly
	list="datalist-id"
/>
<datalist id="datalist-id">
	<option value="...OPTION 1 TEXT..." label="Label 1" />
	...
</datalist>
```

Here is an example:

{{< renderHTML html amp >}}
{{< form "" "post" "#" >}}
<fieldset class="border">
	<fieldset>
		<label for="form-input-text-required">
			8 to 16 Alphanumeric Characters and Required
		</label>
		<input type="text"
			id="form-input-text-required"
			name="example-text-required"
			minlength="8"
			maxlength="16"
			pattern="([A-z0-9À-ž\s]){8,16}"
			required
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-text-required-filled">
			8 to 16 number of characters,
			1 of each Alphanumeric+Symbol Characters,
			Required and Filled
		</label>
		<input type="text"
			id="form-input-text-required-filled"
			name="example-text-required-filled"
			minlength="8"
			maxlength="16"
			pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\(!@#\$%\^&\*\)]).{8,16}"
			required
			value="MotivÀtor123&^%"
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-text-list">
			List Selector
		</label>
		<input type="text"
			id="form-input-text-list"
			name="example-text-list"
			list="form-input-text-datalist"
			required
		/>
		<datalist id="form-input-text-datalist">
			<option value="John Smith" label="Work" />
			<option value="Jane Smith" label="Personal" />
			<option value="Christine Smith" label="Secretary" />
		</datalist>
	</fieldset>
	<fieldset>
		<label for="form-input-text-readonly-filled">
			8 to 16 number of characters,
			1 of each Alphanumeric+Symbol Characters,
			Read Only,
			and Filled
		</label>
		<input type="text"
			id="form-input-text-readonly-filled"
			name="example-text-readonly-filled"
			minlength="8"
			maxlength="16"
			pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\(!@#\$%\^&\*\)]).{8,16}"
			readonly
			value="MotivÀtor123&^%"
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-text-disabled-filled">
			8 to 16 number of characters,
			1 of each Alphanumeric+Symbol Characters,
			Disabled,
			and Filled
		</label>
		<input type="text"
			id="form-input-text-disabled-filled"
			name="example-text-disabled-filled"
			minlength="8"
			maxlength="16"
			pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\(!@#\$%\^&\*\)]).{8,16}"
			disabled
			value="MotivÀtor123&^%"
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-text-disabled-required-filled">
			8 to 16 number of characters,
			1 of each Alphanumeric+Symbol Characters,
			Disabled,
			Required,
			and Filled
		</label>
		<input type="text"
			id="form-input-text-disabled-required-filled"
			name="example-text-disabled-required-filled"
			minlength="8"
			maxlength="16"
			pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\(!@#\$%\^&\*\)]).{8,16}"
			disabled
			required
			value="MotivÀtor123&^%"
		/>
	</fieldset>
</fieldset>
{{< /form >}}
{{< /renderHTML >}}




## CSS
All `{{% param "title" %}}` CSS variables is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Javascript
All `{{% param "title" %}}` Javascript is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Sass
All `{{% param "title" %}}` Sass is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Researches
Here are the researches done to ensure `{{% param "title" %}}` component meets
the necessary quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `{{% param "title" %}}` component in Bissetii. If you need more
feature or need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
