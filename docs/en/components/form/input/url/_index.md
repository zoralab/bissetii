+++
date = "2021-07-10T15:35:44+08:00"
title = "URL Input"
description = """
`URL Input` component is an input field for capturing an Uniformed Resource
Locator (URL) address from user. Bissetii did a number of researches on its own
and decided to only provide styling for proper HTML5 semantic syntax defined by
w3.org.
"""
keywords = ["form",
	"input",
	"url",
	"components",
	"html",
	"w3c",
	"bissetii",
]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
	"form",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Form | URL"
pre = "⌨"
weight = 1


[schema]
selectType = "WebPage"
+++

# `{{% param "title" %}}` Component
{{% param "description" %}}

This component is available starting from `v1.12.1`.




## Hugo
All `{{% param "title" %}}` Hugo interface is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Go
All `{{% param "title" %}}` Go interface is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## HTML
Expanding from  [Form Component]({{< link "/components/form/" "this"
"url-only" />}}), `{{% param title %}}` component is to facilitate an input
for entering an Uniformed Resource Locator (URL) address.

To ensure the accepted URL is complying to your specific pattern, use a regular
expression pattern for `pattern=` attribute. Most browsers will perform native
validation and Bissetii styles each of its state seamlessly.

For HTTPS with controlled names that starts with optional `www.` and `vault.`,
then any domain name characters, then strictly ended with `.com` and `.net`
 would be:

```html {linenos=table,hl_lines=[],linenostart=1}
(https)(:\/\/)(www.|vault.)(([^.]+)\.)?([a-zA-z0-9\-_]+)(.com|.net)(\/[^\s]*)?
```

For any valid HTTPS URL:

```html {linenos=table,hl_lines=[],linenostart=1}
(https)(:\/\/)((www.)?)(([^.]+)\.)?([a-zA-z0-9\-_]+)(.*)?(\/[^\s]*)?
```

For any valid HTTP and HTTPS URL:

```html {linenos=table,hl_lines=[],linenostart=1}
(http(s?))(:\/\/)((www.)?)(([^.]+)\.)?([a-zA-z0-9\-_]+)(.*)?(\/[^\s]*)?
```

For telephone URL with `+123456789...` or `001234567...` formats:

```html {linenos=table,hl_lines=[],linenostart=1}
(tel)(:\/\/)(\+?)([0-9\-_]+)
```

For telephone URL with only `+123456789...` format:

```html {linenos=table,hl_lines=[],linenostart=1}
(tel)(:\/\/\+)([0-9\-_]+)
```

For email URL with `john.smith@local` and `john.smith@domain.com` formats:

```html {linenos=table,hl_lines=[],linenostart=1}
(email)(:\/\/)([a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+)@([a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*)
```

For email URL with only `john.smith@domain.com` format:

```html {linenos=table,hl_lines=[],linenostart=1}
(email)(:\/\/)(?![_.-])((?![_.-][_.-])[a-z\d_.-]){0,63}[a-z\d]@((?!-)((?!--)[a-z\d-]){0,63}[a-z\d]\.){1,2}([a-z]{2,14}\.)?[a-z]{2,14}
```


An all compatible attributes HTML syntax is shown as follows:

```html {linenos=table,hl_lines=[],linenostart=1}
<label for="input-javascript-id">Text</label>
<input type="url"
	id="input-javascript-id"
	name="example"
	value="...YOUR TEXT VALUE..."
	pattern="...YOUR PATTERN..."
	minlength="8"
	maxlength="40"
	spellcheck
	placeholder="...YOUR PLACEHOLDER..."
	required
	disabled
	autofocus
	autocomplete
	readonly
	list="datalist-id"
/>
<datalist id="datalist-id">
	<option value="...OPTION 1 TEXT..." label="Label 1" />
	...
</datalist>
```

Here is an example:

{{< renderHTML html amp >}}
{{< form "" "post" "#" >}}
<fieldset class="border">
	<fieldset>
		<label for="form-input-url-required">
			Any Legitimate URL, Required
		</label>
		<input type="url"
			id="form-input-url-required"
			name="example-url-required"
			placeholder="e.g.: https://www.example.com, email://john.smith@domain.com"
			required
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-url-required-filled">
			HTTPS Only,
			Required and Filled
		</label>
		<input type="url"
			id="form-input-url-required-filled"
			name="example-url-required-filled"
			pattern="(https)(:\/\/)((www.)?)(([^.]+)\.)?([a-zA-z0-9\-_]+)(.*)?(\/[^\s]*)?"
			placeholder="e.g.: https://www.example.com, email://john.smith@domain.com"
			required
			value="https://www.example.com"
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-url-list">
			Email URL, List Selector
		</label>
		<input type="url"
			id="form-input-url-list"
			name="example-url-list"
			list="form-input-url-datalist"
			pattern="(email)(:\/\/)([a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+)@([a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*)"
			placeholder="e.g.: email://john.smith@domain.com"
			required
		/>
		<datalist id="form-input-url-datalist">
			<option value="email://john.smith@local" label="Local" />
			<option value="email://john.smith@domain.com" label="Work" />
			<option value="email://jane.smith@domain.com" label="Secretary" />
		</datalist>
	</fieldset>
	<fieldset>
		<label for="form-input-url-readonly-filled">
			Any URL, Read Only, and Filled
		</label>
		<input type="url"
			id="form-input-url-readonly-filled"
			name="example-url-readonly-filled"
			readonly
			value="https://www.example.com"
			placeholder="e.g.: https://www.example.com, email://john.smith@domain.com"
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-url-disabled-filled">
			Any URL, Disabled, and Filled
		</label>
		<input type="url"
			id="form-input-url-disabled-filled"
			name="example-url-disabled-filled"
			disabled
			value="https://www.example.com"
			placeholder="e.g.: https://www.example.com, email://john.smith@domain.com"
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-url-disabled-required-filled">
			Any URL, Disabled, Required, and Filled
		</label>
		<input type="url"
			id="form-input-url-disabled-required-filled"
			name="example-url-disabled-required-filled"
			disabled
			required
			value="https://www.example.com"
			placeholder="e.g.: https://www.example.com, email://john.smith@domain.com"
		/>
	</fieldset>
</fieldset>
{{< /form >}}
{{< /renderHTML >}}




## CSS
All `{{% param "title" %}}` CSS variables is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Javascript
All `{{% param "title" %}}` Javascript is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Sass
All `{{% param "title" %}}` Sass is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Researches
Here are the researches done to ensure `{{% param "title" %}}` component meets
the necessary quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `{{% param "title" %}}` component in Bissetii. If you need more
feature or need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
