+++
date = "2021-07-08T17:26:00+08:00"
title = "Date Input"
description = """
`Date Input` component is an input field for capturing day, month, and year
time data from user. Bissetii did a number of researches on its own and decided
to only provide styling for proper HTML5 semantic syntax defined by w3.org.
"""
keywords = ["form",
	"input",
	"date",
	"components",
	"html",
	"w3c",
	"bissetii",
]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
	"form",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Form | Date"
pre = "⌨"
weight = 1


[schema]
selectType = "WebPage"
+++

# `{{% param "title" %}}` Component
{{% param "description" %}}

This component is available starting from `v1.12.1`.




## Hugo
All `{{% param "title" %}}` Hugo interface is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Go
All `{{% param "title" %}}` Go interface is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## HTML
Expanding from  [Form Component]({{< link "/components/form/" "this"
"url-only" />}}), `{{% param title %}}` component is to facilitate selecting
day, month, and year in a single input. Depending on brower support, should the
browser fails to facilitate a datepicker user interface, the field will
gracefully fallback to `<input type="text">`.

To ensure the data is working, **ALWAYS facilitate the following pattern** just
in case the fallback happens.

```html {linenos=table,hl_lines=[],linenostart=1}
\d{4}-\d{2}-\d{2}
```

An all compatible attributes HTML syntax is shown as follows:

```html {linenos=table,hl_lines=[],linenostart=1}
<label for="input-javascript-id">Date</label>
<input type="date"
	id="input-javascript-id"
	name="example"
	value="2016-06-01"
	pattern="\d{4}-\d{2}-\d{2}"
	required
	disabled
	autofocus
	autocomplete
	readonly
	list="datalist-id"
/>
<datalist id="datalist-id">
	<option value="1985-06-11" />
	...
</datalist>
```

Here is an example:

{{< renderHTML html amp >}}
{{< form "" "post" "#" >}}
<fieldset class="border">
	<fieldset>
		<label for="form-input-date-required">Required</label>
		<input type="date"
			id="form-input-date-required"
			name="example-date-required"
			pattern="\d{4}-\d{2}-\d{2}"
			required
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-date-required-filled">
			Required and Filled
		</label>
		<input type="date"
			id="form-input-date-required-filled"
			name="example-date-required-filled"
			value="1985-05-12"
			pattern="\d{4}-\d{2}-\d{2}"
			required
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-date-list">
			List Selector
		</label>
		<input type="date"
			id="form-input-date-list"
			name="example-date-list"
			list="form-input-date-datalist"
			pattern="\d{4}-\d{2}-\d{2}"
			required
		/>
		<datalist id="form-input-date-datalist">
			<option value="1985-05-22" label="22 May 1985" />
			<option value="1985-06-22" label="22 June 1985" />
			<option value="1985-07-22" label="22 July 1985" />
		</datalist>
	</fieldset>
	<fieldset>
		<label for="form-input-date-readonly-filled">
			Readonly and Filled
		</label>
		<input type="date"
			id="form-input-date-readonly-filled"
			name="example-date-readonly-filled"
			value="1985-05-12"
			pattern="\d{4}-\d{2}-\d{2}"
			readonly
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-date-disabled-filled">
			Disabled and Filled
		</label>
		<input type="date"
			id="form-input-date-disabled-filled"
			name="example-date-disabled-filled"
			value="1985-05-12"
			pattern="\d{4}-\d{2}-\d{2}"
			disabled
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-date-disabled-required-filled">
			Disabled, Required, and Filled
		</label>
		<input type="date"
			id="form-input-date-disabled-required-filled"
			name="example-date-disabled-required-filled"
			value="1985-05-12"
			pattern="\d{4}-\d{2}-\d{2}"
			required
			disabled
		/>
	</fieldset>
</fieldset>
{{< /form >}}
{{< /renderHTML >}}






## CSS
All `{{% param "title" %}}` CSS variables is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Javascript
All `{{% param "title" %}}` Javascript is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Sass
All `{{% param "title" %}}` Sass is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Researches
Here are the researches done to ensure `{{% param "title" %}}` component meets
the necessary quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `{{% param "title" %}}` component in Bissetii. If you need more
feature or need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
