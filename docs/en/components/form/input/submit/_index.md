+++
date = "2021-07-11T10:52:07+08:00"
title = "Submit Input"
description = """
`Submit Input` component is an input field for triggering a form submission from
user. Bissetii did a number of researches on its own and decided to only provide
styling for proper HTML5 semantic syntax defined by w3.org.
"""
keywords = ["form",
	"input",
	"submit",
	"components",
	"html",
	"w3c",
	"bissetii",
]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
	"form",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Form | Submit"
pre = "⌨"
weight = 1


[schema]
selectType = "WebPage"
+++

# `{{% param "title" %}}` Component
{{% param "description" %}}

This component is available starting from `v1.12.1`.




## Hugo
All `{{% param "title" %}}` Hugo interface is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Go
All `{{% param "title" %}}` Go interface is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## HTML
Expanding from  [Form Component]({{< link "/components/form/" "this"
"url-only" />}}), `{{% param title %}}` component is to facilitate an input
for submitting the form.

Unlike other input, `{{% param title %}}` is a straight foward submit button
in which some cases, can override form's `action=` and `method=` values at will.

An all compatible attributes HTML syntax is shown as follows:

```html {linenos=table,hl_lines=[],linenostart=1}
<input type="submit"
	name="submit[type]"
	value="...AN IDENTIFIABLE VALUE..."
	formaction="...URL OVERRIDING FORM ACTION..."
	formtype="...OVERRIDE FORM SUBMISSION TYPE..."
	formmethod="...OVERRIDE FORM METHOD (get|post|dialog)..."
	formnovalidate="true"
	formtarget="my-iframe"
	disabled
/>
```

{{< note warning "IMPORTANT" >}}
As of July 11, 2021, `formaction=`, `formtype=`, `formmethod=`,
`formnovalidate=`, and `formtarget=` are **DISALLOWED in AMPHTML** (See
[AMP Spec](https://amp.dev/documentation/components/amp-form/#not-allowed)).

For AMP compatibility, keep the from simple by API-driven (using its name
array).
{{< /note >}}

{{< note info "Redirecting Button" >}}
If a redirecting button like `cancel` button is needed, use an
[Anchor "Link" Component]({{< link "/components/anchor" "this" "url-only" />}})
with `.button` CSS class instead.

**Avoid using a `{{% param title %}}` for this purpose as the form data can be
submmited elsewhere which can potentially be a security issue**.
{{< /note >}}

Here is an example with multiple actions, where user can either login or
sign-up with a single form:

```html {linenos=table,hl_lines=["15-22"],linenostart=1}
<fieldset class="border">
	<fieldset class="border">
		<legend>Credentials</legend>
		<fieldset>
			<label>Email</label>
			<input type="email" required />
		</fieldset>
		<fieldset>
			<label>Password</label>
			<input type="password" required />
		</fieldset>
	</fieldset>
	<fieldset class="border">
		<legend>Your Decision</legend>
		<input type="submit"
			name="submit[login]"
			value="Login" />
		<input type="submit"
			name="submit[signup]"
			value="Sign Up" />
	</fieldset>
</fieldset>
```

This will render as (sign-up button disabled for showcasing the disabled
styling):

{{< renderHTML html amp >}}
{{< form "" "post" "#" >}}
<fieldset class="border">
	<fieldset>
		<legend>Credentials</legend>
		<fieldset>
			<label>Email</label>
			<input type="email" required />
		</fieldset>
		<fieldset>
			<label>Password</label>
			<input type="password" required />
		</fieldset>
	</fieldset>
	<fieldset class="center">
		<input type="submit"
			name="submit[login]"
			value="Login" />
		<input type="submit"
			name="submit[signup]"
			disabled
			value="Sign Up" />
	</fieldset>
</fieldset>
{{< /form >}}
{{< /renderHTML >}}




## CSS
All `{{% param "title" %}}` CSS variables is directly supported by
[Button Component]({{< link "/components/button/" "this" "url-only" />}}) and
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Javascript
All `{{% param "title" %}}` Javascript is directly supported by
[Button Component]({{< link "/components/button/" "this" "url-only" />}}) and
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Sass
All `{{% param "title" %}}` Sass is directly supported by
[Button Component]({{< link "/components/button/" "this" "url-only" />}}) and
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Researches
Here are the researches done to ensure `{{% param "title" %}}` component meets
the necessary quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `{{% param "title" %}}` component in Bissetii. If you need more
feature or need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
