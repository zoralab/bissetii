+++
date = "2021-07-08T18:09:22+08:00"
title = "Datetime Local Input"
description = """
`Datetime Local Input` component is an input field for capturing day, month,
year, hour, and minute time data from user. Bissetii did a number of researches
on its own and decided to only provide styling for proper HTML5 semantic syntax
defined by w3.org.
"""
keywords = ["form",
	"input",
	"datetime-local",
	"components",
	"html",
	"w3c",
	"bissetii",
]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
	"form",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Form | Datetime Local"
pre = "⌨"
weight = 1


[schema]
selectType = "WebPage"
+++

# `{{% param "title" %}}` Component
{{% param "description" %}}

This component is available starting from `v1.12.1`.




## Hugo
All `{{% param "title" %}}` Hugo interface is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Go
All `{{% param "title" %}}` Go interface is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## HTML
{{< note "warning" "Heads Up" >}}
Not all browser support this `<input>` type. More info at:
[Can I Use Input Datetime](https://caniuse.com/input-datetime)
{{< /note >}}

Expanding from  [Form Component]({{< link "/components/form/" "this"
"url-only" />}}), `{{% param title %}}` component is to facilitate selecting a
date and time together. Depending on brower support, should the browser fails to
facilitate a datepicker user interface, the field will gracefully fallback to
`<input type="text">`.

To ensure the data is working, **ALWAYS facilitate the following pattern** just
in case the fallback happens.

```html {linenos=table,hl_lines=[],linenostart=1}
[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}
```

An all compatible attributes HTML syntax is shown as follows:

```html {linenos=table,hl_lines=[],linenostart=1}
<label for="input-javascript-id">Date and Time</label>
<input type="datetime-local"
	id="input-javascript-id"
	name="example"
	value="2016-06-01"
	pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}"
	required
	disabled
	autofocus
	autocomplete
	readonly
	list="datalist-id"
/>
<datalist id="datalist-id">
	<option value="2018-06-12T19:30" label="12 June 2018, 19:30" />
	...
</datalist>
```

Here is an example:

{{< renderHTML html amp >}}
{{< form "" "post" "#" >}}
<fieldset class="border">
	<fieldset>
		<label for="form-input-datetime-required">Required</label>
		<input type="datetime-local"
			id="form-input-datetime-required"
			name="example-datetime-required"
			pattern="\d{4}-\d{2}-\d{2}"
			required
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-datetime-required-filled">
			Required and Filled
		</label>
		<input type="datetime-local"
			id="form-input-datetime-required-filled"
			name="example-datetime-required-filled"
			value="2018-06-15T22:30"
			pattern="\d{4}-\d{2}-\d{2}"
			required
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-datetime-list">
			List Selector
		</label>
		<input type="datetime"
			id="form-input-datetime-list"
			name="example-datetime-list"
			list="form-input-datetime-datalist"
			pattern="\d{4}-\d{2}-\d{2}"
			required
		/>
		<datalist id="form-input-datetime-datalist">
			<option value="2018-06-12T19:30"
				label="12 June 2018, 19:30"
			/>
			<option value="2019-12-31T22:45"
				label="31 December 2019, 22:45"
			/>
			<option value="2020-01-01T00:05"
				label="01 January 2020, 00:05"
			/>
		</datalist>
	</fieldset>
	<fieldset>
		<label for="form-input-datetime-readonly-filled">
			Readonly and Filled
		</label>
		<input type="datetime-local"
			id="form-input-datetime-readonly-filled"
			name="example-datetime-readonly-filled"
			value="2018-06-15T22:30"
			pattern="\d{4}-\d{2}-\d{2}"
			readonly
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-datetime-disabled-filled">
			Disabled and Filled
		</label>
		<input type="datetime-local"
			id="form-input-datetime-disabled-filled"
			name="example-datetime-disabled-filled"
			value="2018-06-15T22:30"
			pattern="\d{4}-\d{2}-\d{2}"
			disabled
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-datetime-disabled-required-filled">
			Disabled, Required, and Filled
		</label>
		<input type="datetime-local"
			id="form-input-datetime-disabled-required-filled"
			name="example-datetime-disabled-required-filled"
			value="2018-06-15T22:30"
			pattern="\d{4}-\d{2}-\d{2}"
			required
			disabled
		/>
	</fieldset>
</fieldset>
{{< /form >}}
{{< /renderHTML >}}




## CSS
All `{{% param "title" %}}` CSS variables is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Javascript
All `{{% param "title" %}}` Javascript is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Sass
All `{{% param "title" %}}` Sass is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Researches
Here are the researches done to ensure `{{% param "title" %}}` component meets
the necessary quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `{{% param "title" %}}` component in Bissetii. If you need more
feature or need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
