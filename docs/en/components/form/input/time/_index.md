+++
date = "2021-07-10T19:26:51+08:00"
title = "Time Input"
description = """
`Time Input` component is an input field for capturing hour and minute time data
from user. Bissetii did a number of researches on its own and decided to only
provide styling for proper HTML5 semantic syntax defined by w3.org.
"""
keywords = ["form",
	"input",
	"time",
	"components",
	"html",
	"w3c",
	"bissetii",
]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
	"form",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Form | Time"
pre = "⌨"
weight = 1


[schema]
selectType = "WebPage"
+++

# `{{% param "title" %}}` Component
{{% param "description" %}}

This component is available starting from `v1.12.1`.




## Hugo
All `{{% param "title" %}}` Hugo interface is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Go
All `{{% param "title" %}}` Go interface is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## HTML
Expanding from  [Form Component]({{< link "/components/form/" "this"
"url-only" />}}), `{{% param title %}}` component is to facilitate selecting
hours and minutes time format.

Most browsers are already supporting `{{% param title %}}` so no `pattern=`
attribute is required.

In extreme cases where a browser fails to facilitate a datepicker user
interface, the field will gracefully fallback to `<input type="text">`. Unless
absoute necessary, **facilitate the following pattern** shall preserve the
format:

```html {linenos=table,hl_lines=[],linenostart=1}
(?:[01]|2(?![4-9]))\d:[0-5]\d
```

An all compatible attributes HTML syntax is shown as follows:

```html {linenos=table,hl_lines=[],linenostart=1}
<label for="input-javascript-id">Time</label>
<input type="time"
	id="input-javascript-id"
	name="example"
	value="22:25"
	min="05:00"
	max="23:00"
	required
	disabled
	autofocus
	autocomplete
	readonly
/>
```

Here is an example:

{{< renderHTML html amp >}}
{{< form "" "post" "#" >}}
<fieldset class="border">
	<fieldset>
		<label for="form-input-time-required">Required</label>
		<input type="time"
			id="form-input-time-required"
			name="example-time-required"
			required
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-time-required-filled">
			Required and Filled
		</label>
		<input type="time"
			id="form-input-time-required-filled"
			name="example-time-required-filled"
			min="05:00"
			max="23:00"
			value="05:00"
			required
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-time-readonly-filled">
			Readonly and Filled
		</label>
		<input type="time"
			id="form-input-time-readonly-filled"
			name="example-time-readonly-filled"
			value="05:00"
			readonly
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-time-disabled-filled">
			Disabled and Filled
		</label>
		<input type="time"
			id="form-input-time-disabled-filled"
			name="example-time-disabled-filled"
			value="05:00"
			disabled
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-time-disabled-required-filled">
			Disabled, Required, and Filled
		</label>
		<input type="time"
			id="form-input-time-disabled-required-filled"
			name="example-time-disabled-required-filled"
			value="05:00"
			required
			disabled
		/>
	</fieldset>
</fieldset>
{{< /form >}}
{{< /renderHTML >}}






## CSS
All `{{% param "title" %}}` CSS variables is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Javascript
All `{{% param "title" %}}` Javascript is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Sass
All `{{% param "title" %}}` Sass is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Researches
Here are the researches done to ensure `{{% param "title" %}}` component meets
the necessary quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `{{% param "title" %}}` component in Bissetii. If you need more
feature or need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
