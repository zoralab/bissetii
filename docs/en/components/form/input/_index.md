+++
date = "2021-07-08T10:43:19+08:00"
title = "Form - All Inputs"
description = """
`Form - All Inputs` are all Bissetii supported input fields. Despite as simple
as it sounds, there are a lot of complicated guides and tutorials all over the
Internet. Bissetii did a number of researches on its own and decided to only
provide styling for proper HTML5 semantic syntax defined by w3.org.
"""
keywords = ["form inputs", "inputs", "components", "html", "w3c", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Form | All Inputs"
pre = "⌨"
weight = 1


[schema]
selectType = "WebPage"
+++

# `{{% param "title" %}}` Component
{{% param "description" %}}

Bissetii supports a large number of input types made available via HTML5
seamlessly. Each type has its own validation pattern and Bissetii's recomemnded
HTML code structure.

To keep communications sharp and percise, All the sample codes here will only be
the `<input>` syntax without any fancy grouping or layout.

Here are all the Bissetii supported input types:

{{< cards "bissetii.data.input.en" "grid" >}}




## Epilogue
That's all about `{{% param title %}}` component in Bissetii. If you need more
feature or need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
