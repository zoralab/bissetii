+++
date = "2021-07-10T20:11:14+08:00"
title = "Radio Input"
description = """
`Radio Input` component is an input field for capturing single selection
from multiple options decision from user. Bissetii did a number of researches
on its own and decided to only provide styling for proper HTML5 semantic syntax
defined by w3.org.
"""
keywords = ["form",
	"input",
	"radio",
	"components",
	"html",
	"w3c",
	"bissetii",
]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
	"form",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Form | Radio"
pre = "⌨"
weight = 1


[schema]
selectType = "WebPage"
+++

# `{{% param "title" %}}` Component
{{% param "description" %}}

This component is available starting from `v1.12.1`.




## Hugo
All `{{% param "title" %}}` Hugo interface is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Go
All `{{% param "title" %}}` Go interface is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## HTML
Expanding from  [Form Component]({{< link "/components/form/" "this"
"url-only" />}}), `{{% param "title" %}}` component is meant for selecting a
single choice from multiple options.

To properly use it with Bissetii:

1. All options for a subject shall share the same value for `name=` attribute.
2. The `<label>` is always exists after the `<input>` for styling requirement.

An all compatible attributes HTML syntax is shown as follows:

```html {linenos=table,hl_lines=[],linenostart=1}
<ul>
	<li>
		<input type="radio"
			id="input-javascript-id"
			name="example"
			value="Checked by Default"
			checked
			required
			disabled
			autofocus
		/>
		<label for="input-javascript-id">Label Here</label>
	</li>
	...
</ul>
```

Here is an example:

{{< renderHTML html amp >}}
{{< form "" "post" "#" >}}
<fieldset class="border">
	<ul class="inline">
		<li>
			<input type="radio"
				id="form-input-radio-required"
				name="Example Radio"
				value="Required"
				required
			/>
			<label for="form-input-radio-required">
				Required
			</label>
		</li>
		<li>
			<input type="radio"
				id="form-input-checkbox-checked"
				name="Example Radio"
				value="Optional"
			/>
			<label for="form-input-checkbox-checked">
				Optional
			</label>
		</li>
		<li>
			<input type="radio"
				id="form-input-checkbox-disabled-checked"
				name="Example Radio"
				value="Disabled and Checked"
				checked
				disabled
			/>
			<label for="form-input-checkbox-disabled-checked">
				Disabled and Checked
			</label>
		</li>
		<li>
			<input type="radio"
				id="form-input-checkbox-disabled"
				name="Example Radio"
				value="Disabled"
				disabled
			/>
			<label for="form-input-checkbox-disabled">
				Disabled
			</label>
		</li>
	</ul>
</fieldset>
{{< /form >}}
{{< /renderHTML >}}




## CSS
All `{{% param "title" %}}` CSS variables is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Javascript
All `{{% param "title" %}}` Javascript is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Sass
All `{{% param "title" %}}` Sass is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Researches
Here are the researches done to ensure `{{% param "title" %}}` component meets
the necessary quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `{{% param "title" %}}` component in Bissetii. If you need more
feature or need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
