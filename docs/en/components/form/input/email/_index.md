+++
date = "2021-07-08T18:24:37+08:00"
title = "Email Input"
description = """
`Email Input` component is an input field for capturing email address data from
user. Bissetii did a number of researches on its own and decided to only provide
styling for proper HTML5 semantic syntax defined by w3.org.
"""
keywords = ["form",
	"input",
	"email",
	"components",
	"html",
	"w3c",
	"bissetii",
]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
	"form",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Form | Email"
pre = "⌨"
weight = 1


[schema]
selectType = "WebPage"
+++

# `{{% param "title" %}}` Component
{{% param "description" %}}

This component is available starting from `v1.12.1`.




## Hugo
All `{{% param "title" %}}` Hugo interface is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Go
All `{{% param "title" %}}` Go interface is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## HTML
Expanding from  [Form Component]({{< link "/components/form/" "this"
"url-only" />}}), `{{% param title %}}` component is to facilitate an input
for entering an email address. To ensure the input email address matches the
correct pattern, apply the following value to the `pattern=` field depending on
your specific needs.

For supporting localized email such as `john.smith@local`, most browser has
already implemented the default pattern natively. Hence, no additional pattern
is required. For learning purposes, the following pattern is used:

```html {linenos=table,hl_lines=[],linenostart=1}
^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$
```

For strictly global domain email such as `john.smith@domain.com`, use the
following pattern:

```html {linenos=table,hl_lines=[],linenostart=1}
(?![_.-])((?![_.-][_.-])[a-z\d_.-]){0,63}[a-z\d]@((?!-)((?!--)[a-z\d-]){0,63}[a-z\d]\.){1,2}([a-z]{2,14}\.)?[a-z]{2,14}
```

For locking towards a specific domain pattern such as `[NAME]@mydomain.com`,
replace the pattern above with a hard-coded domain name after the `@`. Example:

```html {linenos=table,hl_lines=[],linenostart=1}
(?![_.-])((?![_.-][_.-])[a-z\d_.-]){0,63}[a-z\d]@mydomain.com
```

An all compatible attributes HTML syntax is shown as follows:

```html {linenos=table,hl_lines=[],linenostart=1}
<label for="input-javascript-id">Email</label>
<input type="email"
	id="input-javascript-id"
	name="example"
	value="john.smith@domain.me"
	pattern="...YOUR PATTERN..."
	required
	disabled
	autofocus
	autocomplete
	readonly
	list="datalist-id"
/>
<datalist id="datalist-id">
	<option value="john.smith@domain.com" />
	...
</datalist>
```

Here is an example:

{{< renderHTML html amp >}}
{{< form "" "post" "#" >}}
<fieldset class="border">
	<fieldset>
		<label for="form-input-email-required">
			Local-Email and Required
		</label>
		<input type="email"
			id="form-input-email-required"
			name="example-email-required"
			required
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-email-required-filled">
			Global-Email, Required and Filled
		</label>
		<input type="email"
			id="form-input-email-required-filled"
			name="example-email-required-filled"
			value="john.smith@domain.me"
			pattern="(?![_.-])((?![_.-][_.-])[a-z\d_.-]){0,63}[a-z\d]@((?!-)((?!--)[a-z\d-]){0,63}[a-z\d]\.){1,2}([a-z]{2,14}\.)?[a-z]{2,14}"
			required
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-email-list">
			Global-Email and List Selector
		</label>
		<input type="email"
			id="form-input-email-list"
			name="example-email-list"
			list="form-input-email-datalist"
			pattern="(?![_.-])((?![_.-][_.-])[a-z\d_.-]){0,63}[a-z\d]@((?!-)((?!--)[a-z\d-]){0,63}[a-z\d]\.){1,2}([a-z]{2,14}\.)?[a-z]{2,14}"
			required
		/>
		<datalist id="form-input-email-datalist">
			<option value="john.smith@domain.com" label="Work" />
			<option value="john.smith@domain.net" label="Personal" />
			<option value="john.smith@domain.co" label="Secretary" />
		</datalist>
	</fieldset>
	<fieldset>
		<label for="form-input-email-readonly-filled">
			Readonly and Filled
		</label>
		<input type="email"
			id="form-input-email-readonly-filled"
			name="example-email-readonly-filled"
			value="john.smith@domain.me"
			pattern="(?![_.-])((?![_.-][_.-])[a-z\d_.-]){0,63}[a-z\d]@((?!-)((?!--)[a-z\d-]){0,63}[a-z\d]\.){1,2}([a-z]{2,14}\.)?[a-z]{2,14}"
			readonly
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-email-disabled-filled">
			Disabled and Filled
		</label>
		<input type="email"
			id="form-input-email-disabled-filled"
			name="example-email-disabled-filled"
			value="john.smith@domain.me"
			pattern="(?![_.-])((?![_.-][_.-])[a-z\d_.-]){0,63}[a-z\d]@((?!-)((?!--)[a-z\d-]){0,63}[a-z\d]\.){1,2}([a-z]{2,14}\.)?[a-z]{2,14}"
			disabled
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-email-disabled-required-filled">
			Disabled, Required, and Filled
		</label>
		<input type="email"
			id="form-input-email-disabled-required-filled"
			name="example-email-disabled-required-filled"
			value="john.smith@domain.me"
			pattern="(?![_.-])((?![_.-][_.-])[a-z\d_.-]){0,63}[a-z\d]@((?!-)((?!--)[a-z\d-]){0,63}[a-z\d]\.){1,2}([a-z]{2,14}\.)?[a-z]{2,14}"
			required
			disabled
		/>
	</fieldset>
</fieldset>
{{< /form >}}
{{< /renderHTML >}}




## CSS
All `{{% param "title" %}}` CSS variables is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Javascript
All `{{% param "title" %}}` Javascript is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Sass
All `{{% param "title" %}}` Sass is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Researches
Here are the researches done to ensure `{{% param "title" %}}` component meets
the necessary quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `{{% param "title" %}}` component in Bissetii. If you need more
feature or need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
