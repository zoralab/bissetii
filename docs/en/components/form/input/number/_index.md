+++
date = "2021-07-08T18:24:37+08:00"
title = "Number Input"
description = """
`Number Input` component is an input field for capturing numbers (with and
without decimal from user. Bissetii did a number of researches on its own and
decided to only provide styling for proper HTML5 semantic syntax defined by
w3.org.
"""
keywords = ["form",
	"input",
	"number",
	"components",
	"html",
	"w3c",
	"bissetii",
]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
	"form",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Form | Number"
pre = "⌨"
weight = 1


[schema]
selectType = "WebPage"
+++

# `{{% param "title" %}}` Component
{{% param "description" %}}

This component is available starting from `v1.12.1`.




## Hugo
All `{{% param "title" %}}` Hugo interface is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Go
All `{{% param "title" %}}` Go interface is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## HTML
{{< note "warning" "Heads Up" >}}
Not all browser support this `<input>` type. More info at:
[Can I Use Input Number](https://caniuse.com/input-number)
{{< /note >}}

Expanding from  [Form Component]({{< link "/components/form/" "this"
"url-only" />}}), `{{% param title %}}` component is to facilitate an input
for capturing number value with and without decimal format. Should a browser
cannot provide an user interface for this `<input>`, it will falls back to
`<input type="text">` gracefully and automatically.

By default, no pattern validation is needed as most supported browser already
handled it natively.

To allow decimal number (e.g. `0.01`), you need to add `step=` attribute with
the decimal limits you need. Example, for upto 8 decimal places, the attribute
should be `step="0.00000001"`. Comma decimal symbol (e.g. `0,01`) is usually
not supported by the browser. Enabling this attribute allows user to step
up/down via the stepping button.

To limit the number range, use `min=` and `max=` attributes matching the
`step=` attribute format.

An all compatible attributes HTML syntax is shown as follows:

```html {linenos=table,hl_lines=[],linenostart=1}
<label for="input-javascript-id">Email</label>
<input type="number"
	id="input-javascript-id"
	name="example"
	value="25"
	required
	disabled
	autofocus
	autocomplete
	readonly
	min="0"
	max="100"
	step="0.01"
	list="datalist-id"
/>
<datalist id="datalist-id">
	<option value="45.5" />
	...
</datalist>
```

Here is an example:

{{< renderHTML html amp >}}
{{< form "" "post" "#" >}}
<fieldset class="border">
	<fieldset>
		<label for="form-input-round-number-required">
			Round Number and Required
		</label>
		<input type="number"
			id="form-input-round-number-required"
			name="example-round-number-required"
			required
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-decimal-dot-number-required">
			2 Decimal Dot Number and Required
		</label>
		<input type="number"
			id="form-input-decimal-dot-number-required"
			name="example-decimal-dot-number-required"
			step="0.01"
			required
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-round-number-required-filled">
			Round Number, Required, Filled
		</label>
		<input type="number"
			id="form-input-round-number-required-filled"
			name="example-round-number-required-filled"
			required
			value="125"
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-round-number-list">
			Round Number and List Selector
		</label>
		<input type="number"
			id="form-input-round-number-list"
			name="example-round-number-list"
			required
			list="form-input-number-datalist"
		/>
		<datalist id="form-input-number-datalist">
			<option value="35" label="Beginner Level" />
			<option value="45" label="Normal Level" />
			<option value="75" label="Profressional Level" />
			<option value="100" label="God Tier Level" />
		</datalist>
	</fieldset>
	<fieldset>
		<label for="form-input-round-number-readonly-filled">
			Round Number, Read-Only, Filled
		</label>
		<input type="number"
			id="form-input-round-number-readonly-filled"
			name="example-round-number-readonly-filled"
			readonly
			value="125"
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-round-number-disabled-filled">
			Round Number, Disabled, and Filled
		</label>
		<input type="number"
			id="form-input-round-number-disabled-filled"
			name="example-round-number-disabled-filled"
			disabled
			value="125"
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-round-number-disabled-required-filled">
			Round Number, Disabled, Required, and Filled
		</label>
		<input type="number"
			id="form-input-round-number-disabled-required-filled"
			name="example-round-number-disabled-required-filled"
			disabled
			required
			value="125"
		/>
	</fieldset>
</fieldset>
{{< /form >}}
{{< /renderHTML >}}




## CSS
All `{{% param "title" %}}` CSS variables is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Javascript
All `{{% param "title" %}}` Javascript is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Sass
All `{{% param "title" %}}` Sass is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Researches
Here are the researches done to ensure `{{% param "title" %}}` component meets
the necessary quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `{{% param "title" %}}` component in Bissetii. If you need more
feature or need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
