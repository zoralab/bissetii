+++
date = "2021-07-10T16:33:18+08:00"
title = "Tel Input"
description = """
`Tel Input` component is an input field for capturing a short phone number input
from user. Bissetii did a number of researches on its own and decided to only
provide styling for proper HTML5 semantic syntax defined by w3.org.
"""
keywords = ["form",
	"input",
	"tel",
	"components",
	"html",
	"w3c",
	"bissetii",
]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
	"form",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Form | Tel"
pre = "⌨"
weight = 1


[schema]
selectType = "WebPage"
+++

# `{{% param "title" %}}` Component
{{% param "description" %}}

This component is available starting from `v1.12.1`.




## Hugo
All `{{% param "title" %}}` Hugo interface is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Go
All `{{% param "title" %}}` Go interface is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## HTML
Expanding from  [Form Component]({{< link "/components/form/" "this"
"url-only" />}}), `{{% param title %}}` component is to facilitate an input
for entering a telephone number.

To ensure the accepted phone number is complying to your specific pattern, use a
regular expression pattern for `pattern=` attribute.

For accepting phone number with dashes (`-`), either plus `+` prefix or ITU
prefix (`00` or `01`) like `+6012345678912`, `006434774000`, `016434774000`, and
`+6012-345-678-912`, the pattern is as follows:

```html {linenos=table,hl_lines=[],linenostart=1}
(\+?)([0-9\-]+)
```

For accepting phone number witout the dash (`-`) instead, the pattern is as
follows:

```html {linenos=table,hl_lines=[],linenostart=1}
(\+?)([0-9]+)
```

For accepting alphanumeric numbers with the dash (`-`) such as `800-MDN-ROCKS`,
where there is 3 leading numbers, the pattern is as follows:

```html {linenos=table,hl_lines=[],linenostart=1}
(([A-z0-9]){3,3})-([A-z0-9\-]+)
```

For accepting alphanumeric numbers with the dash(`-`) and international
telephone prefix such as `+60-800-MDN-ROCKS`, the pattern is as follows:

```html {linenos=table,hl_lines=[],linenostart=1}
(\+?)([0-9\-]+?)-(([A-z0-9]){3,3})-([A-z0-9\-]+)
```

{{< note "info" "Note" >}}
To ensure sainty and uniformity for telephone number data, the pattern
`(\+?)([0-9]+)` is recommended as the backend server can remove the dashes
before storing it into the database.

In any cases, use `placeholder=` to provide an example for acceptable format.
{{< /note >}}

An all compatible attributes HTML syntax is shown as follows:

```html {linenos=table,hl_lines=[],linenostart=1}
<label for="input-javascript-id">Text</label>
<input type="tel"
	id="input-javascript-id"
	name="example"
	value="...YOUR TEXT VALUE..."
	pattern="...YOUR PATTERN..."
	minlength="8"
	maxlength="40"
	spellcheck
	placeholder="...YOUR PLACEHOLDER..."
	required
	disabled
	autofocus
	autocomplete
	readonly
	list="datalist-id"
/>
<datalist id="datalist-id">
	<option value="...OPTION 1 TEXT..." label="Label 1" />
	...
</datalist>
```

Here is an example:

{{< renderHTML html amp >}}
{{< form "" "post" "#" >}}
<fieldset class="border">
	<fieldset>
		<label for="form-input-tel-required">
			Required Phone Number (+XX-XXX-XXXXX...)
		</label>
		<input type="tel"
			id="form-input-tel-required"
			name="example-tel-required"
			pattern="(\+?)(([A-z0-9]){2,2})-(([A-z0-9]){3,3})-([A-z0-9\-]+)"
			placeholder="+60-123-45678"
			required
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-tel-required-filled">
			Required Phone Number (+XX-XXX-XXXXX)
		</label>
		<input type="tel"
			id="form-input-text-required-filled"
			name="example-text-required-filled"
			pattern="(\+?)(([0-9]){2,2})-(([0-9]){3,3})-(([A-z0-9]){4,5})"
			required
			value="+60-123-4567"
			placeholder="+60-123-4567"
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-tel-list">
			List Selector
		</label>
		<input type="tel"
			id="form-input-tel-list"
			name="example-tel-list"
			list="form-input-tel-datalist"
			placeholder="+60-123-45678"
			required
		/>
		<datalist id="form-input-tel-datalist">
			<option value="+60123456789" label="Work" />
			<option value="00123456789" label="Personal" />
			<option value="00123-MAN-ROCKS" label="Secretary" />
		</datalist>
	</fieldset>
	<fieldset>
		<label for="form-input-tel-readonly-filled">
			Normal Phone Number, Read Only, and Filled
		</label>
		<input type="tel"
			id="form-input-tel-readonly-filled"
			name="example-tel-readonly-filled"
			readonly
			value="+6012345678"
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-tel-disabled-filled">
			Normal Phone Number, Disabled, and Filled
		</label>
		<input type="tel"
			id="form-input-tel-disabled-filled"
			name="example-tel-disabled-filled"
			disabled
			value="+6012345678"
		/>
	</fieldset>
	<fieldset>
		<label for="form-input-text-disabled-required-filled">
			Normal Phone Number, Disabled, Required, and Filled
		</label>
		<input type="tel"
			id="form-input-tel-disabled-required-filled"
			name="example-tel-disabled-required-filled"
			disabled
			required
			value="+6012345678"
		/>
	</fieldset>
</fieldset>
{{< /form >}}
{{< /renderHTML >}}




## CSS
All `{{% param "title" %}}` CSS variables is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Javascript
All `{{% param "title" %}}` Javascript is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Sass
All `{{% param "title" %}}` Sass is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Researches
Here are the researches done to ensure `{{% param "title" %}}` component meets
the necessary quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `{{% param "title" %}}` component in Bissetii. If you need more
feature or need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
