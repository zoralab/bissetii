+++
date = "2020-08-02T12:02:10+08:00"
title = "Form"
description = """
`Form` component is the input gathering interface. Despite as simple as it
sounds, there are a lot of complicated guides and tutorials all over the
Internet. Bissetii did a number of researches on its own and decided to only
provide styling for proper HTML5 semantic syntax defined by w3.org.
"""
keywords = ["form", "components", "html", "w3c", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
	"form",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Form"
pre = "⌨"
weight = 1


[schema]
selectType = "WebPage"
+++

# `{{% param "title" %}}` Component
{{% param "description" %}}

This component is available starting from `v1.12.1`.




## Hugo
Bissetii prepared a number of Hugo interfaces to generate `Form` component
seamlessly across all output formats.



### Module Extension
To use `Form` component, you need to include `"form"` extension module into your
front-matter or the global `amp.toml` data file. The code pattern is as follows:

```toml {linenos=table,hl_lines=[5],linenostart=1}
+++
...
[modules]
extensions = [
	"form",
]
...
+++
```

Bissetii will automatically includes the extension module seamlessly in
accordance to the output format (e.g. `amp-form` extension module for AMPHTML).



### Shortcodes
By default, it is hard to process images across multiple outputs while
maintaining a single input format. Hence, Bissetii prepared the following
shortcodes to standardize the image rendering. These shortcodes are specific to
Bissetii which works differently from Hugo.


#### `form` Shortcode
The `form` shortcode is for creating form quickly in Hugo. Its parameters
definition varies depending on the Bissetii version used.

##### Version `v1.13.0` and Above
The shortcode pattern is as follows:

```md {linenos=table,hl_lines=[],linenostart=1}
{{</* form [METHOD] [ACTION] */>}}
	[CONTENT]
{{</* /form */>}}
```

1. `METHOD` - **COMPULSORY**. The from method (e.g. `POST`).
2. `ACTION` - **COMPULSORY**. The form submission action URL.
3. `CONTENT` - **COMPULSORY**. The form structure in HTML format.

Example usage with shortcode inside shortcode:

```md {linenos=table,hl_lines=[],linenostart=1}
{{</* form "POST" "https://www.example.com/api/query" */>}}
	<fieldset class="border">
		<legend>Login</legend>
		<fieldset>
			<label>Email</label>
			<input type="email" />
		</fieldset>
		<fieldset>
			<label>Password</label>
			<input type="password" />
		</fieldset>
	</fieldset>
{{</* /note */>}}
```

##### Version `v1.12.5` and Below
{{< note "warning" "Deprecation Notice" >}}
Support for version `v1.12.5` and below shall be deprecated and
**rescinded on July 31, 2022**. Please migrate within the timeframe.
{{< /note >}}

The shortcode pattern is as follows:

```md {linenos=table,hl_lines=[],linenostart=1}
{{</* form "" [METHOD] [ACTION] */>}}
	[CONTENT]
{{</* /form */>}}
```

1. Empty string - **COMPULSORY**. A no longer used parameter so supply empty
                  string to it.
2. `METHOD` - **COMPULSORY**. The from method (e.g. `POST`).
3. `ACTION` - **COMPULSORY**. The form submission action URL.
4. `CONTENT` - **COMPULSORY**. The form structure in HTML format.

Example usage with shortcode inside shortcode:

```md {linenos=table,hl_lines=[],linenostart=1}
{{</* form "" "POST" "https://www.example.com/api/query" */>}}
	<fieldset class="border">
		<legend>Login</legend>
		<fieldset>
			<label>Email</label>
			<input type="email" />
		</fieldset>
		<fieldset>
			<label>Password</label>
			<input type="password" />
		</fieldset>
	</fieldset>
{{</* /note */>}}
```




## Go
Coming Soon.




## HTML
Bissetii supports `Form` component via the use of `<form>`, `<fieldset>`, and
`<input>` native HTML tags. Additionally, starting from version `v1.13.0`, the
use of CSS variable is vital for its upgrade from `v1.12.5`.



### Output Format
`Form` component is slightly different depending on the HTML output formats.
Here are the Bissetii's supported formats.


#### Vanilla HTML5
Bissetii supports `Form` component natively and seamlessly. Here is an example:

```html {linenos=table,hl_lines=[],linenostart=1}
<form action="...URL..." method="POST">
	...
</form>
```

The recommended methods for a form are: `POST`, `PUT`, `PATCH`, and `DELETE`.
When in doubt, always use `POST`.

{{< note info "For Your Info" >}}
1. Use of `GET` method for a form is always frown upon because you can use anchor
link instead.
2. Also, `GET` method usually exposes the form data into the URL query string so
it is not feasible for sensitive data (e.g. password) submission.
{{< /note >}}


#### AMPHTML
For AMPHTML to use `Form` component, the AMP Form library is required and shall
be imported inside the `<head>` section as instructed by
[AMP Specification](https://amp.dev/documentation/components/amp-form/).

```html {linenos=table,hl_lines=["3-4"],linenostart=1}
<head>
	...
	<script async custom-element="amp-form"
		src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
	...
</head>
```

Some notable differences for AMPHTML would be the `POST` method. In AMPHTML,
the form uses its specialized `action-xhr=` instead of `action=` attribute.
Here is an example of the AMPHTML form:

```html {linenos=table,hl_lines=["1"],linenostart=1}
<form action-xhr="...URL..." method="POST">
	...
</form>
```

Another differences would be the prohibition of some `<input>` types such as
`button` and `image`. To learn the allowed `<input>` list, please refer to the
[AMP Specification](https://amp.dev/documentation/components/amp-form/).



### Validation Pattern
Both HTML5 and AMPHTML facilitates `pattern=` attribute for validating data in
any `<input>` (including `<textarea>`). This in turns will ensure the input data
from user via the form interface is at least matching the necessary requirement.
Example:

```html {linenos=table,hl_lines=["4"],linenostart=1}
<input type="password"
	id="form-pattern-password"
	name="password"
	pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\(!@#\$%\^&\*\)]).{33,}"
	placeholder="Your Secret Password"
/>
```

Bissetii styled every `<input>` and `<textarea>` to respond for all statuses
like `invalid`, `valid`, and `focus` for both optional and `required` fields.
Here is an example for password field that needs 33 characters minimum length,
minimum 1 captial character, 1 lowercase character, 1 number and 1 symbol. You
can try inserting the following values to see how the fields react to it:

| Result  | Value                                    |
|:--------|:-----------------------------------------|
| invalid | `Testing54645#@#$`                       |
| valid   | `Testing-Monoxide-G@nd@lf-Whatever-2122` |

{{< renderHTML html amp >}}
{{< form "" "post" "#" >}}
<fieldset class="border">
	<legend>Securing Your Account</legend>
	<fieldset>
		<legend>Optional Field</legend>
		<input type="password"
			id="form-pattern-password"
			name="password"
			pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\(!@#\$%\^&\*\)]).{33,}"
			placeholder="Your Secret Password"
		/>
	</fieldset>
	<fieldset>
		<legend>Required Field</legend>
		<input type="password"
			id="form-pattern-password-required"
			name="password"
			pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\(!@#\$%\^&\*\)]).{33,}"
			placeholder="Your Secret Password"
			required
		/>
	</fieldset>
</fieldset>
{{< /form >}}
{{< /renderHTML >}}

Bissetii will document all recommended validation patterns accordingly in the
each of the input types section.

{{< note "warning" "DO NOT FORGET" >}}
Although the browser provided validation pattern attribute to make data
submission from user seamlessly easier, you **STILL AND MUST** perform full
data sanitization and validation on your backend server side.
**This is NOT THE ONLY way** to submit data back to server so **DO NOT ASSUME**.
{{< /note >}}



### Use `<fieldset>` For Grouping
Instead of using custom `<div>` HTML tag for grouping purposes like
`<label>`+`<input>` or a set of them in 1 group, Bissetii uses the native
`<fieldset>` and `<legend>` to get the job done, even for the extreme case where
CSS is unavailable.

The use case is simple: there should only be 1 `<legend>` in each `<fieldset>`
and you can wrap as many `<fieldset>` as needed. Here is an example for login
form:

```html {linenos=table,hl_lines=[],linenostart=1}
<fieldset class="border" style="background: var(--color-primary-300);">
	<legend>Login</legend>

	<fieldset style="background: white;">
		<label><b>Email Account:</b></label>
		<input type="email"
			id="form-fieldset-example-email"
			name="email"
			pattern="^(?![_.-])((?![_.-][_.-])[a-z\d_.-]){0,63}[a-z\d]@((?!-)((?!--)[a-z\d-]){0,63}[a-z\d]\.){1,2}([a-z]{2,14}\.)?[a-z]{2,14}$"
			placeholder="john.smith@domain.com"
			required
		/>
	</fieldset>

	<fieldset style="background: white;">
		<label><b>Password:</b></label>
		<input type="password"
			id="form-fieldset-example-password"
			name="password"
			pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\(!@#\$%\^&\*\)]).{33,}"
			placeholder="Your Secret Password"
			required
		/>
	</fieldset>
</fieldset>
```

{{< note "info" "Note" >}}
Avoid using `<legend>` as a replacement to `<label>`. The `<label>` has a lot of
functionalities including adding responsiveness to its corresonding `<input>`
field.
{{< /note >}}

This will render as:

{{< renderHTML html amp >}}
{{< form "" "post" "#" >}}
<fieldset class="border" style="background: var(--color-primary-300);">
	<legend>Login</legend>

	<fieldset style="background: white;">
		<label><b>Email Account:</b></label>
		<input type="email"
			id="form-fieldset-example-email"
			name="email"
			pattern="^(?![_.-])((?![_.-][_.-])[a-z\d_.-]){0,63}[a-z\d]@((?!-)((?!--)[a-z\d-]){0,63}[a-z\d]\.){1,2}([a-z]{2,14}\.)?[a-z]{2,14}$"
			placeholder="john.smith@domain.com"
			required
		/>
	</fieldset>

	<fieldset style="background: white;">
		<label><b>Password:</b></label>
		<input type="password"
			id="form-fieldset-example-password"
			name="password"
			pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\(!@#\$%\^&\*\)]).{33,}"
			placeholder="Your Secret Password"
			required
		/>
	</fieldset>
</fieldset>
{{< /form >}}
{{< /renderHTML >}}


#### Show Border Using `.border` CSS Class
To quickly set a `<fieldset>` to show its original border, you just need to
append `.border` CSS class tag to the `<fieldset>`. Example:

```html {linenos=table,hl_lines=[],linenostart=1}
<fieldset class="border">
	...
</fieldset>
```


#### Align Everything to Center Using `.center` CSS Class
To quickly set a `<fieldset>` to set everything including its `<legend>` to
center, you just need to append `.center` CSS class tag to the `<fieldset>`.
Example:

```html {linenos=table,hl_lines=[],linenostart=1}
<fieldset class="center">
	...
</fieldset>
```



### Layout Using `Grid` Component
Bissetii recommends to use [`Grid` Component]({{< link "/components/grid/"
"this" "url-only" />}}) to organize the form and layout into a desired style
(e.g. inline fields). Here is an example for generating an inline login form:

```html {linenos=table,hl_lines=["4-5","7-8","16"],linenostart=1}
<fieldset class="border" style="background: var(--color-primary-300);">
	<legend>Login</legend>

	<fieldset class="row" style="background: white; --grid-column-base: 3;">
		<div class="column" style="width: 100px;">
			<label><b>Email Address:</b></label>
		</div>
		<div class="column" style="--grid-column-multiplier: 2">
			<input type="email"
				id="form-fieldset-example-grid-email"
				name="email"
				pattern="^(?![_.-])((?![_.-][_.-])[a-z\d_.-]){0,63}[a-z\d]@((?!-)((?!--)[a-z\d-]){0,63}[a-z\d]\.){1,2}([a-z]{2,14}\.)?[a-z]{2,14}$"
				placeholder="john.smith@domain.com"
				required
			/>
		</div>
	</fieldset>

	...
</fieldset>
```

This will render as:

{{< renderHTML html amp >}}
{{< form "" "post" "#" >}}
<fieldset class="border" style="background: var(--color-primary-300);">
	<legend>Login</legend>

	<fieldset class="row" style="background: white; --grid-column-base: 3;">
		<div class="column" style="width: 100px;">
			<label><b>Email:</b></label>
		</div>
		<div class="column" style="--grid-column-multiplier: 2">
			<input type="email"
				id="form-fieldset-example-grid-email"
				name="email"
				pattern="^(?![_.-])((?![_.-][_.-])[a-z\d_.-]){0,63}[a-z\d]@((?!-)((?!--)[a-z\d-]){0,63}[a-z\d]\.){1,2}([a-z]{2,14}\.)?[a-z]{2,14}$"
				placeholder="john.smith@domain.com"
				required
			/>
		</div>
	</fieldset>

	<fieldset class="row" style="background: white; --grid-column-base: 3;">
		<div class="column" style="width: 100px;">
			<label><b>Password:</b></label>
		</div>
		<div class="column" style="--grid-column-multiplier: 2">
			<input type="password"
				id="form-fieldset-example-grid-password"
				name="password"
				pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\(!@#\$%\^&\*\)]).{33,}"
				placeholder="Your Secret Password"
				required
			/>
		</div>
	</fieldset>
</fieldset>
{{< /form >}}
{{< /renderHTML >}}



### Use `<ol>` or `<ul>` for Listing
Bissetii prepared and styled all list (`<ol>` and `<ul>`) within the form to
behave as invisible non-bullet list. This facilitates listing multiple options
input like checkbox type and radio type. Here is an example of listing a
checkbox:

```html {linenos=table,hl_lines=[],linenostart=1}
<fieldset class="border">
	<legend>Starter Pack</legend>
	<ul>
		<li>
			<input type="checkbox"
				id="form-input-list-example-water"
				name="baggage"
				value="1.5 Litre Water Bottle"
				checked
			/>
			<label for="form-input-list-example-water">
				1.5 Litre Water Bottle
			</label>
		</li>
		<li>
			<input type="checkbox"
				id="form-input-list-example-lunch"
				name="baggage"
				value="Lunch Box"
			/>
			<label for="form-input-list-example-lunch">
				Lunchbox
			</label>
		</li>
	</ul>
</fieldset>
```

This will render as:

{{< renderHTML html amp >}}
{{< form "" "post" "#" >}}
<fieldset class="border">
	<legend>Starter Pack</legend>
	<ul>
		<li>
			<input type="checkbox"
				id="form-input-list-example-water"
				name="baggage"
				value="1.5 Litre Water Bottle"
				checked
			/>
			<label for="form-input-list-example-water">
				1.5 Litre Water Bottle
			</label>
		</li>
		<li>
			<input type="checkbox"
				id="form-input-list-example-lunch"
				name="baggage"
				value="Lunch Box"
			/>
			<label for="form-input-list-example-lunch">
				Lunchbox
			</label>
		</li>
	</ul>
</fieldset>
{{< /form >}}
{{< /renderHTML >}}


#### Horizontal Listing
To set the list scales horizontally, simply add `.inline` CSS class to the list
HTML tag. Example:

```html {linenos=table,hl_lines=[],linenostart=1}
<ul class="inline">
	...
</ul>
```

This will render as:

{{< renderHTML html amp >}}
{{< form "" "post" "#" >}}
<fieldset class="border">
	<legend>Starter Pack</legend>
	<ul class="inline">
		<li>
			<input type="checkbox"
				id="form-input-list-inline-example-water"
				name="baggage"
				value="1.5 Litre Water Bottle"
				checked
			/>
			<label for="form-input-list-inline-example-water">
				1.5 Litre Water Bottle
			</label>
		</li>
		<li>
			<input type="checkbox"
				id="form-input-list-inline-example-lunch"
				name="baggage"
				value="Lunch Box"
			/>
			<label for="form-input-list-inline-example-lunch">
				Lunchbox
			</label>
		</li>
	</ul>
</fieldset>
{{< /form >}}
{{< /renderHTML >}}



### Use `<datalist>` for Pre-defined Values
For some `<input>` types, it is possible to supply pre-defined values to make
data filling easier.

To do that, you need to define a `<datalist>` with specific `id=` attribute then
supply the `id` into the `<input>`'s `list=` attribute.

Only `text`, `search`, `url`, `tel`, `email`, `date`, `month`, `week`, `time`,
`datetime-local`, `number`, `range`, and `color` are allowed to use.

For each `<option>`, there are 2 key attributes to use `value=` and `label=`.
The `label=` may work for those that allows to display like date selection.

Here is an example:

```html {linenos=table,hl_lines=[8,"10-17"],linenostart=1}
<fieldset>
	<label for="form-input-color-list">
		List Selector
	</label>
	<input type="color"
		id="form-input-color-list"
		name="example-color"
		list="form-input-color-datalist"
	/>
	<datalist id="form-input-color-datalist">
		<option value="#515151" label="Some Name 1" />
		<option value="#005100" label="Some Name 2" />
		<option value="#000051" label="Some Name 3" />
		<option value="#FF0000" label="Some Name 4" />
		<option value="#00FF00" label="Some Name 5" />
		<option value="#0000FF" label="Some Name 6" />
	</datalist>
</fieldset>
```



### Use `minlength=` And `maxlength=` To Limit Text Characters
HTML5 supplies `minlength=` and `maxlength=` attributes for compatible `<input>`
to limit the acceptable values' length at the client side. The values shall be
a round number. Here is an example:

```html {linenos=table,hl_lines=["8-9"],linenostart=1}
<fieldset>
	<label for="form-input-password-pin-required">
		8-Digits Pin Number
	</label>
	<input type="password"
		id="form-input-password-pin-required"
		name="example-password-pin-required"
		minlength="8"
		maxlength="8"
		pattern="([0-9]){8,8}"
		required
	/>
</fieldset>
```

This will render as following (try input values less than or more than 8
digits):

{{< renderHTML html amp >}}
{{< form "" "post" "#" >}}
<fieldset class="border">
	<label for="form-input-password-pin-required">
		8-Digits Pin Number
	</label>
	<input type="password"
		id="form-input-password-pin-required"
		name="example-password-pin-required"
		minlength="8"
		maxlength="8"
		pattern="([0-9]){8,8}"
		required
	/>
</fieldset>
{{< /form >}}
{{< /renderHTML >}}



### Use `min=` And `max=` To Limit Acceptable Ranges
HTML5 supplies `min=` and `max=` attributes for compatible `<input>` to limit
the acceptable values at the client side. Their values are the same as `value=`
attribute. Here is an example:

```html {linenos=table,hl_lines=["7-8"],linenostart=1}
<fieldset>
	<label for="lucky-number">Lucky Number</label>
	<input type="number"
		id="lucky-number"
		name="lucky-number"
		required
		min="0"
		max="100"
	/>
</fieldset>
```

This will render as (try providing negative number or number beyond 100):

{{< renderHTML html amp >}}
{{< form "" "post" "#" >}}
<fieldset class="border">
	<label for="lucky-number">Lucky Number</label>
	<input type="number"
		id="lucky-number"
		name="lucky-number"
		required
		min="0"
		max="100"
	/>
</fieldset>
{{< /form >}}
{{< /renderHTML >}}



### Use `step=` To Set Format and Alter Incremental/Decremental Changes
For some `<input>` type, HTML5 supplies `step=` attribute to configure
incremental or decremental stepping values and defining the input format (e.g.
Number). Here is an example:

```html {linenos=table,hl_lines=["9"],linenostart=1}
<fieldset>
	<label for="stepper-number">Stepper Number (2 Decimal)</label>
	<input type="number"
		id="stepper-number"
		name="stepper-number"
		required
		min="0.00"
		max="100.00"
		step="0.05"
	/>
</fieldset>
```

This will render as:

{{< renderHTML html amp >}}
{{< form "" "post" "#" >}}
<fieldset class="border">
	<label for="stepper-number">Stepper Number (2 Decimal)</label>
	<input type="number"
		id="stepper-number"
		name="stepper-number"
		required
		min="0.00"
		max="100.00"
		step="0.05"
	/>
</fieldset>
{{< /form >}}
{{< /renderHTML >}}



### Use `.info` And `.Notice` To Provide Education Info
Bissetii prepared 2 CSS classes for `<label>` to educate users about a specific
`<input>`.

`.info` CSS class onto `<label>` generates a small label information board
designed for education. This is very useful for stating essential information
such as your password requirements. Example:

```html {linenos=table,hl_lines=["1-8"],linenostart=1}
<label for="..." class="info">
<b>RULE:</b> Password must at least has:
	I. <b>ONE</b> lowercase (a-z)
	II. <b>ONE</b> uppercase letter (A-Z or À-ž)
	III. <b>ONE</b> number (0-9)
	IV. <b>ONE</b> symbol (!@#$%^&*()[])
	V. <b>Minimum 8</b> characters
</label>
<input type="password" ... />
```

`.notice` CSS class onto `<label>` with and without `.info` CSS class will
highlight the content into high attention (e.g. colored red) notices. This is
useful for reporting error for the field. Example:

```html {linenos=table,hl_lines=["2"],linenostart=1}
<input type="password" ... />
<label class="notice">Disabled due to insufficient permission.</label>
```

It is advisable to place the `.info` before the `<input>` and `.notice` after
it. This simplifies the rendering process and keeping things simple. Combining
the above examples would be:

```html {linenos=table,hl_lines=["3-10","20"],linenostart=1}
<fieldset class="border">
	<label for="form-input-password-example-guided">Password</label>
	<label for="form-input-password-example-guided" class="info">
<b>RULE:</b> Password must at least has:
	I. <b>ONE</b> lowercase (a-z)
	II. <b>ONE</b> uppercase letter (A-Z or À-ž)
	III. <b>ONE</b> number (0-9)
	IV. <b>ONE</b> symbol (!@#$%^&*()[])
	V. <b>Minimum 8</b> characters
	</label>
	<input type="password"
		id="form-input-password-example-guided"
		name="example-password-example-guided"
		minlength="8"
		pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\(!@#\$%\^&\*\)]).{8,}"
		disabled
		required
		value="MotivÀtor123&^%"
	/>
	<label class="notice">Disabled due to insufficient permission.</label>
</fieldset>
```

This will render as:

{{< renderHTML html amp >}}
{{< form "" "post" "#" >}}
<fieldset class="border">
	<label for="form-input-password-example-guided">Password</label>
	<label for="form-input-password-example-guided" class="info">
<b>RULE:</b> Password must at least has:
	I. <b>ONE</b> lowercase (a-z)
	II. <b>ONE</b> uppercase letter (A-Z or À-ž)
	III. <b>ONE</b> number (0-9)
	IV. <b>ONE</b> symbol (!@#$%^&*()[])
	V. <b>Minimum 8</b> characters
	</label>
	<input type="password"
		id="form-input-password-example-guided"
		name="example-password-example-guided"
		minlength="8"
		pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\(!@#\$%\^&\*\)]).{8,}"
		disabled
		required
		value="MotivÀtor123&^%"
	/>
	<label class="notice">Disabled due to insufficient permission.</label>
</fieldset>
{{< /form >}}
{{< /renderHTML >}}



### Use `placeholder=` To Provide Example
HTML5 provides `placeholder=` field for most `<input>` fields to hint an example
to user.

{{< note "warning" "Remember" >}}
Using placeholder to provider education is frown upon. As soon as user starts
entering the value, the placeholder will be disappeared immediately. Use
`<label>` to educate user and keep `placeholder=` for displaying a desired
example.
{{< /note >}}

Here is an example:

```html {linenos=table,hl_lines=[17],linenostart=1}
<fieldset class="border">
	<label for="form-input-placeholder-text">Team Name</label>
	<label for="form-input-placeholder-text" class="info">
<b>RULE:</b> Name must have at least:
	I. <b>ONE</b> lowercase (a-z)
	II. <b>ONE</b> uppercase letter (A-Z or À-ž)
	III. <b>ONE</b> number (0-9)
	IV. <b>ONE</b> symbol (!@#$%^&*()[]-_ )
	V. <b>Minimum 8</b> characters
	</label>
	<input type="text"
		id="form-input-placeholder-text"
		name="example-placeholder-text"
		minlength="8"
		pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\(_-\ !@#\$%\^&\*\\)]).{8,}"
		required
		placeholder="Example: Golden Armada"
	/>
</fieldset>
```

This will render as:

{{< renderHTML html amp >}}
{{< form "" "post" "#" >}}
<fieldset class="border">
	<label for="form-input-placeholder-text">Team Name</label>
	<label for="form-input-placeholder-text" class="info">
<b>RULE:</b> Name must have at least:
	I. <b>ONE</b> lowercase (a-z)
	II. <b>ONE</b> uppercase letter (A-Z or À-ž)
	III. <b>ONE</b> number (0-9)
	IV. <b>ONE</b> symbol (!@#$%^&*()[]-_ )
	V. <b>Minimum 8</b> characters
	</label>
	<input type="text"
		id="form-input-placeholder-text"
		name="example-placeholder-text"
		minlength="8"
		pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\(_-\ !@#\$%\^&\*\\)]).{8,}"
		required
		placeholder="Example: Golden Armada"
	/>
</fieldset>
{{< /form >}}
{{< /renderHTML >}}




### Input Types
Bissetii supports a large number of input types made available via HTML5
seamlessly. Each type has its own validation pattern and Bissetii's recomemnded
HTML code structure.

To keep communications sharp and percise, All the sample codes here will only be
the `<input>` syntax without any fancy grouping or layout. Each input types are
located in the [Input]({{< link "/components/form/input/" "this"
"url-only" />}}) section. Below are the supported specific inputs:

{{< cards "bissetii.data.input.en" "grid" >}}




## CSS
Bissetii provided a list of CSS variables dedicated for `Form` styling
alteration without needing to recompile Sass. These variables are best applied
directly to the `<form>`, `<fieldset>`, or `<input>` HTML tags whenever
applicable. Example:

```html {linenos=table,hl_lines=["1-2"],linenostart=1}
<form ... style="--form-margin: 0;">
	<fieldset style="--fieldset-border-radius: .8rem;">
		...
	</fieldset>
</form>
```

As CSS variables affect the children of same kind, the use of inline styling is
also encouraged to specifically customize one particular element. Example: to
only affects the parent `<fieldset>` instead of the children, the following
example works:

```html {linenos=table,hl_lines=[2],linenostart=1}
<form ... style="--form-margin: 0;">
	<fieldset style="border-radius: .8rem;">
		<fieldset>
			...
		</fieldset>
		...
	</fieldset>
</form>
```




### `--form-margin`
Affects the margin spacing of the entire form. The acceptable value shall be
compatible with `margin:` CSS field. The default is: `2.5rem auto`.



### `--form-max-width`
Affects the maximum width of the entire form. The acceptable value shall be
compatible with `max-width:` CSS field. The default is: `100%`.



### `--form-list-margin`
Affects the margin spacing for all list (`<ol>` and `<ul>`) inside the form. The
acceptable value shall be compatible with `margin:` CSS field. The default is
`0 0 0 2rem`.



### `--form-list-style-type`
Affects the style type for all list (`<ol>` and `<ul>`) inside the form. The
acceptable value shall be compatible with `list-style-type:` CSS field. The
default is `none`.



### `--form-list-padding`
Affects the padding spacing for all list (`<ol>` and `<ul>`) inside the form.
The acceptable value shall be compatible with `padding:` CSS field. The default
is `0`.



### `--form-li-display`
Affects the display mode of the list item (`<li>`) inside the form. The
acceptable value shall be compatible with `display:` CSS field. The default is
`list-item`.



### `--form-li-margin`
Affects the margin spacing of the list item (`<li>`) inside the form. The
acceptable value shall be compatible with `margin:` CSS field. The default is
`1rem`.



### `--fieldset-display`
Affects the display mode of a `<fieldset>` and its children of same kind inside
the form. The acceptable value shall be compatible with `display:` CSS field.
The default is `block`.



### `--fieldset-margin`
Affects the margin spacing of a `<fieldset>` and its children of same kind
inside the form. The acceptable value shall be compatible with `margin:` CSS
field. The default are:

1. normal: `1.5rem 0`
2. `.center` CSS class object: `1.5rem auto`



### `--fieldset-padding`
Affects the padding spacing of a `<fieldset>` and its children of same kind
inside the form. The acceptable value shall be compatible with `padding:` CSS
field. The default is `1rem`.



### `--fieldset-border-radius`
Affects the edge roundness of a `<fieldset>` and its children of same kind
inside the form. The acceptable value shall be compatible with `border-radius:`
CSS field. The default is `.4rem`.



### `--fieldset-border`
Affects the border styling of a `<fieldset>` and its children of same kind
inside the form. The acceptable value shall be compatible with `border:` CSS
field. The default is `none`.



### `--fieldset-border-show`
Affects the border styling of a `<fieldset>` when `.border` CSS tag is applied.
The acceptable value shall be compatible with `border:` CSS field. The default
is `.1rem solid var(--color-primary-500)`.



### `--fieldset-text-align`
Affects the text alignment of a `<fieldset>` and its children of same kind
inside the form. The acceptable value shall be compatible with `text-align:` CSS
field. The default are:

1. normal: `left`
2. `.center` CSS class object: `center`



### `--legend-font-weight`
Affects the font weight styling of a `<legend>`. The acceptable value shall be
compatible with `font-weight:` CSS field. The default is: `bold`.



### `--legend-color`
Affects the color of a `<legend>`. The acceptable value shall be compatible
with `color:` CSS field. The default is: `var(--color-primary-500)`.



### `--input-font`
Affects the font of an `<input>`. The acceptable value shall be compatible
with `font:` CSS field. The default is `inherit`.



### `--input-font-size`
Affects the font size of an `<input>`. The acceptable value shall be compatible
with `font-size:` CSS field. The default are:

1. normal: `inherit`
2. small: `1.3rem`
3. smallest: `1rem`



### `--input-color`
Affects the text color of an `<input>`. The acceptable value shall be compatible
with `color:` CSS field. The default is `var(--color-grey-900)`.



### `--input-background`
Affects the background styling of an `<input>`. The acceptable value shall be
compatible with `background:` CSS field. The default is `transparent`.



### `--input-background-image`
Affects the background image styling of an `<input>`. The acceptable value shall
be compatible with `background-image:` CSS field. The default is `transparent`.



### `--input-border-radius`
Affects the edge roundness of an `<input>`. The acceptable value shall be
compatible with `border-radius:` CSS field. The default is `.4rem`



### `--input-border-color`
Affects the border color of an `<input>`. The acceptable value shall be
compatible with `border:` CSS field. The default is `var(--color-grey-400)`.



### `--input-border`
Affects the border styling of an `<input>`. The acceptable value shall be
compatible with `border:` CSS field. The default is
`.1rem solid var(--color-grey-700)`.



### `--input-border-left-width`
Affects the border thickness on the left side of an `<input>`. The acceptable
value shall be compatible with `border-left-width:` CSS field. The default is
`.4rem`.



### `--input-outline`
Affects the outline styling of an `<input>`. The acceptable value shall be
compatible with `outline:` CSS field. The default is `0`.



### `--input-box-shadow`
Affects the box shadow styling of an `<input>`. The acceptable value shall be
compatible with `box-shadow:` CSS field. The default is `none`.



### `--input-box-sizing`
Affects the box shadow sizing of an `<input>`. The acceptable value shall be
compatible with `box-sizing:` CSS field. The default is `border-box`.



### `--input-width`
Affects the width of an `<input>`. The acceptable value shall be compatible
with `width:` CSS field. The default is `100%`.



### `--input-height`
Affects the height of an `<input>`. The acceptable value shall be compatible
with `height:` CSS field. The default is `3.8rem`.



### `--input-min-height`
Affects the minimum height of an `<input>`. The acceptable value shall be
compatible with `min-height:` CSS field. The default are:

1. normal: `unset`
2. `<textarea>`: `15rem`



### `--input-padding`
Affects the padding spacing of an `<input>`. The acceptable value shall be
compatible with `padding:` CSS field. The default is `.6rem 1rem`.



### `--input-margin`
Affects the margin spacing of an `<input>`. The acceptable value shall be
compatible with `margin:` CSS field. The default is `0`.



### `--input-appearance`
Affects the appearance styling of an `<input>`. The acceptable value shall be
compatible with `appearance:` CSS field. The default is `none`.



### `--input-transition`
Affects the transition animation timing of an `<input>`. The acceptable value
shall be compatible with `transition:` CSS field. The default is
`var(--animation-timing-fast)`.



### `--placeholder-font-style`
Affects the font styling of the placeholder inside an `<input>`. The acceptable
value shall be compatible with `font-style:` CSS field. The default is
`italic`.



### `--placeholder-color`
Affects the text color of the placeholder inside an `<input>`. The acceptable
value shall be compatible with `color:` CSS field. The default is
`var(--color-grey-500)`.



### `--label-font-size`
Affects the font size of a `<label>`. The acceptable value shall be compatible
with `font-size:` CSS field. The default are:

1. normal: `inherit`
2. `.notice` and `.info` CSS class tag: `1.4rem`



### `--label-color`
Affects the color of a `<label>`. The acceptable value shall be compatible
with `color:` CSS field. The default is `inherit`.

1. normal: `inherit`
2. `.notice` CSS class tag: `var(--color-red-500)`
3. invalid radio or checkbox: `var(--color-red-500)`
4. disabled radio or checkbox: `var(--color-grey-400)`



### `--label-width`
Affects the width of a `<label>`. The acceptable value shall be compatible
with `width:` CSS field. The default is `inherit`.



### `--label-white-space`
Affects the whitespace styling of a `<label>`. The acceptable value shall be
compatible with `white-space:` CSS field. The default are:

1. normal: `normal`
2. `.notice` and `.info` CSS class tag: `pre`



### `--label-vertical-align`
Affects the text vertical alignment of a `<label>`. The acceptable value shall
be compatible with `vertical-align:` CSS field. The default is `middle`.



### `--optgroup-margin`
Affects the margin spacing for `<optgroup>` inside `<select>`. The acceptable
value shall be compatible with `margin:` CSS field. The default is `2rem 0 0`.



### `--option-padding`
Affects the margin spacing for `<option>` inside `<select>`. The acceptable
value shall be compatible with `padding:` CSS field. The default is `1.2rem 0`.




## Javascript
This component does not rely on any Javascript.




## Sass
Depending on release version, the Sass files work differently. Bissetii does not
package Sass codes explictly so please view them via the git repository.



### `v1.13.0` and Above
Bissetii uses Dart Sass to compile the styling Sass codes into CSS file. This
component's Sass codes are available at the following location:

```
pkg/components/internal/form
```



### `v1.12.5` and Before
{{< note "warning" "Deprecation Notice" >}}
Starting from version `v1.13.0`, this version of Sass is no longer used and the
support is scheduled to be removed on **July 31, 2022**.
{{< /note >}}
The Sass scripts responsible for styling the component are located in:

```
assets/css/bissetii/modules/core/_Form.scss
```




## Researches
Here are the researches done to ensure `Form` component meets the necessary
quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `Form` component in Bissetii. If you need more feature or
need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
