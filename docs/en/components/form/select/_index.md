+++
date = "2021-07-11T12:49:43+08:00"
title = "Select"
description = """
`Select` component is an input field for selecting single or multiple choices
from user of a list. Bissetii did a number of researches on its own and decided
to only provide styling for proper HTML5 semantic syntax defined by w3.org.
"""
keywords = ["form",
	"input",
	"select",
	"components",
	"html",
	"w3c",
	"bissetii",
]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
	"form",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Form | Select"
pre = "⌨"
weight = 1


[schema]
selectType = "WebPage"
+++

# `{{% param "title" %}}` Component
{{% param "description" %}}

This component is available starting from `v1.12.1`.




## Hugo
All `{{% param "title" %}}` Hugo interface is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Go
All `{{% param "title" %}}` Go interface is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## HTML
Expanding from  [Form Component]({{< link "/components/form/" "this"
"url-only" />}}), `{{% param title %}}` component is to facilitate an input
for entering a longer and larger text.

An all compatible attributes HTML syntax is shown as follows:

```html {linenos=table,hl_lines=[],linenostart=1}
<label for="input-javascript-id">Text</label>
<select
	id="input-javascript-id"
	name="example"
	autocomplete
	multiple
	required
	disabled
	autofocus
	size="10"
>
	<optgroup label="Group Name">
		<option value="value1">Option 1</option>
		<option value="value2" selected>Option 2</option>
	</optgroup>
</select>
```

Here is an example:

{{< renderHTML html amp >}}
{{< form "" "post" "#" >}}
<fieldset class="border">
	<fieldset>
		<label for="form-select-required">
			Required
		</label>
		<select id="form-select-required"
			name="form-select-required"
			required
		>
			<option value="dog">Dog</option>
			<option value="cat">Cat</option>
			<option value="bear">Bear</option>
			<option value="guppy">Guppy</option>
			<option value="ghost-shrimp">Ghost Shrimp</option>
			<option value="angel-fish">Angel Fish</option>
		</select>
	</fieldset>
	<fieldset>
		<label for="form-select-required-selected">
			Required and Selected
		</label>
		<select id="form-select-required-selected"
			name="form-select-required-selected"
			required
		>
			<optgroup label="4-Legged">
				<option value="dog">Dog</option>
				<option value="cat">Cat</option>
				<option value="bear" selected>Bear</option>
			</optgroup>
			<optgroup label="Aquatic">
				<option value="guppy">Guppy</option>
				<option value="ghost-shrimp">Ghost Shrimp</option>
				<option value="angel-fish">Angel Fish</option>
			</optgroup>
		</select>
	</fieldset>
	<fieldset>
		<label for="form-select-multiple-required">
			Multiple and Required
		</label>
		<select id="form-select-multiple-required"
			name="form-select-multiple-required"
			multiple
			required>
			<option value="dog">Dog</option>
			<option value="cat">Cat</option>
			<option value="bear">Bear</option>
			<option value="guppy">Guppy</option>
			<option value="ghost-shrimp">Ghost Shrimp</option>
			<option value="angel-fish">Angel Fish</option>
		</select>
	</fieldset>
	<fieldset>
		<label for="form-select-multiple-required-selected">
			Multiple Required, and Selected
		</label>
		<select id="form-select-multiple-required-selected"
			name="form-select-multiple-required-selected"
			multiple
			required
			size="8"
		>
			<optgroup label="4-Legged">
				<option value="dog" selected>Dog</option>
				<option value="cat">Cat</option>
				<option value="bear" selected>Bear</option>
			</optgroup>
			<optgroup label="Aquatic">
				<option value="guppy">Guppy</option>
				<option value="ghost-shrimp">Ghost Shrimp</option>
				<option value="angel-fish" selected>Angel Fish</option>
			</optgroup>
		</select>
	</fieldset>
</fieldset>
{{< /form >}}
{{< /renderHTML >}}




## CSS
All `{{% param "title" %}}` CSS variables is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Javascript
All `{{% param "title" %}}` Javascript is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Sass
All `{{% param "title" %}}` Sass is directly supported by
[Form Component]({{< link "/components/form/" "this" "url-only" />}}).




## Researches
Here are the researches done to ensure `{{% param "title" %}}` component meets
the necessary quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `{{% param "title" %}}` component in Bissetii. If you need more
feature or need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
