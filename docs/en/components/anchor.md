+++
date = "2020-02-10T09:23:11+08:00"
title = "Anchor Component"
description = """
`Anchor` component is the HTML hyperlinks for directing user to from the current
webpage to another. This component is the currency for seach engine optimization
to have inference control. Bissetii styles this component seemlessly.
"""
keywords = ["link", "components", "html", "w3c", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Anchor 'Link'"
pre = "🔗"
weight = 1


[schema]
selectType = "WebPage"
+++

# `Anchor` Component
{{% param "description" %}}

This component is available starting from `v1.11.0`.




## Hugo
Bissetii supports Hugo interfaces to make content creation easier depending on
Bissetii version.



### Shortcodes
Bissetii prepared a list of shortcodes to make website creation a lot easier.
Here are some of the available shortcodes related to `Anchor` component.


#### `link` Shortcode
By default, Hugo is not able to process irregular `BaseURL` such as
`https://zoralab.gitlab.io/bissetii`. In fact, it only recognizes
`https://zoralab.gitlab.io` its base URL. This is not feasible for websites
hosted on Git version control platforms
such as [Github Pages](https://pages.github.com/) and
[GitLab Pages](https://about.gitlab.com/stages-devops-lifecycle/pages/) as
project-level pages. Therefore, the need for an independent `link` shortcode is
required.

Also, when using `link` shortcode in multiple outputs with non-base relative
URL (e.g. `some/path`), it will process and point the link towards its
designated output (e.g. AMP will have `index.amp.html` in the end if no file is
specified).

##### Version `v1.13.0` and Above
{{< note "warning" "NON-BACKWARD COMPATIBILITY NOTICE" >}}
Starting from version `v1.13.0`, this shortcode uses many Hugo's
[urls.Parse function](https://gohugo.io/functions/urls.parse/), which in turns
using [Go's `net/url` Package](https://godoc.org/net/url#URL) to process all the
URLs. The reason is to ensure this shortcode is
[RFC3986](https://tools.ietf.org/html/rfc3986#section-4.1) compliant.

While the parameters are not affected, **the data for them are**. Please ensure
your full documentations migrations before commiting!

Another notable security changes is that
**one will no longer be able to supply user info embedded URL** such as:
`https://username:password@www.example.com`. Doing so will raise as fatal error
instead.

The reason is to ban embedded URL in this shortcode mainly because Hugo is a
static site generator and is not a web application. Hence, there is no reason
for anyone to hard-code sensitive information like password into your web
artifact.
{{< /note >}}

The same `link` shortcode has 2 formats shown as follow:

1. Full Hyperlink With Wrapper:

```markdown
{{</* link "[URL]" "[LANG]"
	"[REL]"
	"[TARGET]"
	"[CLASS]"
	"[ID]"
	"[DONWLOAD]"
	"[HREFLANG]" */>}}
[CONTENT]
{{</* /link */>}}
```

2. For Processing URL Only:
```markdown
{{</* link "[URL]" "[LANG]" "url-only" /*/>}}
```

1. `URL` - **COMPULSORY**. You must supply a URL to the shortcode. The URL can
           be absolute or relative URL. The shortcode will always construct the
           URL to absolute URL.
2. `LANG` - **COMPULSORY**. The language code used when the shortcode
            re-construct relative URL into absolute URL. You can also point the
            same content URL from a foreign language into current page. A
            special value called `this` will set it to the current language
            page.
3. `REL` - **Optional**. The hyperlink relationship used for SEO purpose.
           The value can be `nofollow`. Special value `url-only` is available
           for obtaining only the re-constructed absolute URL from the shortcode
           instead. When `url-only` is used, all the following arguments shall
           be ignored.
4. `TARGET` - **Optional**. The link target action. The value can be `_blank` to
           open the hyperlink in a new window.
5. `CLASS` - **Optional**. The CSS class tag for the hyperlink.
6. `ID` - **Optional**. The HTML ID attribute for the hyperlink.
7. `DOWNLOAD` - **Optional**. Add `download` HTML attribute to the hyperlink.
                Provider `download` as value will enable it.
8. `HERFLANG` - **Optional**. Overwrites the `herflang=` HTML attribute
                for the hyperlink.
9. `CONTENT` - **Optional**. The visible link content. Can be both Markdown and
               HTML.

Example usage:

1. Full Hyperlink With Wrapper (shortcode within shortcode is allowed):
```Markdown

{{%/* link "/license/bissetii-license.pdf" "zh-hans" "canonical" "nofollow" "css-class" "my-id" "download" */%}}
{{%/* image "Demo Image"
	"/img/thumbnails/default-1200x1200.png"
	"2500"
	"1250"
	"false"
	"lazy"
	""
	"responsive"
	"dark-color" */%}}
{{%/* /link */%}}

		⥥

{{% link "/license/bissetii-license.pdf" "zh-hans" "canonical" "nofollow" "css-class" "my-id" "download" %}}
{{% image "Demo Image"
	"/img/thumbnails/default-1200x1200.png"
	"2500"
	"1250"
	"false"
	"lazy"
	""
	"responsive"
	"dark-color" %}}
{{% /link %}}
```

2. Unused Arguments (provide empty string to optional argument for positioning):
```Markdown
{{%/* link "/license/bissetii-license.pdf" "" "" "" "" "" "download" */%}}
	<pre><code>Text Here</code></p>
{{%/* /link */%}}

		⥥

{{% link "/license/bissetii-license.pdf" "" "" "" "" "" "download" %}}
	Text Here
{{% /link %}}
```

3. For Processing URL:
```Markdown
# relative URL to BaseURL against `zh-hans` langauge
{{</* link "/img/" "zh-hans" "url-only" /*/>}}
		⥥
{{< link "/img/" "zh-hans" "url-only" />}}

# relative URL to current URL against `zh-hans` language
{{</* link "img/" "zh-hans" "url-only" /*/>}}
		⥥
{{< link "img/" "zh-hans" "url-only" />}}

# relative URL to BaseURL against current langauge
{{</* link "/img/" "this" "url-only" /*/>}}
		⥥
{{< link "/img/" "this" "url-only" />}}

# relative URL to BaseURL
{{</* link "/img/" "" "url-only" /*/>}}
		⥥
{{< link "/img/" "" "url-only" />}}
```


##### Version `v1.12.5` and Below
{{< note "warning" "Deprecation Notice" >}}
Starting from version `v1.13.0`, the support for older `link` shortcode
mechanism shall be deprecated  in favor of RFC3986 compliance version.

Support for version `v1.12.5` and below shall be **rescinded on July 31, 2022**.
{{< /note >}}

The older version is primitive compared to `v1.13.0`. Also, it is not RFC3986
compliant. The `link` shortcode has multiple formats as follow:

1. Full Hyperlink With Wrapper:

```markdown
{{</* link "[URL]" "[LANG]" "[REL]" */>}}
[CONTENT]
{{</* link "close" */>}}
```

2. For Processing URL Only:
```markdown
{{</* link "[URL]" "[LANG]" "url-only" */>}}
```

1. `URL` - **COMPULSORY**. You must supply a URL to the shortcode. The URL can
           be absolute or relative URL. The shortcode will always construct the
           URL to absolute URL. If special value `close` is provided, it will
           generate the closing hyperlink HTML tag (`</a>`).
2. `LANG` - **COMPULSORY**. The language code used when the shortcode
            re-construct relative URL into absolute URL. You can also point the
            same content URL from a foreign language into current page. A
            special value called `this` will set it to the current language
            page.
3. `REL` - **Optional**. The hyperlink relationship used for SEO purpose.
           The value can be `nofollow`. Special value `url-only` is available
           for obtaining only the re-constructed absolute URL from the shortcode
           instead. When `url-only` is used, all the following arguments shall
           be ignored.






## Go
Coming Soon.




## HTML
Bissetii first deisgned its HTML part using HTML5 and then later support
AMPHTML. For this component, it offers both link (`<link>`) and anchor hyperlink
(`<a>`).



### Link (`<link>`)
Bissetii supports all link HTML tag seamlessly. Some basic example can be:

```html
<link rel="canonical" href="https://www.example.com/">

<link rel="apple-touch-icon" sizes="60x60" href="https:// ... /apple-60x60.png">

<link rel="manifest" href="https:// ... /manifest.json">

<link href="https:// ... /bissetii-desktop.min.css"
	rel="stylesheet"
	media="screen and (min-width: 80rem)"
	onload="this.media=&quot;screen and (min-width: 80rem)&quot;"
	integrity="sha512-aQrWSv77nTWlWqKj/pOotQA7/TBIO+pT0xzN0x5ZvwT2d+JYJn1ZcfmKkdMWOKOg9CGwkSW4565Ad+v4WzkLMw=="
	crossorigin="anonymous">
```



### Anchor Hyperlink (`<a>`)
Bissetii supports all native anchor hyperlink seamlessly. All the following
shall be rendered accordingly as such:

| HTML | Render As |
|:-----|:----------|
| `<a href="#">Basic Link</a>` | {{< renderHTML "html" "amp" >}}
<a href="#">Basic Link</a>
{{< /renderHTML >}} |
| `<b><a href="#">Bolded Link</a></b>` | {{< renderHTML "html" "amp" >}}
<b><a href="#">Bolded Link</a></b>
{{< /renderHTML >}} |
| `<i><a href="#">Italic Link</a></i>` | {{< renderHTML "html" "amp" >}}
<i><a href="#">Italic Link</a></i>
{{< /renderHTML >}} |
| `<u><a href="#">Underline Link</a></u>` | {{< renderHTML "html" "amp" >}}
<u><a href="#">Underline Link</a></u>
{{< /renderHTML >}} |
| `<u><a style="--anchor-border-bottom: none;" href="#">Underline Link</a></u>` | {{< renderHTML "html" "amp" >}}
<u><a style="--anchor-border-bottom: none;" href="#">Underline Link</a></u>
{{< /renderHTML >}} |
| `<a class="wrap" href="#">Clean Link</a>` | {{< renderHTML "html" "amp" >}}
<a class="wrap" href="#">Clean Link</a>
{{< /renderHTML >}} |
| `<a style="--anchor-border-bottom: none;" href="#">Clean Link</a>` | {{< renderHTML "html" "amp" >}}
<a style="--anchor-border-bottom: none;" href="#">Clean Link</a>
{{< /renderHTML >}} |

{{< note "info" "Note" >}}
Bissetii uses `border-bottom:` to make anchor hyperlink stands out. Hence, the
underline behavior is expected unless the `--anchor-border-bottom:` is set to
`none` explicitly.
{{< /note >}}




## CSS
This section covers all the CSS part for `Anchor` component. Please go through
each sub-section carefully as they affects Bissetii styling directly.

{{< note "info" "Supported Version" >}}
This CSS section is **ONLY AVAILABLE** starting from version `v1.13.0` and
above.
{{< /note >}}



### `--anchor-color`
Affects the anchor link's color. The value msut be compliant to `color:` CSS
field. Default is `var(--color-primary-700)`.



### `--anchor-focus-color`
Affects the anchor link's color when it is being focused. The value must be
compliant to `color:` CSS field. Default is `var(--color-primary-500)`.



### `--anchor-hover-color`
Affects the anchor link's color when it is being hovered by cursor. The value
must be compliant to `color:` CSS field. Default is `var(--color-primary-500)`.



### `--anchor-border-bottom`
Affects the anchor link's border bottom. The value must be compliant to
`border-bottom:` CSS field. Default is `2px solid var(--color-primary-500)`.



### `--anchor-text-decoration`
Affects the anchor link's font styling decoration. The value must be compliant
to `text-decoration:` CSS field. Default is `none`.



### `--anchor-overflow-wrap`
Affects the anchor link's overflow wrapping behavior. The value must be
compliant to `overflow-wrap:` CSS field. DEfault is: `break-word`.



### `--anchor-word-break`
Affects the anchor link's word breaking behavior. The value must be compliant
to `word-break` CSS field. Default is: `break-word`.



### `--anchor-print-content`
Affects the anchor link's print formatting behavior. The value must be complaint
to `content:` CSS field. Default is: `" (" attr(href) ")"`.




## Javascript
This component does not depend on any Javascript.




## Sass
Depending on release version, the Sass files work differently. Bissetii does not
package Sass codes explictly so please view them via the git repository.



### `v1.13.0` and above
Bissetii uses Dart Sass to compile the styling Sass codes into CSS file. This
component's Sass codes is available at the following location:

```
pkg/components/internal/anchor
```



### `v1.12.5` and before
{{< note "warning" "Deprecation Notice" >}}
Starting from version `v1.13.0`, this version of Sass is no longer used and the
support is scheduled to be removed on **July 31, 2022**.
{{< /note >}}

The Sass scripts responsible for styling link are located at:

```bash
assets/css/bissetii/modules/core/_Link.scss
assets/css/bissetii/modules/core/_Button.scss
```

`_Link.scss` is reponsible for basic link styling like the primary colored
underline.

`_Button.scss` is responsible for styling link into a button-like object
instead of the conventional link. See
{{< link "/components/button/" "this" >}}
Button Component
{{< /link >}}.




## Epilogue
That is all about `Anchor` "link" component in Bissetii. If you need more
feature or encountered a bug. Please feel free to contact us by filling an issue
ticket at our [Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
We will be glad to help you.
