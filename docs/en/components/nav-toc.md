+++
date = "2020-12-03T17:10:11+08:00"
title = "Nav-TOC Component"
description = """
`Nav-TOC` component (Table of Contents Navigation) is the list of links
presentations in vertical manner. It is a kind of navigation layout commonly
used to list page headings for quick references and jump.
"""
keywords = ["nav-toc",
	"nav",
	"toc",
	"table of contents",
	"components",
	"html",
	"w3c",
	"bissetii"
]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Nav - TOC (BETA)"
pre = "🧭"
weight = 1


[schema]
selectType = "WebPage"
+++

# `Nav-TOC` Component
{{% param "description" %}}

This component is available starting from `v1.13.0`.

{{< note "warning" "Beta Testing Notice" >}}
This is a fairly new component and it is still under user-level API beta
testing. Hence, there may be some major updates in the future that are
**not backward compatible**. Please **ONLY use at your own risk**.
{{< /note >}}




## Hugo
Coming soon.




## Go
Coming Soon.




## HTML
Bissetii supports `Nav-TOC` component seamlessly across multiple output formats.
Additionally, starting from version `v1.13.0`, the use of CSS variable is vital
for its upgrade from `v1.12.5`.

Bissetii HTML codes for this component are the same for Vanilla HTML5 and
AMPHTML.

Bissetii facilitates `Nav-TOC` using the CSS class tag `<nav class="nav-toc">`
and only uses ordered list (`<ol>`) to list its items. The HTML pattern would
be:

```html {linenos=table,hl_lines=[],linenostart=1}
<nav class="nav-toc">
	<p class="title">Contents</p>
	<ol>
		<li><a href="...">Subject Heading 1</a></li>
		<li><a href="...">Subject Heading 2</a></li>
		<li><a href="...">Subject Heading 3</a></li>
		<li>
			<a href="...">Subject Heading 4</a>
			<ol>
				<li><a href="...">Subject Heading 4.1</a></li>
				<li><a href="...">Subject Heading 4.2</a></li>
				<li><a href="...">Subject Heading 4.3</a></li>
				...
			</ol>
		</li>
	</ol>
</nav>
```

This will render as:

{{< renderHTML "html" "amp" >}}
<nav class="nav-toc">
	<p class="title">Contents</p>
	<ol>
		<li><a href="#">Subject Heading 1</a></li>
		<li><a href="#">Subject Heading 2</a></li>
		<li><a href="#">Subject Heading 3</a></li>
		<li>
			<p>Subject Heading 4</p>
			<ol>
				<li><a href="#">Subject Heading 4.1</a></li>
				<li><a href="#">Subject Heading 4.2</a></li>
				<li><a href="#">Subject Heading 4.3</a></li>
			</ol>
		</li>
	</ol>
</nav>
{{< /renderHTML >}}




## CSS
Bissetii provided a list of CSS variables dedicated for `Nav-TOC` styling
alteration without needing to recompile Sass. These variables are best applied
directly to the `<nav>` HTML tags. Example:

```html
<nav class="nav-toc" style="--dl-padding: 25px;">
	...
</nav>
```



### `--nav-toc-width`
Affects the width of the overall `Nav-TOC`. The acceptable value shall be
compatible with `width:` CSS field. The default is `fit-content`.



### `--nav-toc-padding`
Affects the padding spacing of the overall `Nav-TOC`. The acceptable value shall
be compatible with `padding:` CSS field. The default is `.5rem`.



### `--nav-toc-border`
Affects the border styling of the overall `Nav-TOC`. The acceptable value shall
be compatible with `border:` CSS field. The default is
`.2rem solid var(--color-primary-300)`.



### `--nav-toc-border-radius`
Affects the edge roundness of the overall `Nav-TOC`. The acceptable value shall
be compatible with `border-radius:` CSS field. The default is `.4rem`.



### `--nav-toc-background`
Affects the background styling of the overall `Nav-TOC`. The acceptable value
shall be compatible with `background:` CSS field. The default is
`var(--color-primary-300)`.



### `--nav-toc-title-margin`
Affects the margin spacing of the `.title` CSS class object inside `Nav-TOC`.
The acceptable value shall be compatible with `margin:` CSS field. The default
is `.5rem 0 0`.



### `--nav-toc-title-font-size`
Affects the font size of the `.title` CSS class object inside `Nav-TOC`. The
acceptable value shall be compatible with `font-size:` CSS field. The default is
`1.6rem`.



### `--nav-toc-title-text-align`
Affects the text alignment of the `.title` CSS class object inside `Nav-TOC`.
The acceptable value shall be compatible with `text-align:` CSS field. The
default is `center`.



### `--nav-toc-title-font-weight`
Affects the font weight of the `.title` CSS class object inside `Nav-TOC`. The
acceptable value shall be compatible with `font-weight:` CSS field. The default
is `bold`.



### `--nav-toc-list-margin`
Affects the margin spacing for the ordered list `<ol>` inside `Nav-TOC`. The
acceptable value shall be compatible with `margin:` CSS field. The default is
`1rem`.



### `--nav-toc-list-padding`
Affects the padding spacing for the ordered list `<ol>` inside `Nav-TOC`. The
acceptable value shall be compatible with `padding:` CSS field. The default is
`0`.



### `--nav-toc-li-margin`
Affects the padding spacing for the list item `<li>` inside `Nav-TOC`. The
acceptable value shall be compatible with `margin:` CSS field. The default is
`.8rem`.




## Javascript
This component does not rely on any Javascript.




## Sass
Depending on release version, the Sass files work differently. Bissetii does not
package Sass codes explictly so please view them via the git repository.



### `v1.13.0` and Above
Bissetii uses Dart Sass to compile the styling Sass codes into CSS file. This
component's Sass codes are available at the following location:

```
pkg/components/internal/nav-toc
```




## Researches
Here are the researches done to ensure `Nav-TOC` component meets the necessary
quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `Nav-TOC` component in Bissetii. If you need more feature or
need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
