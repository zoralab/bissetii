+++
date = "2021-08-01T19:58:29+08:00"
title = "Nav-Catalog Component"
description = """
`nav-catalog` is the component that processes a list of page data and render
them into a shopping catalog style navigation section. This is commonly used
for index page where multiple child pages is available for reader to navigate
into.
"""
keywords = ["nav-catalog", "components", "html", "w3c", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Nav - Catalog (BETA)"
pre = "🧭"
weight = 1


[schema]
selectType = "WebPage"
+++

# `nav-catalog` Component
{{% param "description" %}}

This component is available starting from `v1.13.0`.

{{< note "warning" "Beta Testing Notice" >}}
This is a fairly new component and it is still under user-level API beta
testing. Hence, there may be some major updates in the future that are
**not backward compatible**. Please **ONLY use at your own risk**.
{{< /note >}}




## Hugo
Bissetii prepared a number of Hugo interfaces to generate `nav-catalog`
component seamlessly across all output formats for all index pages (e.g.
folders containing the sub-pages). They are configurable.



### Render Selection
To select the index pages to render with `nav-catalog` instead of other options,
change the following settings to select `nav-catalog` as its option:

```toml {linenos=table,hl_lines=[2],linenostart=1}
[List]
Type = "nav-catalog"
```



### Configuration Datafile
To configure `nav-catalog` for all index pages, the configurations are located
in `bissetii/nav.toml` inside Hugo's `data/` directory. The table you're looking
for would always be `[Catalog]`. There are a number of sections exist to ensure
the `nav-catalog` is functioning properly.


#### General Configurations
The general configurations for `nav-catalog` would be as follows:

```toml {linenos=table,hl_lines=[],linenostart=1}
[Catalog]
Class = "my-css-class"
ID = "my-catalog-01"
Style = """
--nav-catalog-margin: 10rem;
"""
```

* **[Catalog]**
	* Denotes the following fields are owned by `Catalog` table.
* **Class**
	* The `nav-catalog`'s `class=` attribute.
* **ID**
	* The `nav-catalog`'s `id=` attribute.
* **Style**
	* The `nav-catalog`'s `style=` attribute.



### Card General Configurations
These are general configurations specific to the [Card Component]({{< link
"/components/card/" "this" "url-only" />}}) used for categorizing each content.
Bissetii uses summary method (title, modified date, and short description) and
its page link (absolute permalink) to create the content of the card for each
child page.

```toml {linenos=table,hl_lines=[],linenostart=7}
[Catalog.Card]
Header = 3
Type = "vertical"
Class = ""
Style = ""
Width = "300px"
Height = "750px"
```

* **[Catalog.Card]**
	* Denotes the following fields are owned by `Catalog.Card` table.
* **Header**
	* Denotes the level of heading (default is `3`). If bad or missing
	  value is given, `nav-catalog` will raise an error in Hugo log.
* **Type**
	* Denotes the type of card to use (see [Card Component]({{< link
	  "/components/card/" "this" "url-only" />}}) for list of designs).
* **Class**
	* The cards' `class=` attribute that applies to all cards.
* **Style**
	* The cards' `style=` attribute that applies to all cards.
* **Width**
	* The cards' `width=` attribute that applies to all cards.
* **Height**
	* The cards' `height=` attribute that applies to all cards.



### Card CTA Configurations
These are CTA specific configurations for the [Card Component]({{< link
"/components/card/" "this" "url-only" />}}) used in content categorizations.
They are generally made available for customizing the CTA designs.

```toml {linenos=table,hl_lines=[],linenostart=14}
[Catalog.Card.CTA]
Class = "button clean"
```

* **[Catalog.Card.CTA]**
	* Denotes the following fields are owned by `Catalog.Card.CTA` table.
* **Class**
	* The CTA's `class=` attribute (applies to all).



### Card CTA Content Display Configuration
These are CTA link content to be displayed across different languages. The
language selection is determined by the page language itself.

```toml {linenos=table,hl_lines=[],linenostart=16}
[Catalog.Card.CTA.Content]
en = "Explore"
zh-hans = "探索"
```

* **[Catalog.Card.CTA.Content]**
	* Denotes the following fields are owned by `Catalog.Card.CTA.Content`
	  table.
* **{LANGUAGE_CODE} = {DISPLAY_TEXT}**
	* The display text corresponding to the page language's code. Note that
	  the language code is customizable. Bissetii supplies `en` and
	  `zh-hans` as defaults.



### Card Thumbnail Configuration
These are thumbnail specific configurations for the [Card Component]({{< link
"/components/card/" "this" "url-only" />}}) used in content categorization.
They are generally made available for customizing the thumbnail design.

```toml {linenos=table,hl_lines=[],linenostart=19}
[Catalog.Card.Thumbnail]
Detection = "auto"
Height = "300px"
```

* **[Catalog.Card.Thumbnail]**
	* Denotes the following fields are owned by `Catalog.Card.Thumbnail`
	  table.
* **Detection**
	* Set the thumbnail selection from the page's list of thumbnails. These
	  are the available options:
		* `auto` - uses Bissetii image dimension algorithm to determine
		           the most suitable thumbnail for the given card width.
		* `first` - use the first thumbnail of the list.
		* `last`  - use the last thumbnail of the list (default
		            behavior).
* **Height**
	* Set the thumbnail `height=` attribute. It must be a fixed value
	(e.g. `300px` or `30rem`). Use of flexible determiner like `max-content`
	is forbidden.

{{< note "info" "Thumbnail Width" >}}
Bissetii natively uses the `Catalog.Card.Width` as the thumbnail width so no
explicit and manual configuration is required.
{{< /note >}}




## Go
At the moment, `nav-catalog` is a Hugo-only component. No custom HTML usage is
available.




## HTML
Bissetii supports `nav-catalog` component seamlessly across multiple output
formats. Additionally, starting from version `v1.13.0`, the use of CSS variable
is vital for its upgrade from `v1.12.5`.

Bissetii HTML codes for this component are the same for Vanilla HTML5 and
AMPHTML. Bissetii uses [Grid Component]({{< link "/components/grid/" "this"
"url-only" />}}) to organize and tabulate all the catalog items in a neat
manner.

To ensure maximum design flexibility for `nav-catalog`, Bissetii shall not
specify what and how would you construct your catalog items' HTML codes. The
generic and minimal format is as follows:

```html {linenos=table,hl_lines=[],linenostart=1}
<nav class="nav-catalog">
	<div class="row">
		<div class="column">
			<!-- Your catalog item's HTML code here -->
		</div>
		<div class="column">
			<!-- Your catalog item's HTML code here -->
		</div>
		...
	</div>
</nav>
```

* **Line 1**
	* Uses `<nav>` HTML syntax.
	* Must tag `.nav-catalog` CSS class tag to denotes the `<nav>` is
	  a catalog navigation.
* **Line 2 and 3**
	* Uses `.row` and `.column` CSS class tags to organize the catalog
	  items. See [Grid Component]({{< link "/components/grid" "this"
	  "url-only" />}}) for more info on how to construct one.
	* Generally, each catalog item shall have its own `<div class="column">`
	  to neatly organize itself in the catalog.
* **Line 4**
	* Your catalog item's HTML codes. Bissetii does not enforce or specify
	  any rules for it.




## CSS
Bissetii provided a list of CSS variables dedicated for list styling alteration
without needing to recompile Sass. These variables are best applied directly to
the `nav-catalog` HTML tags. Example:

```html {linenos=table,hl_lines=[],linenostart=1}
<nav class="nav-catalog" style="--nav-catalog-background: var(--color-primary-300)">
	...
</nav-catalog>
```



### `--nav-catalog-margin`
Affects the margin spacing of the `nav-catalog`. The acceptable value shall be
compatible with `margin:` CSS field. The default is `5rem 0`.



### `--nav-catalog-padding`
Affects the padding spacing of the `nav-catalog`. The acceptable value shall be
compatible with `padding:` CSS field. The default is `0`.



### `--nav-catalog-border`
Affects the border styling of the `nav-catalog`. The acceptable value shall be
compatible with `border:` CSS field. The default is `border`.



### `--nav-catalog-background`
Affects the background styling of the `nav-catalog`. The acceptable value shall
be compatible with `background:` CSS field. The default is `transparent`.




## Javascript
This component does not rely on any Javascript.




## Sass
At the moment, `nav-catalog` is a Hugo-only component. No custom HTML usage is
available.




## Researches
Here are the researches done to ensure `nav-catalog` component to meet the
necessary quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `nav-catalog` component in Bissetii. If you need more feature
or need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
