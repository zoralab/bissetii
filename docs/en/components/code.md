+++
date = "2020-02-02T21:12:57+08:00"
title = "Code Component"
description = """
`Code` component is for displaying preformatted statements or specifically,
computer codes in the HTML. Bissetii styles the standard W3C HTML tags
seemlessly.
"""
keywords = ["code", "components", "html", "w3c", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Code"
pre = "💻"
weight = 1


[schema]
selectType = "WebPage"
+++

# `Code` Component
{{% param "description" %}}

This component is available starting from `v1.11.0`.




## Hugo
Hugo supports `Code` seamlessly so no special development is required.




## Go
Coming Soon.




## HTML
Bissetii supports `Code` component seamlessly across multiple output formats.
Additionally, starting from version `v1.13.0`, the use of CSS variable
is vital for its upgrade from `v1.12.5`.

Bissetii HTML codes for this component are the same for Vanilla HTML5 and
AMPHTML. It uses the `<pre>` and `<code>` HTML tags alone or together. Here
are a few ways to create this components.



### Single `<code>` Tag
This is useful for inline code styling without needing to create code snippet.
It is commonly used to highlight code syntax in a sentence. Example:

```html {linenos=table,hl_lines=[],linenostart=1}
<p>A Quick Brown <code>Fox</code> Jumps Over The Lazy Dog</p>
```

This will render as:

{{< renderHTML "html" "amp" >}}
<p>A Quick Brown <code>Fox</code> Jumps Over The Lazy Dog</p>
{{< /renderHTML >}}



### Single `<pre>` Tag
This is useful for creating raw presentation snippet. It is commonly used to
present a content in its raw display. Example:

```html {linenos=table,hl_lines=[],linenostart=1}
<pre>A Quick Brown <code>Fox</code> Jumps Over The Lazy Dog</pre>
```

This will render as:

{{< renderHTML "html" "amp" >}}
<pre>A Quick Brown <code>Fox</code> Jumps Over The Lazy Dog</pre>
{{< /renderHTML >}}



### `<pre>` with `<code>` Tag
This is to present code snippet since the content is all about codes. Example:

```html {linenos=table,hl_lines=[],linenostart=1}
<pre><code>This_is_a_code_displayed_in_preformatted_wrap() {
	containing another code.
}
This_is_a_code()</code></pre>
```

This will render as:

{{< renderHTML "html" "amp" >}}
<pre><code>This_is_a_code_displayed_in_preformatted_wrap() {
	containing another code.
}
This_is_a_code()</code></pre>
{{< /renderHTML >}}





## CSS
Bissetii provided a list of CSS variables dedicated for `Code` component
styling alteration without needing to recompile Sass. For consistency, these
variables are best applied directly to the `<body>` HTML tag. Example:

```html {linenos=table,hl_lines=[],linenostart=1}
<body style="--code-border-radius: 1rem; --pre-border-radius: 1rem;">
	...
</body>
```



### `--code-border-radius`
Set the `<code>` edge roundness. The acceptable value shall be compatible with
`border-radius:` CSS field. Default is `.4rem`.



### `--code-padding`
Set the `<code>` padding spacing. The acceptable value shall be compatible with
`padding:` CSS field. Default is `.2rem .5rem`.



### `--code-letter-spacing`
Set the `<code>`'s character spacing. The acceptable value shall be compatible
with `letter-spacing:` CSS field. Default is `0`.



### `--code-color`
Set the `<code>` text color. The acceptable value shall be compatible with
`color:` CSS field. Default is `var(--color-primary-500)`.



### `--code-background`
Set the `<code>` background styling. The acceptable value shall be compatible
with `background:` CSS field. Default is `var(--color-grey-100)`.



### `--pre-display`
Set the `<pre>` display type. The acceptable value shall be compatible with
`display:` CSS field. Default is `block`.



### `--pre-margin`
Set the `<pre>` margin spacing. The acceptable value shall be compatible with
`margin:` CSS field. Default is `0 auto`.



### `--pre-overflow`
Set the `<pre>` overflow behavior. The acceptable value shall be compatible with
`overflow:` CSS field. Default is `auto auto`.



### `--pre-border-left`
Set the `<pre>` border styling in the left direction. The acceptable value shall
be compatible with `border-left:` CSS field. Default is
`.3rem solid var(--color-primary-500)`.



### `--pre-border-radius`
Set the `<pre>` edge roundness. The acceptable value shall be compatible with
`border-radius:` CSS field. Default is `.4rem`.



### `--pre-padding`
Set the `<pre>` padding spacing. The acceptable value shall be compatible with
`padding:` CSS field. Default is `1.5rem`.



### `--pre-max-width`
Set the `<pre>`'s maximum width. The acceptable value shall be compatible
with `letter-spacing:` CSS field. Default is `100%`.



### `--pre-letter-spacing`
Set the `<pre>`'s character spacing. The acceptable value shall be compatible
with `letter-spacing:` CSS field. Default is `0`.



### `--pre-color`
Set the `<pre>` text color. The acceptable value shall be compatible with
`color:` CSS field. Default is `var(--color-grey-800)`.



### `--pre-background`
Set the `<pre>` background styling. The acceptable value shall be compatible
with `background:` CSS field. Default is `var(--color-grey-100)`.




## Javascript
This component does not rely on any Javascript.




## Sass
Depending on release version, the Sass files work differently. Bissetii does not
package Sass codes explictly so please view them via the git repository.



### `v1.13.0` and Above
Bissetii uses Dart Sass to compile the styling Sass codes into CSS file. This
component's Sass codes are available at the following location:

```
pkg/components/internal/code
```



### `v1.12.5` and Before
{{< note "warning" "Deprecation Notice" >}}
Starting from version `v1.13.0`, this version of Sass is no longer used and the
support is scheduled to be removed on **July 31, 2022**.
{{< /note >}}
The Sass scripts responsible for styling the component are located in:

```
assets/css/bissetii/modules/core/_Codes.scss
```




## Researches
Here are the researches done to ensure `Code` component meets the necessary
quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `Code` component in Bissetii. If you need more feature or need
to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
