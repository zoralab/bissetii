+++
date = "2020-02-10T13:01:16+08:00"
title = "Footer Component"
description = """
`Footer` component is the last body content appearing at the end of the website.
Bissetii uses the W3C hirarchy standard footer tag. This allows seamless styling
when applying to compatible HTML codes.
"""
keywords = ["footer", "components", "html", "w3c", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "amp-sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Footer"
pre = "📇"
weight = 1


[schema]
selectType = "WebPage"
+++

# `Footer` Component
{{% param description %}}

This component is available starting from `v1.11.0`.




## Hugo
Hugo supports `Footer` seamlessly so no special development is required.




## Go
Coming soon.




## HTML
Bissetii supports `Footer` component seamlessly across multiple output formats.
Additionally, starting from version `v1.13.0`, the use of CSS variable is vital
for its upgrade from `v1.12.5`.

Bissetii HTML codes for this component are the same for Vanilla HTML5 and
AMPHTML.



### Layout
`Footer` component **SHALL stay inside `<main>` component** as part of its
content (the main content). Hence, the HTML page layout is complying to the
following pattern:

```html {linenos=table,hl_lines=["8-10"],linenostart=1}
<html ...>
<head>
	...
</head>
<body>
	<main>
		...
		<footer>
			...
		</footer>
	</main>
	<nav>
		...
	</nav>
	...
</html>
```

The content is similar to normal page building where `<footer>` is a section
of the main content page.



### Printing
Since the webpage is a content of its own, the page count and page number are
not available since the native printing system dictates it. Therefore, the
contents shall always designed primarily for website viewing and treat
`<footer>` as the last section of content.

Bissetii set the background color as exect during print mode. To save ink and
paper friendly, one can either disable the `Footer` entire by setting the inline
CSS variable `--footer-display-print: none` or configure its background and
foreground color respectively using CSS variables.




## CSS
Bissetii provided a list of CSS variables dedicated for `Footer` styling
alteration without needing to recompile Sass. Depending on the area of effects,
these variables are best applied directly to the `Footer` HTML tag. Example:

```html {linenos=table,hl_lines=[],linenostart=1}
<footer style="--footer-display-print: none;">
	...
</footer>
```



### `--footer-display`
Affects the display mode of the `Footer`. The acceptable value shall be
compatible with `display:` CSS field. The default is `block`.



### `--footer-padding`
Affects the padding spacing of the `Footer`. The acceptable value shall be
compatible with `padding:` CSS field. The default is
`2.5rem var(--main-padding)`.



### `--footer-margin`
Affects the margin spacing of the `Footer`. The acceptable value shall be
compatible with `margin:` CSS field. The default is
`0 calc(-1 * var(--main-padding)) calc(-1 * var(--main-padding))`.



### `--footer-color`
Affects the content and text color of the `Footer`. The acceptable value shall
be compatible with `color:` CSS field. The default is `var(--color-grey-50)`.



### `--footer-background`
Affects the background styling of the `Footer`. The acceptable value shall be
compatible with `background:` CSS field. The default is
`var(--color-primary-700)`.



### `--footer-border`
Affects the border styling of the `Footer`. The acceptable value shall be
compatible with `border:` CSS field. The default is `none`.



### `--footer-display-print`
Affects the display mode of the `Footer` while printing. The acceptable value
shall be compatible with `display:` CSS field. The default is `block`.




## Javascript
This component does not rely on any Javascript.




## Sass
Depending on release version, the Sass files work differently. Bissetii does not
package Sass codes explictly so please view them via the git repository.



### `v1.13.0` and Above
Bissetii uses Dart Sass to compile the styling Sass codes into CSS file. This
component's Sass codes are available at the following location:

```
pkg/components/internal/footer
```



### `v1.12.5` and Below
{{< note "warning" "Deprecation Notice" >}}
Starting from version `v1.13.0`, this version of Sass is no longer used and the
support is scheduled to be removed on **July 31, 2022**.
{{< /note >}}

The Sass scripts responsible for styling the component are located in:

```bash
assets/css/bissetii/modules/core/_Base.scss
```

By default, it shares the same styling as `Main` component.




## Researches
Here are the researches done to ensure `Footer` component meets the necessary
quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `Footer` component in Bissetii. If you need more feature or
need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
