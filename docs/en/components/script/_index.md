+++
date = "2021-08-29T10:29:27+08:00"
title = "Script Component"
description = """
Script component is a module to render page enhancement script-based codes into
your webpage. Bissetii supports this component seamlessly as a mean to import
various script-based utilities like JSON and Javascript.
"""
keywords = ["script", "components", "html", "w3c", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Script"
pre = "⚙️"
weight = 1


[schema]
selectType = "WebPage"
+++

# `Script` Component
{{% param "description" %}}

This component is available starting from `v1.13.0`.




## Hugo
Hugo supports list seamlessly so no special development is required.




## Go
Coming soon.




## HTML
Bissetii supports `Script` component seamlessly across multiple output formats.

However, please keep in mind that this component is prohibited from being used
in AMPHTML as part of its design requirements.

There are multiple ways to use `Script` component.



### Using External URL Loading
To perform external URL loading, the HTML codes with all the options are as
follows:

```html
<script type="text/javascript"
	id="my-script-id"
	defer
	integrity="..."
	crossorigin="anonymous"
	integrity="..."
	nomodule
	nonce="..."
	src="..."></script>
```



### Using Inline Scripting
To perform inline scripting, the HTML codes with all the options are available
as follows:

```html
<script id="my-script-id">
	// Your Codes Here
	// ...
</script>
```




## CSS
CSS implementation is not applicable for this component.




## Javascript
This component does not rely on any Javascript.




## Sass
Sass implementation is not applicable for this component.




## Researches
Here are the researches done to ensure `Script` component meets the necessary
quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `Script` component in Bissetii. If you need more feature or
need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
