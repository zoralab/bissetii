+++
date = "2021-08-29T12:38:53+08:00"
title = "Style Component"
description = """
Style component is a module to render page enhancement using inline CSS codes
into your webpage. Bissetii supports this component seamlessly as a mean to
render inline web pages.
"""
keywords = ["style", "components", "html", "w3c", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Style"
pre = "⚙️"
weight = 1


[schema]
selectType = "WebPage"
+++

# `Style` Component
{{% param "description" %}}

This component is available starting from `v1.13.0`.




## Hugo
Hugo supports list seamlessly so no special development is required.




## Go
Coming soon.




## HTML
Bissetii supports `Style` component seamlessly across multiple output formats.
To use inline styling, simply use the following HTML syntax:

```html
<style type="..." title="..."  media="...">
	// Your inline CSS here
</style>
```




## CSS
CSS implementation is not applicable for this component.




## Javascript
This component does not rely on any Javascript.




## Sass
Sass implementation is not applicable for this component.




## Researches
Here are the researches done to ensure `Style` component meets the necessary
quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `Style` component in Bissetii. If you need more feature or
need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
