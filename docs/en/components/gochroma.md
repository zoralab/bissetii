+++
date = "2021-06-24T20:35:13+08:00"
title = "Go Chroma Component"
description = """
`Go Chroma` component is for displaying preformatted computer codes with syntax
highlighting using Go Chroma package used in Hugo. Bissetii styles its outcome
seemlessly to make it compatible with the main design.
"""
keywords = ["gochroma", "components", "html", "w3c", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Code | Go Chroma"
pre = "💻"
weight = 1


[schema]
selectType = "WebPage"
+++

# `Go Chroma` Component
{{% param "description" %}}

This component is available starting from `v1.11.0`.




## Hugo
Due to the requirement to support Hugo as a theme module and Hugo started to
use Go Chroma to perform syntax highlighting, Bissetii has to inheritly support
Go Chroma rendering adjustment as a module.

The Go Chroma repository is available on
[Github](https://github.com/alecthomas/chroma) and Hugo's
[guide](https://gohugo.io/content-management/syntax-highlighting/).



### Highlighting Code Using Code Fencing
To highlight codes with syntax highlighting, you can use the code fencing
ability. Example:

````md {linenos=table,hl_lines=[1],linenostart=1}
```go {linenos=table,hl_lines=[4,"7-9"],linenostart=1}
package main

import (
	"fmt"
)

func main() {
	fmt.Printf("Hello World, %s\n", "Cheri")
}
```
````

It renders as:

```go {linenos=table,hl_lines=[4,"7-9"],linenostart=1}
package main

import (
	"fmt"
)

func main() {
	fmt.Printf("Hello World, %s\n", "Cheri")
}
```

{{< note "default" "Available Options" >}}
1. `linenos` = one of this: `true`, `false`, `table`, or `inline`
   (**NOTE**: `table` is copy-paste friendly)
2. `hl_lines` = array of numbers (can be number in digit or range in string)
3. `linenostart` = starting number in digits
{{< /note >}}



### Customizing Highlighting Style
The configuration file is located in the `markup.toml` file inside Hugo's config
file:

```
name             :                        markup.toml
local directory  :        config/_default/markup.toml
Bissetii repo-doc: .sites/config/_default/markup.toml
```

Inside the Hugo config `markup.toml` file, there is a field called `style`.
Bissetii set the default to `"monokailight"`. If needed, simply browse the
following gallery here and change it to that title.

1. [Summary Text Styling](https://xyproto.github.io/splash/docs/all.html)
2. [Full Text Styling](https://xyproto.github.io/splash/docs/longer/all.html)




## Go
Coming Soon.




## HTML
Since `Go Chroma` package generates its own HTML codes seamlessly, there is no
HTML guide available.




## CSS
Since `Go Chroma` handles all the styling with inline CSS styling, there is no
CSS available for customization.




## Javascript
This component does not rely on any Javascript.




## Sass
Depending on release version, the Sass files work differently. Bissetii does not
package Sass codes explictly so please view them via the git repository.



### `v1.13.0` and Above
Bissetii uses Dart Sass to compile the styling Sass codes into CSS file. This
component's Sass codes are available at the following location:

```
pkg/components/internal/gochroma
```



### `v1.12.5` and Before
{{< note "warning" "Deprecation Notice" >}}
Starting from version `v1.13.0`, this version of Sass is no longer used and the
support is scheduled to be removed on **July 31, 2022**.
{{< /note >}}
The Sass scripts responsible for styling the component are located in:

```
assets/css/bissetii/modules/core/_Codes.scss
```

This is an optional module since not everyone is using Go Chroma to render
code blocks. To include Go Chroma adjustment, you need to enable Go Chroma
config in your Sass configuration file:

```sass
$config-gochroma: 1
```




## Researches
Here are the researches done to ensure `Go Chroma` component meets the necessary
quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `Go Chroma` component in Bissetii. If you need more feature or
need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
