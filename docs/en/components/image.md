+++
date = "2020-02-07T09:45:02+08:00"
title = "Image Component"
description = """
Web HTML facilitates a wide-varieties of displaying images using various image
tags like "<img>", "<amp-img>" and "<picture>" in their respective outputs.
Bissetii supports them seemlessly in their respective outputs.
"""
keywords = ["image", "components", "html", "w3c", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Image"
pre = "🏞️"
weight = 1


[schema]
selectType = "WebPage"
+++

# `Image` Component
Web HTML facilitates a wide-varieties of displaying images using various image
tags like `<img>`, `<amp-img>` and `<picture>` in their respective outputs.
Bissetii supports them seemlessly in their respective outputs.

This component is available starting from `v1.11.0`.




## Hugo
Bissetii prepared some Hugo interfaces to generate image seamlessly across all
output formats.



### Shortcodes
By default, it is hard to process images across multiple outputs while
maintaining a single input format. Hence, Bissetii prepared the following
shortcodes to standardize the image rendering input. These shortcodes are
specific to Bissetii which is working differently from Hugo.


#### `image` Shortcode
`image` shortcode is a quick-and-easy image insertion. It does not require any
data file configurations. Depending on Bissetii version, `image` shortcode may
behaves differently.

##### Version `v1.13.0` and Above
The shortcode complies to the following pattern:

```markdown {linenos=table,hl_lines=[],linenostart=1}
{{</* image [ALT]
	[URL]
	[WIDTH]
	[HEIGHT]
	[ISMAP]
	[LOADING]
	[SRCSET]
	[LAYOUT]
	[CLASS]
	[SIZES] */>}}
```

* `ALT` - **COMPULSORY**
	1. The image alternate text (`alt=`).
* `URL` - **COMPULSORY**
	1. The fallback image URL source (`src=`). Bissetii's compatible
	relative URL is allowed and shall be processed into absolute URL as the
	final output.
* `WIDTH` - **COMPULSORY**
	1. The width of the image (`width=`). It is compulsory because some
	output formats has strict rule for it.
* `HEIGHT` - **COMPULSORY**
	1. Height of the image (`height=`). It is compulsory because some output
	formats has strict rule for it.
* `ISMAP` - **OPTIONAL**
	1. Specify the image is mapped under an anchor hyperlink. To disable it,
	simply supply an empty string (`""`).
* `LOADING` - **OPTIONAL**
	1. The image `loading=` attribute. It **ONLY** accepts `eager` or
	`lazy`.
* `SRCSET` - **OPTIONAL**
	1. Specify the image's source URL set (`srcset=`). The value shall be
	the final compiled long string version. To disable it, simply supply an
	empty string (`""`).
* `LAYOUT` - **COMPULSORY**
	1. Specify the image layout using Bissetii's values. There are 4 types:
		1. `responsive` - ensures the image is always responsive.
		2. `responsive-amp-only` - ensures the responsiveness only
		applies to AMPHTML output format.
		3. `responsive-height-only` - ensures the responsiveness only
		applies to height. For Vanilla HTML5, `width=` attribute is
		retained while `height=` attribute is removed even when it is a
		valid value.
		4. `responsive-width-only` - ensures the responsiveness only
		applies to the height. For Vanilla HTML5, `height=` attribute is
		retained while  `width=` attribute is removed even when it is a
		valid value.
9. `CLASS` - **OPTIONAL**
	1. Specify the CSS `class=` attribute.
10. `SIZES` - **OPTIONAL**.
	1. Specify the responsive image sizes (`sizes=`). This field is
	automatically removed if `SRCSET` is empty.

To use it, simply call it inside the markdown. Example:

```markdown {linenos=table,hl_lines=[],linenostart=1}
{{</* image "My Portrait" "/img/me/portrait.png" "200" "400"
	"false" "lazy" "/img/me/portrait-2x.png 2x, /img/me/portrait-3x.png 3x"
	"responsive-amp-only" "id-img-portrait"
	"(min-width: 640px) 2x" "John Smith" */>}}

		⥥

{{% image "My Portrait" "/img/me/portrait.png" "200" "400"
	"false" "lazy" "/img/me/portrait-2x.png 2x, /img/me/portrait-3x.png 3x"
	"responsive-amp-only" "id-img-portrait"
	"(max-width: 640px) 2x, (min-width: 641px) 3x" "John Smith" %}}
```

##### Version `v1.12.5` and Below
{{< note "warning" "Deprecation Notice" >}}
Support for `image` shortcode version `v1.12.5` and below is deprecated and
shall be rescinded on **June 30, 2021**.
{{< /note >}}

The shortcode complies to the following pattern:

```markdown {linenos=table,hl_lines=[],linenostart=1}
{{</* image [ALT]
	[URL]
	[WIDTH]
	[HEIGHT]
	[ISMAP]
	[LOADING]
	[SRCSET]
	[LAYOUT]
	[CLASS] */>}}
```

* `ALT` - **COMPULSORY**
	1. The image alternate text (`alt=`).
* `URL` - **COMPULSORY**
	1. The fallback image URL source (`src=`). Bissetii's compatible
	relative URL is allowed and shall be processed into absolute URL as the
	final output.
* `WIDTH` - **COMPULSORY**
	1. The width of the image (`width=`). It is compulsory because some
	output formats has strict rule for it.
* `HEIGHT` - **COMPULSORY**
	1. Height of the image (`height=`). It is compulsory because some output
	formats has strict rule for it.
* `ISMAP` - **OPTIONAL**
	1. Specify the image is mapped under an anchor hyperlink. To disable it,
	simply supply an empty string (`""`).
* `LOADING` - **OPTIONAL**
	1. The image `loading=` attribute. It **ONLY** accepts `eager` or
	`lazy`.
* `SRCSET` - **OPTIONAL**
	1. Specify the image's source URL set (`srcset=`). The value shall be
	the final compiled long string version. To disable it, simply supply an
	empty string (`""`).
* `LAYOUT` - **COMPULSORY**
	1. Specify the image layout using Bissetii's values. There are 4 types:
		1. `responsive` - ensures the image is always responsive.
		2. `responsive-amp-only` - ensures the responsiveness only
		applies to AMPHTML output format.
		3. `responsive-height-only` - ensures the responsiveness only
		applies to height. For Vanilla HTML5, `width=` attribute is
		retained while `height=` attribute is removed even when it is a
		valid value.
		4. `responsive-width-only` - ensures the responsiveness only
		applies to the height. For Vanilla HTML5, `height=` attribute is
		retained while  `width=` attribute is removed even when it is a
		valid value.
9. `CLASS` - **OPTIONAL**
	1. Specify the CSS `class=` attribute.


#### `picture` Shortcode
For images requiring the use of `<picture>` tag in Vanilla HTML5 and using data
file to organize the image data (as opposed to `image` shortcode), Bissetii
supplied `picture` shortcode for the job.

##### Version `v1.13.0` and Above
The shortcode complies to the following pattern:

```markdown
{{</* picture [DATASET_PATH]  */>}}
```

* `DATASET_PATH` - **COMPULSORY**
	1. The Hugo data pathing to the image data set.
		1. For example, if the data file is located in
		`data/pictures/myAlbum.toml` where the data directory is called
		`data/` and the dataset is `IMG01`, the Hugo data pathing shall
		be: `pictures.myAlbum.IMG01`.


###### Data File Structure
The data file shall be stored inside your Hugo `data/` directory stated inside
your configuration file. The file structure is as follows:

```toml {linenos=table,hl_lines=[],linenostart=1}
[IMG01]
IsMap = false
LoadingMode = "lazy"
Class = "my-class"
URL = "/img/bg/img_placeholder.svg"
Width = "400"
Height = "300"
Layout = "responsive-amp-only"
SrcList = """
/img/bg/img_480w.svg 480w,
/img/bg/img_800w.svg 800w,
/img/bg/img_1024w.svg 1024w,
/img/bg/img_2048w.svg 2048w,
/img/bg/img_4096w.svg 4096w
"""
Sizes = """
(max-width: 480px) 480px,
(max-width: 800px) 800px,
(max-width: 1024px) 1024px,
(max-width: 2048px) 2048px,
(min-width: 2049px) 4096px
"""

[IMG01.AltText]
en = "picture elements"
zh-hans = "图画元素"




# Picture Source HTML Tag
[IMG01.Sources.01]
Type = "image/svg+xml"
SourceSet = """
/img/bg/img_800w.svg 1x,
/img/bg/img_1024w.svg 2x,
/img/bg/img_2048w.svg 3x
"""
Media = "(max-width: 20em)"

[IMG01.Sources.02]
Type = "image/svg+xml"
SourceSet = """
/img/bg/img_800w.svg 1x,
/img/bg/img_1024w.svg 2x,
/img/bg/img_2048w.svg 3x
"""
Media = "(max-width: 40em)"
```

Calling the shortcode against the data file above yields:

```markdown {linenos=table,hl_lines=[],linenostart=1}
{{% picture "bissetii.data.examples.picture.samples.IMG01" %}}
```




## Go
Coming Soon.




## HTML
Bissetii supports the `<img>` and `<amp-img>` seamlessly and accordingly based
on their respective output formats. Additionally, starting from version
`v1.13.0`, `<picture>` tag for non-AMPHTML output format is now available.



### Basic Image Inclusion
To cater responsive images, Bissetii recommends the following HTML codes for
quick and easy usage.


#### Vanilla HTML5
For vanilla HTML5, the basic image inclusion example would be:

```html {linenos=table,hl_lines=[],linenostart=1}
<img alt="image srcset with sizes"
	src="{{< link "/img/bg/img_480w.svg" "" "url-only" />}}"
	srcset="{{< link "/img/bg/img_480w.svg" "" "url-only" />}} 480w,
		{{< link "/img/bg/img_800w.svg" "" "url-only" />}} 800w,
		{{< link "/img/bg/img_1024w.svg" "" "url-only" />}} 1024w,
		{{< link "/img/bg/img_2048w.svg" "" "url-only" />}} 2048w,
		{{< link "/img/bg/img_4096w.svg" "" "url-only" />}} 4096w"
	sizes="(max-width: 480px) 480px,(max-width: 800px) 800px,(min-width: 801px) 4096px"
	width="200"
	height="150"
	loading="lazy"
/>
```

* `alt=` - **COMPULSORY**
	1. `alt` text for describing the image when the image fails to load and
	to allow a search engine bot to comprehend the image.
* `src=` - **COMPULSORY**
	1. the fallback version of the image URL.
* `srcset=` - **OPTIONAL**
	1. the responsive image URL sources and their configurations. Best use
	would be the image width or resolution (e.g. `1x`, `2x`). The browser
	will comprehend the last and only download the one that is making sense.
* `sizes=` - **OPTIONAL**
	1. setting the rules of selecting an appropriate image using media
	query. It works closely with `srcset` attribute as its source of image
	URL and image conditions.
	2. It **shall only exist when `srcset=` is available**.
* `width=` - **OPTIONAL**
	1. the width of the image you plan to display in `px` unit. It's
	recommended to state the attribute for spacing allocation consistency
	purposes.
* `height=` - **OPTIONAL**
	1. the height of the image you plan to display in `px` unit. It's
	recommended to state the attribute for spacing allocation consistency
	purposes.
* `loading=` - **OPTIONAL** - the image loading instruction. Setting the value
	to `lazy` allows user to only load the image when it enters view port.
	This is useful for saving download bandwidth during bad connectivity.


#### AMPHTML
For AMPHTML, the basic image inclusion example would be:

```html {linenos=table,hl_lines=[],linenostart=1}
<amp-img alt="image title/purpose"
	src="{{< link "/img/bg/img_placeholder.svg" "this" "url-only" "" />}}"
	srcset="{{< link "/img/bg/img_480w.svg" "this" "url-only" "" />}} 480w,
		{{< link "/img/bg/img_800w.svg" "this" "url-only" "" />}} 800w,
		{{< link "/img/bg/img_1024w.svg" "this" "url-only" "" />}} 1024w,
		{{< link "/img/bg/img_2048w.svg" "this" "url-only" "" />}} 2048w,
		{{< link "/img/bg/img_4096w.svg" "this" "url-only" "" />}} 4096w"
	sizes="(max-width: 480px) 480px,(max-width: 800px) 800px,(min-width: 801px) 4096px"
	width="200"
	height="150"
	layout="responsive">
</amp-img>
```

The main difference is the use of its custom `<amp-img>` AMPHTML tag instead of
the conventional `<img>` HTML tag. Also, it has a tailing close tag
(`</amp-img>`).

* `alt=` - **COMPULSORY**
	1. `alt` text is for describing the image when the image fails to load
	and to allow a search engine bot to comprehend the image.
* `src=` - **COMPULSORY**
	1. the fallback version of the image URL.
* `srcset=` - **OPTIONAL**
	1. the responsive image URL sources and their configurations. Best use
	would be the image width or resolution (e.g. `1x`, `2x`). The browser
	will comprehend the last and only download the one that is making sense.
* `sizes=` - **OPTIONAL**
	1. setting the rules of selecting an appropriate image using media
	query. It works closely with `srcset` attribute as its source of image
	URL and image conditions.
	2. It **shall only exist when `srcset=` is available**.
* `width=` - **OPTIONAL**
	1. the width of the image you plan to display in `px` unit. It's
	recommended to state the attribute for spacing allocation consistency
	purposes.
* `height=` - **OPTIONAL**
	1. the height of the image you plan to display in `px` unit. It's
	recommended to state the attribute for spacing allocation consistency
	purposes.
* `layout=` - **OPTIONAL**
	1. the AMP layout instruction. Setting the value to `responsive` will
	set the image size to resize based on the screen size automatically.

Please keep in mind that `<amp-img>` is always being lazy-loaded.



### Art-Directed Image Inclusion
Sometimes, the image needs to be art-directed where small screen should be
presented with other images or cropped version of the original image (which is
a completely different image). Bissetii recommends the following HTML
implementations based on your output formats.


#### Vanilla HTML5
For vanilla HTML5, it is highly recommended to use `<picture>` HTML tag
alongside its `<source>` child HTML tag. The reason is to keep the HTML codes
sane and maintainable where each sources can be detailed easily in its own
HTML line. Although `srcset` and `sizes` can do the same job, it would be insane
to stuff too many things inside them.

Here is an example:

```html {linenos=table,hl_lines=[],linenostart=1}
<picture>
	<source srcset="//holloway-seraphim:8080/img/bg/img_800w.jpg 1x,
		//holloway-seraphim:8080/img/bg/img_1024w.jpg 2x,
		//holloway-seraphim:8080/img/bg/img_2048w.jpg 3x"
		type="image/jpeg"
		media="(max-width: 20em)" type="image/svg&#43;xml">
	<source srcset="//holloway-seraphim:8080/img/bg/img_1x.png 1x,
		//holloway-seraphim:8080/img/bg/img_2x.png 2x,
		//holloway-seraphim:8080/img/bg/img_3x.png 3x"
		type="image/png"
		media="(max-width: 40em)" type="image/svg&#43;xml">
	<img alt="picture elements"
		src="//holloway-seraphim:8080/img/bg/img_placeholder.svg"
		width="400"
		height="300"
		loading="lazy"/>
</picture>
```

Some noticable changes would be:

1. Each sources now has its own `srcset`, `media`, and even file `type` changes.
	1. Use the `srcset` to supply multiple image sources (e.g. different
	   resolution).
	2. Use `media` to set the rules of applications (e.g. usually
	   `max-width`).
	3. The `type` must be a compliant MIME type value. E.g. `image/jpeg` and
	   not `image/jpg`.
2. You no longer need to specify `srcset` and `sizes` for the fallback `img`
   tag. It makes no sense to cause confusion between the 2.
3. You can define more sources and let the browser to the best selection
   accordingly.


#### AMPHTML
Currently, **there is no way** to specify multiple sources for art-directing
image display. You will need to compile the final value and stuff it inside the
existing `srcset` and `sizes` respectively.




## CSS
This section covers all the CSS part for `Image` component. Please go through
each sub-section carefully as they affects Bissetii styling directly.

This CSS section is **ONLY AVAILABLE** starting from version `v1.13.0` and
above.



### CSS Variables
Bissetii provided a list of CSS variables dedicated for image styling
alternation without needing to recompile Sass. These variables are best applied
directly to the image HTML tag. Example:

```html
<img src="..." alt="..." style="--image-max-width: 450px;">
<amp-img src="..." alt="..." style="--image-max-width: 450px;" ... ></amp-img>
```

To set it globally, the variables can be applied to the `<body>` tag. Example:

```html
<body style="--image-max-width: 30vw" ...>
```


#### `--image-max-width`
Affects the image max width. The value must be compliant to `max-width:` CSS
field. Default is `100%`.


#### `--image-max-height`
Affects the image max height. The value must be compliant to `max-width:` CSS
field. Default is `unset`.


#### `--image-height`
Affects the image immediate height. The value must be compliant to `height:` CSS
field. Default is `auto`.


#### `--image-filter`
Affects the image overlaying effect. The value must be compliant to `filter:`
CSS field. Default is `unset`.


#### `--image-border`
Affects the image border. The value must be compliant to `border:` CSS field.
Default is `none`.


#### `--image-border-radius`
Affects the image border radius (or roundness corder). The value must be
compliant with `border-radius:` CSS field. Default is `0`.


#### `--image-animation`
Affects the image CSS animation. The value must be compliant with `animation:`
CSS field. Default is `unset`.




## Javascript Parts
This component does not rely on any Javascript.




## Sass
Depending on release version, the Sass files work differently. Bissetii does not
package Sass codes explictly so please view them via the git repository.



### `v1.13.0` and Above
Bissetii uses Dart Sass to compile the styling Sass codes into CSS file. This
component's Sass codes is available at the following location:

```
pkg/components/internal/image
```



### `v1.12.5` and Before
{{< note "warning" "Deprecation Notice" >}}
Starting from version `v1.13.0`, this version of Sass is no longer used and the
support is scheduled to be removed on **July 31, 2022**.
{{< /note >}}

The Sass scripts responsible for styling image are located at:

```
assets/css/bissetii/modules/core/_Image.scss
```




## Researches
Here are the researches done to ensure `Image` component meets the necessarily
quality assurances:

1. **Image Web User Interface Architectural Design Analysis**
(https://doi.org/10.5281/zenodo.4925642)



## Epilogue
That is all about `Image` component in Bissetii. If you need more feature or
encountered a bug, Please feel free to contact us by filling an issue ticket
at our [Issue Section](https://gitlab.com/zoralab/bissetii/-/issues). We will
be glad to help you.
