+++
date = "2020-02-02T16:17:20+08:00"
title = "Button Component"
description = """
`Button` as the name implies, is the clickable knob meant for users to pressed
onto. `Button` is commonly used in forms and link styling for implying clickable
items. Bissetii prepared a number of CSS styles to support native button-like
HTML5 syntax from W3C.
"""
keywords = ["button", "components", "html", "w3c", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "amp-sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Button (BETA)"
pre = "🛎️"
weight = 1


[schema]
selectType = "WebPage"
+++

# `Button` Component
{{% param description %}}

This component is available starting from `v1.11.0`.




## Hugo
Hugo supports `Button` seamlessly so no special development is required.




## Go
Coming Soon




## HTML
Bissetii supports `Button` component seamlessly across multiple output formats.
Additionally, starting from version `v1.13.0`, the use of CSS variable is vital
for its upgrade from `v1.12.5`.

Bissetii HTML codes for this component are the same for Vanilla HTML5 and
AMPHTML.



### The HTML5 `<button>` Tag
Bissetii automatically styles `<button>` seamlessly regardless it is inside
the `<form>` or otherwise. Hence, no additional steps are required unless
applying quick styling.

Here is an example:

```html {linenos=table,hl_lines=[],linenostart=1}
<button type="button">Default</button>
<button type="button" class="outline">Outline</button>
<button type="button" class="clean">Clean</button>
<button type="button" disabled>Disabled</button>
<button type="button" class="outline" disabled>Outline Disabled</button>
<button type="button" class="clean" disabled>Clean Disabled</button>
```

This will render as:

{{< renderHTML amp html >}}
<button type="button">Default</button>
<button type="button" class="outline">Outline</button>
<button type="button" class="clean">Clean</button>
<button type="button" disabled>Disabled</button>
<button type="button" class="outline" disabled>Outline Disabled</button>
<button type="button" class="clean" disabled>Clean Disabled</button>
{{< /renderHTML >}}



### The HTML5 `<input type="submit|button|reset">` Tag
Bissetii also automatically styles `<input type="submit|button|reset">`
seamlessly working closely with [Form Component]({{< link "/components/form/"
"this" "url-only" />}}). Hence, no additional steps are required unless applying
quick styling.

Here is an example:

```html {linenos=table,hl_lines=[],linenostart=1}
<input type="submit" name="submit[login]" value="Login" />
<input type="submit" name="submit[add-user]" class="outline" value="Add User" />
<input type="submit" name="submit[delete-user]" class="clean" value="Delete User" />
<input type="submit" name="submit[login]" value="Login" disabled />
<input type="submit" name="submit[add-user]" class="outline" value="Add User" disabled />
<input type="submit" name="submit[delete-user]" class="clean" value="Delete User" disabled />
```

This will render as:

{{< renderHTML amp html >}}
{{< form "" "POST" "#" >}}
<fieldset class="border center">
	<legend>Your Instruction?</legend>
	<input type="submit" name="submit[login]" value="Login" />
	<input type="submit" name="submit[add-user]" class="outline" value="Add User" />
	<input type="submit" name="submit[delete-user]" class="clean" value="Delete User" />
	<input type="submit" name="submit[login]" value="Login" disabled />
	<input type="submit" name="submit[add-user]" class="outline" value="Add User" disabled />
	<input type="submit" name="submit[delete-user]" class="clean" value="Delete User" disabled />
</fieldset>
{{< /form >}}
{{< /renderHTML >}}



### The CSS Class `.button` Tag
In the case where `<button>` and `<input type="submit">` is not applicable
such as [Anchor "Link" Component]({{< link "/components/anchor/" "this"
"url-only" />}}), Bissetii prepared `.button` tag to the object as a button.

Here is an example:

```html {linenos=table,hl_lines=[],linenostart=1}
<a href="#" class="button">Default</a>
<a href="#" class="button outline">Outline</a>
<a href="#" class="button clean">Clean</a>
<a href="#" class="button disabled">Default Disabled</a>
<a href="#" class="button outline disabled">Outline Disabled</a>
<a href="#" class="button clean disabled">Clean Disabled</a>
```

This will render as:

{{< renderHTML amp html >}}
<a href="#" class="button">Default</a>
<a href="#" class="button outline">Outline</a>
<a href="#" class="button clean">Clean</a>
<a href="#" class="button disabled">Default Disabled</a>
<a href="#" class="button outline disabled">Outline Disabled</a>
<a href="#" class="button clean disabled">Clean Disabled</a>
{{< /renderHTML >}}



### Quick Styling
For some applications, Bissetii prepared a number of quick styling CSS classes
to quickly customize the button outlook.


#### `.outline` CSS class
The `.outline` CSS class set the button to appear as outline when it is not
under hover or focus statuses.


#### `.clean` CSS class
The `.clean` CSS class set the button to remain clean with no background and
border when it is not under hover or focus statuses.


#### `.pinpoint` CSS class
{{< note "warning" "Deprecation Notice" >}}
Starting from version `v1.13.0`, `.pinpoint` CSS class is scheduled to be
removed on **July 31, 2022**.
{{< /note >}}

The `.pinpoint` CSS class is **NO LONGER** used since it is now the default
styling.




## CSS
Bissetii provided a list of CSS variables dedicated for `Button` styling
alteration without needing to recompile Sass. Depending on the area of effects,
these variables are best applied directly to `<body>` or the `Button` HTML tags
respectively. Example:

```html
<body style="--button-color-focus: white;
	--button-background-focus: crimson;
	--button-border-color-focus: crimson;">
	...
	<button style="--button-color: crimson;
		--button-color-invert: crimson;
		--button-background: white;">
		...
	</button>
	...
</body>
```



### `--button-display`
Affects the display mode of the `Button`. The acceptable value shall be
compatible with `display:` CSS field. The default is `inline-block`.



### `--button-opacity`
Affects the opacity visibility of the `Button`. The acceptable value shall be
compatible with `opacity:` CSS field. The default is `1`.



### `--button-transition`
Affects the animation transition timing of the `Button`. The acceptable value
shall be compatible with `transition:` CSS field. The default is
`var(--animation-timing-normal)`.



### `--button-margin`
Affects the margin spacing of the `Button`. The acceptable value shall be
compatible with `margin:` CSS field. The default is `.5rem 1.2rem`.



### `--button-padding`
Affects the padding spacing of the `Button`. The acceptable value shall be
compatible with `padding:` CSS field. The default is `1.5rem`.



### `--button-border-radius`
Affects the edge roundness of the `Button`. The acceptable value shall be
compatible with `border-radius:` CSS field. The default is `.4rem`.



### `--button-color`
Affects the content and text coloration of the `Button`. The acceptable value
shall be compatible with `color:` CSS field. The default is
`var(--color-grey-50)`.



### `--button-background`
Affects the background styling of the `Button`. The acceptable value shall be
compatible with `background:` CSS field. The default is
`var(--color-primary-500)`.



### `--button-border-width`
Affects the border thickness of the `Button`. The acceptable value shall be
compatible with `border-width:` CSS field. The default is `.3rem`.



### `--button-border-style`
Affects the border style of the `Button`. The acceptable value shall be
compatible with `border-style:` CSS field. The default is `solid`.



### `--button-border-color`
Affects the border color of the `Button`. The acceptable value shall be
compatible with `border-color:` CSS field. The default is
`var(--color-primary-500)`.



### `--button-font-size`
Affects the text's font size inside the `Button`. The acceptable value shall
be compatible with `font-size:` CSS field. The default is `1.35rem`.



### `--button-font-weight`
Affects the text's font weight inside the `Button`. The acceptable value shall
be compatible with `font-weight:` CSS field. The default is `700`.



### `--button-letter-spacing`
Affects the text's letter spacing inside the `Button`. The acceptable value
shall be compatible with `letter-spacing:` CSS field. The default is `.1rem`.



### `--button-text-align`
Affects the text alignment in the `Button`. The acceptable value shall be
compatible with `text-align:` CSS field. The default is `center`.



### `--button-text-decoration`
Affects the text decoration in the `Button`. The acceptable value shall be
compatible with `text-decoration:` CSS field. The default is `none`.



### `--button-text-transform`
Affects the text transformation in the `Button`. The acceptable value shall be
compatible with `text-transform:` CSS field. The default is `uppercase`.



### `--button-overflow-wrap`
Affects the content and text overflow wrapping behavior in the `Button`. The
acceptable value shall be compatible with `overflow-wrap:` CSS field. The
default is `break-word`.



### `--button-word-break`
Affects the content and text breaking behavior during an overflow wrapping
inside the `Button`. The acceptable value shall be compatible with
`word-break:` CSS field. The default is `break-word`.



### `--button-color-invert`
Affects the content and text color when the `Button` is applied with `.outline`
or `.clean` CSS class tags. The acceptable value shall be compatible with
`color:` CSS field. The default is `var(--color-primary-500)`.



### `--button-color-focus`
Affects the content and text color when the `Button` is hovered or focused. The
acceptable value shall be compatible with `color:` CSS field. The default is
`var(--color-grey-50)`.



### `--button-background-focus`
Affects the background styling when the `Button` is hovered or focused. The
acceptable value shall be compatible with `background-focused:` CSS field. The
default is `var(--color-contrast-700)`.



### `--button-border-color-focus`
Affects the border color when the `Button` is hovered or focused. The acceptable
value shall be compatible with `border-color:` CSS field. The default is
`var(--color-contrast-700)`.



### `--button-opacity-disabled`
Affects the opacity visibility when the `Button` is disabled including anchor
link (`<a>`). The acceptable value shall be compatible with `opacity:` CSS
field. The default is `.5`.




## Javascript
This component does not rely on any Javascript.




## Sass
Depending on release version, the Sass files work differently. Bissetii does not
package Sass codes explictly so please view them via the git repository.



### `v1.13.0` and Above
Bissetii uses Dart Sass to compile the styling Sass codes into CSS file. This
component's Sass codes are available at the following location:

```
pkg/components/internal/button
```



### `v1.12.5` and Below
{{< note "warning" "Deprecation Notice" >}}
Starting from version `v1.13.0`, this version of Sass is no longer used and the
support is scheduled to be removed on **July 31, 2022**.
{{< /note >}}

The Sass scripts responsible for styling the component are located in:

```bash
assets/css/bissetii/modules/core/_Button.scss
```

Currently, the Sass script offers 3 types of styling:

1. `normal` - light background to contrast heavy background.
2. `pinpoint` - heavy background to contrast heavy background.
3. `clean` - colored text with transparent background.

For colors, they are currently limited to theme-based colors.




## Researches
Here are the researches done to ensure `Button` component meets the necessary
quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `Button` component in Bissetii. If you need more feature or
need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
