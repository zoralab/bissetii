+++
date = "2020-02-14T11:32:31+08:00"
title = "Color Component"
description = """
Bissetii provides a single config method called `Color` component to switch from
its main bambooish green color scheme to other color available color scheme.
This allows users to quickly change the theme color without needing to break any
of the existing features.
"""
keywords = ["color", "components", "html", "w3c", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[amp]
modules = [
        # Example: "amp-sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnail]
url = "/img/thumbnails/default.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Color (DEPRECATED)"
pre = "❗"
weight = 1


[schema]
selectType = "WebPage"
+++

# `Color` Component
Bissetii provides a single config method called `Color` component to switch from
its main bambooish green color scheme to other color available color scheme.
This allows users to quickly change the theme color without needing to break any
of the existing features.

This component is available starting from `v1.11.0`.

{{< note "warning" "DEPRECATION NOTICE" >}}
This component is deprecated starting from version `v1.13.0` and its bamboo
green color is merged into [Root Component]({{< link "/components/root"
"this" "url-only" />}}). All other colors shall not be supported beyond this
point in favor of CSS variables alterations.

This component will be rescinded on **July 31, 2022**.
{{< /note >}}


## Javascript Parts
This component does not depend on any Javascript.



## HTML Parts
This component does not depend on any HTML.



## Sass Styling Parts
The Sass script responsible to facilitate color set are located in:

```bash
assets/css/bissetii/modules/core/color/*
```

These color schemes are mainly to facilitate usable color codes through defined
variable sets. To change color theme, simply set the following config:

```sass
$config-color: <codename>
```


### Bamboo Green
Bamboo green is themed against the bamboo's green and yellow color pairs. It
is the default and fallback color scheme in Bissetii Sass rendering. The
config codename is:

```sass
$config-color: bamboo-green
```

The styling color is as follows:

{{< renderHTML "html" "amp" >}}
<table>
<thead>
	<tr>
		<th>Role</th>
		<th>Lightest</th>
		<th>Light</th>
		<th>Normal</th>
		<th>Heavy</th>
		<th>Heaviest</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>Primary</td>
		<td>N/A</td>
		<td style="background-color:#ccff90;color:black;">#ccff90</td>
		<td style="background-color:#1ea82b;color:white;">#1ea82b</td>
		<td style="background-color:#012104;color:white;">#012104</td>
		<td>N/A</td>
	</tr>
	<tr>
		<td>Scondary, Tertiary, Quarternary, Quinary</td>
		<td style="background-color:#606c76;color:white;">#606c76</td>
		<td style="background-color:#f4f5f6;color:black;">#f4f5f6</td>
		<td style="background-color:#d1d1d1;color:black;">#d1d1d1</td>
		<td style="background-color:#e1e1e1;color:black;">#e1e1e1</td>
		<td>N/A</td>
	</tr>
	<tr>
		<td>Contrast</td>
		<td>N/A</td>
		<td style="background-color:#fff59d;color:black;">#fff59d</td>
		<td style="background-color:#ffeb3b;color:black;">#ffeb3b</td>
		<td style="background-color:#f9c22d;color:black;">#f9c22d</td>
		<td>N/A</td>
	</tr>
	<tr>
		<td>Typography (Themed BG)</td>
		<td style="background-color:#012104;color:#fcfcfc;">#fcfcfc</td>
		<td style="background-color:#012104;color:#d1d1d1;">#d1d1d1</td>
		<td style="background-color:#1ea82b;color:#606c76;">#606c76</td>
		<td style="background-color:#ccff90;color:#3e454c;">#3e454c</td>
		<td style="background-color:#ccff90;color:#272c30;">#272c30</td>
	</tr>
	<tr>
		<td>Typography (White BG)</td>
		<td style="background-color: white;color: #fcfcfc;">#fcfcfc</td>
		<td style="background-color: white;color: #d1d1d1;">#d1d1d1</td>
		<td style="background-color: white;color: #606c76;">#606c76</td>
		<td style="background-color: white;color: #3e454c;">#3e454c</td>
		<td style="background-color: white;color: #272c30;">#272c30</td>
	</tr>
	<tr>
		<td>Green Indicator</td>
		<td style="background-color: #e6fae6;">#e6fae6</td>
		<td style="background-color: #6ce569;">#6ce569</td>
		<td style="background-color: #76ff03;color: black;">#76ff03</td>
		<td style="background-color: #079404;color: white;">#079404</td>
		<td style="background-color: #022a01;color: white;">#022a01</td>
	</tr>
	<tr>
		<td>Yellow Indicator</td>
		<td style="background-color: #ffffe5;color: black;">#ffffe5</td>
		<td style="background-color: #ffff66;color: black;">#ffff66</td>
		<td style="background-color: #ffff00;color: black;">#ffff00</td>
		<td style="background-color: #ffea00;color: black;">#ffea00</td>
		<td style="background-color: #ffcc00;color: black;">#ffcc00</td>
	</tr>
	<tr>
		<td>Red Indicator</td>
		<td style="background-color: #ffe5e5;color: black;">#ffe5e5</td>
		<td style="background-color: #ff6666;color: black;">#ff6666</td>
		<td style="background-color: #ff0000;color: white;">#ff0000</td>
		<td style="background-color: #b20000;color: white;">#b20000</td>
		<td style="background-color: #330000;color: white;">#330000</td>
	</tr>
	<tr>
		<td>Blue Indicator</td>
		<td style="background-color: #e5e5ff;color: black;">#e5e5ff</td>
		<td style="background-color: #6666ff;color: white;">#6666ff</td>
		<td style="background-color: #0000ff;color: white;">#0000ff</td>
		<td style="background-color: #0000b2;color: white;">#0000b2</td>
		<td style="background-color: #000033;color: white;">#000033</td>
	</tr>
</tbody>
</table>
{{< /renderHTML >}}


### Deep Blue Sky
Deep blue sky is themed against the seriousness of air force and enforcer's
integrity. It brings the chilling and noble blue out of Bissetii.
The config codename is:

```sass
$config-color: deep-blue-sky
```

The styling color is as follows:

{{< renderHTML "html" "amp" >}}
<table>
<thead>
	<tr>
		<th>Role</th>
		<th>Lightest</th>
		<th>Light</th>
		<th>Normal</th>
		<th>Heavy</th>
		<th>Heaviest</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>Primary</td>
		<td>N/A</td>
		<td style="background-color:#24daf2;color:black;">#24daf2</td>
		<td style="background-color:#1565c0;color:white;">#1565c0</td>
		<td style="background-color:#000030;color:white;">#000030</td>
		<td>N/A</td>
	</tr>
	<tr>
		<td>Scondary, Tertiary, Quarternary, Quinary</td>
		<td style="background-color:#606c76;color:white;">#606c76</td>
		<td style="background-color:#f4f5f6;color:black;">#f4f5f6</td>
		<td style="background-color:#d1d1d1;color:black;">#d1d1d1</td>
		<td style="background-color:#e1e1e1;color:black;">#e1e1e1</td>
		<td>N/A</td>
	</tr>
	<tr>
		<td>Contrast</td>
		<td>N/A</td>
		<td style="background-color:#fff59d;color:black;">#fff59d</td>
		<td style="background-color:#ffeb3b;color:black;">#ffeb3b</td>
		<td style="background-color:#f9c22d;color:black;">#f9c22d</td>
		<td>N/A</td>
	</tr>
	<tr>
		<td>Typography (Themed BG)</td>
		<td style="background-color:#000030;color:#fcfcfc;">#fcfcfc</td>
		<td style="background-color:#000030;color:#d1d1d1;">#d1d1d1</td>
		<td style="background-color:#1565c0;color:#606c76;">#606c76</td>
		<td style="background-color:#24daf2;color:#3e454c;">#3e454c</td>
		<td style="background-color:#24daf2;color:#272c30;">#272c30</td>
	</tr>
	<tr>
		<td>Typography (White BG)</td>
		<td style="background-color: white;color: #fcfcfc;">#fcfcfc</td>
		<td style="background-color: white;color: #d1d1d1;">#d1d1d1</td>
		<td style="background-color: white;color: #606c76;">#606c76</td>
		<td style="background-color: white;color: #3e454c;">#3e454c</td>
		<td style="background-color: white;color: #272c30;">#272c30</td>
	</tr>
	<tr>
		<td>Green Indicator</td>
		<td style="background-color: #e6fae6;">#e6fae6</td>
		<td style="background-color: #6ce569;">#6ce569</td>
		<td style="background-color: #76ff03;color: black;">#76ff03</td>
		<td style="background-color: #079404;color: white;">#079404</td>
		<td style="background-color: #022a01;color: white;">#022a01</td>
	</tr>
	<tr>
		<td>Yellow Indicator</td>
		<td style="background-color: #ffffe5;color: black;">#ffffe5</td>
		<td style="background-color: #ffff66;color: black;">#ffff66</td>
		<td style="background-color: #ffff00;color: black;">#ffff00</td>
		<td style="background-color: #ffea00;color: black;">#ffea00</td>
		<td style="background-color: #ffcc00;color: black;">#ffcc00</td>
	</tr>
	<tr>
		<td>Red Indicator</td>
		<td style="background-color: #ffe5e5;color: black;">#ffe5e5</td>
		<td style="background-color: #ff6666;color: black;">#ff6666</td>
		<td style="background-color: #ff0000;color: white;">#ff0000</td>
		<td style="background-color: #b20000;color: white;">#b20000</td>
		<td style="background-color: #330000;color: white;">#330000</td>
	</tr>
	<tr>
		<td>Blue Indicator</td>
		<td style="background-color: #e5e5ff;color: black;">#e5e5ff</td>
		<td style="background-color: #6666ff;color: white;">#6666ff</td>
		<td style="background-color: #0000ff;color: white;">#0000ff</td>
		<td style="background-color: #0000b2;color: white;">#0000b2</td>
		<td style="background-color: #000033;color: white;">#000033</td>
	</tr>
</tbody>
</table>
{{< /renderHTML >}}


### Purple Might
Purple Might is themed against the original color scheme adopted from
[Milligram Project](https://milligram.io/) for nostalgia memory. The config
codename is:

```sass
$config-color: purple-might
```

The styling color is as follows:

{{< renderHTML "html" "amp" >}}
<table>
<thead>
	<tr>
		<th>Role</th>
		<th>Lightest</th>
		<th>Light</th>
		<th>Normal</th>
		<th>Heavy</th>
		<th>Heaviest</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>Primary</td>
		<td>N/A</td>
		<td style="background-color:#e1bee7;color:black;">#e1bee7</td>
		<td style="background-color:#ab47bc;color:white;">#ab47bc</td>
		<td style="background-color:#38006b;color:white;">#38006b</td>
		<td>N/A</td>
	</tr>
	<tr>
		<td>Scondary, Tertiary, Quarternary, Quinary</td>
		<td style="background-color:#606c76;color:white;">#606c76</td>
		<td style="background-color:#f4f5f6;color:black;">#f4f5f6</td>
		<td style="background-color:#d1d1d1;color:black;">#d1d1d1</td>
		<td style="background-color:#e1e1e1;color:black;">#e1e1e1</td>
		<td>N/A</td>
	</tr>
	<tr>
		<td>Contrast</td>
		<td>N/A</td>
		<td style="background-color:#fff59d;color:black;">#fff59d</td>
		<td style="background-color:#ffeb3b;color:black;">#ffeb3b</td>
		<td style="background-color:#f9c22d;color:black;">#f9c22d</td>
		<td>N/A</td>
	</tr>
	<tr>
		<td>Typography (Themed BG)</td>
		<td style="background-color:#38006b;color:#fcfcfc;">#fcfcfc</td>
		<td style="background-color:#38006b;color:#d1d1d1;">#d1d1d1</td>
		<td style="background-color:#ab47bc;color:#606c76;">#606c76</td>
		<td style="background-color:#e1bee7;color:#3e454c;">#3e454c</td>
		<td style="background-color:#e1bee7;color:#272c30;">#272c30</td>
	</tr>
	<tr>
		<td>Typography (White BG)</td>
		<td style="background-color: white;color: #fcfcfc;">#fcfcfc</td>
		<td style="background-color: white;color: #d1d1d1;">#d1d1d1</td>
		<td style="background-color: white;color: #606c76;">#606c76</td>
		<td style="background-color: white;color: #3e454c;">#3e454c</td>
		<td style="background-color: white;color: #272c30;">#272c30</td>
	</tr>
	<tr>
		<td>Green Indicator</td>
		<td style="background-color: #e6fae6;">#e6fae6</td>
		<td style="background-color: #6ce569;">#6ce569</td>
		<td style="background-color: #76ff03;color: black;">#76ff03</td>
		<td style="background-color: #079404;color: white;">#079404</td>
		<td style="background-color: #022a01;color: white;">#022a01</td>
	</tr>
	<tr>
		<td>Yellow Indicator</td>
		<td style="background-color: #ffffe5;color: black;">#ffffe5</td>
		<td style="background-color: #ffff66;color: black;">#ffff66</td>
		<td style="background-color: #ffff00;color: black;">#ffff00</td>
		<td style="background-color: #ffea00;color: black;">#ffea00</td>
		<td style="background-color: #ffcc00;color: black;">#ffcc00</td>
	</tr>
	<tr>
		<td>Red Indicator</td>
		<td style="background-color: #ffe5e5;color: black;">#ffe5e5</td>
		<td style="background-color: #ff6666;color: black;">#ff6666</td>
		<td style="background-color: #ff0000;color: white;">#ff0000</td>
		<td style="background-color: #b20000;color: white;">#b20000</td>
		<td style="background-color: #330000;color: white;">#330000</td>
	</tr>
	<tr>
		<td>Blue Indicator</td>
		<td style="background-color: #e5e5ff;color: black;">#e5e5ff</td>
		<td style="background-color: #6666ff;color: white;">#6666ff</td>
		<td style="background-color: #0000ff;color: white;">#0000ff</td>
		<td style="background-color: #0000b2;color: white;">#0000b2</td>
		<td style="background-color: #000033;color: white;">#000033</td>
	</tr>
</tbody>
</table>
{{< /renderHTML >}}
