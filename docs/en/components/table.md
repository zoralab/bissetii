+++
date = "2020-02-10T11:44:48+08:00"
title = "Table Component"
description = """
The `Table` component is a simple way to present row-column matrix-based data
since the early HTML. It tabulates data in an organize way for searches and
comparison. Bissetii supports mobile friendliness rendering table, allowing it
to be friendly for viewing table in mobile devices.
"""
keywords = ["table", "components", "html", "w3c", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Table"
pre = "🧾"
weight = 1


[schema]
selectType = "WebPage"
+++

# `Table` Component
{{% param "description" %}}

This component is available starting from `v1.11.0`.




## Hugo
Hugo supports `Table` seamlessly so no special development is required.




## Go
Coming Soon.




## HTML
Bissetii supports `Table` component seamlessly across multiple output formats.
Additionally, starting from version `v1.13.0`, the use of CSS variable is vital
for its upgrade from `v1.12.5`.

The HTML codes for this component are the same for Vanilla HTML5 and AMPHTML.
Also, Bissetii is very **strict** with `<thead>` and `<tbody>` conventions for
styling the appropriate titles.

Bissetii uses the conventional HTML table syntax instead of the custom types
HTML. To facilitate mobile-friendliness, the y-direction overflow is enabled
with scrolling automatically.

{{< note "warning" "Be Careful" >}}
Do not use table for layout. Use
[Grid Component]({{< link "/components/grid" "this" "url-only" />}}) instead.
{{< /note >}}

```html {linenos=table,hl_lines=[],linenostart=1}
<table>
	<thead>
		<tr>
			<th>Header 1</th>
			<th>Header 2</th>
			<th>Header 3</th>
			<th>Header 4</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><p>Data 1,1</p></td>
			<td><p>Data 1,2</p></td>
			<td><p>Data 1,3</p></td>
			<td><p>Data 1,4</p></td>
		</tr>
		<tr>
			<td><p>Data 2,1</p></td>
			<td><p>Data 2,2</p></td>
			<td><p>Data 2,3</p></td>
			<td><p>Data 2,4</p></td>
		</tr>
		...
	</tbody>
</table>
```

Here is a rendering example:

{{< renderHTML "html" "amp" >}}
<table>
	<thead>
		<tr>
			<th>Header 1</th>
			<th>Header 2</th>
			<th>Header 3</th>
			<th>Header 4</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><p>Data 1,1</p></td>
			<td><p>Data 1,2</p></td>
			<td><p>Data 1,3</p></td>
			<td><p>Data 1,4</p></td>
		</tr>
		<tr>
			<td><p>Data 2,1</p></td>
			<td><p>Data 2,2</p></td>
			<td><p>Data 2,3</p></td>
			<td><p>Data 2,4</p></td>
		</tr>
		<tr>
			<td><p>Data 3,1</p></td>
			<td><p>Data 3,2</p></td>
			<td><p>Data 3,3</p></td>
			<td><p>Data 3,4</p></td>
		</tr>
		<tr>
			<td><p>Data 4,1</p></td>
			<td><p>Data 4,2</p></td>
			<td><p>Data 4,3</p></td>
			<td><p>Data 4,4</p></td>
		</tr>
	</tbody>
</table>
{{< /renderHTML >}}




## CSS
Bissetii provided a list of CSS variables dedicated for `Table` styling
alteration without needing to recompile Sass. These variables are best applied
directly to the `<table>` HTML tags. Example:

```html {linenos=table,hl_lines=[],linenostart=1}
<table style="--dl-padding: 25px;">
	...
</table>
```




## Javascript
This component does not rely on any Javascript.




## Sass
Depending on release version, the Sass files work differently. Bissetii does not
package Sass codes explictly so please view them via the git repository.



### `v1.13.0` and Above
Bissetii uses Dart Sass to compile the styling Sass codes into CSS file. This
component's Sass codes are available at the following location:

```
pkg/components/internal/table
```



### `v1.12.5` and Before
{{< note "warning" "Deprecation Notice" >}}
Starting from version `v1.13.0`, this version of Sass is no longer used and the
support is scheduled to be removed on **July 31, 2022**.
{{< /note >}}
The Sass scripts responsible for styling the component are located in:

```
assets/css/bissetii/modules/core/_Table.scss
```




## Researches
Here are the researches done to ensure `Table` component meets the necessary
quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `Table` component in Bissetii. If you need more feature or
need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
