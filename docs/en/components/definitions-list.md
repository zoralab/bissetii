+++
date = "2020-02-12T14:29:18+08:00"
title = "DefList Component"
description = """
`DefList` (Definition List) is the component that defines a list of items.
This is commonly used for arconym definitions and quick name-value labelling.
Bissetii supportes `DefList` seemlessly with its W3C tags out of the box.
"""
keywords = ["definitions-list", "components", "html", "w3c", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Definitions List"
pre = "🗒️"
weight = 1


[schema]
selectType = "WebPage"
+++

# `DefList` Component
{{% param "description" %}}

This component is available starting from `v1.11.0`.




## Hugo
Hugo supports `DefList` seamlessly so no special development is required.




## Go
Coming Soon.




## HTML
Bissetii supports `DefList` component seamlessly across multiple output formats.
Additionally, starting from version `v1.13.0`, the use of CSS variable is vital
for its upgrade from `v1.12.5`.

Bissetii HTML codes for this component are the same for Vanilla HTML5 and
AMPHTML.

Bissetii facilitates multiple `<dd>` definition list by default. One HTML
example pattern would be:

```html
<dl>
	<dt>Sun</dt>
	<dd>The core of our solar system.</dd>
	<dd>
		Primary source of energy. Its body is primarily consisting of
		hot plasma consistently dumping light and heat energies to its
		surrounding.
	</dd>
	<dd>
		Provides heats and gives life to plants on Earth.
	</dd>

	<dt>Mercury</dt>
	<dd>FIRST planet away from Sun.</dd>

	<dt>Venus</dt>
	<dd>SECOND planet away from Sun.</dd>

	<dt>Earth</dt>
	<dd>THIRD planet away from the Sun.</dd>
	<dd>Contains organic life on and inside the planet.</dd>
</dl>
```

This will render as:

{{< renderHTML "html" "amp" >}}
<dl>
	<dt>Sun</dt>
	<dd>The core of our solar system.</dd>
	<dd>
		Primary source of energy. Its body is primarily consisting of
		hot plasma consistently dumping light and heat energies to its
		surrounding.
	</dd>
	<dd>
		Provides heats and gives life to plants on Earth.
	</dd>

	<dt>Mercury</dt>
	<dd>FIRST planet away from Sun.</dd>

	<dt>Venus</dt>
	<dd>SECOND planet away from Sun.</dd>

	<dt>Earth</dt>
	<dd>THIRD planet away from the Sun.</dd>
	<dd>Contains organic life on and inside the planet.</dd>
</dl>
{{< /renderHTML >}}




## CSS
Bissetii provided a list of CSS variables dedicated for list styling alteration
without needing to recompile Sass. These variables are best applied directly to
the List HTML tags. Example:

```html
<dl style="--dl-padding: 25px;">
	<dt style="--dt-font-weight: lighter;">...</dt>
	<dd style="--dd-margin: 100px;">...</dd>
	...
</dl>
```



### `--dl-overflow`
Set the `DefList` overflow. The acceptable value shall be compatible with
`overflow:` CSS field. The default is `auto`.



### `--dl-padding`
Set the `DefList` padding. The acceptable value shall be compatible with
`padding:` CSS field. The default is `0.5rem`.



### `--dl-border`
Set the `DefList` border. The acceptable value shall be compatible with
`border-top:` CSS field. The default is `1px solid var(--color-primary-500)`.



### `--dt-font-weight`
Set the term (`<dt>`)'s font weight. The acceptable value shall be compatible
with `font-weight:` CSS field. The default is `bold`.



### `--dt-margin`
Set the term (`<dt>`)'s margin spacing. The acceptable value shall be compatible
with `margin:` CSS field. The default is `0.5rem`.



### `--dt-padding`
Set the term (`<dt>`)'s padding spacing. The acceptable value shall be
compatible with `padding:` CSS field. The default is `0.5rem`.



### `--dt-border-top`
Set the term (`<dt>`)'s top border styling. The acceptable value shall be
compatible with `border-top:` CSS field. The default is
`1px solid var(--color-grey-400)` for all and `none` for the first of type.



### `--dd-margin`
Set the definition (`<dd>`)'s margin spacing. The acceptable value shall be
compatible with `margin:` CSS field. The default is
`0 0 0 2.5rem`.



### `--dd-padding`
Set the definition (`<dd>`)'s padding spacing. The acceptable value shall be
compatible with `padding:` CSS field. The default is `0 0 0 5rem`.




## Javascript
This component does not rely on any Javascript.




## Sass
Depending on release version, the Sass files work differently. Bissetii does not
package Sass codes explictly so please view them via the git repository.



### `v1.13.0` and Above
Bissetii uses Dart Sass to compile the styling Sass codes into CSS file. This
component's Sass codes are available at the following location:

```
pkg/components/internal/deflist
```



### `v1.12.5` and Before
{{< note "warning" "Deprecation Notice" >}}
Starting from version `v1.13.0`, this version of Sass is no longer used and the
support is scheduled to be removed on **July 31, 2022**.
{{< /note >}}
The Sass scripts responsible for styling the component are located in:

```
assets/css/bissetii/modules/core/_DefinitionList.scss
```




## Researches
Here are the researches done to ensure `DefList` component meets the necessary
quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `DefList` component in Bissetii. If you need more feature or
need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
