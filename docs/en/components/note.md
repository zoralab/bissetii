+++
date = "2020-08-02T12:02:10+08:00"
title = "Note"
description = """
`Note` component is the inline content snippet that is indirectly related to
the main content. This is usually used for highlighting important reminders,
warning, and to convey and error. Bissetii supports a number of quick styling
notes using CSS class tag.
"""
keywords = ["notes", "components", "html", "w3c", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "Note (BETA)"
pre = "🗒️"
weight = 1


[schema]
selectType = "WebPage"
+++

# `{{% param "title" %}}` Component
{{% param "description" %}}

This component was inspired from an idea created by
[Swarup Kumar Kuila](https://codepen.io/uiswarup/pen/RwNraeW). Bissetii adopted
and modified the idea heavily to keep quite a large number of aspects to simple
and nice to use.

This component is available starting from `v1.12.1`.

{{< note "warning" "Beta Testing Notice" >}}
This is a fairly new shortcode and it is still under user-level API beta
testing. Hence, there may be some **major updates in the future that are not
backward compatible**. Please use at your own risk.
{{< /note >}}




## Hugo
Bissetii prepared a number of Hugo interfaces to generate `Note` component
seamlessly across all output formats.



### Shortcodes
By default, it is hard to process images across multiple outputs while
maintaining a single input format. Hence, Bissetii prepared the following
shortcodes to standardize the image rendering. These shortcodes are specific to
Bissetii which works differently from Hugo.


#### `note` Shortcode
The `note` shortcode is for creating note quickly using Bissetii's `Note`
component's quick styling. The shortcode pattern is as follows:

```md {linenos=table,hl_lines=[],linenostart=1}
{{</* note [TYPE] [TITLE] [ICON_URL] */>}}
	[CONTENT]
{{</* /note */>}}
```

1. `TYPE` - **COMPULSORY**. The type of quick styling. To use default quick
            styling, use `info`. Other options are: `warning`, `success`, and
            `error`.
2. `TITLE` - **COMPULSORY**. The title of the note.
3. `ICON_URL` - **OPTIONAL**. The custom icon image source URL. Relative URL
               shall be reconstructed automatically to absolute URL.
4. `CONTENT` - **COMPULSORY**. The content of the note. It **MUST ONLY** be
               either in Markdown format or HTML format. Markdown format shall
               be rendered first and if HTML code is detected, it shall fall
               back to HTML format rendering.

Example usage with shortcode inside shortcode:

```md {linenos=table,hl_lines=[],linenostart=1}
{{</* note warning "Be Careful" */>}}
You should only use this just once. Otherwise, a restart is required. To
restart, refers to the following image:
	{{</* image ... */>}}
{{</* /note */>}}
```



### Customizing Quick Styling Icon
To customize the quick styling icon across `Note` component via Hugo interface,
you need to supply the necessary input into the `data/bissetii/note.toml` data
file. An example of the data file structure is as follows:

```toml {linenos=table,hl_lines=[],linenostart=1}
[info]
URL = "/img/icons/info.svg"    # image source URL (relative will be converted)
Width="25"                     # px without unit
Height="25"                    # px without unit
Class=""                       # additional CSS class tag

[warning]
URL = "/img/icons/warning.svg" # image source URL (relative will be converted)
Width="25"                     # px without unit
Height="25"                    # px without unit
Class=""                       # additional CSS class tag

[success]
URL = "/img/icons/success.svg" # image source URL (relative will be converted)
Width="25"                     # px without unit
Height="25"                    # px without unit
Class=""                       # additional CSS class tag

[error]
URL = "/img/icons/error.svg"   # image source URL (relative will be converted)
Width="25"                     # px without unit
Height="25"                    # px without unit
Class=""                       # additional CSS class tag
```

By default, all customizations are disabled (commented out) and Hugo will revert
to using Bissetii's default note icons.




## Go
Coming Soon.




## HTML
Bissetii supports `Note` component via the use of `<aside>` HTML tag.
Additionally, starting from version `v1.13.0`, the use of CSS variable is vital
for its upgrade from `v1.12.5`.

### Output Format
Depending on the need of imagery icon, the image component is solely depends
the desired output format. In general, the component is encapsulated with the
following HTML codes pattern:

```html {linenos=table,hl_lines=[],linenostart=1}
<aside class="note">
	... Icon HTML code here with class="icon" ...
	<p class="title">Note Title Here</p>
	<div class="body">
		... HTML body content here ...
	</div>
</aside>
```

1. The note icon **MUST** have a `.icon` CSS class tag for layout usage.
2. The note body **MUST** be encapsulated inside a `<div>` HTML tag with `.body`
CSS class for layout usage.
3. The note title **MUST** have a `.title` CSS class tag. (Tip: avoid the use of
heading like `<h1>`, `<h2>` to preserve proper page schema).


#### Vanilla HTML5
For vanilla HTML5, should there a need to use image icon, you need to use
`<img>` HTML tag alongside with `.icon` CSS class tag. The recommended size
is `25x25` **and shall be specified within the `<img>` tag to avoid disruptive
rendering**. The following would be the recommended HTML code pattern:

```html {linenos=table,hl_lines=[],linenostart=1}
<aside class="note">
	<img class="icon" alt="image icon alternate text" src="..."
		width="25"
		height="25" />
	<p class="title">Note Title Here</p>
	<div class="body">
		... HTML body content here ...
	</div>
</aside>
```


#### AMPHTML
For vanilla HTML5, should there a need to use image icon, you need to use
`<amp-img>` AMPHTML tag alongside with `.icon` CSS class tag. The recommended
size is `25x25` **and shall be specified within the `<amp-img>` tag as required
by AMPHTML**. The following would be the recommended HTML code pattern:

```html {linenos=table,hl_lines=[],linenostart=1}
<aside class="note">
	<amp-img class="icon" alt="image icon alternate text" src="..."
		width="25"
		height="25"></amp-img>
	<p class="title">Note Title Here</p>
	<div class="body">
		... HTML body content here ...
	</div>
</aside>
```


#### Universally Applicable
To keep notes simple, the use of inline `<svg>` is allowed as long as it has
`.icon` CSS class tag applied to it. Using this method produces a more modular
`Note` component that is usable anywhere regardless of HTML output format.
The recommended default svg size is `25x25` **and shall be specified within the
`<svg>` tag to avoid disruptive rendering**. An example would be:

```html {linenos=table,hl_lines=[],linenostart=1}
<aside class="note">
	<svg class="icon" width="25" height="25"...> SVG DRAWING HERE </svg>
	<p class="title">Note Title Here</p>
	<div class="body">
		... HTML body content here ...
	</div>
</aside>
```



### Quick Note Styling
Since `Note` component is small and usually used in ad-hoc manner, Bissetii
prepared a number of quick styling so that the user only focus with building the
note content and not styling customization.


#### Default Style
The default styling is to denote the note as informative. Here is an example
of the default styling:

{{< note "" "Important Note" >}}
<p>This is the content. It is aligned with justified styling. You may write
anything inside here as HTML.</p>
{{< /note >}}


#### Warning Style
Warning style is to denote the note requires moderate level of attention. The
styling is applied by appending `.warning` CSS class tag after the `.note` CSS
class tag such as:

```html {linenos=table,hl_lines=[],linenostart=1}
<aside class="note warning">
	...
</aside>
```

Here is an example of the warning styling:

{{< note "warning" "Attention Required" >}}
<p>This is the content. It is aligned with justified styling. You may write
anything inside here as HTML.</p>
{{< /note >}}


#### Success Style
Success style is to denote the note has a good gesture information. The styling
is applied by appending `.success` CSS class tag after the `.note` CSS class tag
such as:

```html {linenos=table,hl_lines=[],linenostart=1}
<aside class="note success">
	...
</aside>
```

Here is an example of the error styling:

{{< note "success" "Data Submitted Safely" >}}
<p>This is the content. It is aligned with justified styling. You may write
anything inside here as HTML.</p>
{{< /note >}}


#### Error Style
Error style is to denote the note with highest level of attention. The styling
is applied by appending `.error` CSS class tag after the `.note` CSS class tag
such as:

```html {linenos=table,hl_lines=[],linenostart=1}
<aside class="note error">
	...
</aside>
```

Here is an example of the error styling:

{{< note "error" "Network Failure" >}}
<p>This is the content. It is aligned with justified styling. You may write
anything inside here as HTML.</p>
{{< /note >}}




## CSS
Bissetii provided a list of CSS variables dedicated for `Note` styling
alteration without needing to recompile Sass. These variables are best applied
directly to the `<aside>` HTML tags. Example:

```html
<aside class="note" style="--note-margin: 0;">
	...
</aside>
```

### `--note-layout`
Affects the CSS grid layout setting for the note. The acceptable value shall
be compatible with `grid:` CSS field. The default is:

```css
"note-icon		note-title"	minmax(0, max-content)
"note-content		note-content"	auto
/minmax(0, max-content)	1fr
```



### `--note-layout-gap`
Affects the gap spacing between `.icon`, `.title`, and `.content` CSS objects
horizontally and vertically. The acceptable value shall be compatible with
`gap:` CSS field. The default is `1rem`.



### `--note-padding`
Affects the padding spacing of the note. The acceptable value shall be
compatible with `padding:` CSS field. The default is `1.5rem`.



### `--note-margin`
Affects the margin spacing of the note. The acceptable value shall be compatible
with `margin:` CSS field. The default is: `1.5rem 0`.



### `--note-background`
Affects the background styling of the note. The acceptable value shall be
compatible with `background:` CSS field. The default are:

1. default: `var(--color-blue-100)`
2. `.warning` CSS class tag: `var(--color-yellow-100)`
3. `.success` CSS class tag: `var(--color-green-100)`
4. `.error` CSS class tag: `var(--color-red-100)`



### `--note-border-radius`
Affects the edge roundness of the note. The acceptable value shall be compatible
with `border-radius:` CSS field. The default is `.8rem`.



### `--note-border`
Affects the border styling of the note. The acceptable value shall be compatible
with `border-top:` CSS field. The default is
`.1rem solid var(--color-blue-700)`.



### `--note-border-top`
Affects the border-top styling of the note. The acceptable value shall be
compatible with `border-top:` CSS field. The default is
`.4rem solid var(--color-blue-700)`.



### `--note-title-margin`
Affects the margin spacing of the text for `.title` CSS class object. The
acceptable value shall be compatible with `margin:` CSS field. The default is
`auto 0`.



### `--note-title-font-weight`
Affects the font weight of the text for `.title` CSS class object. The
acceptable value shall be compatible with `font-weight:` CSS field. The default
is `bold`.



### `--note-title-align-self`
Affects the vertical alignment of the text for `.title` CSS class object. The
acceptable value shall be compatible with `align-self:` CSS field. The default
is `center`.



### `--note-title-justify-self`
Affects the horizontal alignment of the text for `.title` CSS class object. The
acceptable value shall be compatible with `justify-self:` CSS field. The default
is `left`.



### `--note-title-color`
Affects the color of the text for `.title` CSS class object. The acceptable
value shall be compatbile with `color:` CSS field. The default are:

1. default: `var(--color-blue-700)`
2. `.warning` CSS class tag: `var(--color-yellow-700)`
3. `.success` CSS class tag: `var(--color-green-700)`
4. `.error` CSS class tag: `var(--color-red-700)`



### `--note-icon-margin`
Affects the margin spacing of the `.icon` CSS class object. The acceptable value
shall be compatible with `margin:` CSS field. The default is `auto`.




## Javascript
This component does not rely on any Javascript.




## Sass
Depending on release version, the Sass files work differently. Bissetii does not
package Sass codes explictly so please view them via the git repository.



### `v1.13.0` and Above
Bissetii uses Dart Sass to compile the styling Sass codes into CSS file. This
component's Sass codes are available at the following location:

```
pkg/components/internal/note
```




## Researches
Here are the researches done to ensure `Note` component meets the necessary
quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `Note` component in Bissetii. If you need more feature or
need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
