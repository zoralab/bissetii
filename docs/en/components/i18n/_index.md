+++
date = "2021-08-08T10:40:32+08:00"
title = "i18n Component"
description = """
`i18n`, shorts for internationalization, is the component to support
multi-lingual websites with language specific tools and parts. Bissetii prepared
a number of CSS styled language tools for easier multi-lingual content
management.
"""
keywords = ["i18n", "components", "html", "w3c", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
        # Example: "amp-sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "i18n (ALPHA)"
pre = "🍱"
weight = 1


[schema]
selectType = "WebPage"
+++

# `i18n` Component
{{% param description %}}

This component is available starting from `v1.11.0`.




## Hugo
Bissetii prepared a number of Hugo interfaces to manage `i18n` component
seamlessly across all output formats.



### Shortcodes
By default, it is hard to process single or multiple card data across multiple
outputs while maintaining a single input format. Hence, Bissetii prepared the
following shortcodes to standardize the `i18n` rendering. These shortcodes are
specific to Bissetii which works differently from Hugo.


#### `translate` Shortcode
To translate a given keyword to a specific language using multilingual
dictionary inside the `data/` directory.

{{< note "warning" "Beta Testing Notice" >}}
This is a fairly new component and it is still under user-level API beta
testing. Hence, there may be some major updates in the future that are
**not backward compatible**. Please **ONLY use at your own risk**.
{{< /note >}}

```md {linenos=table,hl_lines=[],linenostart=1}
{{</* translate "[LANG_DICTONARY]" "[LANG]" "[KEYWORD]" */>}}
```

1. `[LANG_DICTIONARY]` - **COMPULSORY**
	1. Points to the multi-lingual dictionary dataset.
	2. It can be Hugo data pathing (e.g. `lang.dictionaries`).
	3. Dictionary data shall be located inside `data/` directory and not
	   Hugo's `i18n/` directory.
2. `[LANG]` - **COMPULSORY**
	1. Selected dictionary type.
	2. To select current page's language, use `this` as its value.
3. `[KEYWORD]` - **COMPULSORY**
	1. The common keyword search for the translated value.
	2. It can be Hugo data pathing (e.g. `list.section.title`).


Example usage:

```md {linenos=table,hl_lines=[],linenostart=1}
{{</* translate "bissetii.i18n" "this" "list.Section.Description" */>}}
{{</* translate "bissetii.i18n" "zh-hans" "list.Section.Description" */>}}
```

This will render as:

```md {linenos=table,hl_lines=[],linenostart=1}
{{< translate "bissetii.i18n" "this" "list.Section.Description" >}}
{{< translate "bissetii.i18n" "zh-hans" "list.Section.Description" >}}
```




## Go
Coming Soon




## HTML
Bissetii supports `Button` component seamlessly across multiple output formats.
Additionally, starting from version `v1.13.0`, the use of CSS variable is vital
for its upgrade from `v1.12.5`.

Bissetii HTML codes for this component are the same for Vanilla HTML5 and
AMPHTML.

As of now, development is still investigating the requirements for this
component.




## CSS
Bissetii provided a list of CSS variables dedicated for `Button` styling
alteration without needing to recompile Sass.

As of now, development is still investigating the requirements for this
component.




## Javascript
This component does not rely on any Javascript.




## Sass
Depending on release version, the Sass files work differently. Bissetii does not
package Sass codes explictly so please view them via the git repository.

As of now, development is still investigating the requirements for this
component.




## Researches
Here are the researches done to ensure `Button` component meets the necessary
quality assurances:

**SCHEDULED COMING SOON**




## Epilogue
That's all about `i18n` component in Bissetii. If you need more feature or
need to report a bug, please feel free to file an issue at our
[Issue Section](https://gitlab.com/zoralab/bissetii/-/issues).
