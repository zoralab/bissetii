+++
date = "2020-08-03T20:46:57+08:00"
title = "iFrame Component"
description = """
`iFrame` component is a nested browsing context, allowing one to embed another
web page into the current one. This is widely used by may service providers
to offer their third party services without needing heavy backend integrations.
Bissetii styles `iFrame` seemlessly.
"""
keywords = ["iframe", "components", "html", "w3c", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
	"iframe",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.1]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Hugo Theme Module"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Hugo Theme Module"


[menu.main]
parent = "Components"
name = "iFrame (BETA)"
pre = "🔮"
weight = 1


[schema]
selectType = "WebPage"
+++

# `iFrame` Component
{{% param "description" %}}

This component is available starting from `v1.12.1`.

{{< note "warning" "Beta Testing Notice" >}}
This is a fairly new shortcode and it is still under user-level API beta
testing. Hence, there may be some **major updates in the future that are not
backward compatible**. Please use at your own risk.
{{< /note >}}




## Hugo
Bissetii provides a number of useful Hugo interfaces to make content creation
easier.



### Module Extension
To use `iFrame` component, you need to include `"iframe"` extension module
into your front-matter or the global `amp.toml` data file. The code pattern is
as follows:

```toml {linenos=table,hl_lines=[5],linenostart=1}
+++
...
[modules]
extensions = [
	"iframe",
]
...
+++
```

Bissetii will automatically includes the extension module seamlessly in
accordance to the output format (e.g. `amp-iframe` extension module for
AMPHTML).



### Shortcodes
Bissetii prepared a list of shortcodes to make `iFrame` component easier for
content creation using Hugo. Here are some of the available shortcodes related
to `iFrame` component.


#### `iframe` shortcode
`iframe` shortcode creates the correct iframe simultenously for multiple HTML
output formats such as Vanilla HTML5 and AMPHTML. It was created to maintain
sanity over managing a single set of iframe data.

##### Import Module
Before using iframe shortcode, you need to import the module extension called
`"iframe"` inside your page's front matter:

```toml {linenos=table,hl_lines=[5],linenostart=1}
...
[modules]
extensions = [
	...,
	"iframe",
]
...
```

Bissetii will pull the necessary library (e.g. `amp-iframe`) for the respective
output format.

##### Construct Data File
Due to the highly robust, unique, and use-case specific security requirements
for each `iframe`, it is at best to use dataset approach via the Hugo `data/`
directory. `iframe` shortcode recognizes the following `TOML` data file pattern:

```toml {linenos=table,hl_lines=["1-3","7-12", "16-17"],linenostart=1}
[show]
URL = "/"
Sandbox = [
	"allow-scripts",
	"allow-same-origin",
]
ID = "my-iframe-id-01"
Width = "300"
Height = "300"
Class = "custom-iframe"
VanillaHTML = "bissetii-only/iframe/myExample/content.html"
AMPHTML = """
### Markdown Header
This is a markdown message.
"""
Layout = "repsonsive"
Title = "Example iframe Data File"
```

* **Line 1** - defines the dataset title.
* **Line 2** - specifies the `iFrame` URL.
* **Line 3** - specifies the `iFrame` `sandbox=` permissions property. Default
               is empty value. This field is an array of strings holding all
               compatible sandbox security policy.
* **Line 7** - specifies the `iFrame` ID HTML attribute.
* **Line 8** - defines the `width` parameter. This goes by pixel or etc.
               defining on how wide you need it to be. Dynamic values like
               percentage won’t work here since the component is dynamically
               styled. This is **compulsory** for AMP HTML.
* **Line 9** - defines the `height` parameter. This goes by pixel or etc.
               defining on how high you need it to be. Dynamic values like
               percentage won’t work here since the component is dynamically
               styled. This is **compulsory** for AMP HTML.
* **Line 10** - the values for `class=` property.
* **Line 11** - defines the iframe content for Vanilla HTML. It first scans for
                available partial for scanning. If the file is unavailable,
                it will then process the content as Markdown format. Should any
                HTML error occurs, the content will finally be processed as HTML
                format.
* **Line 12** - defines the iframe content partials path for AMP HTML. Its
                choices of processing is the same as `VanillaHTML`.
* **Line 16** - defines iFrame `layout=` property. This is only used in AMP HTML
                where the value is always `responsive`.
* **Line 17** - defines iFrame `title=` property. This is for accessibility
                usage.

##### Call the Shortcode
Once done, you can call the shortcode. The shortcode pattern is as follows:

```markdown
{{</* iframe [PATH-TO-DATA] */>}}
```

`iframe` shortcode takes the following parameters:

1. `[PATH-TO-DATA]` - **COMPULSORY**. the data path pointing to your targeted
                      data file.

Example, for data TOML file shown above located in
`data/bissetii/data/examples/iframes/myExample.toml` (Hugo's data directory path
is `data/`), calling the `show` dataset, the shortcode should be:

```markdown
{{</* iframe "bissetii.data.examples.iframes.myExample.show" */>}}
		⥥
{{% iframe "bissetii.data.examples.iframes.myExample.show" %}}
```

{{< note "info" "Observed Notes" >}}
1. The pathing slashes are replaced with period (`.`).
2. You do not need to specify the Hugo's data directory pathing. It knows how to
   filter it based on your `config.toml` settings.
3. If you organize your data into dataset, include it at the end of the file
   pathing.
4. The result above will get rendered differently if you view it in different
   output format (check out its AMPHTML format).
{{< /note >}}




## Go
Coming Soon.




## HTML
Bissetii uses the standard `<iframe>` or `<amp-iframe>` to render `iFrame`
Component seamlessly. Depending on the type of HTML you're using, Bissetii
suggests the following practices to best suit your use case.



### Vanilla HTML5
For Vanilla HTML5, the code pattern is as follow:

```html {linenos=table,hl_lines=["1-6"],linenostart=1}
<iframe src="https://www.zoralab.com/en-us/"
	sandbox="allow-scripts allow-same-origin"
	width="300"
	height="300"
	title="Example iframe Data File">
	<!-- fallback mode contents -->
</iframe>
```

* **Line 1** - `<iframe>` tag with the web page `URL` into `src`.
* **Line 2** - for security reason, `sandbox=` property should be implemented
               in a compulsory manner.
* **Line 3** - specifies the width of the `<iframe>`. Default is `100%`.
* **Line 4** - specifies the height of the `<iframe>`.
* **Line 5** - specifies the title of the `<iframe>` for accessibility
               compatibility.
* **Line 6** - specifies the fallback content if the `<iframe>` fails.




### AMPHTML
For AMPHTML, you need to import the AMP extension for `amp-iframe` library as
instructed by the
[AMP developers](https://amp.dev/documentation/components/amp-iframe/).

```html
<script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>
```

One key requirement for using `amp-frame` is that the original page length must
attain a minimum of **600px** before using the first one. This can work against
short pages.

The recommended code pattern is as follows:

```html {linenos=table,hl_lines=["1-6"],linenostart=1}
<amp-iframe src="https://www.zoralab.com/en-us/"
	sandbox="allow-scripts allow-same-origin"
	width="300"
	height="300"
	layout="repsonsive">
	<p>This is the internal content</p>
</amp-iframe>
```

* **Line 1** - `<amp-iframe>` tag with the web page `URL` into `src`.
* **Line 2** - for security reason, `sandbox=` property should be implemented
               in a compulsory manner.
* **Line 3** - specifies the width of the `<amp-iframe>`.
* **Line 4** - specifies the height of the `<amp-iframe>`.
* **Line 5** - specifies the AMP layout of the `<amp-iframe>`.
* **Line 6** - specifies the placeholder content if the `<amp-iframe>` is not
               loaded.




## CSS
This section covers all the CSS part for `iFrame` component. Please go through
each sub-section carefully as they affects Bissetii styling directly.



### CSS Class Tags
Bissetii provides the following CSS class tags for re-usable and common styling
alterations.


#### `hidden`
The `.hidden` CSS class tag set the iframe to be invisible from user (assuming
you're using Bissetii's default CSS variables' value). Although hiding within
the page UI, the iframe `display:` CSS tag is not set to `none`.



### CSS Variables
Bissetii provides a number of CSS variables for styling customizations.
Depending on where is the alternation being applied, it can affects either
targeted or all iframes.

Bissetii recommends to provide the CSS variables to the targeted iframe.
Example:

```html {linenos=table,hl_lines=[6],linenostart=1}
<iframe src="https://www.zoralab.com/en-us/"
	sandbox="allow-scripts allow-same-origin"
	width="300"
	height="300"
	title="Example iframe Data File"
	style="iframe-border: 5px solid black;">
	<!-- fallback mode contents -->
</iframe>
```


#### `--iframe-position`
Affects the iframe display positioning. The value must be compliant to
`position:` CSS field. Default is `initial`.

When the iframe has `.hidden` class tag, the default value is changed to
`absolute`.


#### `--iframe-border`
Affects the iframe border. The value must be compliant to `border:` CSS field.
Default is `none`.


#### `--iframe-margin`
Affects the iframe margin. The value must be compliant to `margin:` CSS field.
Default is `1.5rem 0`.

When the iframe has `.hidden` class tag, the default value is changed to `0`.


#### `--iframe-z-index`
Affects the iframe z-index layering. The value must be compliant to `z-index:`
CSS field. Default is `initial`.

When the iframe has `.hidden` class tag, the default value is changed to
`var(--z-index-hidden)`.

{{< note "info" "Note" >}}
See [Root Component]({{< link "/components/root" "this" "url-only" />}}) for
information about `var(--z-index-hidden)`.
{{< /note >}}


#### `--iframe-overflow`
Affects the iframe overflow behavior. The value must be compliant to `overflow:`
CSS field. Default is `auto`.


#### `--iframe-width`
Affects the iframe width. The value must be compliant to `width:` CSS field.
Default is `100%`.


#### `--iframe-min-height`
Affects the iframe mininal height. The value must be compliant to `min-height:`
CSS field. Default is `10rem`.

When the iframe has `.hidden` class tag, the default value is changed to `0` and
it affects `height:` CSS field simultenously.




## Javascript
This component does not depend on any Javascript.




## Sass
Bissetii uses Dart Sass to compile the styling Sass codes into CSS file. This
component’s Sass codes is available at the following location:

```
pkg/components/internal/iframe
```




## Epilogue
That is all about `iFrame` component in Bissetii. If you need more feature or
encountered a bug. Please feel free to contact us by filling an issue ticket at
our [Issue Section](https://gitlab.com/zoralab/bissetii/-/issues). We will be
glad to help you.
