+++
date = "2020-12-01T12:13:48+08:00"
title = "v1.13.0 Release Note"
description = """
Bissetii version `v1.13.0` has a number of changes worth mentioning in its
(this) release note. There are notable changes, migrations notes, deprecation
notes and etc. It's good to read it through before use.
"""
keywords = [ "v1.13.0", "Bissetii", "release", "note" ]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/releases/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Release Note"

[thumbnails.1]
url = "/img/thumbnails/releases/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Release Note"

[thumbnails.2]
url = "/img/thumbnails/default-480x480.png"
type = "image/png"
width = "480"
height = "480"
alternateText = "Bissetii Release Note"


[menu.main]
parent = "Version History"
name = "v1.13.0"
pre = "🎁"
weight = 1


[schema]
selectType = "WebPage"
+++

# Version `v1.13.0` Release Note
{{% param "description" %}}




## Minimum Version
For this release, the minimum version of the following software were tested:

| Software      | Primary Audiences   | Version  |
|:--------------|:--------------------|:---------|
| Hugo          | Everyone            | `0.87.0` |
| Go            | Bissetii Developers | `1.16.7` |
| GolangCI-Lint | Bissetii Developers | `1.41.1` |
| Dart Sass     | Bissetii Developers | `1.37.5` |


## What's New
Version `v1.13.0` marks a completely new transformation for Bissetii where it
is now:

1. Component driven so that we can easily create, manage, delete, and most
   importantly scalable by friendly to maintenance.
2. All components are now fully customizables via CSS variables.
3. The website and server are now content distribution network (CDN) supported
   using bissetii.zoralab.com domain name.
4. Go interfacing package is now under development. Currently, we're developing
   the low-level Bissetii's source codes using its build CI tool. Development
   is still in-progress before we publish out.
5. Migration from using `master` branch to `main` branch starting from this
   release to support no-slavery movement and avoid getting the project from
   being politicized. However, **the `master` branch will retain its existence
   for backwards-compatibility**.
6. Clear decision made **NOT to use `canary` branch** to avoid the project from
   being politicized for animal abuses.

{{< note warning "CAUTION" >}}
Due to the massive changes in version `v1.13.0`, Bissetii team recommends to use
Bissetii's `git tag` to lock your current version while you perform migration.

That way, you won't be pressured while working on the massive migrations.
{{< /note >}}



### Backward Compatible Changes
For backwards compatible changes, here are some of seamless upgardes:

1. Updated logo and thumbnails to Bissetii 13.
2. Added [`nav-toc` Component]({{< link "/components/nav-toc"
"this" "url-only" />}} ) in
[Issue 106](https://gitlab.com/zoralab/bissetii/-/issues/106).
3. Added 480x480 png default thumbnails for social messaging.
4. Added [`nav-catalog` Component]({{< link "/components/nav-catalog"
"this" "url-only" />}} ) in
[Issue 109](https://gitlab.com/zoralab/bissetii/-/issues/109).
5. Added [`translate` Shortcode]({{< link "/shortcodes/translate"
"this" "url-only" />}} ) in
[Issue 113](https://gitlab.com/zoralab/bissetii/-/issues/113)
6. Implemented partial functions for Hugo interface developers to systematically
   work on the frontend development and data processing.
7. Implemented partial templates for Hugo interface to make page designs
   modular and reusable across various pages and output formats.
8. Implemented Go low-level interface to generate Bissetii components' codes for
   Hugo interface.



### Non-Backward Compatible Changes
However, there are some non-backward compatible changes done to for this
release. Here are some of the **breaking** changes:

1. Removed redundant `redirect.md` default archetype.
2. Refactored `link` shortcode to be RFC3986 compliant in
[Issue 110](https://gitlab.com/zoralab/bissetii/-/issues/110).
3. Removed 5th Parameter (URL) for
   [SVG/ShieldTag Shortcode]({{< link "/shortcodes/svg_shieldtag/" "this"
   "url-only" />}}).
4. Applied CSS variables implementation to
   [Grid Component]({{< link "/components/grid" "this" "url-only" />}}).
5. Added [Root Component]({{< link "/components/root" "this" "url-only" />}}).




## Deprecation Notice
Certainly, We deprecate a large mumbers of old and outdated components to
welcome the new changes. Among them are mainly due to the CSS variable and
Component driven development implementations. Hence, **please migrate before
its stated deadline!** Among them are:

1. Support for **`form` shortcode** AMP form extension modules in page
   front-matter version `v1.12.5 and below` will be rescinded starting from
   **July 31, 2022**. More info at
   [Issue 105](https://gitlab.com/zoralab/bissetii/-/issues/105).
2. Support for **`SVG/ShieldTag` shortcode** will be
   rescinded starting from **July 31, 2022**. More info at:
   [Issue 112](https://gitlab.com/zoralab/bissetii/-/issues/112).
3. Support for **`grid` component** version `v1.12.5 and below` will be
   rescinded starting from **July 31, 2022**. More info at:
   [Issue 114](https://gitlab.com/zoralab/bissetii/-/issues/114)
4. Support for **`color` component** version `v1.12.5 and below` will be
   rescinded starting from **July 31, 2022**. More info at:
   [Issue 115](https://gitlab.com/zoralab/bissetii/-/issues/115)
5. Support for **`navbar` component** version `v1.12.5 and below` will be
   rescinded starting from **July 31, 2022**. More info at:
   [Issue 116](https://gitlab.com/zoralab/bissetii/-/issues/116)
6. Support for **`typography` component** version `v1.12.5 and below` will be
   rescinded starting from **July 31, 2022**. More info at:
   [Issue 117](https://gitlab.com/zoralab/bissetii/-/issues/117)




## Important Migrations Notice
Here are some important notices for moving up to this version from its
immediate past version:



### Page Front-Matter Extended Module Migration
Version `v1.13.0` now uses the `modules.extensions` convention to unify all
common components together. Previously, it was specific to AMP modules. Hence,
you need to manually update **ALL** page front-matter that are using AMP form
module into the latest version. Example, from this:

```toml {linenos=table,hl_lines=["3-6"],linenostart=1}
...

[amp]
modules = [
	# Example: "amp-sidebar",
	"amp-form",
]

...
```

to this:

```toml {linenos=table,hl_lines=["3-6"],linenostart=1}
...

[modules]
extensions = [
	# Example: "sidebar",
	"form",
]

...
```



### `link` Shortcode RFC3986 Compliance Mandatory Changes
By this version, the `link` shortcode is refactored to comply with RFC3986
URL grammar definition. This does not introduce any new parameters and neither
altering their respective positions. **However, all input data do change**.

The highly notable changes would be the following:

```md
# Get static files from base URL with 'this' language prefix
# CHANGES = now refers to current URL instead of base URL
{{</* link "/img/file.png" "this" "url-only" /*/>}}
		⥥ (BEFORE)
http://localhost:8080/img/file.png
		⥥ (AFTER)
http://localhost:8080/en/img/file.png


# Get static files from base URL without language prefix
{{</* link "/img/file.png" "" "url-only" /*/>}}
		⥥ (BEFORE)
http://localhost:8080/img/file.png
		⥥ (AFTER)
http://localhost:8080/img/file.png


# Relative URL against current language Prefix
# CHANGES: now relative to current URL directory
{{</* link "img/file.png" "this" "url-only" /*/>}}
		⥥ (BEFORE)
http://localhost:8080/en/img/file.png
		⥥ (AFTER)
http://localhost:8080/en/current/path/img/file.png



# Relative URL against current language Prefix
# CHANGES: now relative to current URL directory
{{</* link "img/file.png" "" "url-only" /*/>}}
		⥥
http://localhost:8080/en/img/file.png
		⥥
http://localhost:8080/en/current/path/img/file.png
```

Things that are known affected by this change:
1. Relative URL in `RedirectURL` page front matter - now relative to base URL
   capable to redirect towards static files. If you are using other languages
   or redirect towards the same language, **you need to include language
   prefix manually**.
2. `link` partial shortcode now offers `GivenLang` parameter to specify the
   language prefix. If empty, the relative URL will be relative to base URL.
3. Any `link` shortcode being used with `"this"` language prefix to generate
   URL for static files (e.g. image). **You need to remove the `"this"` prefix**
   now.
4. `card`, `iframe`, and others shortcode TOML data file containing relative
   links. If you rely on relative URL to construct a link relative to current
   URL, **you need to append the language prefix** now.

Things that are known not affected by this change:
1. Default menu links (language prefix was prepended).
2. Page alternate formats links (used in meta).
3. Page canonicalization (used in meta).
4. Page thumbnails (used in meta).
5. CSS links (used in `<head>`).
6. Javascript links (used in `<head>`).
7. Resources icon links.


#### Shortcode Inner Content
Version `v1.13.0` implemented the shortcode's inner content to correct its
existing `"close"` implementation. This affects **ALL** `link` shortcodes as it
would requires one to change the existing non-inner shortcode type to inner
type. Here are some samples:

```md
# URL-ONLY Deployment
# CHANGES = add closing slash '/' at the end of the shortcode
{{</* link "/img/file.png" "this" "url-only" */>}}
		⥥ (UNCHANGED)
Error is raised for correction.
		⥥ (AFTER CHANGES)
{{</* link "/img/file.png" "this" "url-only" /*/>}}


# Link Construction
# CHANGES = use shortcode close tag over the older `"close"` parameter
{{</* link "/img/file.png" "this" "url-only" */>}}
	... link content here ...
{{</* link "close" */>}}
		⥥ (AFTER CHANGES)
{{</* link "/img/file.png" "this" "url-only" */>}}
	... link content here ...
{{</* /link */>}}


# Close Tag
{{</* link "close" */>}}
		⥥ (UNCHANGED)
Removed. Error is raised for correction.
		⥥ (AFTER CHANGES)
{{</* /link */>}}
```


#### Deep in Our Heart
On behalf of the team, we apologise to introduce such non-compatible changes
in a drastic measures instead of scheduling a deprecation planning. This is
mainly due to security and URL compliance matter and should be done correctly.

Please ensure **you update ALL the used shortcode input data** in
**ALL contents** during migration before commiting to publication. The
[`link` shortcode documentation]({{< link "/shortcodes/link"
"this" "url-only" />}}) has already been updated to the latest specification for
detailed read-through.



### `svg/shieldTag` Shortcode
For version `1.13.0`, this shortcode is deprecated. Hence, use `shieldbadge`
shortcode instead. Here is an example of the transformation:

```md
{{</* svg/shieldTag ... "https://example.com" */>}}
```

to

```md
{{</* link "https://example.com" ... */>}}
        {{</* shieldbadge ... */>}}
{{</* /link */>}}
```




### `grid` Component HTML Changes
The Sass part of the `grid` component was heavily upgraded with the CSS
variables implementation to the point that it requires some alteration to the
HTML.


#### Deprecation of Multi-Sizes CSS Classes
The multi-sizes CSS classes (`.grid-2x`, `.grid-3x`, ...) are scheduled to be
deprecated and removed starting from **July 31, 2022**. It is being replaced
by the CSS variables implementations.

The code changes would be something from:

```html
<div class="row">
	<div class="column">
		<p>Column 1</p>
	</div>

	<div class="column grid-5x">
		<p>Column 2</p>
	</div>
</div>
```

to

```html
<div class="row" style="--base-grid-column: 5;">
	<div class="column" style="--multiplier-grid-column: 3">
		<p>Column 1</p>
	</div>

	<div class="column" style="--multiplier-grid-column: 2">
		<p>Column 2</p>
	</div>
</div>
```

This change is necessary in order to free one from being locked to a fixed
values gridding (e.g. locked to `10` columns rule). Now, one can freely defines
the base and multiplier.


#### Deprecation of Vertical Alignment CSS Classes
The vertical alignments CSS classes (`align-top`, `align-middle`, ...) are
scheduled to be deprecated and removed starting from **July 31, 2022**. It is
being replaced by the CSS variable implementations.

Instead of creating one CSS class for a value, Bissetii now uses a CSS variable
to do it easily.

The code changes would be something from:

```html
<div class="row align-top">
	...
</div>
```

to

```html
<div class="row" style="--align-items-grid: flex-start;">
	...
</div>
```




## Epilogue
That's all for Bissetii release note version `v1.13.0`. If you
have any questions, please raise your queries in our
[Issues Section](https://gitlab.com/zoralab/bissetii/-/issues).
