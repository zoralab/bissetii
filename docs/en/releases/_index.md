+++
date = "2021-08-12T13:33:41+08:00"
title = "Downloads Portal"
description = """
Bissetii offers a lot of downloadable contents depending on your use cases.
Here, you can download deb and apt repo, css bundles, and many more. As we
continuously develop Bissetii, we shall offer more options for you in the
future.
"""
keywords = ["releases", "notes", "bissetii"]
draft = false
type = ""
# redirectURL=""
layout = "list"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
]


[creators.JohnSmith]
"@type" = "Person"
"name" = "John Smith"

[creators.SuccessTeam]
"@type" = "Organization"
"name" = "Success Team"


[thumbnails.0]
url = "/img/thumbnails/releases/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Release Note"

[thumbnails.1]
url = "/img/thumbnails/releases/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Release Note"


[menu.main]
parent = ""
name = "Downloads"
pre = "🎁"
weight = 2


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}




## Deb Software Package
Bissetii does package its very own controller software to make web development
a lot easier. To install Bissetii program locally using `.deb` packaging system,
Bissetii hosts its own apt repository server. In your Debian-based OS, you need
to perform the following 1-time setup:

**1. Adding Bissetii's GnuPG Signing Key** - The very first step is to add
Bissetii's GnuPG signing key into your local apt trusted key. You can obtain
the key from the known SKS keyservers pool.

```bash
$ sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv 7AE8191A
```

**2. Add Source File into Apt List** - The next step is to add a source list
into the apt source file.
You may easily add the file using the following command:

```bash
$ sudo echo 'deb http://zoralab.gitlab.io/bissetii/releases/deb master main' \
	 > /etc/apt/sources.list.d/bissetii.list
```

**3. Update Your Apt** - Next is to pick up the update from the server.
You may easily do that using the following command:

```bash
$ sudo apt update -y
```

**4. Install Bissetii** - Once done, you may easily install Bissetii using
the standard apt installation step.

```bash
$ sudo apt install bissetii -y
```

**5. Updating Bissetii** - Bissetii updates the package via the apt server.
Hence, whenever there is a new update, *you can get them using this command
repeatedly in the future*:

```bash
$ sudo apt update -y && sudo apt upgrade -y
```

**6. Check Bissetii** - With the program installed, you can test it out by
issuing the following command. It should produces a good result:

```bash
$ bissetii --version
1.12.4
```




## CSS
Here are the available compiled CSS package for deployment.


{{< note "error" "IMPORTANT NOTE" >}}
**DO NOT HOT-LINK any of the CSS here**. Until Bissetii is supported by any
CDN services, doing so can cause connection sluggishness. Always download and
host locally at your server sides.
{{< /note >}}

{{< note "default" "About the AMP" >}}
The [Accelerated Mobile Pages](https://amp.dev/) is made available here as a
CSS page. This is meant for you to
**[construct your `<style amp-custom>` tag](https://amp.dev/documentation/guides-and-tutorials/develop/style_and_layout/?format=websites)**,
not linking it like the normal CSS stylesheet.
{{< /note >}}

{{< note "default" "SRI Checksum" >}}
Optionally and for security reason, you should always checksum the CSS file with
its corresponding
[SRI](https://developer.mozilla.org/en-US/docs/Web/Security/Subresource_Integrity)
value. You can generate the SRI using [openSSL](https://www.openssl.org/)
software with the following commnad:

```bash
$ openssl dgst -<algo> -binary /path/to/file.css | openssl base64 -A
```
{{< /note >}}

{{< bissetii-css >}}




## Past Releases
Here are the available release notes depending on Bissetii's release version.
