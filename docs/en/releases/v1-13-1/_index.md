+++
date = "2021-08-13T21:18:18+08:00"
title = "v1.13.1 Release Note"
description = """
Bissetii version `v1.13.1` has a number of changes worth mentioning in its
(this) release note. There are notable changes, migrations notes, deprecation
notes and etc. It's good to read it through before use.
"""
keywords = [ "v1.13.1", "Bissetii", "release", "note" ]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[modules]
extensions = [
	# Example: "sidebar",
]


[creators.ZORALab]
"@type" = "Organization"
"name" = "ZORALab Team"

[creators.KeanHo]
"@type" = "Person"
"name" = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "/img/thumbnails/releases/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Bissetii Release Note"

[thumbnails.1]
url = "/img/thumbnails/releases/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Bissetii Release Note"


[menu.main]
parent = "Version History"
name = "v1.13.1"
pre = "🎁"
weight = 1


[schema]
selectType = "WebPage"
+++

# Bissetii Version {{% param "title" %}}
{{% param "description" %}}




## What's New
Version `v1.13.1` marks an incremental development from `v1.13.0`. Among the
notable changes are documentation updates, a bunch of critical and backward
compatible bug fixes, and new additional useful features addition.

The most noticable changes would be [Grid Component]({{< link
"/components/grid/" "this" "url-only" />}}) as new shortcodes, CSS variables,
adjustment and updates were done to it.

The second noticable changes would be [Card Component]({{< link
"/components/cards" "this" "url-only" />}}) as it received a large amount of
bug fixes.

The annoying sub-list bulleting bug has been fixed in this repository.

Lastly, a new [Align Component]({{< link "/components/align/" "this"
"url-only" />}}) was added to make content alignment easier (Finally)!

Hence, if you're using `v1.13.0`, be sure to update into this version for
a bunch of updated goodies!



### Backward Compatible Changes
For backwards compatible changes, here are some of seamless upgardes:

1. Fix Hugo interface hugo bug where `cards` still requires `grid` or `embed`
   to render its deck.
2. Merge internal Meta component into Root component since it is a root
   component.
3. Updated `.golangci.yml` file to match the latest golanci-lint configurations.
4. Fix Hugo interface `iframe` shortcode bug where it did not respect a given
   height value.
5. Added `max-width: 100%` to the card component to limit the max size to its
   parent.
6. Fix Hugo interface `image` shortcode bug where `width=` and `height=` weren't
   set when the shortcode is using `repsonsive-amp-only` for non-AMP output
   format.
7. Fixed Hugo interface `link` partial function bug where it was disrepecting
   custom scheme for given URL (e.g. `mailto:`, `tel:`, and etc.).
8. Alters `cards` shortcode to accept `class=`, `style=`, and `id=` attribute
   at its `.row` container level, allowing cards deck level uniformed
   customization.
9. Added `--grid-column-padding` CSS variable for customizing padding spacing
   to [Grid Component]({{< link "/components/grid/" "this" "url-only" />}})
   `.column` container.
10. Fixed sub-listing bullet bug for [List Component]({{< link
   "/components/list/" "this" "url-only" />}}).
11. Added [Align Component]({{< link "/components/align/" "this" "url-only"
    />}}).
12. Restored `data/bissetii/internal` directory to keep sensitive internal-use
    only data files away from user for not distrupting Bissetii rendering.
13. Fixed Hugo bug where `public/` directory was no longer staying in the
    root directory. It was misconfigured to stay inside `.sites/` directory.
14. Fixed [List Component]({{< link "/components/list/" "this" "url-only" />}})
    bug where it failed to sub-list with different list types.
15. Added `grid` shortcode into [Grid Component]({{< link "/components/grid/"
    "this" "url-only" />}}) to grid things easily on Hugo.



### Non-Backward Compatible Changes
However, there are some non-backward compatible changes done to for this
release. Here are some of the **breaking** changes:

1. No such changed for this release.




## Deprecation Notice
Certainly, We deprecate a large mumbers of old and outdated components to
welcome the new changes. Among them are mainly due to the CSS variable and
Component driven development implementations. Hence, **please migrate before
its stated deadline!** Among them are:

1. Support for **`form` shortcode** AMP form extension modules in page
   front-matter version `v1.12.5 and below` will be rescinded starting from
   **July 31, 2022**. More info at
   [Issue 105](https://gitlab.com/zoralab/bissetii/-/issues/105).
2. Support for **`SVG/ShieldTag` shortcode** will be
   rescinded starting from **July 31, 2022**. More info at:
   [Issue 112](https://gitlab.com/zoralab/bissetii/-/issues/112).
3. Support for **`grid` component** version `v1.12.5 and below` will be
   rescinded starting from **July 31, 2022**. More info at:
   [Issue 114](https://gitlab.com/zoralab/bissetii/-/issues/114)
4. Support for **`color` component** version `v1.12.5 and below` will be
   rescinded starting from **July 31, 2022**. More info at:
   [Issue 115](https://gitlab.com/zoralab/bissetii/-/issues/115)
5. Support for **`navbar` component** version `v1.12.5 and below` will be
   rescinded starting from **July 31, 2022**. More info at:
   [Issue 116](https://gitlab.com/zoralab/bissetii/-/issues/116)
6. Support for **`typography` component** version `v1.12.5 and below` will be
   rescinded starting from **July 31, 2022**. More info at:
   [Issue 117](https://gitlab.com/zoralab/bissetii/-/issues/117)




## Important Migrations Notice
Here are some important notices for moving up to this version from its
immediate past version:

1. No migration notices available.




## Epilogue
That's all for Bissetii {{% param "title" %}}. If you
have any questions, please raise your queries in our
[Issues Section](https://gitlab.com/zoralab/bissetii/-/issues).
