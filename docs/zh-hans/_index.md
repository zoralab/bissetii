<!--
+++
date = "2019-03-10T13:16:02+08:00"
title = "Bissetii 官方网站"
description = """
Bissetii 是 ZORALab 的网络设计组件，适合快速网络平台建设。
"""
keywords = ["bissetii", "hugo", "site", "website"]
authors = [ "ZORALab", "Holloway Chew Kean Ho" ]
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = ""
weight = 1
+++
-->

![Bissetii Banner](https://lh3.googleusercontent.com/PgIqMgqNO8EMnQiKdB7TYFxObQaq0WpJUnp40K2bKMQLsprwduLhChfi2Mf0yWweoys4x51elm93EAJuFa9jXHV2oPxc1IkDjTfJdYOew8SnUS-pNczroULWoXf--C0DsAFsV5KJG-t0R3SVBhId3hyYIQjsWLavyOtHavtrss8MgsbBTTwh-LWkV4y5PoD2lBSQg4p4NmyUGfy7MB2hXZ6L1VznBZwGplWSt8G8TaqIHBuV5bMObxws-TWcBgmubNwX-8_uWDb8jGcuNir3wLt_0TxKC2-bEqIgPgkEc0MZx-K_zN3YWUO7QvEM2Lh6Wo81AVaffTqX7fPLMysT9NLS9adfaxhNPwNmfQiCud4mWUWzjOK_Qgmrr-Gy2BwgW48VApZx0TiACnXlxczb8DW8WqoWFZ1RYxuyT2JmDnNSKebM7qNzKuriQUZANce7obUC06XaPPEipS3B3poRmFZ36_trCn9mSnk0CVYjZhjO2ZkROH--jZhD4t2H3VKUledEap7KPnc0x1FiWEDLCe7oBQwb5tMZB37EetMxsz-i7pGsJkgH3ViQxyccM_cNCUBChV2RxZ2EAo7TeObQrgMPPnnfdzpuIpHHOi8I0VsdyuzyPUKJDw716haT4yCgPBHF9c7gHpZGu0kD2HTKZ-_Y6koAJxn9VElPlvEymAdAFpPqm1ZW9sPvHesgxGNSYPIpiicVjT1BKHmOQoA4Zquz=w1726-h863-no)

# Bissetii
Bissetii 是 ZORALab 的网络设计组件，适合快速网络平台建设。

Bissetii 是从 [Hugo Theme](https://gohugo.io/themes/creating/) 组件设计开始研发
至今的。原本来Bissetii只是支持Hugo; 如今发展了那么远，Bissetii现在开始支持Go的
网络设计组件，好吧它们两统一研制，让他们双双支持Hugo和Go。

Bissetii 华语翻译暂时有限,请您还是观望英语平台好些。如果您对Bissetii数码有兴趣，
请观光一下的的网站：

> **[https://gitlab.com/ZORALab/bissetii](https://gitlab.com/ZORALab/bissetii)**

<br/>

## 金钱赞助
这个策划是由以下公司和有心人士赞助：

[![ZORALab](https://lh3.googleusercontent.com/jwB6L7Jl2A9DkqolBhnR915KvgMid66zieSDqh5hMK6oMJR81mw_QWzynMEJL-R_8yA1yKXawvGy3wb3pFmfeoCu-mVrBFPhY6BaORathrfVogDNUvZ5WvKtpH9s7faaOMDP9_KKUBkWny87gnX1mNDclbJn1NqY6rF237CbYHSyqjnYaFtACRdqR7Pl4ZZeqzdaybQWsF2KWbyqSNfQA-wsMFGf9An_xxrvS-oUKqB7G_cxotfFqpK2NvVdhQ80G7scO3WHNqlhTKmcPrMCk-BlTyNCmLsaqeAw9rfWV66ty_ximBVMnOOHVOwW4hb3RCGG9X130FiaX9Vj4Retu4wSW8DpvrvBGJqfMqqYqNo4w2c0oQUiDn3sH0IKyNMuTGiegXZUt-IcLK437uwf_FGO42XBorQSB4bEt0ZoQweUVmOADM03VEivIKFaM8bI0obdBtFoOCSz29mUeF1Pa5Oc7EJPfGakC5TAf5HJsnbjYRvuVDsXshHuuZ7JH1JXs93SE_8mf-3flN3N18ORwvTIKsiGJ4t38pEk9QWqXt2PToBuCJBjBdAWFUwWEU1_NOIM8mY_rpfOH_AmH_CtX0Ql6KUF-sIcdQzKKSn4cQjICOvPfF5bSHOzXWzEwHeqvjFmkRybHH5HORdk-BaGRZQQh3so3h9m=w300-h100-no)](https://www.zoralab.com)

<br/>

## 快速仪表板
![重心语言-Hugo和Go](https://img.shields.io/badge/重心语言-hugo--go-1313c3.svg?style=for-the-badge)
![通用执照-Apache 2.0](https://img.shields.io/badge/通用执照-APACHE%202%2E0-orange.svg?style=for-the-badge)
![版本发布素质 - Alpha](https://img.shields.io/badge/版本发布素质-alpha-red.svg?style=for-the-badge)

| Git 分枝 | 测试仪表板  | 测试素质仪表板 |
|:---------|:------------|:---------------|
| `master` | ![pipeline status](https://gitlab.com/ZORALab/bissetii/badges/master/pipeline.svg) | ![coverage report](https://gitlab.com/ZORALab/bissetii/badges/master/coverage.svg) |
| `next`   | ![pipeline status](https://gitlab.com/ZORALab/bissetii/badges/next/pipeline.svg) | ![coverage report](https://gitlab.com/ZORALab/bissetii/badges/next/coverage.svg) |
