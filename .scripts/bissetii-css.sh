#!/bin/bash
##############################################################################
# USER VARIABLES
##############################################################################
VERSION="1.0.0"
SASS_VERSION="${SASS_VERSION:-""}"

##############################################################################
# APP VARIABLES
##############################################################################
action=""
exit_code=0

repo_path="$PWD"
program_path="${repo_path}/.bin"
configs_path="${repo_path}/.configs/sass"
layout_path="${repo_path}/layouts/partials"
data_path="${repo_path}/.sites/themes/main/data/bissetii"
css_data_path="${data_path}/data/css"

hugo_css_datapath="${repo_path}/data/bissetii/internal/css.toml"
css_data_filepath=""
css_config_filepath="${configs_path}/bissetii"
sass_filespath="${configs_path}/files"

amp_layout_input_file="${layout_path}/bissetii-amp.min.css"
amp_layout_output_file="${layout_path}/bissetii-amp-css.html"

openssl="$(type -p openssl)"

operating_variables=(
	"SASS_NAME"
	"SASS_STYLE"
	"SASS_SRC"
	"SASS_DST"
	"SASS_CLONES"
	"SASS_SRI_ALGO"
	"SASS_COID"
	"SASS_SRI"   # internal variable
	"SASS_URL"   # internal variable
	"ASSET_URL"
	"ASSET_DISABLED"
	"ASSET_MEDIA"
	"ASSET_ONLOAD"
	"ASSET_COID"
	"ASSET_POSITION"
)

##############################################################################
# FUNCTIONS LIBRARY
##############################################################################
_print_status() {
	__status_mode="$1" && shift 1
	__msg=""
	__stop_color="\033[0m"
	case "$__status_mode" in
	debug)
		__msg="[ DEBUG   ] ${@}"
		__start_color="\e[93m"
		;;
	error)
		__msg="[ ERROR   ] ${@}"
		__start_color="\e[91m"
		;;
	info)
		__msg="[ INFO    ] ${@}"
		__start_color="\e[96m"
		;;
	ok)
		__msg="[ INFO    ] == OK =="
		__start_color="\e[96m"
		;;
	plain)
		__msg="$@"
		;;
	success)
		__msg="[ SUCCESS ] ${@}"
		__start_color="\e[92m"
		;;
	warning)
		__msg="[ WARNING ] ${@}"
		__start_color="\e[93m"
		;;
	*)
		return 0
		;;
	esac

	if [ $(tput colors) -ge 8 ]; then
		__msg="${__start_color}${__msg}${__stop_color}"
	fi

	1>&2 echo -e "${__msg}"
	unset __status_mode __msg __start_color __stop_color
}


_exit_app() {
	if [[ $1 =~ ^[0-9]+$ ]]; then
		exit_code=$1
	fi

	if [ $exit_code -eq 0 ]; then
		_print_status success "Sass processing completed!"
	else
		_print_status error "Sass processing error occured!"
	fi

	exit "$exit_code"
}


##############################################################################
# PRIVATE FUNCTIONS
##############################################################################
_check_openssl_program() {
	if [ "$openssl" == "" ] || [ ! -f "$openssl" ]; then
		_print_status error "missing openssl program."
		_exit_app 1
	fi
}


_check_bissetii_css_config() {
	if [ ! -f "$css_config_filepath" ]; then
		_print_status error "missing .configs/sass/bissetii file."
		_exit_app 1
	fi
}


_check_sass_config_files() {
	if [ ! -d "$sass_filespath" ]; then
		_print_status error "missing .configs/sass/files/ directory."
		_exit_app 1
	fi
}


__prepare_sass_version() {
	# prepare $SASS_VERSION
	if [ "$SASS_VERSION" == "" ]; then
		_print_status error "missing SASS_VERSION."
		_exit_app 1
	fi
	SASS_VERSION="${SASS_VERSION// /-}"
	SASS_VERSION="${SASS_VERSION//_/-}"
	SASS_VERSION="${SASS_VERSION//./-}"
	SASS_VERSION="${SASS_VERSION//,/-}"
}


__prepare_sass_configurations() {
	# prepare $SASS_SRI_ALGO
	case "$SASS_SRI_ALGO" in
	sha256|sha384|sha512)
		;;
	*)
		SASS_SRI_ALGO="sha512"
		;;
	esac


	# prepare SASS_COID
	if [ "$SASS_COID" == "" ]; then
		SASS_COID="anonymous"
	fi


	# prepare css_data_path
	mkdir -p "$css_data_path"
	css_data_filepath="${css_data_path}/${SASS_VERSION}.toml"
}


__process_sri() {
	printf "${SASS_SRI_ALGO}-$($openssl dgst -"$SASS_SRI_ALGO" -binary "$1" \
					| $openssl base64 -A)"
}


__cleanup_workspace() {
	workspace_list=(
		"$css_data_filepath"
		"$hugo_css_datapath"
	)

	for f in "${workspace_list[@]}"; do
		if [ $exit_code -eq 0 ]; then
			mv "${f}.tmp" "$f"
		fi

		rm "${f}.tmp" &> /dev/null
	done

	unset __list
}


__clone_css_artifacts() {
	for __dst in "${SASS_CLONES[@]}"; do
		cp "$SASS_DST" "${repo_path}/${__dst}"
	done
}


___write_warning_tag_to_file() {
	if [ "$1" == "true" ]; then
		echo "\
# WARNING: auto-generated content. DO NOT EDIT!
# config location: ./.configs/sass/files/...
" >> "$2"
	fi
}


___write_newline_for_non_first() {
	if [ "$1" != "true" ]; then
		echo "" >> "$2"
	fi

}


__write_to_css_toml_data_file() {
	__path="${css_data_filepath}.tmp"

	___write_warning_tag_to_file "$1" "$__path"
	___write_newline_for_non_first "$1" "$__path"

	# write content
	echo "\
[$SASS_NAME]
URL = '$SASS_URL'
Cross-Origin = '$SASS_COID'
Integrity = '$SASS_SRI'
Version = '${SASS_VERSION//-/.}'" >> "$__path"

	# clear any local variables
	unset __path
}


__write_to_hugo_toml_data_file() {
	__path="${hugo_css_datapath}.tmp"

	mkdir -p "${__path%/*}"
	___write_warning_tag_to_file "$1" "$__path"
	___write_newline_for_non_first "$1" "$__path"

	# write content
	echo "\
[$SASS_NAME]
Name = '$SASS_NAME'
Position = '$ASSET_POSITION'
URL = '$ASSET_URL'
CrossOrigin = '$ASSET_COID'
Integrity = '$SASS_SRI'
Disabled = $ASSET_DISABLED
Media = '$ASSET_MEDIA'
OnLoad = '$ASSET_ONLOAD'
Version = '${SASS_VERSION//-/.}'" >> "$__path"

	# clear any local variables
	unset __path
}


__cleanup_variables() {
	for ___name in "${operating_variables[@]}"; do
		unset $___name
	done
}


_process_bissetii_css() {
	__prepare_sass_version


	# initiate compilation process
	__isFirst=true
	for file in "$sass_filespath"/*; do
		__cleanup_variables


		# source the config file from sass helper
		source "$file"
		if [ "$SASS_DST" == "" ]; then
			continue
		fi


		__prepare_sass_configurations


		# identify and process the output file
		SASS_DST="${SASS_DST/\[VERSION\]/-$SASS_VERSION}"
		SASS_DST="${repo_path}/${SASS_DST}"
		if [ ! -f "$SASS_DST" ]; then
			_print_status error "missing css file: $SASS_DST"
			exit_code=1
			continue
		fi
		_print_status info "processing: $SASS_DST"


		# obtain SRI from file
		SASS_SRI="$(__process_sri "$SASS_DST")"
		if [ "$SASS_SRI" == "" ]; then
			_print_status error "bad sri generation. Skipping..."
			exit_code=1
			continue
		fi


		# obtain URL from file
		SASS_URL="${SASS_DST##*static}"


		# obtain NAME from file
		if [ "$SASS_NAME" == "" ]; then
			_print_status error "missing SASS_NAME. Skipping..."
			exit_code=1
			continue
		fi


		# perform repo arrangements
		__clone_css_artifacts
		__write_to_css_toml_data_file $__isFirst
		__write_to_hugo_toml_data_file $__isFirst


		# clean up
		__cleanup_variables
		__isFirst=false
	done

	__cleanup_workspace
}


_process_amp_layout() {
	_print_status info "processing Bissetii AMP CSS layout..."
	if [ ! -f "$amp_layout_input_file" ]; then
		_print_status error "missing $amp_layout_input_file"
		exit_code=1
		_exit_app
	fi

	echo "\
{{- /* WARNING - Auto-generated file. DO NOT EDIT! */ -}}

{{- /* .Before       = CSS contents place before Bissetii AMP */ -}}
{{- /* .After        = CSS contents place after Bissetii AMP  */ -}}

<style amp-custom>
{{- if .Before -}}{{- .Before | safeCSS -}}{{- end -}}
$(cat "$amp_layout_input_file")
{{- if .After -}}{{- .After | safeCSS -}}{{- end -}}
</style>

" > "$amp_layout_output_file"

	rm "$amp_layout_input_file" &> /dev/null

	# conclude process
	_print_status ok
}

##############################################################################
# PUBLIC FUNCTIONS
##############################################################################
run_repo() {
	_check_openssl_program
	_check_sass_config_files
	_process_bissetii_css
	_process_amp_layout
	_exit_app
}


print_version() {
	echo $VERSION
}


################################
# CLI Parameters and Help      #
################################
print_help() {
	echo "\
BISSETII-CSS
A helper to manage Bissetii's newly compiled css artifacts.
--------------------------------------------------------------------------------
To use: $0 [ACTION] [ARGUMENTS]

ACTIONS
1. -cv, --compile-version [VALUE]	the Sass version number you want to
					compile with. This will replace
					[VERSION] placeholder in destination
					filepath. Example:
						1) ./sass.sh -b -cv \"1.12.5\"


2. -r, --run			start the maangement sequences.


3. -h, --help			print this program's help messages.


4. -v, --version		print app version.
"
}


run_action() {
case "$action" in
"h")
	print_help
	;;
"r")
	run_repo
	;;
"v")
	print_version
	;;
*)
	_print_status error "invalid command"
	exit 1
	;;
esac
}


process_parameters() {
while [[ $# != 0 ]]; do
case "$1" in
-cv|--compile-version)
	if [ "$2" != "" ] && [ "${2:0:1}" != "-" ]; then
		SASS_VERSION="$2"
		shift 1
	fi
	;;
-h|--help)
	action="h"
	;;
-r|--run)
	action="r"
	;;
-v|--version)
	action="v"
	;;
*)
	;;
esac
shift 1
done
}


main() {
	process_parameters $@
	run_action
}


if [[ $BASHELL_TEST_ENVIRONMENT != true ]]; then
	main $@
fi
