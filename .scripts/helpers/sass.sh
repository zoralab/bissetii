#!/bin/bash
##############################################################################
# USER VARIABLES
##############################################################################
VERSION="1.0.0"
SASS_VERSION="${SASS_VERSION:-""}"

##############################################################################
# APP VARIABLES
##############################################################################
action=""
exit_code=0

repo_path="$PWD"
program_path="${repo_path}/.bin"
configs_path="${repo_path}/.configs/sass"
sass_filespath="${configs_path}/files"

sass="${program_path}/dart-sass/sass"
sass_style=""

##############################################################################
# FUNCTIONS LIBRARY
##############################################################################
_print_status() {
	__status_mode="$1" && shift 1
	__msg=""
	__stop_color="\033[0m"
	case "$__status_mode" in
	error)
		__msg="[ ERROR   ] ${@}"
		__start_color="\e[91m"
		;;
	warning)
		__msg="[ WARNING ] ${@}"
		__start_color="\e[93m"
		;;
	info)
		__msg="[ INFO    ] ${@}"
		__start_color="\e[96m"
		;;
	success)
		__msg="[ SUCCESS ] ${@}"
		__start_color="\e[92m"
		;;
	ok)
		__msg="[ INFO    ] == OK =="
		__start_color="\e[96m"
		;;
	plain)
		__msg="$@"
		;;
	*)
		return 0
		;;
	esac

	if [ $(tput colors) -ge 8 ]; then
		__msg="${__start_color}${__msg}${__stop_color}"
	fi

	1>&2 echo -e "${__msg}"
	unset __status_mode __msg __start_color __stop_color
}


##############################################################################
# PRIVATE FUNCTIONS
##############################################################################
_exit_app() {
	if [[ $1 =~ ^[0-9]+$ ]]; then
		exit_code=$1
	fi

	if [ $exit_code -eq 0 ]; then
		_print_status success "Sass compilation completed!"
	else
		_print_status error "Sass compilation error occured!"
	fi

	exit "$exit_code"
}

_check_sass_program() {
	if [ ! -f "$sass" ]; then
		_print_status error "missing sass compiler: $sass"
		_exit_app 1
	fi
}

_check_sass_config_files() {
	if [ ! -d "$sass_filespath" ]; then
		_print_status error "missing .configs/sass/files"
		_exit_app 1
	fi
}

__prepare_sass_version_configuration() {
	if [ "$SASS_VERSION" != "" ]; then
		SASS_VERSION="${SASS_VERSION// /-}"
		SASS_VERSION="${SASS_VERSION//_/-}"
		SASS_VERSION="${SASS_VERSION//./-}"
		SASS_VERSION="${SASS_VERSION//,/-}"
	else
		SASS_VERSION=""
	fi
}


__compile_sass_file() {
	_print_status info "Compiling $1 -> $2"

	$sass --style "$SASS_STYLE" \
		--no-source-map \
		"$1" \
		"$2"

	if [ $? -ne 0 ]; then
		exit_code=1
	fi
}


_compile_sass() {
	__prepare_sass_version_configuration

	# initiate compilation process
	for file in "$sass_filespath"/*; do
		# source the config file
		source "$file"
		if [ "$SASS_SRC" == "" ] || [ "$SASS_DST" == "" ]; then
			continue
		fi

		# prepare for compilation
		if [ "$SASS_VERSION" == "" ]; then
			SASS_DST="${SASS_DST/\[VERSION\]/}"
		else
			SASS_DST="${SASS_DST/\[VERSION\]/-$SASS_VERSION}"
		fi

		SASS_STYLE="${SASS_STYLE,,}"
		case "$SASS_STYLE" in
		default)
			;;
		compressed)
			;;
		*)
			SASS_STYLE="default"
			;;
		esac

		# convert to full path
		SASS_SRC="${repo_path}/${SASS_SRC}"
		SASS_DST="${repo_path}/${SASS_DST}"

		# begin compile
		__compile_sass_file "$SASS_SRC" "$SASS_DST"

		# clean up
		unset SASS_SRC SASS_DST SASS_STYLE
	done
}

##############################################################################
# PUBLIC FUNCTIONS
##############################################################################
build_repo() {
	_check_sass_program
	_check_sass_config_files
	_compile_sass
	_exit_app
}

print_version() {
	echo $VERSION
}


################################
# CLI Parameters and Help      #
################################
print_help() {
	echo "\
SASS
A Sass repository helper to build css artifacts.
--------------------------------------------------------------------------------
To use: $0 [ACTION] [ARGUMENTS]

ACTIONS
1. -b, --build			start gopher build sequences.


2. -cv, --compile-version [VALUE]	the Sass version number you want to
					compile with. This will replace
					[VERSION] placeholder in destination
					filepath. Example:
						1) ./sass.sh -b -cv \"1.12.5\"


3. -h, --help			print this program's help messages.


4. -v, --version		print app version.
"
}

run_action() {
case "$action" in
"b")
	build_repo
	;;
"h")
	print_help
	;;
"v")
	print_version
	;;
*)
	_print_status error "invalid command"
	exit 1
	;;
esac
}


process_parameters() {
while [[ $# != 0 ]]; do
case "$1" in
-b|--build)
	action="b"
	;;
-cv|--compile-version)
	if [ "$2" != "" ] && [ "${2:0:1}" != "-" ]; then
		SASS_VERSION="$2"
		shift 1
	fi
	;;
-h|--help)
	action="h"
	;;
-v|--version)
	action="v"
	;;
*)
	;;
esac
shift 1
done
}

main() {
	process_parameters $@
	run_action
}

if [[ $BASHELL_TEST_ENVIRONMENT != true ]]; then
	main $@
fi
