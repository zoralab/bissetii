#!/bin/bash
##############################################################################
# USER VARIABLES
##############################################################################
VERSION="1.0.0"
SASS_STYLE="${SASS_STYLE:-"compressed"}"

##############################################################################
# APP VARIABLES
##############################################################################
action=""
exit_code=0

repo_path="$PWD"
program_path="${repo_path}/.bin"
go_internal_path="${repo_path}/pkg/components/internal"

sass="${program_path}/dart-sass/sass"
gofmt="${program_path}/golang/bin/gofmt"

dependencies=(
	"$sass"
	"$gofmt"
	"$(type -p date)"
	"$(type -p cat)"
)

media_types=(
	"amp"
	"critical"
	"tablet"
	"desktop"
	"widescreen"
	"print"
	"variables"
	"variablesAMP"
)

blocklist=(
	"css"
)

##############################################################################
# FUNCTIONS LIBRARY
##############################################################################
_print_status() {
	__status_mode="$1" && shift 1
	__msg=""
	__stop_color="\033[0m"
	case "$__status_mode" in
	debug)
		__msg="[ DEBUG   ] ${@}"
		__start_color="\e[93m"
		;;
	error)
		__msg="[ ERROR   ] ${@}"
		__start_color="\e[91m"
		;;
	info)
		__msg="[ INFO    ] ${@}"
		__start_color="\e[96m"
		;;
	ok)
		__msg="[ INFO    ] == OK =="
		__start_color="\e[96m"
		;;
	plain)
		__msg="$@"
		;;
	success)
		__msg="[ SUCCESS ] ${@}"
		__start_color="\e[92m"
		;;
	warning)
		__msg="[ WARNING ] ${@}"
		__start_color="\e[93m"
		;;
	*)
		return 0
		;;
	esac

	if [ $(tput colors) -ge 8 ]; then
		__msg="${__start_color}${__msg}${__stop_color}"
	fi

	1>&2 echo -e "${__msg}"
	unset __status_mode __msg __start_color __stop_color
}


_exit_app() {
	if [[ $1 =~ ^[0-9]+$ ]]; then
		exit_code=$1
	fi

	if [ $exit_code -eq 0 ]; then
		_print_status success "Completed!"
	else
		_print_status error "Error occured!"
	fi

	exit "$exit_code"
}


##############################################################################
# PRIVATE FUNCTIONS
##############################################################################
_check_dependencies() {
	for i in "${dependencies[@]}"; do
		_print_status info "checking dependencies $i ..."
		if [ ! -f "$sass" ]; then
			_print_status error "missing $i program".
			exit_code=1
			_exit_app
		fi
		_print_status ok
	done
}

_build_go_css() {
	for f in "${go_internal_path}"/*; do
		# continue if it is a file
		if [ ! -d "$f" ]; then
			continue
		fi

		# check blocklist component
		__name="${f##*/}"
		__name="${__name,,}"
		__skip=false
		for item in "${blocklist[@]}"; do
			if [ "$__name" == "$item" ]; then
				__skip=true
			fi
		done
		if [ "$__skip" == "true" ]; then
			continue
		fi

		# compile all available Sass
		for format in "${media_types[@]}"; do
			__target="${f}/${format}.scss"
			__output="${f}/${format}.min.css"

			# skip if format is missing
			if [ ! -f "$__target" ]; then
				continue
			fi
			_print_status info "compiling $__target ..."

			# compile each sass file...
			$sass --style "$SASS_STYLE" \
				--no-source-map \
				--no-charset \
				"$__target" \
				"$__output"
			if [ $? -gt 0 ]; then
				exit_code=1
			fi

			if [ ! -f "$__output" ]; then
				_print_status error "failed"
				exit_code=1
			else
				_print_status ok
			fi
		done
	done
}

_generate_go_css_codes() {
	__year="$(date +"%Y")"

	for f in "${go_internal_path}"/*; do
		# continue if it is a file
		if [ ! -d "$f" ]; then
			continue
		fi

		# check blocklist component
		__name="${f##*/}"
		__name="${__name,,}"
		__skip=false
		for item in "${blocklist[@]}"; do
			if [ "$__name" == "$item" ]; then
				__skip=true
			fi
		done
		if [ "$__skip" == "true" ]; then
			continue
		fi

		# process variables
		_print_status info "converting CSS in $f to Go..."
		__output="${f}/CSS.go"

		# write the Go header
		echo "\
// Copyright $__year ZORALab Enterprise (hello@zoralab.com)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// WARNING: This file is auto-generated! DO NOT EDIT!
//          Original source codes from this directory:" > "$__output"

		i=1
		for format in "${media_types[@]}"; do
			echo "//		${i}. ${format}.scss" \
				>> "$__output"
			i=$((i+1))
		done

		echo "
package $__name

// All Bissetii Compressed Compiled CSS
const (
" >> "$__output"

		for format in "${media_types[@]}"; do
			__target="${f}/${format}.min.css"


			# forming constant title
			__title="CSS${format^}"
			if [ "$format" == "amp" ]; then
				__title="CSS${format^^}"
			fi


			# skip if format is missing
			if [ ! -f "$__target" ]; then
				echo "\
	$__title = \`\`" >> "$__output"

			else
				echo "\
	${__title} = \`$(cat $__target)\`
" >> "$__output"

			fi


			# remove artifact
			rm "$__target" &> /dev/null
		done

		# close the constant group
		echo ")" >> "$__output"


		# gofmt the file
		$gofmt -s -w "$__output"

		# conclude conversion
		_print_status ok
	done
}

##############################################################################
# PUBLIC FUNCTIONS
##############################################################################
run_repo() {
	_check_dependencies
	_build_go_css
	_generate_go_css_codes
	_exit_app
}


print_version() {
	echo $VERSION
}


################################
# CLI Parameters and Help      #
################################
print_help() {
	echo "\
BISSETII-CSS Go Script
A helper to manage Bissetii's newly compiled css artifacts for Go
--------------------------------------------------------------------------------
To use: $0 [ACTION] [ARGUMENTS]

ACTIONS
1. -r, --run			start the maangement sequences.


2. -h, --help			print this program's help messages.


3. -v, --version		print app version.
"
}


run_action() {
case "$action" in
"h")
	print_help
	;;
"r")
	run_repo
	;;
"v")
	print_version
	;;
*)
	_print_status error "invalid command"
	exit 1
	;;
esac
}


process_parameters() {
while [[ $# != 0 ]]; do
case "$1" in
-h|--help)
	action="h"
	;;
-r|--run)
	action="r"
	;;
-v|--version)
	action="v"
	;;
*)
	;;
esac
shift 1
done
}


main() {
	process_parameters $@
	run_action
}


if [[ $BASHELL_TEST_ENVIRONMENT != true ]]; then
	main $@
fi
