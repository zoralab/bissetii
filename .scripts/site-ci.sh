#!/bin/bash
##############################################################################
# APP VARIABLES
##############################################################################
VERSION="1.0.0"
action=""
current_version=""
machine=""
arch=""


export repo_path="$PWD"
app_path="${repo_path}/app"
assets_path="${repo_path}/assets"
sites_path="${repo_path}/.sites"
data_path="${repo_path}/data"
docs_path="${repo_path}/docs"
program_path="${repo_path}/.bin"
script_path="${repo_path}/.scripts"
static_path="${repo_path}/static"
tests_path="${repo_path}/tests"
partial_path="${repo_path}/layouts/partials"
releases_path="${sites_path}/static"
tmp_path="${repo_path}/tmp"
godocgen_path="${docs_path}/en/go"
go_src_path="${repo_path}/pkg"

bissetii="${app_path}/bissetii.sh"
hugo="${program_path}/hugo"
bashell="${script_path}/helpers/bashell.sh"
helix="${script_path}/helpers/helix.sh"
bissetii_css="${script_path}/bissetii-css.sh"
bissetii_gocss="${script_path}/bissetii-gocss.sh"
manta="${script_path}/helpers/stingrays/manta.bash"
reprepro="${script_path}/helpers/reprepro.sh"
sass="${script_path}/helpers/sass.sh"
godocgen="${script_path}/helpers/godocgen.sh"
gopher="${script_path}/helpers/gopher.sh"

program_dependencies=(
	"$bissetii"
	"$bissetii_css"
	"$hugo"
	"$manta"
	"$reprepro"
	"$sass"
)
publish_branch="gh-pages"

##############################################################################
# FUNCTIONS LIBRARY
##############################################################################
_print_status() {
	_status_mode="$1" && shift 1

	# process status message
	_status_message=""
	case "$_status_mode" in
	error)
		_status_message="[  ERROR  ] $@"
		;;
	warning)
		_status_message="[  WARNING  ] $@"
		;;
	info)
		_status_message="[  INFO  ] $@"
		;;
	plain)
		_status_message="$@"
		;;
	*)
		return 0
		;;
	esac

	1>&2 echo "${_status_message}"
	unset _status_mode _status_message
}


__start_bashell_testing() {
	$bashell -r
}


__start_hugo() {
	$bissetii -r
}


##############################################################################
# PRIVATE FUNCTIONS
##############################################################################
_get_machines_properties() {
	# detect CPU
	case "$(uname -m)" in
	x86_64)
		arch="amd64"
		;;
	i386|i686|x86|i686-AT386)
		arch="i386"
		;;
	aarch64)
		arch="arm64"
		;;
	armv5*)
		arch="armv5"
		;;
	armv6*)
		arch="armv6l"
		;;
	armv7*)
		arch="armv7"
		;;
	BePC)
		arch="bepc"
		;;
	ppc)
		arch="ppc"
		;;
	ppc64)
		arch="ppc64le"
		;;
	sparc64)
		arch="sparc64"
		;;
	*)
		_print_status error "unknown CPU architecture."
		exit 1
		;;
	esac
	_print_status info "detected architecture: $arch"


	# detect OS
	case "$(uname -s | tr '[:upper:]' '[:lower:]')" in
	darwin)
		machine="darwin"
		;;
	dragonfly)
		machine="dragonfly"
		;;
	freebsd)
		machine="freebsd"
		;;
	linux)
		machine="linux"
		;;
	android)
		machine="android"
		;;
	nacl)
		machine="nacl"
		;;
	netbsd)
		machine="netbsd"
		;;
	openbsd)
		machine="openbsd"
		;;
	plan9)
		machine="plan9"
		;;
	solaris)
		machine="solaris"
		;;
	windows)
		machine="windows"
		;;
	*)
		_print_status error "unknown operating system."
		exit 1
		;;
	esac
	_print_status info "detected operating system: $machine"
}


_check_supported_machine() {
	for _program in "${program_dependencies[@]}"; do
		if [ ! -e "$_program" ]; then
			_print_status error "missing $_program. Aborted."
			exit 1
		fi
		_print_status info "found dependency: $_program"
	done
}


_get_app_version() {
	current_version="$($bissetii -v)"
	current_version="${current_version//./-}"
	current_version="${current_version// /-}"
	current_version="${current_version//_/-}"
}


_farewell_message() {
	_print_status info "==BISSETII CI COMPLETED=="
}


_package_for_release() {
	_job="package for relaese"
	_print_status info "'$_job' job included. Executing..."

	ret="$($manta -b)"
	if [ $? -ne 0 ]; then
		_print_status error "stopping CI. '$_job' job failed."
		exit 1
	fi

	_print_status info "==DONE== '$_job' job completed successfully."
	unset _job
}


_process_sass() {
	_job="build Sass"
	_print_status info "'$_job' job included. Executing..."

	if [ ! -e "$sass" ]; then
		_print_status error "stopping CI. missing: $sass"
		exit 1
	fi

	if [ ! -e "$bissetii_css" ]; then
		_print_status error "stopping CI. missing: $bissetii_css"
		exit 1
	fi

	if [ ! -e "$bissetii_gocss" ]; then
		_print_status error "stopping CI. missing: $bissetii_gocss"
		exit 1
	fi

	if [ -z "$current_version" ]; then
		_print_status error "stopping CI. missing: current_version."
		exit 1
	fi


	# sass compilation
	$sass --build --compile-version "$current_version"
	if [ $? -ne 0 ]; then
		_print_status error "stopping CI. compilation failed."
		exit 1
	fi


	# bissetii CSS repository management
	$bissetii_css --run --compile-version "$current_version"
	if [ $? -ne 0 ]; then
		_print_status error "stopping CI. compilation failed."
		exit 1
	fi

	# bissetii CSS for Go
	$bissetii_gocss --run
	if [ $? -ne 0 ]; then
		_print_status error "stopping CI. compilation failed."
		exit 1
	fi


	_print_status info "==DONE== '$_job' job completed successfully."
}


_process_go_testing() {
	export HUGO_GO_ROOT_DIRECTORY="$PWD"

	# enter workspace
	cd "$go_src_path"

	# clean up
	$gopher --clean --input "${go_src_path}/..."
	if [ $? -ne 0 ]; then
		unset HUGO_GO_ROOT_DIRECTORY
		cd "$repo_path"
		exit 1
	fi

	# test
	$gopher --test --input "${go_src_path}/..."
	if [ $? -ne 0 ]; then
		unset HUGO_GO_ROOT_DIRECTORY
		cd "$repo_path"
		exit 1
	fi

	# benchmark
	$gopher --benchmark --input "${go_src_path}/..."
	if [ $? -ne 0 ]; then
		unset HUGO_GO_ROOT_DIRECTORY
		cd "$repo_path"
		exit 1
	fi

	# exit workspace
	cd "$repo_path"

	unset HUGO_GO_ROOT_DIRECTORY
}


_process_godocgen_build() {
	_job="build hugo artifacts"
	_print_status info "'$_job' job included. Executing..."

	$godocgen --output "$godocgen_path" --input "$go_src_path" --run
	if [ $? -ne 0 ]; then
		_print_status error "stopping CI. Godocgen build failed."
		exit 1
	fi

	_print_status info "==DONE== '$_job' job completed successfully."
}


_process_hugo_build() {
	_job="build hugo artifacts"
	_print_status info "'$_job' job included. Executing..."

	$bissetii -B
	if [ $? -ne 0 ]; then
		_print_status error "stopping CI. Hugo build failed."
		exit 1
	fi

	_print_status info "==DONE== '$_job' job completed successfully."
}


_publish_artifacts() {
	_job="publish hugo artifact"
	_print_status info "'$_job' job included. Executing..."

	$bissetii -P clean -t "$publish_branch"
	if [ $? -ne 0 ]; then
		_print_status error "stopping CI. Publish failed."
		exit 1
	fi

	_print_status info "==DONE== '$_job' job completed successfully."
}


_setup_repository() {
	rm -rf "$program_path" &> /dev/null
	"$helix" -i
}


_test_with_bashell() {
	_job="bashell testing"
	_print_status info "'$_job' job included. Executing..."

	__start_bashell_testing
	if [ $? -ne 0 ]; then
		_print_status error "stopping CI. Bashell test failed."
		exit 1
	fi

	_print_status info "==DONE== '$_job' job completed successfully."
}


_update_changelog_for_release() {
	_job="update changelog"
	_print_status info "'$_job' job included. Executing..."

	ret="$($manta -c && $manta -r)"
	if [ $? -ne 0 ]; then
		_print_status error "stopping CI. '$_job' job failed."
		exit 1
	fi

	if [[ "$ret" == *"show 0 differences"* ]]; then
		_print_status error "stopping CI. No differences in changelog."
		exit 1
	fi

	_print_status info "==DONE== '$_job' job completed successfully."
	unset _job
}


_upstream_reprepro() {
	_job="upstream reprepro"
	_print_status info "'$_job' job included. Executing..."

	ret="$("$reprepro" -r)"
	if [ $? -ne 0 ] || [[ "$ret" == *ERROR* ]]; then
		_print_status error "stopping CI. '$_job' job failed."
		exit 1
	fi

	_print_status info "==DONE== '$_job' job completed successfully."
	unset _job
}


_develop_notice() {
	_job="develop"
	_print_status info "'$_job' job included. Executing..."

	_print_status error "sorry, function is not available."

	_print_status info "==DONE== '$_job' job completed successfully."
	unset _job
}



##############################################################################
# PUBLIC FUNCTIONS
##############################################################################
bad_command() {
	_print_status error "invalid command."
	exit 1
}


develop() {
	_get_machines_properties
	_check_supported_machine
	_get_app_version
	_develop_notice
}


build() {
	_get_machines_properties
	_check_supported_machine
	_get_app_version
	_process_sass
	_process_go_testing
	_process_godocgen_build
	_process_hugo_build
	_farewell_message
}


release() {
	_get_machines_properties
	_check_supported_machine
	_get_app_version
	_process_sass
	_update_changelog_for_release
	_package_for_release
	_upstream_reprepro
	_farewell_message
}


setup() {
	_get_machines_properties
	_setup_repository
	_farewell_message
}


testing() {
	_get_machines_properties
	_check_supported_machine
	_get_app_version
	_test_with_bashell
	_process_go_testing
	_farewell_message
}


print_version() {
	echo $VERSION
}

publish() {
	_get_machines_properties
	_check_supported_machine
	_get_app_version
	_publish_artifacts
}


print_help() {
	echo "\
BISSETII CI AUTOMATOR
Bissetii continuous integration automation tool.

-------------------------------------------------------------------------------
To use: $0 [ACTION] [ARGUMENTS]

ACTIONS
1. -d. --develop                start the development environment.

2. -h, --help                   print help for this script app.

3. -b, --build                  execute CI build.

4. -p, --publish                execute CI publish.

5. -r, --release                execute CI release.

6. -s, --setup                  setup Bissetii development environment.

7. -t, --test                   execute CI test.

8. -v, --version                print this app version.
"
}

##############################################################################
# MAIN CLI
##############################################################################
run_action() {
case "$action" in
"d")
	develop
	;;
"h")
	print_help
	;;
"b")
	build
	;;
"p")
	publish
	;;
"r")
	release
	;;
"s")
	setup
	;;
"t")
	testing
	;;
"v")
	print_version
	;;
*)
	bad_command
	;;
esac
}


process_parameters() {
while [[ $# != 0 ]]; do
case "$1" in
-b|--build)
	action="b"
	;;
-d|--develop)
	action="d"
	;;
-h|--help)
	action="h"
	;;
-p|--publish)
	action="p"
	;;
-r|--release)
	action="r"
	;;
-s|--setup)
	action="s"
	;;
-t|--test)
	action="t"
	;;
-v|--version)
	action="v"
	;;
*)
	;;
esac
shift 1
done
}


main() {
	process_parameters $@
	run_action
}


if [[ $BASHELL_TEST_ENVIRONMENT != true ]]; then
	main $@
fi
