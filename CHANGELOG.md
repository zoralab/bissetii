# Version 1.13.1
## Wed, 01 Sep 2021 22:07:30 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. 5a6f1c94 root: updated documentations for release
2. 94904063 docs/en/components/image.md: updated based on feedback
3. 97cea828 root: restored internal data directory to house internal use data files
4. 46334a3a app/bissetii.sh: fixed master branch tracking bug
5. 62ec2fdb config/_default/config.toml: fixed destination bugs
6. a6f4dee2 docs/en/hugo/getting-started/_index.md: fixed git clone typo
7. f686c502 pkg/components/internal: added align component
8. 67b4db48 .scripts/bissetii-gocss.sh: upgraded to include variables and variablesAMP
9. b141c849 docs/en/components: added style/_index.md
10. 0951cfa2 pkg/components/internal: added script component
11. 27a1eb2a pkg/components/internal/root: refactored meta codes to match root
12. ba89b76c pkg/components/internal/list: fixed sub-bulleting bug
13. fa29ed15 pkg/components/internal/grid: added --grid-column-padding CSS variable
14. 4c4defd6 layouts/partials/shortcodes/*cards.html: enable style, class, id attributes
15. f6239b9f pkg/components/internal/grid: added Hugo shortcode support
16. e3dbf963 layouts/partials/functions/link.html: fixed disrepecting scheme bug
17. 49e9f632 layouts/partials/shortcodes/image.html: fixed responsive-amp-only bug
18. f03ec7e5 pkg/components/internal/card: added max-width to all cards
19. 5676068f layouts/partials/shortcodes/iframe.html: fixed height bug
20. 291194c3 pkg/components/internal/root: merged pkg/components/internal/meta
21. fced1e4d docs/en/releases/v1-13-1/_index.md: updated release note
22. c4d2f720 components/internal: shifted output format prefix into internal directory
23. 784e41a6 .golangci.yml: updated to new linters
24. 6bde4184 docs/en/components: added v1.13.0 below deprecation notice to Sass
25. 0e6cc56c docs/en/_index.md: replaced master branch status to main branch status
26. 4e593aec root: changed next release from v1.14.0 to v1.13.1
27. b405fcf6 layouts/partials/shortcodes/*cards.html: removed grid and embed keywords
28. 1c14f112 .configs/stingrays/manta.cfg: updated to use main branch as target
29. 84bfba21 docs/en/releases/v1-13-0/_index.md: updated the missing git branch naming
30. f8f2f660 layouts/partials/template/404.html: fixed broken crystal URL
31. 2a18b66a root: begin development for version v1.14.0

# Version 1.13.0
## Fri, 13 Aug 2021 20:44:44 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. eddb7f7d .scripts/helpers/stingrays/manta.bash: fixed release build bug
2. 59ad0665 docs/en/releases: updated v1-13-0/_index.md for release
3. 8e3ecc0c .configs/helix: updated to the latest dependencies
4. a9d16ffd docs/en/_index.md: updated to release quality
5. bee34421 docs/en/getting-started.md: temporary redirect to Hugo version
6. b03250ef docs/en/contribute: ported sponsorship/_index.md
7. cd457c0e docs/en/releases/_index.md: updated to downloads portal
8. 0f60c7d7 docs/en: ported emoji to root component
9. 2b98e980 docs/en/hugo: redirect create-new-content.md traffic
10. d63bee0e docs/en/hugo: ported manage-publishers/_index.md
11. bdae01ec docs/en/hugo: ported manage-page/_index.md
12. 1c9b6270 .sites/themes/main/data/.../schema/SoftwareApplication.toml: fixed bug
13. c1ef79d0 docs/en/hugo: ported manage-schema/_index.md
14. 26d46184 layouts/partials/templates/404.html: added robots no processing metas
15. 7ceecb8c docs/en/hugo: ported manage-404/_index.md
16. fb345649 docs/en/hugo: ported manage-redirect/_index.md
17. 9811ec86 docs/en/hugo: added escape-shortcode/_index.md
18. 786d6f95 docs/en/hugo: ported manage-social-network/_index.md
19. 8cf1570b docs/en/hugo: ported manage-crawler/_index.md
20. 38a4b5eb docs/en/hugo: ported manage-menu/_index.md
21. b9103eb4 layouts/partials/shortcodes/__page.html: updated outdated label
22. e86864b7 data/bissetii/schema: fixed broken fields
23. 9da80f60 layouts/partials/shortcodes/__page.html: fixed thumbnail URL bug
24. 9d6bbea7 docs,archetypes: corrected authorship keys
25. df320255 docs/en/hugo: ported manage-authorship/_index.md
26. b73fecf1 docs/en/components: updated Hugo module extension instructions
27. 44844450 data/bissetii/amp.toml: updated module name
28. 31f2261a docs/en/hugo: ported manage-amp/_index.md
29. 7400e5fd docs/en/hugo/include-google-analytics/_index.md: ported AMP guides
30. 4cc85304 docs/en/hugo: ported include-google-analytics/_index.md
31. dc906201 docs/en/hugo: ported include-js/_index.md
32. 836f0f2e docs/en/hugo: ported include-css/_index.md
33. 4a4491aa docs/en/hugo: ported getting-started/_index.md
34. d37043ed docs/en/how-to: removed unsupported features
35. 396789bb .configs/godocgen/template/md: updated go docs to sidebar interfaces
36. 5ed86f18 docs/en/components: added hugo component
37. e4b3a787 docs/en/components: added i18n
38. 398d8340 sass: removed old sass support
39. bec9f1c5 pkg/components/internal: removed all unused public API
40. 5124e985 pkg/components/internal: shifted CSS variables to its own API
41. 689bd884 pkg/components/internal/navcatalog: enabled print mixins
42. c05c6e81 pkg/components/internal/navcatalog: renamed _Pkg.scss to _index.scss
43. c621483c layouts/partials/shortcodes/__nav-catalog.html: implemented auto thumbnail
44. b94fef12 layouts/partials/functions: added betterImage.html image selector
45. 7820b175 pkg/components/internal/navsidebar: fixed desktop scrolling bug
46. baf14237 pkg/components/internal/navcatalog: refactored with latest features
47. 7af6af91 layouts/partials/functions/isType.html: corrected int checker
48. ddc1bdf0 layouts/partials/shortcodes/image.html: updated to identify mediaTypes
49. d18f5bad layouts/partials/shortcodes/link.html: updated to identify MediaTypes
50. 083b62ee pkg/components/internal/meta: fixed image URL bug
51. 61812712 layouts/partial/shortcodes/_card.html: refactored for proper processing
52. 80cab4f4 layouts/partials/shortcodes/*card.html: fixed typos
53. eb01e4b2 .sites/.../data/input/en: updated all card data structures
54. 1cc988c2 layouts/partials/shortcodes/_card.html: added image shortcode thumbnail
55. 60a61888 .sites/.../data/examples/cards/card1.toml: corrected picture source
56. c7c801ee layouts/partials/shortcodes/image.html: added error checking.
57. 31c205f5 docs/en/components/image.md: organized example data
58. 01172ce0 docs/en/components/iframe.md: organized example data
59. c8c83871 docs/en/components/carousel.md: organized example data
60. 4f3fb5a0 docs/en/components/card: structured navigation and readability
61. 511ee639 docs/en/components/card: added vertical/_index.md
62. 23b99d1e pkg/components/internal/card: fixed max-content for firefox support
63. 355bf0d2 pkg/components/internal/card: keep content first-child to 0 margin top
64. f6133f6d docs/en/components/card/_index.md: updated to release quality
65. 0a0e25e5 .sites/themes/main/data/bissetii/data/getting-started: removed unused deck
66. b82c7ce7 .sites/themes/main/data/bissetii/data/features: updated card data
67. 53f8dcde .sites/themes/main/data/bissetii/data/sponsor: updated card data
68. f0358ee4 pkg/components/internal/card*: re-styled against the new HTML layout
69. 2419839e pkg/components/internal/card/Card.go: added style attribute
70. 8c3e2e7f layouts/partials/shortcodes/*card.html: updated to match HTML changes
71. da56668e layouts/partials/functions: added isType.html to check data type
72. 0843792c layouts/partials/functions: added getType.html
73. 592c38ab pkg/components/internal/card: refactored to have common HTML codes
74. bb3b0c6c pkg/components/internal/card: enabled print mixin
75. 4c6aa4c7 pkg/components/internal/card: renamed _Pkg.scss to _index.scss
76. 002e285c docs/en/components/carousel.md: updated to release quality
77. 238d643c layouts/partials/shortcodes/carousel.html: shift .Type into datafile
78. 6948583b pkg/components/internal/carousel: refactored for sanity
79. 3bb20e7f pkg/components/internal/carousel: renamed _Pkg.scss to _index.scss
80. f0acb417 pkg/components/internal/carousel: upgraded component for usability
81. 8d71b54b pkg/components/button: updated a bunch of AMP error feedbacks
82. 035df0fb docs/en/components/footer.md: updated documentation for release quality
83. 3ff5513b pkg/components/internal/footer: enabled Sass part
84. edde4479 pkg/components: re-structured footerbasic to footer component
85. 1c93d89f */components/*/button: upgraded button component
86. 6c1e9153 docs/en/components: promote to "Components" parent category
87. 74f6a125 docs/en/components/anchor.md: updated to "Component" parent category
88. bd6076ca docs/en/components/form: added select/_index.md
89. 94acd231 docs/en/components/form/input/file/_index.md: updated AMP restriction
90. 950b595c docs/en/components/form/input: added file/_index.md
91. aae3709a docs/en/components/form/input/submit/_index.md: updated AMP restriction
92. 2a6e2228 docs/en/components/form/input: added submit/_index.md
93. d894eeb2 docs/en/components/form/input: added radio/_index.md
94. 222a2352 docs/en/components/form/input: added time/_index.md
95. 283bc51c docs/en/components/form/input: added week/_index.md
96. 4639f3d4 docs/en/components/form: added textarea/_index.md
97. 38da4d67 docs/en/components/form/input: added tel/_index.md
98. b1f30583 docs/en/components/form/input: added url/_index.md
99. a11c93a0 docs/en/components/form/input: added search/_index.md
100. f68a92ef docs/en/components/form/_index.md: added placeholder=, .info, and .notice
101. eb061da8 docs/en/components/form/_index.md: touched up styling
102. 44297fa9 docs/en/components/form/_index.md: added minlength= and maxlength=
103. 1aa43013 docs/en/components/form/input: added password/_index.md
104. fecebad1 docs/en/components/form/input: added text/_index.md
105. e2d0ba59 docs/en/components/form/_index.md: added min=, max=, and step=
106. 504b8b96 docs/en/components/form/input/_index.md: removed unncessary info
107. f31c6493 docs/en/components/form/input: added number/_index.md
108. 144b3791 .sites/themes/main/data: added bissetii/data/input/en/file.toml
109. 7426ca83 docs/en/components/form/input: added month/_index.md
110. fda10be4 docs/en/components/form/input: added email/_index.md
111. 4b334bed docs/en/components/form/input: added datetime-local/_index.md
112. 2a5c5ed6 docs/en/components/form/input: added date/_index.md
113. e85fde42 docs/en/components/form/input: added color/_index.md
114. 44e50c06 docs/en/components/form/input: added checkbox/_index.md
115. 042d8757 layouts/partials/templates/list.html: moved description to default block
116. 67b19b9f docs/en/components/form: added input/_index.md
117. bb6b3e4f docs/en/components/form: updated to release quality
118. 50ae317b pkg/components/internal/form: refactored for release
119. 3153b8b5 pkg/components/internal/form: enabled print mixin
120. e818bc68 pkg/components/internal/form: renamed _Pkg.scss to _index.scss
121. 984660bb docs/en/components/table.md: updated to release quality
122. 5e89317f pkg/components/internal/table: refactored for release
123. f6dd61fe pkg/components/internal/table: enabled print mixin
124. 05fe5f45 pkg/components/internal: synchonized all .scss files
125. 65c9a28f pkg/components/internal/table: renamed _Pkg.scss to _index.scss
126. fccd6525 docs/en/components/note.md: updated to release quality
127. 67348458 pkg/components/internal/note/Icon.go: updated icons to latest design
128. 671a1eef pkg/components/internal/note: refactored Sass styling
129. c6e7efde pkg/components/internal/note: enabled print mixin
130. aa100f10 pkg/components/internal/note: updated code base to match latest
131. 514086d8 docs/en/components/note.md: updated for new CSS variable roll out
132. 8059e64d pkg/components/internal/note: renamed _Pkg.scss to _index.scss
133. 5e691649 pkg/components/internal/gochroma: fix unscrollable bug for pre tag
134. fe4ce74a docs/en/components/nav-toc.md: updated to release quality
135. 5fa717d7 pkg/components/internal/navtoc: styled component
136. 5ec8a24b pkg/components/internal/navtoc: reset to clean slate
137. 93a9f343 pkg/components/internal/navtoc: refactored to match documentation design
138. 864506de components/internal/nav: port common nav into its own component
139. 7c3c4866 pkg/components/internal/navtoc: renamed _Pkg.scss to _index.scss
140. e62e5cd9 docs/en/components/nav-toc.md: updated to table of content design
141. 4dbf06db data/bissetii/nav.toml: shift Hugo default list page to nav-catalog
142. f93e4b88 pkg/components/internal/code: set the <code> overflow wrapping behavior
143. 295ae78f pkg/components/internal/root: updated layout to sensible sidebar width
144. 1f266abd docs: consolidate and re-organize contents
145. 0af126b8 docs/en/components: added gochroma.md
146. 0fd5a5ee docs/en/components/code.md: updated to release quality
147. 2e3207ed pkg/components/internal/code: refactored for quality release
148. 63fb201b pkg/components/internal/code: enabled print mixin
149. 2db5baf2 pkg/components/internal/code: renamed _Pkg.scss to _index.scss
150. 3c941aa6 docs: updated deprectation notice to July 31, 2022
151. b509777a layouts/partials/shortcodes/shieldsbadge.html: removed default color
152. 9cd0831b layouts/partials/shortcodes/shieldsbadge.html: removed color overrides
153. 5ec0f0d4 layout/*/shortcodes/shields-badge.*: renamed filename to shieldsbadge
154. 61b23812 pkg/components/internal/shieldsbadge: updated to latest HTML pattern
155. 4001ecd3 docs/en/components: renamed shields-badge.md to shieldsbadge.md
156. b703ec6f docs/en/components/shields-badge.md: updated for release quality
157. 6beefd62 layouts/shortcodes: added shieldsbadge shortcode
158. f930700f pkg/components/internal/shieldsbadge: refactored for scalability
159. 3ee2c45c docs/en/shortcodes/svg_shieldTag.md: set redirect to component
160. 73af9d45 pkg/components/internal/shieldbadge: refactored for quality
161. f7f8a772 pkg/components/internal/shieldsbadge: enabled print mixin
162. 3bcaf012 pkg/components/internal/shieldsbadge: renamed _Pkg.scss to _index.scss
163. 986e1d25 docs/en/components/divider.md: updated to release quality
164. ee2004db pkg/components/internal/divider: refactored for quality
165. bdd07bfe pkg/components/internal/grid: fixed grid block display bug
166. b69837fd pkg/components/internal/divider: enabled print mixin
167. 8bfc227f pkg/components/internal/navsidebar: fixed tablet and above layout bug
168. d4f4cb06 pkg/components/internal/navsidebar: fixed list style bullet problem
169. 340ccf42 pkg/components/internal/divider: renamed _Pkg.scss to _index.scss
170. 5f23afbb docs/en/components/blockquote.md: updated to release quality
171. 4a0be6ea pkg/components/internal/blockquote: refactored for better quality
172. baec7390 pkg/components/internal/blockquotes: enabled print mixin
173. 711d5819 pkg/components/internal/blockquote: renamed _Pkg.scss to _index.scss
174. 249a9fa4 pkg/components/internal/deflist: enable print mixin
175. fd71e4b2 docs/en/components/definitions-list.md: updated to release quality
176. 3efac36e pkg/components/internal/deflist: refactored Sass for uniformity
177. 4edb29b1 pkg/components/internal/deflist: applied Sass _index.scss over _Pkg.scss
178. b7d4a942 pkg/components/internal/list: restored unordered list disc bullet
179. 9f2618f6 docs/en/components/list.md: updated document to release quality
180. eb9149ac pkg/components/internal/list: use Sass default list type
181. 8dac4592 pkg/components/internal/list: updated Sass to use common fields
182. 60db8467 pkg/components/internal/list: refactored Sass parts
183. 48a4698b pkg/components/internal/list: enabled print styling
184. 9f15dfce pkg/components/internal/list: use Sass _index.scss over _Pkg.scss
185. c829e213 pkg/components/internal/grid: removed css-display for row
186. 934d19b2 docs/en/components/grid.md: updated documentations to release quality
187. 33065a90 pkg/components/internal/grid: refactored Sass to match common pattern
188. e524789a pkg/components/internal/grid: enable print CSS mixin
189. 59053bd7 pkg/components/internal/grid: corrected _Pkg.scss to _index.scss
190. a019540d docs/en/components/nav-sidebar.md: simplified document
191. 2a9a5660 pkg/components/internal/image: honored all CSS variables from R&D
192. 67219b0d layouts/partials/shortcodes/_picture.html: added multi-lang AltText
193. f9d67531 pkg/components/internal/image: removed Caption field
194. b9945192 layouts/partials/shortcodes/image.html: added error checking for .Layout
195. 84d8071c layouts/partials/shortcodes/image.html: added Layout support for picture
196. 0bdf1166 .sites/themes/main/.../data/picture/sample.toml: updated as per doc
197. 4bb7dffc pkg/components/internal/image: updated Sass to match documentations
198. 2892fb2b docs/en/*/image.md: updated documentations matching the latest
199. 7087f2b1 docs/en/shortcodes/image.md: updated image shortcode documentation
200. be9000d4 pkg/components/internal/image: added print CSS support
201. f0dd8506 components/internal/image: complied to Sass file structure
202. 17a858db layouts/partials/shortcodes/image.html: fixed srcList parsing bug
203. 2c61f1f2 pkg/components/internal/image: fixed picture closing spacing issue
204. 5e87c65b .configs/helix/Hugo.sh: bumped Hugo to version 0.83.1
205. 5861171c pkg/components/internal/image: added Sizes attributes
206. 8a1d4e67 pkg/component/internal/image: added picture HTML tag support
207. 6b8d11b0 layouts/partials/shortcodes/_iframe.toml: removed reserved type parameter
208. 735a1169 docs/en/*/iframe.md: updated documentations to reflect the latest
209. c343072c pkg/components/internal/iframe: facilitated print query
210. e491fb16 pkg/components/internal/iframe: refactored Sass for code cleanliness
211. 381656a5 pkg/components/internal/iframe/IFrame.go: added ID HTML attribute
212. db63cdae components/internal/iframe: updated framework to match Sass structure
213. 1bf019f6 pkg/components/internal/anchor/Anchor.go: tidy up anchor rendering
214. d46f9aa0 pkg/components/internal/anchor: added tablet.scss compiler file
215. d9138af8 docs/en/components/anchor.md: updated to latest documentation
216. 9eebd477 pkg/components/internal/anchor: fixed bad merged from link component
217. 1455319c pkg/components/internal/anchor: added print support
218. 1f2f031b pkg/components/internal/anchor: restructure to comply Sass structure
219. db65a847 pkg/components/internal/navsidebar: fixed w3c validation issues
220. 5c7287c8 pkg/components/internal/navsidebar: added CSS variable for print display
221. f65cafc6 pkg/components/internal/navsidebar: added print mode
222. 8960338c pkg/components/internal/anchor: fixed word-break bug with long link
223. 73ee9289 pkg/components/internal/navsidebar: fixed known sidebar bugs
224. 8f9f0e90 .configs/sass/files: fixed media query loading
225. deb4ea12 app/bissetii.sh: removed render to disk option
226. f74a4bac layouts/shortcodes/bissetii-css.html: migrated to site-specific theme
227. a242f42f data/bissetii/data: shift to using site-specific theme module data
228. e3b8f204 layouts/shortcodes/bissetii-css.html: fixed SRI variable mislabelling
229. 339acf53 pkg/components/internal: enabled print css stylesheet
230. a016ae28 docs/en/components/typography.md: deprecated it
231. 4563d216 docs/en/components/navbar.md: deprecated it
232. 1ac016ca docs/en/components/color.md: deprecated old color component
233. 30c9fe9a docs/en/releases/v1-13-0.md: updated with Root Component.
234. b5663320 docs/en/components/root.md: added HTML documentations
235. 121ccf36 docs/en/components: created root component
236. caad21f0 pkg/components/internal/root/_Timing.scss: refactored for cleanup
237. 1b27b769 layouts: removed all emoji fonts rendering components
238. ea1772e9 pkg/components/internal/root: added font-face support for Noto fonts
239. f466b065 pkg/components/internal/anchor: refactored styling for finalization
240. 3cad0c7c pkg/components/internal/anchor: merged duplicated link component
241. d0846e30 pkg: organized internal into components/ directory
242. 540755b0 layouts/partials/functions: added generateBody.html
243. e4ade153 pkg/internal/navsidebar/_NavSidebar.scss: use Z-Index-Nav-Topping
244. 08ba93d9 pkg/internal/root: refactored for tablet, desktop, and widescreen
245. f061ca7f pkg/internal/root: renamed _Pkg.scss to _index.scss
246. f662f1b9 docs/en/go/internal: added navsidebar/_index.md
247. 612203b0 .scripts: fixed CI workaround issues
248. ad3b8191 .scripts/site-ci.sh: fixed non-functioning -d command
249. 3142073d pkg/internal/navsidebar: renamed _Pkg.scss to _index.scss
250. 4e8661ec pkg/internal/navsidebar: supported desktop and widescreen styling
251. f30748db pkg/internal/navsidebar/_Pkg.scss: supported tablet screen
252. 7135c3a9 pkg/internal/navsidebar/_Pkg.scss: further enhanced close button
253. ef28665f docs/en/components/nav-sidebar.md: corrected AMP miscommunications
254. 77a84107 pkg/internal/navsidebar/_Pkg.scss: fixed close button positioning bug
255. 74ed049a pkg/internal/navsidebar/NavSidebar.go: added drawer flag support
256. eef89f63 layouts/partials/shortcodes/__page.html: added DrawerModeOnly flag
257. 094de425 .sites/archetypes/components.md: reorganize template from feedback
258. 009b32c5 docs/en/components: added nav-sidebar.md component documentation
259. c3abf79c .sites/archetypes/components.md: updated to single components section
260. e2940d8c internal/navsidebar: styled right-side sidebar
261. 95cff3c3 layouts/partials/shortcodes/__page.html: added right-side sidebar
262. d811d579 pkg/internal/root/_Layout.scss: updated to facilitate right sidebar
263. 3c49bc1b pkg/internal/root/_Font.scss: set "Noto Sans" and "Noto Serif" as priority
264. 21571401 pkg/internal/navsidebar/_Pkg.scss: styled title with logo
265. da357b29 layouts/partials/shortcodes/__page.html: facilitated nav logo
266. ed18d2cd data/bissetii/nav.toml: update submenu icon to a better looking one
267. 65ea4b14 layouts/partials/shortcodes/__page.html: fixed class bracquet bug
268. 89daea27 data/bissetii/nav.toml: facilitate submenu icon customization
269. 8eb49e02 pkg/internal: added navsidebar component left-side only styling
270. 84ef8f32 pkg/internal: added Go compatible navsidebar component
271. bca40027 internal/card/Card.go: set to use css variable instead
272. dae1b15a pkg/internal/card: fixed bad margin bug
273. 12c1598d pkg/internal/navcatalog: added nav-catalog Sass
274. 7767cf0b .configs/helix: bumped all dependencies version to the latest
275. 6b0ffd54 pkg/internal: ported card Sass into new implementations
276. 00d374a0 pkg/internal/grid/_Pkg.scss: fixed the max-width bug when border is applied
277. 633be1ce pkg/internal/carousel: ported to new implementation
278. 668abff0 pkg/internal: updated copyright year to 2021
279. c4eb91da pkg/internal/form: ported Sass parts into new implemnentation
280. 0d82213b pkg/internal: ported Grid Sass component
281. 9e0237fa pkg/internal: ported table sass component
282. e18831d2 pkg/internal/note: ported Sass component
283. 055a7393 pkg/internal/note: refactored to use inline svg
284. 8e8e269a pkg/internal/root/_Layout.scss: add max-width 100vw to main
285. 090a1d23 pkg/internal/navtoc: added navtoc Sass component
286. 0f004dd7 root: compiled Sass artifacts to checkpoint current state
287. bd333583 pkg/internal/root/_Font.scss: restored default font-weight to normal
288. 36649176 pkg/internal: ported code Sass component
289. d893c17c pkg/internal: ported shieldsbadge Sass component
290. e78d6961 pkg/internal: ported divider Sass component
291. f5987db0 pkg/internal: ported Sass for blockquote component
292. 55d97e0d pkg/internal: ported deflist Sass component
293. 5dc75a41 pkg/internal/root: added Grey-300 and Grey-400
294. 3d0ef650 pkg/internal/list: ported CSS component
295. a7447aef pkg/internal/iframe: ported Sass component
296. d5d13b79 pkg/internal: added license clauses into sass control files
297. d9c47e4c pkg/internal/root: added license clauses into Sass component
298. 05932d62 pkg/internal: ported image Sass component
299. 693202d9 pkg/internal: ported anchor sass component
300. 030b2b8e pkg/internal/root: ported Sass _Init component
301. efb68795 pkg/internal/root/_Color.scss: refactored based on test results
302. ddd251b1 pkg/internal/root/_Font.scss: refactored based on testing
303. f3637d43 pkg/internal/root: fixed layout for responsive design
304. 333b8883 root: upgraded Sass CI/CD job to deploy all CSS files
305. d6d85a5b pkg/internal/*/CSS.go: generated all CSS files using Go CSS automation
306. 8e9cc025 .scripts: added Go CSS compilation for each Go internal components
307. 187fd0c4 pkg/internal: ported base layout sass into root component
308. 96d9aa04 pkg/internal: added all media device width oriented sass main file
309. 66249f51 pkg/internal/root: added root component font configurations
310. 7c275740 pkg/internal: added css variables as the new sass skeletal framework
311. 02a57b7f .configs/helix/Dart-Sass.sh: updated to symbolic link executable
312. f33d13f2 layouts/partials/templates/redirect.html: styled redirect template
313. 97bcd8bb .scripts/helpers/helix.sh: upgraded to generate helix config for sourcing
314. 7349df98 pkg/internal/image/Image.go: added caption parameter and rendering
315. 4b0d607b docs/en/releases/v1-13-0.md: added translate shortcode
316. 823b4c18 docs/en/how-to-go/getting-started.md: updated CTA link
317. a28809a0 i18n: removed unused i18n/ directory
318. 3cb8a84d layouts/partials/shortcodes/__page.html: use translate sc. for menu
319. 1b730f08 layouts/partials/shortocodes/__page.html: use translate sc. for copyright
320. 47b70c4c layouts/partials/templates/list.html: updated to use translate shortcode
321. d4c4207b layouts/shortcodes: added translate shortcode
322. f12d4e2c docs/en/go: updated to match the latest pkg/ directory pattern
323. 9ca2ecd2 .scripts: upgraded to process pkg/ as go root directory
324. 38aa8f6e pkg/internal: migrated navtoc into new directory structure pattern
325. c9dbee01 pkg/internal: migrated meta component into new directory pattern
326. f5e92ca9 pkg/internal: migrated google and twitter vendors to new directory pattern
327. 50677446 pkg/internal: migrated navcatalog into the new directory structure
328. e611ca5a pkg/internal: ported card/ component into new directory structure
329. 664e32ad pkg/internal: migrated note component into new directory structure
330. e5204385 pkg/internal: migrated image component into new directory pattern
331. c6b5877d pkg/internal: migrated css components into new directory structure
332. 24e18a7c pkg/internal: migrated footerbasic component into new directory structure
333. b64a4bb7 pkg/internal: migrated form component into new directory pattern
334. 1ed5e233 pkg/internal: migrated list component into new directory pattern
335. 69481ba6 pkg/internal: migrated shieldsbadge component into new directory pattern
336. 5e43b4a3 pkg/internal: migrated carousel component into new directory structure
337. c4de1f86 pkg/internal: ported iframe component into new directory pattern
338. 8b411f23 pkg/internal: migrated anchor component to new directory pattern
339. 40ae5368 pkg/internal: migrated link component into new directory pattern
340. 530b3fd3 .scripts/site-ci.sh: fixed missing bashell.sh script
341. 67a61677 pkg/internal: migrated style component into new directory pattern
342. 8ce8d13c pkg/internal: migrated script component to new directory pattern
343. aa82172e pkg/internal: shift root component into pkg/internal directory
344. e9b20460 pkg/bissetii: shifted root directory into using pkg/ convention
345. 71a130a8 .scripts/helpers/gophers.sh: provide pathing parameter
346. 6fa7efa0 .scripts/helpers/godocgen.sh: updated to handle parameters
347. 0e2b978a .scripts: migrate all helpers scripts into helpers/ directory
348. 2e80b4d9 .scripts: shift helix.sh into helpers/helix.sh
349. 7de123e7 internal/components/style: ported <style> component into Go package
350. 44951e55 internal/components: ported <link> component into Go package
351. f247728c internal/components: renamed link/ to anchor/ component
352. e74df7d4 .sites/documents: consolidate all document processors
353. c29761ce internal/components/script: ported script component to Go interface
354. 590981e5 internal/components/badge/shieldbadge/Shields.go: deprecate 5th params
355. 0be5eb67 data/bissetii/data/sponsors: updated CTA to use .Content over .Label
356. 142c7219 layouts/partials/shortcodes/cards.html: fixed missing column in grid
357. 1c2c407d docs/en/how-to/configure-browser-settings.md: removed invalid docs
358. f90a7e5d docs: clean up broken first-time user experience links
359. 326544a1 docs/*: fixed all errors and missing fields
360. 5efe6496 layouts/shortcodes/link.*.html: implemented inner content
361. 43af8aec docs/en/getting-started.md: updated to latest configurations
362. e4f19495 docs/en/license.md: refactored to the latest format
363. 829395f8 .sites/static/license: added GPG signed PDF version of license
364. 2f2715d3 data/*: updated to match recent link shortcode implementation
365. 17955897 layouts/partials/shortcodes/link.html: added GivenLang parameter
366. a284a2d4 layouts/partials/functions/link.html: updates to comply RFC3986
367. 704e2e91 layouts/partials/shortcodes/iframe.html: fix missing .context bug
368. f2b4ec88 docs/en/releases/v1-13-0.md: added `nav-catalog` component update
369. 5a337dff .configs/helix/Hugo.sh: bump Hugo version to latest v0.79.0
370. 44bddc4c internal/components/nav: ported navcatalog into Go package
371. c8a925c9 .sites/archetypes/*: updated to match the latest setup
372. ed93330d archetypes/redirect.md: removed redundant redirect
373. a868f1b8 docs/en/releases/v1-13-0.md: added 480x480 thumbnail changes
374. ec293ac7 static/img/thumbnails: added default-480x480.png image
375. 41e52a49 internal/components/meta: added og:image:type meta tag
376. c802e907 docs/en/releases/v1-13-0.md: added nav-toc into new changes
377. c4a100de docs/en/components: added nav-toc.md document into repository
378. 6f2c2bbd .sites/archetypes/{components,shortcodes}.md: apply beta notices
379. 3236bd1b internal/components/nav: ported navtoc into Go package
380. db9496ea internal/components/meta/Meta.go: merge .Section and .Nav data structure
381. f258486f static/favicon*: updated icons to the latest designs
382. 7a408010 docs/en/releases/*: updated to use PNG format for thumbnails
383. 906b990c docs/en/releases/v1-13-0.md: updated release note with logo update
384. b342501b root: updated thumbnails to the Bissetii 13 version
385. 35a8528f root: upgraded logo for Bissetii 13
386. f579b263 internal/components/card/Card.go: added AMPName
387. a337a9a4 internal/components: ported list component into Go interface
388. 593c7e05 docs/en/release/v1-13-0.md: added form extended mod deprecation notice
389. b351733f internal/components: ported form into Go package
390. 445f6a36 docs/en/releases: created v1-13-0.md release note
391. b601fd7e .sites/archetypes: added releases.md for tracking release notes
392. 24b34853 layouts/partials/shortcodes/*card.html: fixed missing image width height
393. 0e1fe61d internal/components/*: applied AMPName field
394. 08ef8063 .godocgen/templates/md: updated to match bissetii settings
395. 82989c28 internal/components: added OutputFormat control package
396. 9bd03aba pkg/helpers/hugohelpers/Functions.go: added TOML data generator
397. 0f83db29 sass/bissetii/modules/core/_Base.scss: corrected footer margin
398. 4df4eb94 docs/en/sponsoring.md: updated to remove broken banner
399. 2d817278 .sites/archetypes/*: fixed modules.extensions example typo
400. 17400fdd archetypes: fixed wrong example for modules.extensions
401. b06f18bf internal: shifted components/ to internal/ directory
402. e3b54650 components/: ported card component into Go package
403. 973e85e5 layouts/partials/functions: facilitate SrcSet data processor
404. c7fdbba5 components/link: added .Content parameter
405. 83780c87 data/bissetii/sass.toml: removed unused sass.toml data file
406. fd954feb components: ported Carousel into Go package
407. fce4e91b components/note: updated to import image dependency
408. 15d73061 layouts/partials/functions: added safety filters to all outputs
409. cdb715ce components: ported badges/shields into Go interface
410. 18be7ead layouts/partials/*: ported shield badges for Go interface
411. 789534c2 layouts/partials/functions: added hexColor.html data processor
412. 2c9fbcf6 docs/en/: updated to comply with checkbot SEO reports.
413. 71428bed layouts/robots.txt: added managable configurators
414. 809f8662 root: housekeeping unused files
415. ff155d63 docs/en/shortcodes: removed absLangLink, absLink, and sponsors
416. d55596e6 docs/en/how-to/manage-robots.md: updated documentations
417. 7197b93a layouts/robots.txt: implemented Hugo layout for robots.txt
418. 599d8c87 layouts/partials/templates/base.html: fixed fatal template
419. 5f7415dd components: note: fixed icon URL source path
420. 292ee76c .sites/archetypes: updated to use modules extensions
421. de54ae70 components/meta/Meta.go: added extensions feature
422. 96369fdf data/bissetii: reorganize unused navbars.toml to blocklist.toml
423. 3b9831bd layouts/404.html: updated 404 page to match latest designs
424. f458aa00 root: added Resources with Icons feature
425. 679d4168 layouts/partials/templates/list.html: fixed nav-sidebar for amp
426. 10781e3c components/meta/Meta.go: add canonical hinting support
427. 9edf531c sass: shift entire assets/sass out from assets directory
428. e8d243fa components/nav/Nav.go: ported navigation general data structure
429. a4f1a033 components/nav/Nav.go: added navigation links list data structure
430. 40fb512c components/link/Link.go: added id attribute
431. 3091b9c5 docs/en/how-to-go: added getting-started.md Go documentation page
432. dbf05c99 docs/en: shifts how-to-go/ to go/
433. 7dcf7bfa components: added link component into Go
434. 8a3210ef .sites/archetypes: updated to prioritize 1200x628 thumbnails
435. 8107cf8c docs/en/how-to/manage-social-sharing.md: reflect latest settings
436. 34164d16 components/vendors/twitter/Twitter.go: corrected Twitter product
437. 8bb841df data/bissetii/thumbnails.toml: re-prioritize thumbnails
438. 07ad4c89 .sites/config/_default/params.toml: removed unused configs
439. cee42ac8 components/meta/Meta.go: group important data structure together
440. c0124a6e components/meta/Meta.go: ported Vendor data structure
441. 6b10f2a8 layouts/shortcodes/bissetii-css.html: fixed download link issues
442. 67bdf824 components/meta/Meta.go: use image.Data as thumbnails data structure
443. 7662d674 components/image/Image.go: fixed missing fields
444. b6f4ecec go.mod: replace its own as local directory
445. 7bb2d372 assets: renamed css/ to sass/
446. fc9dbea7 layouts/partial/shortcodes/_iframe.html: re-organize for partial/shortcodes
447. a06695c1 layouts/shortcodes/renderHTML.*.html: comply to new shortcode approach
448. 7b0e6acd layouts/partials/shortcodes/emoji.css: fixed link issue
449. 424ae59e layouts/partials/shortcodes/image.html: standardized coding practice
450. b645c9d2 layouts/shortcodes: removed sponsors/PayPalME.html
451. 8602d135 layouts/functions/link.html: re-organize for layout/shortcodes
452. f09e1da2 layouts/shortcodes: removed absLangLink and absLink
453. 6657c6e5 components/image/Image.go: corrected .Title to .AltText
454. 05e0c848 layouts/partial/shortcodes/_note.html: facilitate data preprocessing
455. 14d1b09e layouts/partials/shortcodes/_image.html: provide parameterized input
456. 51c3471e components/meta/Meta.go: ported .Section data structure
457. 755a5773 .sites/config/_default/config.toml: use CDN bissetii.zoralab.com
458. fa974a2b components/meta/Meta.go: ported .Page.Copyright data structure
459. 97be030f components/meta/Meta.go: ported .Page.Content data structure
460. 6b7942ea layouts/partials/shortcodes/link.html: fixed missing outputURL origin
461. c3306940 components/meta/Meta.go: ported .Page.Schema data structure
462. e30d4435 components/meta/Meta.go: restructured .Page.Robots data structure
463. 001fd206 components/meta/Meta.go: restructured .Page.Thumbnails data structure
464. 5ff19218 layouts/partials/shortcodes/__page.html: fixed LD+JSON parsing algorithm
465. 45d9368d layouts/partials/hugo-only/blocks/nav.html: fixed title problem
466. de247068 test/scripts/bissetii_sh: fixed language specific test failures
467. 13c644eb .scripts/helpers/godocgen.sh: stop run if missing OUTPUT_PATH
468. a37f11de .scripts/helpers/godocgen.sh: fixed godocgen.sh CI hard-code issue
469. e0da5e13 components/meta/Meta.go: restructured .Page.Alternative data structure
470. 3858f9de root: corrected the proper language code for Bissetii
471. a4e2da15 components/meta/Meta.go: re-structured Page.Language field
472. 96a2da99 components/meta/Meta.go: restructuring basic Page data structure
473. cda2f85f layouts/shortcodes/iframe.*.html: fixed shortcode issues
474. 7c57ff9a app/bissetii.sh: added renderToDisk and --gc for hugo server
475. 98a49890 layouts/shortcodes/note.*.html: fixed shortcode issues
476. 17b9a792 layouts/partials/shortcodes/__page.html: corrected layout selection algo
477. 6a0d1cab layouts/partials/shortcodes: unified renderHTML.html algorithm
478. 2ef4b30a .configs/helix/Dart-Sass.sh: bumped to version 1.29.0
479. dd4c45a2 root: migrated layouts/_default to single template partialling
480. 4540db38 root: re-organized JS data structure, processes, and functions
481. f3c3e2ed docs/en-us/how-to/include-css.md: updated include CSS documentation
482. e8df2264 root: fixed CSS CI pipeline to comply with the new CSS data structure
483. 10909b45 root: re-organized CSS data structure, processes, and functions
484. e78979c7 root: re-structured sass CI and its data management
485. b7a1dc3b components/footer: ported basicfooter Go package
486. 546d5a5a .configs/helix/GolangCI-Lint.sh: bump GolangCI-Lint to version 1.32.2
487. fba1129a pkg/helpers/hugohelper: applied golangci-lint changes
488. 4112870f root: added .golangci.yml linter configuration file
489. b3cdc0ce components/meta/Meta.go: ported amp/boilerplate.html
490. 69477595 components/*: synchonized all documentations
491. b1591d19 layouts/partials/*/metas*: removed unused meta html codes
492. 20b86b74 layouts/partials/functions: added outputFormat.html replacing outputType.html
493. 52fad4e9 layouts/partials/hugo-only/cards/card.html: use parseContent partial
494. c306c619 .configs/helix/Hugo.sh: updated to use Hugo version 0.78.2
495. 70fe76f8 components: added iframe component Go package
496. 844e1d57 layouts/shortcodes: added link.html partial level shortcode
497. aaefe9a1 assets/css/bissetii/modules/nav/_Sidebar.scss: fixed content width
498. df4c5bae components/css: added cssfont Go package component
499. 6d0aef23 docs/en-us/*/*emoji.md: updated docs to reflect the correct version
500. 661a1cb6 root: update to v1.13.0 as we are breaking things for Go support
501. c2378e78 root: migrated emoji configurations from Sass to inline HTML
502. 888a1e96 components: ported Go package for note component
503. 777bcfa2 components: added Go package for image and its associated tags
504. 574fb8a0 docs/en-us/components/table.md: corrected </tr> closure
505. 9801fa4e layouts/partials/shortcodes/image.html: use dictionary parameters
506. 14f1b3bf layouts/partials/functions/outputType.html: use centralized data source
507. 4645c89f layouts/shortcodes/image.html: consolidate shortcode into 1
508. 733d2ceb layouts/partials/functions: added outputType identification partial
509. 22cb6bc1 root: added go data codes and documentations in root repository
510. b7d127d7 root: successfully cross-compatible between goHugo and Go
511. d3c6506b components: added meta HTML template component package.
512. a0ba2b1c pkg/helpers: added hugohelper into repository
513. c9ee83ed root: added gopher.sh integrations to CI
514. 0b1d0772 docs/en-us/_index.md: fixed getting started URL call-to-action link
515. cadc56db data/bissetii/data/sponsors/zoralab.toml: corrected banner image URL
516. 009d5bd4 root: added godocgen automation into CI
517. a790e582 root: initialized go module for Go development
518. cf807bb5 data/bissetii/data/sponsors/zoralab.toml: sync up typography
519. 62bb1339 .scripts/bissetii-ci.sh: renamed to .scripts/site-ci.sh
520. 6fa16cab .configs/helix/Hugo.sh: upgraded to use hugo v0.75.0
521. 2e84904e app/bissetii.sh: updated to remove bissetii-specific data directory
522. 9b07baca config/_default/markup.toml: updated syntax highlighting template
523. 23c3842c app/bissetii.sh: add delete artifact publish path before build
524. d0bfed2a data/bissetii/data/sponsors: set card to 400x400
525. 4397f943 sponsors: special thanks to Johnson Lam for His Sponsorship
526. 10896f83 app/bissetii.sh: prepare v1.12.6 development

# Version 1.12.5
## Sat, 12 Sep 2020 23:02:42 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. 598800b .scripts/reprepro.sh: fixed relative path warning issue
2. c957058 root: shift all assets/static/releases back into .sites/static
3. bf2b8f0 .scripts/bissetii-ci.sh: added localized CSS usage for Hugo
4. b80ce33 .scripts/bissetii-ci.sh: added manta clean up before build
5. 6df7092 .scripts/bissetii-ci.sh: added build_css to release pipeline
6. 4cd3674 app/bissetii.sh: prepare v1.12.5 development

# Version 1.12.4
## Sat, 12 Sep 2020 14:33:08 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. 7758359 docs/en-us/how-to-sass: change parent category to B from M
2. 0e87154 docs/en-us/getting-started: setup multi-tech getting started guides
3. 2c4b922 .sites/config/_default/markup.toml: change syntax to pygments
4. 20ff1e6 assets/css: remove unused sass configurations
5. bec9be6 layouts/_default/baseof.html: fixed baseURL double slash bug
6. 4c14662 layouts/partials/hugo-only/blocks/css.html: load amp css using resources
7. 08e81ed assets: added static directory to facilitate all release artifacts
8. 09f0565 docs/en-us: migrate entire getting started category to hugo section
9. d7c89a6 docs/en-us/how-to-sass/download-css.md: added amp css into list
10. 95e06a0 .gitignore: added all empty version css file
11. c127e8d assets/css/bissetii/modules/card/_Base.scss: fixed animation bug
12. 1cf7932 docs/en-us/how-to-sass/available-css.md: renamed to download-css.md
13. 43d78e3 docs/en-us/how-to-sass: added available-css.md for downloads
14. a7047b0 .sites/archetypes: added new how-to-sass.md template
15. d60bb51 .sites/archetypes: updated accordingly to archetypes/default.md
16. 8962de5 .scripts/bissetii-ci.sh: added current_version checking before build
17. a5e3c1d layouts/_default/baseof.html: removed duplicated comment
18. 631a2e6 .scripts/bissetii-ci.sh: added individual css data file generation
19. 7c9833d docs/en-us/_index.md: updated to latest and with the latest features
20. 763ce02 .gitlab-ci.yml: fixed publish pipeline problems
21. 843f3ec layouts/partials/hugo-only/blocks/css.html: remove extended dependency
22. 5a1ed20 root: added version 1.12.4 CSS artifacts
23. a56e2d9 .gitlab-ci.yml: added git into the installation package
24. cecd59b app/bissetii.sh: added checking to build
25. 0b6b3e1 app/bissetii.sh: colorize status messages
26. d460c9e .gitlab-ci.yml: updated GitLab CI to use Bissetii CI
27. bccece6 .scripts/bissetii-ci.sh: fixed css_assets_path mis-watch bug
28. 276e3a1 assets/css/bissetii/modules/card/_Base.scss: added embed styling
29. d0af686 layouts/partials/hugo-only/js: tested Javascript in non-extended hugo
30. 9d2724a .gitignore: added gh-pages
31. 42ee1da .scripts/bissetii-ci.sh: added publish pipeline
32. 0dacf97 app/bissetii.sh: refactored for quality assurance
33. b78162c app/bissetii.sh: refactored publish to use built artifact instead
34. 60cbbb3 .scripts/bissetii-ci.sh: refactored for quality assurance
35. 522b121 .scripts/bissetii-ci.sh: added release pipeline
36. 243083e .scripts/bissetii-ci.sh: added test pipelines
37. 45db896 test/.../able_to_install_critical_directories.test: mocked git pull
38. 2190b80 app/bissetii.sh: upgrade __shift_404 to use __get_publish_path
39. 08de5b8 .scripts/bissetii-ci.sh: added hugo build into build pipeline
40. 64813c9 .scripts/bissetii-ci.sh: rename run to build
41. f7a78d6 .scripts/bissetii-ci.sh: added local hugo program support
42. d8e2777 .scripts/bissetii-ci.sh: added setup.sh as part of CI program
43. e831ca1 .scripts: added helix.sh dependencies automator program
44. 2bbc5ec .scripts: removed unused gh-pages.sh
45. 598dde9 layouts/partials/hugo-only/css: remove unused direct.html
46. 32d7d98 .scripts/bissetii-ci.sh: upgraded to use scalable automation approach
47. beb55fd .scripts/bissetii-ci.sh: added support to AMP CSS output
48. adee322 layouts/_default/baseof.*.html: fixed fatal CSS and JS inclusion bug
49. 0e8c3a6 assets/css/bissetii/modules/core/_Base.scss: fixed tablet padding
50. 273d5e0 assets/css/bissetii/modules/nav/_Sidebar.scss: fixed tablet bugs
51. 2993237 .scripts: added bissetii-ci.sh to standardize process
52. 0b03f89 assets/css:*.sass: set @charset for all non-amp Sass files
53. e594780 assets/css: corrected all Sass scripts
54. 9a8b6b2 .scripts: added sass-setup.sh
55. 0fff828 layouts/_default: added sitemap.xml processing to remove redirect pages
56. a83223f layouts/partials/list/toc-card.*.html: added thumbnail supports
57. 6d13760 app/bissetii.sh: added publish function to unify process
58. aff3d94 .sites/config/_default/markup.toml: use back monokailight for code
59. 954bc98 assets/css/bissetii/modules/nav/_Sidebar.scss: fixed max-width bug
60. 9d95803 .sites/static/releases: updated release deb package
61. b0ea82d app/bissetii.sh: prepare for v1.12.4 development

# Version 1.12.3
## Tue, 18 Aug 2020 12:17:39 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. 62755c6 docs/en-us/*: updated all beta notices to single format
2. 8462527 layouts/partials/hugo-only/cards/card.html: added multi-format rendering
3. f10539f layouts/shortcodes/card.html: added grid system rendering feature
4. e1a4490 changelog: updated spelling error
5. ce573dd .sites/static/releases/deb: updated to package 1.12.2 deb
6. 6c34ac9 app/bissetii.sh: prepare for version 1.12.3 development

# Version 1.12.2
## Sun, 16 Aug 2020 00:15:18 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. c6778af assets/css/bissetii/modules/core/_Grid.scss: fixed horizontal spacing bug
2. 89f0e51 layouts/_default/list.*.html: restyled for better display
3. c356018 squash
4. fa68f13 docs/en-us/how-to/create-redirect.md: added menu url overwriting guide
5. 9a45868 layouts/_default/baseof.*.html: fixed nav blocklist to child item bug
6. 4036b3b assets/css/bissetii/modules/core/_Base.scss: fixed background color bug
7. 99704b7 assets/css/bissetii/modules/nav/_Sidebar.scss: fixed bug for main width
8. 99d6017 root: updated deb package server to v1.12.1

# Version 1.12.1
## Thu, 13 Aug 2020 15:59:48 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. 879a28c docs/en-us/how-to/customize-schema-org.md: updated thumbnails
2. 1f84c62 docs/en-us/how-to/manage-amp.md: updated thumbnails
3. 31a83da docs/en-us/license.md: updated thumbnail images for sharing
4. 1a45d97 docs/en-us/specifications: updated for release
5. 28252c5 docs/en-us/components: refactored for release
6. 4789406 docs/en-us/shortcodes: updated for release
7. 6a2060a docs/en-us/how-to: updated documentations for release
8. 11542c4 docs/en-us/_index.md: updated for release.
9. cb8fd7e assets/css/bissetii/modules/card/_Base.scss: centered all cards
10. deac474 assets/css/bissetii/modules/nav: refactored for print
11. e778491 assets/css/bissetii/modules/badges/_Shields.scss: refactored for print
12. deb69a2 assets/css/bissetii/modules/card/_Base.scss: refactored for print
13. 05936ba assets/css/bissetii/modules/carousel/_Base.scss: refactored for style
14. 6e5a0d0 assets/css/bissetii/modules/core/_Scrollbar.scss: refactors for print
15. 6a7d23a assets/css/bissetii/modules/core/_Grid.scss: refactored for print
16. 9eb4b1c assets/css/bissetii/modules/core/_Form.scss: refactored for print
17. ec9acb0 assets/css/bissetii/modules/core/_Divider.scss: refactored for print
18. 413bc87 assets/css/bissetii/modules/core/_DefinitionList.scss: refactored for print
19. 15fc39a assets/css/bissetii/modules/core/_Code.scss: refactored for print
20. 5689383 assets/css/bissetii/modules/core/_Blockquote.scss: refactored for print
21. 563bf1d assets/css/bissetii/modules/core/_Iframe.scss: ported for print
22. 5b1ca34 assets/css/bissetii/modules/core/_Image.scss: refactored for print
23. 75553dd assets/css/bissetii/modules/core/_Button.scss: refactored for print
24. 6863604 assets/css/bissetii/module/core/_Link.scss: refactored for print
25. 8e37c92 assets/css/bissetii/modules/core: ported nav/_Base.scss into core
26. 283f797 assets/css/bissetii/modules/core/_List.scss: refactored for print
27. f972e08 assets/css/bissetii/modules/core/_Note.scss: refactored for print
28. 4eae9f9 assets/css/bissetii/modules/core/_Table.scss: refactored for print
29. 6cc301b assets/css/bissetii/modules/core/_Typography.scss: restyled for print
30. f0c2184 assets/css/bissetii/modules/core/_Base.scss: restyled for print
31. 4752cc1 docs/en-us/shortcodes/card.md: added multiple thumbnails FYI
32. ebe0484 layouts/shortcodes/form.*.html: ported for using hugo-only partial
33. 680d346 layouts/shortcodes/link.*.html: ported to fully use hugo-only partials
34. 56ce134 layouts/shortcodes/iframe.*.html: ported to use hugo-only partials
35. b7a06e5 layouts/shortcodes/note.*.html: ported to use hugo-only partials
36. b4cee9a layouts/shortcodes/renderHTML.*.html: ported to use hugo-only partials
37. 1b60842 layouts/shortcodes/carousel.*.html: ported hugo-only carousel usage.
38. 791c2c4 root: added card component and its associated shortcode
39. 173e601 assets/css/bissetii/modules/core/_Init.scss: added box-shadow mixin
40. 38ef249 assets/css/bissetii/modules/core/_Init.scss: added transform mixin
41. d416fcb layouts/shortcodes/image.*.html: shift algo to partials
42. b4e83ce layouts/partials/hugo-only/links/link.html: fixed base "/" rendering
43. deb984a docs/en-us/how-to/custom-404.md: updated documentation for custom-404.
44. 0968457 app/bissetii.sh: backported __shift_404 function
45. 81efd5e layouts/404.html: redesigned for single page multiple languages
46. 6252964 layouts/partials/hugo-only/css: fixed css inclusion and tag bugs
47. 5e4a29f root: added iframe component
48. 4c8ffd8 .sites/archetypes: fixed template problems
49. ef24b56 root: added note component
50. 9ffce22 assets/css/bissetii/.../core/color: harmonized color schemes
51. df6683d assets/css/bissetii/modules/core/_Init.scss: added animation mixin
52. c262f48 layouts/_default/baseof.*.html: added menu item block list
53. fafe9dc root: updates thumbnails to use multiple images
54. 7e799a1 assets/css/bissetii/.../carousel/_Base.scss: added print rendering
55. 2c66e82 root/*/carousel: added AMP HTML support
56. 3e99d5f layouts/shortcodes/link.amp.html: fixed bad index.amp.html append bug
57. 5b1dd4a docs/en-us/components/carousel.md: added original creators attributions
58. 907a349 .sites/archetypes/shortcodes.md: fixed description toml field
59. c6c0158 layouts/shortcodes: added carousel shortcode
60. 3104006 layouts/partials/images/amp-img.html: removed typo closure
61. 3bd4b28 app/bissetii.sh: added --command function
62. a9e7488 root: added carousel component for vanilla HTML
63. 19a5d66 assets/css/bissetii/modules/core/_Init.scss: added keyframe helper
64. d55e8d9 .sites/archetypes: fixed typo
65. 7904c34 assets/css/bissetii/..core/_Typography.scss: added clean h2 and h3
66. 5f191cd app/bissetii.sh: added get_theme to get module
67. faec409 app/bissetii.sh: corrected option numbering
68. 6159ee0 docs/en-us/how-to/create-new-content.md: updated to clone module
69. 9c15516 archetypes: used dynamic param shortcode to render title and description
70. 14ce88a docs/en-us/components/form.md: fixed missing indexLink

# Version 1.12.0
## Sat, 25 Jul 2020 17:53:56 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. abac8ba README.md: removed duplicated contents and redirect to official site
2. e9e763d docs/en-us/shortcodes/sponsors_PayPalME.md: deprecate shortcode
3. b57b732 docs/en-us/how-to: refactored for release
4. f0aaafb docs/en-us/components: refactored for release
5. 338f080 docs/en-us/shortcodes: refactored for release
6. 7cccdb6 docs/en-us/specifications/upstream-process.md: refactored for release
7. 4766b26 docs/en-us/specifications/logo-and-banner.md: updated for release
8. 3b0ea1a docs/en-us/specifications/licensing.md: refactored for release
9. 1fa643a docs/en-us/specifications/file-structures.md: refactored for release
10. 6c8abb0 docs/en-us/specifications/design-concepts.md: refactored for release
11. 7f58a49 docs/en-us/specifications/codes-of-conducts.md: updated for release
12. 4b238ef root: updated root repository for release
13. 604149a docs/en-us/sponsoring.md: refactored for release
14. 01a53b9 docs/en-us/license.md: refactored for release
15. 15611a2 docs/en-us/_index.md: updated landing page profile
16. 17cb882 docs/en-us/getting-started: refactored for release
17. 45a4553 layouts/shortcodes: added link.amp.html for auto-append index.amp.html
18. 67af5e8 layouts/_default/baseof.*html: removed redirect Section Pages
19. 702c224 archetypes/redirect.md: fixed param conflicting issue
20. b8a59aa docs/en-us/getting-started: ported readme-first.md
21. c228152 docs/en-us/how-to/create-new-content.md: added publish section
22. 6c08c08 docs/en-us/_index.md: updated shell script for automating setup feature
23. b2a7ae4 docs/en-us/how-to: added create-new-content.md
24. 0a1cf08 assets/css/bissetii/modules/core/_Base.scss: shrink to A4 look
25. 2de5999 docs/en-us/getting-started/install-bissetii.md: updated for release
26. 456436b app/bissetii.sh: added docs/en-us and docs/zh-cn creation
27. b8cfc32 .sites/config/_default/config.toml: facilitates app/ for network use.
28. a03982f data/bissetii/schema: used type as filename for direct query
29. 8ed6af6 tests/scripts/bissetii_sh: added uninstall/able_to_uninstall.test
30. 83b9eb1 tests/scripts/bissetii_sh: added version/able_to_render_version.test
31. de5a489 tests/scripts/bissetii_sh: added date/able_to_render_date.test
32. dec4649 tests/scripts/bissetii_sh: added build/able_to_build.test
33. bcf5a89 test/scripts/bissetii_sh: added help/able_to_render_help.test
34. 1db18c1 tests/scripts/bissetii_sh: added run/able_to_run_server.test
35. fca2610 tests/scripts: added install test case for app/bissetii.sh
36. d766dec app: removed unused app directories
37. 606530e app/bissetii.sh: merged repo/manager.sh into single bissetii app
38. 8ae91e3 root: consolidate master config directory
39. 1a28316 .sites/archetypes: synchonized to master archetypes
40. 58aa36f archetypes: switched robots to use TOML map table
41. 789d3ff archetypes: switched page-specific amp modules to use map table
42. 4cd293a archetypes: changed default scheme to WebPage
43. 35bd323 layouts/partials/payments: refactored for release
44. 54b623b layouts/partials/navigators: refactored for release
45. b94d9a3 layouts/partials/metas: refactored for release
46. 50ecc57 layouts/partials/hugo-only/js: refactored for release
47. 128d17b layouts/partials/hugo-only/css: refactored for release
48. eeed61b layouts/partials/hugo-only/blocks: refactored for release
49. 951b404 layouts/partials: renamed hugo_only to hugo-only
50. c6ff63b assets/css/bissetii/modules/badges: refactored for release
51. 6f4e53b layouts/partials/header.html: delete unused header.html
52. ef19d66 layouts/partials/footers: refactored for release
53. ad2d92a layouts/partials/amp/metas: refactored for release
54. 379d886 assets/css/bissetii/modules/nav: refactored for release
55. 7b6d98e assets/css/bissetii/modules/gochroma: refactored for release
56. eadefa4 assets/css/bissetii/modules/emoji: refactors for releases
57. 918bc85 assets/css/bissetii/modules/core: refactors for release
58. 2cf67be assets/css: updated all Sass configuration files
59. 30c3b9e assets/css/bissetii/templates: added AMP supports.
60. 0de6c67 assets/css/bissetii/libs/navbars/_Base.sass: removed deprecated
61. 33c8111 assets/css/bissetii/modules/nav/_Sidebar.scss: fixed anchor jump
62. b99cb2f assets/css/bissetii/modules/core/color: removed unusable select svg
63. ba06618 archetypes: refactored for release
64. fcff27f layouts/_default: refactored for housekeeping
65. 1f1a940 data/bissetii/schema/*: updated for more data elements
66. 4682022 docs/*/how-to/manage-social-sharing.md: added validation linter
67. 2702beb data/bissetii: added thumbnails.toml
68. 1107509 data/bissetii/vendors: added twitter.toml
69. 3ffe438 data/bissetii: added vendors/google.toml
70. 12df2e6 data/bissetii: added navbars.toml
71. 7f8e055 data/bissetii: added browsers.toml
72. 1236647 .gitlab-ci.yml: updated to use v0.74.0 hugo-extended program
73. dcaf9ee data/bissetii/data: added softwareApplication.toml data file
74. 0b2084d root: shifted docs/.data to theme data directory
75. a94ec2f docs/.data/bissetii/schema: shift schema to use data directory
76. 2ecb42f docs/.data: migrated creators to use toml data file
77. 8e238b3 docs/.data: shifts javascript, css, and amp assets config to data
78. f12d788 docs/en-us/_index.md: updated for rich rendering contents
79. ab59b9f .sites/config/_default/params.toml: updated type to organization
80. 4b1cb5b root: added schema.org supports
81. 13c0833 layouts/_default/baseof.*.html: updated for normal page type
82. 08c3b6d root: added schema.org basic rendering
83. 182ef4a archetypes/redirect.md: updated for cleaner front-matter
84. 5025402 docs/en-us: clean-up dirty documentations
85. 3d00e31 docs/en-us: ported to use new authorship scheme
86. 5e8d038 .sites/archetypes: updated with authorship management
87. 2d99b8c root: added authorship management feature
88. 3845c55 root: added manage social media sharing outlook configuration
89. 1d5b12f docs/en-us/_index.md: added supported new features
90. 9665dac docs/en-us/specifications: updated for AMP rendering
91. 1b1a127 docs/en-us/components/typography.md: update for AMP rendering
92. cf260d0 docs/en-us/components/table.md: updated for AMP rendering
93. 66a93c5 docs/en-us/components/shields-badge.md: updated for AMP rendering
94. 734bb71 docs/en-us/components/navbar.md: updated for AMP rendering
95. d9e995c layouts/shortcodes/image.*.html: added class parameter supports
96. a7576fd docs/en-us/components/list.md: refactored for AMP rendering
97. 6248141 docs/en-us/components/link.md: updated for AMP rendering
98. 77c6d10 docs/en-us/components/grid.md: updated for AMP rendering
99. d94746b docs/en-us/components/form.md: updated for AMP rendering
100. 6e32011 layouts/shortcodes: added form shortcode
101. f290ff6 docs/en-us/components/footer.md: updated for AMP rendering
102. 6affda1 docs/en-us/components/emoji.md: updated for AMP rendering
103. 9d872ed docs/en-us/components/divider.md: updated for AMP rendering
104. 382f8e1 assets/css/bissetii/modules/core/_Codes.scss: removed > selector
105. 73c80a4 assets/css/bissetii/modules/badges/_Shields.scss: updated rendering
106. 02c780f docs/en-us/components/definitions-list.md: updated for AMP rendering
107. 74df76c docs/en-us/components/color.md: updated for AMP rendering
108. 1132d4e docs/en-us/components/code.md: updated for AMP rendering
109. bbca3a9 docs/en-us/components/button.md: updated for AMP rendering
110. e6ba4b1 docs/en-us/components/blockquote.md: updated for AMP rendering
111. e5f66f6 docs/en-us/shortcodes/sponsors_PayPalME.md: updated for AMP rendering
112. 0ea6eb6 docs/en-us/*/shields-badges: updated for AMP rendering
113. e1b537e layout/shortcodes/renderHTML: updated for AMP rendering
114. d0ca3f4 */links: updated for AMP rendering
115. 0ae2592 layouts/*/images: updated to AMP compatible rendering
116. e090236 docs/en-us/shortcodes/abs*Link.md: updated for AMP rendering
117. da596ea archetypes/redirect.md: added fallback contents for failed redirect
118. 2f0b8e2 docs/en-us/how-to/manage-robots.md: updated for AMP rendering
119. cb5c9c4 docs/en-us/how-to/manage-menu.md: placed 302 redirect from old URL
120. e4a5301 docs/en-us/how-to/include-js.md: updated for AMP rendering
121. 7c4a076 docs/en-us/how-to/include-google-analytics.md: updated for AMP rendering
122. 3cc3fa8 docs/en-us/how-to/include-css.md: updated to AMP rendering
123. 31c030a docs/en-us/how-to/codes-syntax-hightlighting.md: updated for AMP
124. 2558302 docs/en-us/how-to/escape-hugo-shortcode.md: updated for AMP rendering
125. 7160a16 root: updated redirect to use parent instead of name as keyword
126. da730f8 docs/en-us/how-to/enable-emoji.md: refactored for AMP rendering
127. 196cdbd docs/en-us/how-to/custom-404.md: updated for AMP rendering
128. ae189ba docs/en-us/how-to/change-theme-color.md: updated for AMP rendering
129. 12dd311 .sites/archetypes/redirect.md: updated to match actual redirect.md
130. edb4b2a */redirect.md: updated redirect.md to have smooth redirect creation
131. 13fcfd8 layouts/*/metas: added google_analytics AMP pages support
132. 3d979df layouts/shortcodes: added renderAMPHTML shortcode
133. 5eba0d6 assets/css/bissetii/.../_NotoColorEmoji.scss: use remote URL first
134. bbf79a1 repo/config/_default/params.toml: updated from engine
135. 81d7950 .sites/config/_default/outputs.toml: enabled AMP rendering
136. 6cc1523 .gitlab-ci.yml: updated Hugo version to the latest
137. c08440c layouts/partials/navigatiors/sidebar.html: added safeHTML URL
138. 59008f3 root: corrected bad example for amp-modules
139. 506d372 docs/en-us/projects: updated with AMP rendering
140. e7fe518 layouts/partials: move sponsors to payments
141. e5575c7 layouts/*/sponsors: updated all sponsor partials for AMP supports
142. da4ece4 docs/en-us/getting-started: updated to AMP friendly
143. 6f6c21c root: enabled amp-sidebar for amp rendering
144. 7a25513 assets/css/bissetii/nav: removed all icons completely
145. c0915c8 .sites/archetypes: updated all archetypes to match latest settings
146. 1d6f36b archetypes: removed front-matter comment shieldings
147. def2718 s
148. 5fbe49d docs/en-us/_index.md: updated to be AMP compatible
149. 2f89135 layouts/shortcodes/absLink.html: deprecated absLink
150. d7d6247 layouts/shortcodes/absLangLink.html: deprecated absLangLink
151. a9b70cc .sites/config/_default/outputs.toml: diable AMP for git push
152. d2c1c73 repo/config/_default: ported all latest configurations
153. 7e44b58 layouts/shortcodes: added link shortcode
154. 7de0ff7 layouts/shortcodes: added image shortcodes
155. 5c2191e archetypes: added pre tagging to facilitate navigation icons
156. b374037 root: added AMP modules inclusion mechanisms
157. 5bb4b13 assets/css/bissetii/modules/nav: ported for AMP feature
158. 4727bc0 layouts/partials/metas/modules/general.html: updated viewport
159. a68c611 assets/css/bissetii/modules/nav: updated for enabling AMP feature
160. a8f659b assets/css/bissetii/modules/gochroma: updated for AMP rendering
161. ae35d4b assets/css/bissetii/modules/badges: supported AMP styling
162. 37b97f0 assets/css/bissetii/modules/emoji: ported for AMP
163. 815f5e4 assets/css/bissetii: enable AMP for bissetii Sass core module
164. 6451fee .sites/config/_default/config.toml: amended robots.txt config
165. d0a3ed5 docs/en-us/how-to: added manage-amp.md to document AMP details
166. 14bca95 assets/.../nav/_Sidebar.scss: fixed content first-child margin top
167. 1c5f910 root: enabled AMP rendering for Hugo
168. f970e92 assets/css/bissetii/.../nav/_Sidebar.scss: remove desktop top header
169. 9a31011 docs/en-us/spec.../file-structures.md: updated hugo_only pathing
170. c803b3e assets/css: added query specific sass configurations
171. 2042ba2 layouts/partials: added hugo_only separations
172. 2de851f .sites/config/_default/params.toml: added googleAnalytics ID field
173. 97fb2b6 root: added Google Analytics support
174. 167dadb .sites/archetypes: updated localized archtypes to match latest design
175. 4cdd4c2 root: added SEO robots management
176. f5e92ae .sites/archetypes: added redirect
177. 408feaf root: added redirect feature
178. 9d3455b repo/assets/css/main.sass: updated main.sass to match the latest
179. bd29388 root: releasing v1.11.0 deb package

# Version 1.11.0
## Sat, 15 Feb 2020 01:05:53 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. c0d7d06 repo/manager.sh: updated manager.sh to have custom 404 page copy
2. 63b49b1 docs/en-us/specifications/file-structures.md: updated to latest
3. 62a2d19 docs/en-us/_index.md: updated landing page
4. 041ee9e docs/en-us/how-to: updated documentations to match latest features
5. eb7ebb2 assets/css/bissetii/modules/nav/_Sidebar.scss: fixed anchoring bug
6. abc691e docs/en-us/components/divider.md: updated docs to match latest
7. 157dd7c docs/en-us/components/button.md: updated spec to latest standards
8. 2a44842 assets/css/bissetii/modules/core/_Base.scss: add print to elements
9. c8ab4d3 docs/en-us/_index.md: added print rendering feature
10. 7cae430 assets/css/bissetii/modules/badges/_Shields.scss: added print render
11. 9207842 assets/css/bissetii/modules/nav: added print rendering
12. 38a8afe assets/css/bissetii/modules/core: added print styling
13. 3268097 assets/css/bissetii/modules/core/_Init.scss: disposed media query
14. 7eec3ed docs/en-us/*: added custom-404.md specifications
15. ac8dc43 .sites/manager.sh: support 404.html copying
16. 3f027f9 layouts/404.html: styled to match library crystal chandelier design
17. f330f4e assets/css/bissetii: removed _Bissetii.sass
18. 2bd59b4 assets/css/bissetii/modules/emoji_NotoColorEmoji.scss: fixed typo
19. 65fa009 docs/en-us/_index.md: marked component documentations completed
20. b6b2c09 assets/css/bissetii/modules/core: ported color schemes
21. 15f9b70 docs/en-us/components/blockquote.md: updated spec to match standards
22. 83fa92c assets/css/bissetii/libs: removed shieldTags library
23. b20ac2a assets/css/bissetii: facilitate default Render API
24. 03f9502 assets/css/bissetii: added single renderer
25. f31bd6b docs/en-us/components/shields-badges.md: updated Sass parts
26. 14c4aec assets/css/bissetii/modules: shift core module back
27. 2a6f588 assets/css/bissetii/modules: ported emoji module
28. ab13085 assets/css/bissetii/cores/_Code.scss: fixed margin for code block
29. c40d1a5 assets/css/bissetii/templates: added _ModuleIndex.scss
30. 7cd730c assets/css/bissetii/modules: added gochroma adjustment module
31. edd5668 assets/css/bissetii: refactored to media specific compilations.
32. 9616d86 assets/css/bissetii/modules/nav/_Base.scss: update bullet to flower
33. 016438b assets/css/bissetii/modules/nav/_Base.scss: added bullet right margin
34. d01492c assets/css/bissetii/*: set left text alignment as default
35. aeadc37 docs/en-us/projects/status.md: refactored page for new badges
36. b30aead layouts/partials/badges: ported shields.html to use new badge
37. d7e85c4 assets/css/bissetii/cores/_Link.scss: added wrap class tag
38. 3359a42 assets/css/bissetii/modules: added badges module
39. 99e3387 assets/css/bissetii/cores: added _DefinitionList.scss
40. fc59848 docs/en-us/components/*: corrects availability version number
41. 6f64c3a assets/css/bissetii/modules/nav/_.scss: set default value for base
42. feecd00 docs/en-us/components: added navbar.md
43. 54c1bd6 assets/css/bissetii/modules/nav/_Base.scss: export nav-bullets
44. 57e6f6a assets/css/bissetii/cores/_Typography.scss: highlight h3
45. 9ad68d4 assets/css/bissetii/modules/nav: removed padding div
46. ac39a1e assets/css/bissetii/modules/nav/*: shift transition to main nav
47. b140d04 assets/css/bissetii/modules/nav/_Sidebar.scss: ported desktop version
48. 9aa241d layouts: added nav.html blocks for main menu ID generation
49. f929692 assets/css/bissetii/modules/nav: ported mobile and tab sidebar
50. 6d6dccf assets/css/bissetii/cores/_Init.scss: fix typo for rapid transition
51. 6ac919e assets/css/bissetii/modules/nav: ported base navigation bar
52. 1e7472d assets/css/bissetii/cores/_Typography.scss: styled title with underline
53. 1ad4462 assets/css/bissetii/cores/_Base.scss: export margin variables
54. 596c6a5 assets/css/bissetii/cores/_image.scss: removed object-fit
55. eea2f91 assets/css/bissetii/cores/_List.scss: removed sublist font-size
56. 347ba4b assets/css/bissetii/modules: removed legacy Milligram codes
57. ab0f0af assets/css/bissetii/core: ported _Footer.sass
58. 2694303 .sites/archetypes/components.md: shift HTML parts to the bottom
59. 349b8d7 assets/css/bissetii/cores: ported _Typography.scss
60. be1cbe6 assets/css/bissetii/cores/_Blockquote.scss: fixed link styling bug
61. 9f71f34 assets/css/bissetii/cores: ported _Table.scss
62. cf0610e assets/css/bissetii/cores: ported _Spacing.sass
63. 7317e06 assets/css/bissetii/_Bissetii.sass: removed unused _Utility module
64. f7479e2 assets/css/bissetii/cores: ported _List.scss
65. 3d78c07 assets/css/bissetii/cores/_.scss: fix scss/sass mixed up convention
66. 94b6776 assets/css/bissetii/cores/_Button.scss: style transition animation
67. b6cf28b assets/css/bissetii/cores: added _Link.scss
68. 4a6d8a2 assets/css/bissetii/cores/_.scss: offers media query mixins
69. c697421 docs/en-us/components/*: change all available versions to v1.11.0
70. 4dbe96e assets/css/bissetii/cores: uses _.scss as index.scss
71. c2c99c5 assets/css/bissetii/cores: ported _Image.scss
72. d4fe1d1 assets/css/bissetii/cores: ported _Grid.scss
73. b66f914 assets/css/bissetii/.../navbars/_Core.sass: fixed broken anchor bug
74. 63f45b9 assets/css/bissetii/modules/_Typography.sass: fixed h{4,5,6} line-height
75. 73a5284 assets/css/bissetii/core/_Base.scss: added text-align: justify
76. 10e00de assets/css/bissetii/.../emoji/_NotoColorEmoji.sass: fixed src bug
77. 794bfb8 assets/css/bissetii/cores: ported _Form.scss
78. b0f295d docs/en-us/projects: fix a sponsoring.md full name field bug
79. ffde4e1 assets/css/bissetii/cores: ported _Divider.scss
80. c3f18da docs/en-us/components/code.md: tone down the heading for renders
81. 761dc7a assets/css/bissetii/cores/_Blockquote.scss: match _Code.scss styling
82. 217ff26 assets/css/bissetii/{cores,templates}: added public api label
83. 3548d2a docs/en-us/components: fix typo where the title is plural
84. a73d5c3 assets/css/bissetii/cores: ported _Code.scss
85. a519b16 assets/css/bissetii/cores: ported _Button.scss
86. 4ae99a9 assets/css/bissetii/cores/_Init.scss: fixed desktop dimensions
87. bb12d5f assets/css/bissetii/_Bissetii.sass: deploy display specific mixins
88. 4b83407 assets/css/bissetii/cores: ported _Blockquote.scss
89. a38b395 assets/css/bissetii/cores: ported _Scrollbar into scss cores
90. 188657a assets/css/bissetii: added templates/_Base.scss for future development
91. ce9d81c assets/css/bissetii: re-wrote _Base and _Init from sass to scss
92. b9b7b84 .sites/static/css/fonts: added NotoColorEmoji.tff
93. dc7ee67 .sites/archetypes/components.md: title case all the name
94. c7d7c63 docs/en-us/components/list.md: fix typo on title
95. 60ba9dd assets/css/bissetii/modules/_List.sass: fix content alignment
96. 2a581b1 layouts/partials/css/sass.html: set sass CSS as critical CSS
97. eadce3b layouts: added 404.html
98. e2d5ed4 docs/en-us/how-to/include-js.md: fix typo for close parenthesis
99. 1fc9314 docs/en-us/how-to/include-css.md: update CSS html tag to async
100. 0aae709 assets/css/bissetii/modules/_List.sass: removed margin for <li>
101. 1ad1ee6 docs/en-us/components: added list.md for <ol> and <ul> documentations
102. c7838e2 .sites/archetypes: added components.md for HTML components specs
103. bd27749 assets/css/bissetii/modules/_list.sass: styled <ol> and <ul>
104. d539943 docs/en-us/getting-started: recategorized for Hugo specific guides
105. d5bafec docs/en-us/shortcodes: recatergorized shortcodes to Hugo
106. a533e44 docs/en-us/how-to: recategorize how-to to hugo for navigation
107. ed5e5a1 root: refactored layouts to have common point data structure
108. e63ac58 layouts/_default/list.html: added list rendering page
109. 0f3873f root: added shieldTag partial and shortcode feature
110. 80c2a13 CODE_OF_CONDUCT.md: corrected the URL
111. ed95005 docs/en-us/*/code-of-conduct.md: publish code of conduct documentation
112. 7978c65 docs/en-us/_index.md: updated _index.md to match latest feature list
113. 4cb3882 assets/navbars/_Sidebar.sass: added close navbar to shaded area
114. 24a2c81 docs/.../install-bissetii.md: added note for min version for hugo
115. 6d4378f .gitlab-ci.yml: update fennec hugo's version
116. 49417a8 layouts/partials/metas: refactored to process data once
117. 812ee91 .sites/static/release/deb: releasing v1.10.0 to package server

# Version 1.10.0
## Wed, 25 Dec 2019 20:30:25 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. 0ec495d docs/en-us/_index.md: updating feature list to the latest
2. 8c7848f assets/css/.../_Base.sass: reduced main margin left and right
3. 8e1d644 root: enabled Github Pages support
4. 0cdf814 assets/css/.../navbars: refactored navbar to solve rendering problems
5. a664dad assets/css/.../modules/_Table.sass: corrected vertical-align value
6. 34af7a6 assets/css/.../_Code.sass: fix code syntax highlighting for Chroma
7. 1fb7e3f docs/.../howto: added escape_hugo_shortcode.md
8. 5898fdf .configs/debian: re-published deb reprepro server
9. df40d3a root/*/reprepro: updated to the proper layouts
10. 909006a docs/.../install-bissetii.md: corrected typo
11. dcb83d0 .gitlab/issue_templates: updated all automations to comments
12. b134127 app/bissetii.sh: fixed .gitignore warning from debuild lintian
13. c70b20d docs/.../install-bissetii.md: updated guide to match codename
14. 817053f .sites/static/releases/deb: published v1.9.0 package
15. 6f4ccb2 .configs/debian/conf/distributions: revert back to Codebase

# Version 1.9.0
## Sun, 22 Dec 2019 11:05:08 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. 5472cf7 root: discontinued Snapcraft support
2. 8e0feff root: removed server builds to stage v1.9.0 onwards
3. 9d2fc64 .gitlab: updated templates to the latest
4. 40f967e docs/en-us/_index.md: updated with new features
5. 47b62f5 docs/.../install-bissetii.md: added Launchpad deprecation notice
6. 5c33c33 docs/*/specs/upstream-process.md: revised to latest
7. 9e2d3b1 docs/*/specs/logo-and-banner.md: updated to latest and greatest
8. 5b426f1 docs/*/spec/licensing.md: revised contents to latest
9. 1a3aaab docs/*/spec/ci-pipelines.md: revised contents to match latest output
10. f58a53c assets/css/.../modules/_Blockquotes.sass: fixed hover color bug
11. 1b28996 docs/*/shortcodes: publish absLangLink and absLink
12. a57a7fc docs/*/specifications/file-structures.md: revised contents
13. bec2ece .gitlab-ci.yml: update CI to match .scripts directory
14. 30dbb50 root: shifted scripts to .scripts
15. 2a26c61 docs/*/specifications/user-experiences.md: corrected documents
16. 930da0d docs/*/shortcodes/*.md: corrected grammar mistakes and added usage
17. adb4fec docs/en-us/shortcodes: added absLangLink.md documentation
18. c716537 docs/*/shortcodes: added absLink shortcode
19. b519d61 */archetypes/shortcodes.md: fixed shortcode cancelling issue
20. 6ed5459 docs/en-us: shifted all shortcodes to its dedicated section
21. 086dbcc */archetypes: refactored whitespace and spacing problems
22. 780e1b0 docs/.../design-concepts.md: refactored to reduce writings
23. 30d5925 layouts/partials/metas: use replace instead of trim for descriptions
24. 71dcfec root: added code of conduct convenant into the repository
25. f9d34e0 docs/.../howto: added change_theme_color.md
26. a8d9c34 layouts/partials/meta/*: fixed meta description newline bug
27. c94c390 */shortcodes/sponsors/PayPalME: documented sponsors/PayPalME shortcode
28. 5d07868 docs/en-us/howto: added renderHTML_shortcode.md
29. 1e93cb3 docs/en-us/howto/codes_syntax_highlighting.md: publish the page
30. 12f5126 layouts/shortcodes/sponsorViaPayPalME.html: added validation check
31. f9838c8 docs/en-us/projects/sponsoring.md: updated with latest tech
32. 2f7d09c docs/en-us/_index.md: updated to latest styling and features list
33. 3110d2a assets/css/.../modules/_Form.sass: refactored to match min taste
34. 3b6f43f layouts/shortcodes: added renderHTML.html shortcode
35. 138bf73 assets/css/bissetii/modules_Init.sass: added transitions variables
36. 24b5a91 layouts/shortcodes: added sponsorViaPayPalME shortcode
37. bee886b docs/.../projects/license.md: updated full-body license
38. a140b51 docs/.../projects/contributors.md: amended grammar mistakes and typo
39. 76ca084 docs/.../projects/status.md: updated to v1.9.0
40. 8288a49 docs/.../howto/codes_syntax_highlighting.md: added gallery guides
41. ddb7e00 docs/.../howto/organize-menu.md: corrected grammar mistakes
42. 3503326 docs/.../howto/codes_syntax_highlighting.md: corrected grammar
43. 1d55489 docs/en-us/enable-emoji.md: corrected some grammar mistakes
44. 68edb70 docs/.../getting-started/using-bissetii.md: updated to match v1.9.0
45. faa168c docs/.../getting-started/setup-repository: updated outputs
46. 1e43bb2 assets/css/.../modules/_Blockquotes.sass: changed link to contrast
47. fdba116 docs/en-us/_index.md: updated landing page documentations
48. 24e49bf docs/en-us/.../install-bissetii.md: updated debian server instructions
49. b4d2b99 .sites/config/_default/config.toml: added .sites/static pathing
50. 6e0c4c9 app/bissetii.sh: fixed test failed issues
51. 85b470c root: added reprepro deb server setup for managing releases
52. 3caa636 app/bissetii.sh: refactored for simplifications
53. c2a0f6f assets/css/main.sass: add all configurations in commented sequence
54. 46ae4bb assets/css/bissetii: refactored sass to match latest codes
55. b102298 assets/css/.../configs/colors: deleted duplicated _GreenBamboo.sass
56. 5395af6 assets/css/.../configs/colors: added contrast colors for striking
57. 687073c assets/.../libs/_Sidebar.sass: fix navbar animation bug
58. a7e437f .root: added code syntaxes highlighting feature
59. 6b90fc8 root: added emoji rendering feature
60. f534b5b assets/css/bissetii/modules/_Init.sass: added charset definition
61. e343d9c layout/.../meta/general.html: replace proper charset meta

# Version 1.8.0
## Mon, 14 Oct 2019 16:04:51 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. 4501905 docs/en-us/_index.md: added code syntax highlighting feature
2. a0ddc6a docs: updated documentations
3. fcd70aa docs/en-us/specfications/design-concepts.md: updated spacing issue
4. 23de12f docs/en-us/_index.md: added feature lists into landing page.
5. 53f4123 .sites/archetypes: added "bissetii" and "hugo" keywords
6. 09eab73 assets/css/.../_Divider.sass: replace symbol to ellipse
7. 5ca728d docs/en-us: added howto/organize-menu.md
8. d0377c4 test/scripts/bissetii_sh: fix broken path due to location shifting
9. d5db05a app: shifted bissetii.sh program into a reserved app directory
10. d73e4c0 .sites, docs: reorganize sidebar menu ordering
11. 0421e08 docs/en-us/getting-started: added dependencies installations guides
12. 7d65acc .sites/*: updated to v1.7.0 configurations
13. cd58b76 assets/css/.../_Typography.sass: updated to match better reading
14. 7101ee9 repo/content/en-us: added contributing/documentations.md

# Version 1.7.0
## Sun, 13 Oct 2019 14:42:53 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. 7c19659 repo/config/_default/langauges.toml: removed langauge weights
2. 068e884 assets/css/.../_Sidebar.sass: added margin-right for desktop view

# Version 1.6.0
## Sun, 13 Oct 2019 13:19:33 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. 56525f4 assets/css/.../_Typography.sass: adjust typo across devices
2. 18d965b assets/css/.../_Scrollbars.sass: adjust height thickness
3. fa03a54 assets/css/.../navbars: adjust size to appropriate version
4. 684a294 assets/css/.../_Scrollbars.sass: resized scrollbars width to 0.4rem
5. d7a0af5 repo/manager.sh: removed auto-dependency installation
6. 1ce2402 repo/config/_default/languages.toml: replace positions per weight
7. 41c70b0 docs/en-us/specifications/logo-and-banner.md: resized square logo
8. cf50686 README.md, .../status.md, .../_index.md: added staging branch status
9. 56aa5b5 docs/*: updated to use Go date format and change draft to false
10. 040f17d .sites/manager.sh: updated to v1.5.0
11. fb127d5 repo/manager.sh: added Go compatible date generator
12. 2e4bd63 .gitlab: added gitlab issues and merge_request templates

# Version 1.5.0
## Fri, 15 Mar 2019 08:32:21 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. 9c3299e docs/en-us/getting-started: added using-bissetii.md into repository
2. 714eea6 docs/en-us/getting-started: added setup-repository.md
3. dc5bd57 docs/*/getting-started: added install-bissetii.md getting started guide
4. e9b02d0 archetypes/default.md: added title and keywords automation
5. 1ce3f08 .gitlab-ci.yml: update to match fennec v1.4.0 functions
6. 8368ad6 .fennec: updated to v1.4.0

# Version 1.4.0
## Thu, 14 Mar 2019 12:00:19 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. d0b9071 bissetii.sh: remove automatic manager.sh installation
2. c928127 root: updatd README.md and LICENSE to match the web documentation

# Version 1.3.0
## Thu, 14 Mar 2019 10:42:57 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. f94cc29 .config/stingrays/manta.cfg: update to track staging as reference
2. 8126129 .fennec: added .fennec automation tool into repository
3. a06c4a6 .config/stingray/manta.cfg: added debian-mentors into unstable branch
4. 384682e repo/content/en-us/_index.md: changed the contents back to english
5. 80a87bd docs/*/specifications/file-structures.md: added stingrays directories
6. 147abb7 bissetii.sh: added _patch_repo_doc function
7. 315e5a5 repo/*: updated to match single install user experiences
8. 53bc793 .configs/stingrays: updated for building package
9. 876da3a bissetii.sh: added install/uninstall repo functions
10. 18ca4be assets/css/bissetii/libs/navbars: resized desktop sidebar
11. 9d2d1e7 docs/en-us/specifications: added upstream-process.md
12. 2b3daa5 docs/en-us/specifications: added logo-and-banner.md
13. f2a9e00 docs/en-us/specifications: added licensing.md
14. b2d27bc docs/en-us/specifications: added ci-pipelines.md
15. 5d9496d static: removed css directory
16. 219eea1 docs/en-us/specifications: added user-experiences.md into docs
17. 28c9968 docs/en-us/specifications: added file-structures.md into docs
18. 48e26ed docs/en-us/specifications: added design-concepts.md into docs
19. d6a2e7f archetypes/default.md: change to use multi-line strings for descriptions
20. 5cd266c layouts/partials/navigators/modules/_list.html: set active link to null
21. 468b504 doc/en-us/_index.md: fixed typo for Quick Dashboard
22. 6178809 docs/en-us/projects: added sponsoring page into documentation
23. 6d73fee docs/en-us: added project pages
24. 059e5d9 assets/css/bissetii/configs/colors: added GreenBamboo color
25. 47153c7 .sites/assets: removed unused assets directory
26. dd115b1 .gitignore: added Hugo _gen for resources
27. d231317 repo/*: updated based on local testing
28. af293ea .sites: added local website generator
29. 9bcd9e0 repo: updated to match a master template

# Version 1.2.0
## Sat, 09 Mar 2019 23:29:09 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. 930f698 assets/css/bissetii/modules/_Typography.sass: corrected section padding

# Version 1.1.0
## Sat, 09 Mar 2019 19:38:09 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. fdbf9fa assets/css/bissetii/*: fixed sidebar flickering issue on Desktop

# Version 1.0.0
## Sat, 09 Mar 2019 16:40:30 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. a081c68 assets/css/bissetii: created _Scrollbars.sass module
2. cd60d9e assets/css/main.sass: moved main.sass outside of bissetii directory
3. bd7339a assets/css/bissetii/configs/colors: added _DeepBlueSky.sass color
4. 53b8c20 assets/css/libs/navbars/*: split Sidebar.sass and Base.sass
5. 6d803b3 libs/navbars/_Sidebar.sass: fix menu's 'X' top distorted position
6. 86ef47d root/*: added Bissetii SASS styling function

# Version 0.6.0
## Thu, 07 Mar 2019 18:41:22 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. c703f0f repo/manager.sh: fix server and build bug
2. 86614d8 assets/css/.../sidebar_from-left_with_opacity.css: fixed huge font issue
3. b4032dc layouts/_default/baseof.html: refactored for better readability
4. 4689803 layouts/partials/metas/meta.html: fixed general meta position
5. 66d89d3 *: implemented assets minifications and fingerprinting
6. 17a9695 layouts/partials/links/*: updated to use strings.HasPrefix

# Version 0.5.0
## Wed, 06 Mar 2019 14:38:30 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. a6a668f archetypes/default.md: commented thumbnailURL to set it as an option
2. 6538edd layout/partials/meta/modules/*: use absLink for Thumbnail URL

# Version 0.4.0
## Wed, 06 Mar 2019 12:33:33 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. da89f4f layouts/partials/links/*: added smart slash checking
2. c566e94 layouts/partials/navigatiors/modules/_list.html: fixed isset warning

# Version 0.3.0
## Wed, 06 Mar 2019 11:47:52 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. c8accf8 archetypes/default.md: added automatic date generation
2. 6f45783 layouts/_default/*: updated to use abs*Link partials instead
3. 38a1d41 layouts/partials/navigators/modules/*: updated to use _list module
4. 0fb1588 layouts/partials/links/absLink.html: add full URL checking
5. 057a580 layouts/partials/links/*: updated to remove newline syntax
6. 9d989e8 layouts/*/links: updated absLangLink and absLink for partials
7. b2ae334 repo/manager.sh: fix git clone in wrong directory issue

# Version 0.2.0
## Tue, 05 Mar 2019 18:00:07 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. 336235e static/css/*/bissetii/main.css: added styling for hr tag
2. a5ab1d1 layouts/shortcodes/*Link: added absolute links shortcodes
3. 033db1d */navbars/sidebar_from_left_with_opacity: fixed scrollbar display bug
4. c512a8a static/css/*/bissetii/typography.css: added typography styling
5. 8760dee */footer/simple.*: change h2 tag into p tag
6. 8709c25 archetypes/default.md: updated navigation bar settings

# Version 0.1.0
## Tue, 26 Feb 2019 11:24:26 +0800 - (Urgency: low)
--------------------------------------------------------------------------------
1. ad37397 stingrays: added stingrays support to repository
2. e67ff02 repo: remove master tag as we want to maintain only single version
3. cdef39e root/CONTRIBUTING.md: added CONTRIBUTING.md notes
4. 43109c6 */favicon: added favicon and banner supports
5. 94e65d9 README.md: updated README.md with to-date resources
6. ecf909f repo/master/config/*: synchornized test configuartions
7. 85af803 repo/master/*: corrected i18n symbol with RFC5646 standard
8. 8b53d55 i18n: enabled translation for footer's All Rights Reserved String
9. b5d8419 layout/_default/single: append defer tag to js code block
10. 226a9aa layouts/partials/footers/simple.html: corrected copyright typo
11. c51cfcb static/css/*/sidebar_from_left_with_opacity: corrected hover highlights
12. a1c3259 static/css/.../footer/simple.css: added desktop margin left correction
13. 2db4651 layouts/single.html: shifted all baseof.html block codes to single.html
14. 19797ec footers: added simple footer
15. 79aa708 static/css/edge/bissetii/main.css: fix over width problem
16. ebb823b static/css/.../navbar: added hover highlight for submenu titles
17. ebb81ab */navbars/sidebar_from_left_with_opacity: added "active" indicator
18. 47f31bb .gitignore: added .gitignore to avoid adding unnecessary files
19. 0c82612 */sidebar_from_left_with_opacity: added sha256 ID for submenu
20. 87eaac5 */sidebar_from_left_with_opacity: added scrollable menu feature
21. 3f80ab8 */navigators/sidebar_from_left_with_opacity: updated navbar styling
22. f1c5c29 repo/*/config/*/menus*: uses separate i18n menu configuration files
23. b712e5f */languages: added multi-lingual menu support
24. 97295a7 repo/master/config/*/languages.toml: added languages.toml config file
25. d493309 repo/master/config/*/config.toml: ported back googleAnalytic field
26. 7f06184 static/css/*/bissetii/navbars/*: updated link hover color
27. 3483e3e *: added meta tags generator into repository
28. 054ff7c menu: added Hugo automated menu generator
29. 0ee8b89 static/css/*/bissetii/navbars/*: update navbar border color
30. 1294770 static/css/*/bissetii/navbars/sidebar*: change color to milligram
31. e74a53c statics/css/*: use omni-browsers' transition
32. 3fb0503 static/css/edge/bissetii/main.css: increase padding for main
33. 7dbf2c0 layout|static/navigators: updated to use CSS only sidebar navbar
34. 382f83f static/css/*/bissetii/*: updated font-sizes and layout based on viewport
35. 0ececf2 static/css/*/bissetii/*: added desktop size support for navigation
36. aa25ab3 static/css/*/bissetii/sidebars: added support for tablet screen
37. c4b9410 static/css/*/bissetii/main.css: shifted z-index to main.css
38. 6179cb3 layout/*, static/*/bissetii/navbars,sidebars: add scrolling effect
39. 54a57ec archetypes/default.md: added type and layout specification
40. 69bc1f2 layout/*, static/*: added css design skeleton for bissetii
41. 8fdf44d repo/master/manager.sh: refactored with info printout
42. b06f45f repo/master/manager.sh: refactored _launch_server variables
43. b098e8c repo/master/manager.sh: added build function
44. 70612de repo/master: rename repo/docs to repo/master
45. 85dede0 i18n: added i18n language translation bundle
46. e15f8f1 repo/docs/config/_default/*: added all standard configurations
47. c2063c1 config/*: enable multiple environment configuration modes
48. 9c66614 repo/docs: shifted existing repo template to repo/docs
49. 00536d2 repo/manager.sh: added launch server command feature
50. 22ba626 repo/manager.sh: added _check_location function
